<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;
use Skadmin\TournamentGame\BaseControl;

use function sprintf;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200123174211 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $data = [
            'base_control' => BaseControl::class,
            'webalize'     => 'tournament-game',
            'name'         => 'Tournament game',
            'is_active'    => 1,
        ];
        $this->addSql(
            'INSERT INTO core_package (base_control, webalize, name, is_active) VALUES (:base_control, :webalize, :name, :is_active)',
            $data
        );

        $resource = [
            'name'        => BaseControl::RESOURCE,
            'title'       => sprintf('role-resource.%s.title', BaseControl::RESOURCE),
            'description' => sprintf('role-resource.%s.description', BaseControl::RESOURCE),
        ];
        $this->addSql('INSERT INTO core_role_resource (name, title, description) VALUES (:name, :title, :description)', $resource);
    }

    public function down(Schema $schema): void
    {
    }
}
