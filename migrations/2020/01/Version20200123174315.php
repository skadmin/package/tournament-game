<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200123174315 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE tournament_game_player (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, team_id INT DEFAULT NULL, role INT NOT NULL, created_at DATETIME NOT NULL, INDEX IDX_6EF80FE7A76ED395 (user_id), INDEX IDX_6EF80FE7296CD8AE (team_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE tournament_game_team (id INT AUTO_INCREMENT NOT NULL, nationality_id INT DEFAULT NULL, code VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL, webalize VARCHAR(255) NOT NULL, name VARCHAR(255) NOT NULL, nickname VARCHAR(255) NOT NULL, image_preview VARCHAR(255) DEFAULT NULL, website VARCHAR(255) NOT NULL, facebook VARCHAR(255) NOT NULL, content LONGTEXT NOT NULL, UNIQUE INDEX UNIQ_FF7562221C9DA55 (nationality_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE tournament_game (id INT AUTO_INCREMENT NOT NULL, is_online TINYINT(1) DEFAULT \'0\' NOT NULL, confirmation_from INT DEFAULT NULL, confirmation_to INT DEFAULT NULL, number_of_participants INT NOT NULL, prize_pool LONGTEXT NOT NULL, price INT NOT NULL, webalize VARCHAR(255) NOT NULL, name VARCHAR(255) NOT NULL, is_active TINYINT(1) DEFAULT \'1\' NOT NULL, status INT NOT NULL, created_at DATETIME NOT NULL, code VARCHAR(255) NOT NULL, term_from DATETIME NOT NULL, term_to DATETIME NOT NULL, content LONGTEXT NOT NULL, place_name VARCHAR(255) NOT NULL, place_address VARCHAR(255) NOT NULL, place_gps_lat VARCHAR(255) NOT NULL, place_gps_lng VARCHAR(255) NOT NULL, image_preview VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE tournament_game_type (id INT AUTO_INCREMENT NOT NULL, number_of_participants_from INT NOT NULL, number_of_participants_to INT NOT NULL, webalize VARCHAR(255) NOT NULL, name VARCHAR(255) NOT NULL, content LONGTEXT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE tournament_game_player ADD CONSTRAINT FK_6EF80FE7A76ED395 FOREIGN KEY (user_id) REFERENCES customer (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE tournament_game_player ADD CONSTRAINT FK_6EF80FE7296CD8AE FOREIGN KEY (team_id) REFERENCES tournament_game_team (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE tournament_game_team ADD CONSTRAINT FK_FF7562221C9DA55 FOREIGN KEY (nationality_id) REFERENCES nationality (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE tournament_game_player DROP FOREIGN KEY FK_6EF80FE7296CD8AE');
        $this->addSql('DROP TABLE tournament_game_player');
        $this->addSql('DROP TABLE tournament_game_team');
        $this->addSql('DROP TABLE tournament_game');
        $this->addSql('DROP TABLE tournament_game_type');
    }
}
