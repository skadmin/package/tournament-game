<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200124152915 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE tournament_game ADD tournament_type_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE tournament_game ADD CONSTRAINT FK_14A683B2AE437799 FOREIGN KEY (tournament_type_id) REFERENCES tournament_game_type (id) ON DELETE CASCADE');
        $this->addSql('CREATE INDEX IDX_14A683B2AE437799 ON tournament_game (tournament_type_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE tournament_game DROP FOREIGN KEY FK_14A683B2AE437799');
        $this->addSql('DROP INDEX IDX_14A683B2AE437799 ON tournament_game');
        $this->addSql('ALTER TABLE tournament_game DROP tournament_type_id');
    }
}
