<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200129103746 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE tournament_game_registration_user (id INT AUTO_INCREMENT NOT NULL, tournament_id INT DEFAULT NULL, team_id INT DEFAULT NULL, user_id INT DEFAULT NULL, payment INT NOT NULL, paid_at DATETIME DEFAULT NULL, price INT NOT NULL, surname VARCHAR(255) NOT NULL, name VARCHAR(255) NOT NULL, nickname VARCHAR(255) NOT NULL, email VARCHAR(255) NOT NULL, phone VARCHAR(255) NOT NULL, is_public_facebook TINYINT(1) DEFAULT \'0\' NOT NULL, facebook VARCHAR(255) NOT NULL, status INT NOT NULL, created_at DATETIME NOT NULL, INDEX IDX_69098B7233D1A3E7 (tournament_id), INDEX IDX_69098B72296CD8AE (team_id), INDEX IDX_69098B72A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE tournament_game_registration_user ADD CONSTRAINT FK_69098B7233D1A3E7 FOREIGN KEY (tournament_id) REFERENCES tournament_game (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE tournament_game_registration_user ADD CONSTRAINT FK_69098B72296CD8AE FOREIGN KEY (team_id) REFERENCES tournament_game_team (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE tournament_game_registration_user ADD CONSTRAINT FK_69098B72A76ED395 FOREIGN KEY (user_id) REFERENCES customer (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE tournament_game_registration_user');
    }
}
