<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;
use Nette\Utils\DateTime;
use Skadmin\TournamentGame\Mail\CMailRegistrationUserCreate;
use Skadmin\TournamentGame\Mail\CMailRegistrationUserUpdate;

use function serialize;
use function sprintf;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200201104251 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $mailTemplates = [
            [
                'content'            => '',
                'parameters'         => serialize(CMailRegistrationUserCreate::getModelForSerialize()),
                'type'               => CMailRegistrationUserCreate::TYPE,
                'class'              => CMailRegistrationUserCreate::class,
                'name'               => sprintf('mail.%s.name', CMailRegistrationUserCreate::TYPE),
                'subject'            => sprintf('mail.%s.subject', CMailRegistrationUserCreate::TYPE),
                'last_update_author' => 'Cron',
                'last_update_at'     => new DateTime(),
                'recipients'         => '',
                'preheader'          => '',
            ],
            [
                'content'            => '',
                'parameters'         => serialize(CMailRegistrationUserUpdate::getModelForSerialize()),
                'type'               => CMailRegistrationUserUpdate::TYPE,
                'class'              => CMailRegistrationUserUpdate::class,
                'name'               => sprintf('mail.%s.name', CMailRegistrationUserUpdate::TYPE),
                'subject'            => sprintf('mail.%s.subject', CMailRegistrationUserUpdate::TYPE),
                'last_update_author' => 'Cron',
                'last_update_at'     => new DateTime(),
                'recipients'         => '',
                'preheader'          => '',
            ],
        ];

        foreach ($mailTemplates as $mailTemplate) {
            $this->addSql(
                'INSERT INTO mail_template (content, parameters, type, class, name, subject, last_update_author, last_update_at) VALUES (:content, :parameters, :type, :class, :name, :subject, :last_update_author, :last_update_at)',
                $mailTemplate
            );
        }
    }

    public function down(Schema $schema): void
    {
    }
}
