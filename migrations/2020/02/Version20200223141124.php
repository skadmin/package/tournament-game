<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200223141124 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE tournament_game_payment (id INT AUTO_INCREMENT NOT NULL, payment_id INT DEFAULT NULL, tournament_id INT DEFAULT NULL, price INT NOT NULL, INDEX IDX_6E6887744C3A3BB (payment_id), INDEX IDX_6E68877433D1A3E7 (tournament_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE tournament_game_payment ADD CONSTRAINT FK_6E6887744C3A3BB FOREIGN KEY (payment_id) REFERENCES payment (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE tournament_game_payment ADD CONSTRAINT FK_6E68877433D1A3E7 FOREIGN KEY (tournament_id) REFERENCES tournament_game (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE tournament_game DROP price');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE tournament_game_payment');
        $this->addSql('ALTER TABLE tournament_game ADD price INT NOT NULL');
    }
}
