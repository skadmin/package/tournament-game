<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200223182507 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE tournament_game_registration_user ADD payment_id INT DEFAULT NULL, DROP payment');
        $this->addSql('ALTER TABLE tournament_game_registration_user ADD CONSTRAINT FK_69098B724C3A3BB FOREIGN KEY (payment_id) REFERENCES payment (id)');
        $this->addSql('CREATE INDEX IDX_69098B724C3A3BB ON tournament_game_registration_user (payment_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE tournament_game_registration_user DROP FOREIGN KEY FK_69098B724C3A3BB');
        $this->addSql('DROP INDEX IDX_69098B724C3A3BB ON tournament_game_registration_user');
        $this->addSql('ALTER TABLE tournament_game_registration_user ADD payment INT NOT NULL, DROP payment_id');
    }
}
