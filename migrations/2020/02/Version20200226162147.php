<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200226162147 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE tournament_game_registration_user_supplement (id INT AUTO_INCREMENT NOT NULL, supplement_id INT DEFAULT NULL, registration_id INT DEFAULT NULL, price INT NOT NULL, INDEX IDX_B9BD97BE7793FA21 (supplement_id), INDEX IDX_B9BD97BE833D8F43 (registration_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE tournament_game_registration_user_supplement ADD CONSTRAINT FK_B9BD97BE7793FA21 FOREIGN KEY (supplement_id) REFERENCES tournament_game_supplement (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE tournament_game_registration_user_supplement ADD CONSTRAINT FK_B9BD97BE833D8F43 FOREIGN KEY (registration_id) REFERENCES tournament_game_registration_user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE tournament_game_registration_user ADD price_payment INT NOT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE tournament_game_registration_user_supplement');
        $this->addSql('ALTER TABLE tournament_game_registration_user DROP price_payment');
    }
}
