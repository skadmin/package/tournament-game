<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;
use Nette\Utils\DateTime;
use Skadmin\TournamentGame\Mail\CMailRegistrationUserCreate;
use Skadmin\TournamentGame\Mail\CMailRegistrationUserUpdate;

use function serialize;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200226165643 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $mailTemplates = [
            [
                'parameters'         => serialize(CMailRegistrationUserCreate::getModelForSerialize()),
                'last_update_author' => 'Cron',
                'last_update_at'     => new DateTime(),
                'type'               => CMailRegistrationUserCreate::TYPE,
            ],
            [
                'parameters'         => serialize(CMailRegistrationUserUpdate::getModelForSerialize()),
                'last_update_author' => 'Cron',
                'last_update_at'     => new DateTime(),
                'type'               => CMailRegistrationUserUpdate::TYPE,
            ],
        ];

        foreach ($mailTemplates as $mailTemplate) {
            $this->addSql(
                'UPDATE mail_template set parameters = :parameters, last_update_author = :last_update_author, last_update_at = :last_update_at WHERE type = :type',
                $mailTemplate
            );
        }
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
    }
}
