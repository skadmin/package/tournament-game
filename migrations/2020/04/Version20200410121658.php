<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200410121658 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE tournament_game ADD second_place_id INT DEFAULT NULL, ADD third_place_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE tournament_game ADD CONSTRAINT FK_14A683B23EB2FD7C FOREIGN KEY (second_place_id) REFERENCES customer (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE tournament_game ADD CONSTRAINT FK_14A683B2785EA6DC FOREIGN KEY (third_place_id) REFERENCES customer (id) ON DELETE CASCADE');
        $this->addSql('CREATE INDEX IDX_14A683B23EB2FD7C ON tournament_game (second_place_id)');
        $this->addSql('CREATE INDEX IDX_14A683B2785EA6DC ON tournament_game (third_place_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE tournament_game DROP FOREIGN KEY FK_14A683B23EB2FD7C');
        $this->addSql('ALTER TABLE tournament_game DROP FOREIGN KEY FK_14A683B2785EA6DC');
        $this->addSql('DROP INDEX IDX_14A683B23EB2FD7C ON tournament_game');
        $this->addSql('DROP INDEX IDX_14A683B2785EA6DC ON tournament_game');
        $this->addSql('ALTER TABLE tournament_game DROP second_place_id, DROP third_place_id');
    }
}
