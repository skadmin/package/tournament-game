<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200418073847 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE tournament_game_team_player (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, team_id INT DEFAULT NULL, role INT NOT NULL, created_at DATETIME NOT NULL, INDEX IDX_9303724CA76ED395 (user_id), INDEX IDX_9303724C296CD8AE (team_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE tournament_game_team_player ADD CONSTRAINT FK_9303724CA76ED395 FOREIGN KEY (user_id) REFERENCES customer (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE tournament_game_team_player ADD CONSTRAINT FK_9303724C296CD8AE FOREIGN KEY (team_id) REFERENCES tournament_game_team (id) ON DELETE CASCADE');
        $this->addSql('DROP TABLE tournament_game_player');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE tournament_game_player (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, team_id INT DEFAULT NULL, role INT NOT NULL, created_at DATETIME NOT NULL, INDEX IDX_6EF80FE7296CD8AE (team_id), INDEX IDX_6EF80FE7A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE tournament_game_player ADD CONSTRAINT FK_6EF80FE7296CD8AE FOREIGN KEY (team_id) REFERENCES tournament_game_team (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE tournament_game_player ADD CONSTRAINT FK_6EF80FE7A76ED395 FOREIGN KEY (user_id) REFERENCES customer (id) ON DELETE CASCADE');
        $this->addSql('DROP TABLE tournament_game_team_player');
    }
}
