<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;
use Nette\Utils\DateTime;
use Skadmin\TournamentGame\Mail\CMailInvitationToTheTeam;

use function serialize;
use function sprintf;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200418111120 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE tournament_game_team_invitation (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, team_id INT DEFAULT NULL, created_at DATETIME NOT NULL, hash VARCHAR(255) NOT NULL, INDEX IDX_5CBF3FDA76ED395 (user_id), INDEX IDX_5CBF3FD296CD8AE (team_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE tournament_game_team_invitation ADD CONSTRAINT FK_5CBF3FDA76ED395 FOREIGN KEY (user_id) REFERENCES customer (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE tournament_game_team_invitation ADD CONSTRAINT FK_5CBF3FD296CD8AE FOREIGN KEY (team_id) REFERENCES tournament_game_team (id) ON DELETE CASCADE');

        $mailTemplate = [
            'content'            => '',
            'parameters'         => serialize(CMailInvitationToTheTeam::getModelForSerialize()),
            'type'               => CMailInvitationToTheTeam::TYPE,
            'class'              => CMailInvitationToTheTeam::class,
            'name'               => sprintf('mail.%s.name', CMailInvitationToTheTeam::TYPE),
            'subject'            => sprintf('mail.%s.subject', CMailInvitationToTheTeam::TYPE),
            'last_update_author' => 'Cron',
            'last_update_at'     => new DateTime(),
            'recipients'         => '',
            'preheader'          => '',
        ];

        $this->addSql(
            'INSERT INTO mail_template (content, parameters, type, class, name, subject, last_update_author, last_update_at) VALUES (:content, :parameters, :type, :class, :name, :subject, :last_update_author, :last_update_at)',
            $mailTemplate
        );
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE tournament_game_team_invitation');
    }
}
