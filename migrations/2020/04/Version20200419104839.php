<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200419104839 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE tournament_game_registration_team (id INT AUTO_INCREMENT NOT NULL, payment_id INT DEFAULT NULL, tournament_id INT DEFAULT NULL, team_id INT DEFAULT NULL, captain_id INT DEFAULT NULL, paid_at DATETIME DEFAULT NULL, price INT NOT NULL, price_payment INT NOT NULL, surname VARCHAR(255) NOT NULL, name VARCHAR(255) NOT NULL, nickname VARCHAR(255) NOT NULL, phone VARCHAR(255) NOT NULL, email VARCHAR(255) NOT NULL, facebook VARCHAR(255) NOT NULL, status INT NOT NULL, created_at DATETIME NOT NULL, variable_symbol VARCHAR(255) NOT NULL, INDEX IDX_207AFB244C3A3BB (payment_id), INDEX IDX_207AFB2433D1A3E7 (tournament_id), INDEX IDX_207AFB24296CD8AE (team_id), INDEX IDX_207AFB243346729B (captain_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE tournament_game_registration_team_supplement (id INT AUTO_INCREMENT NOT NULL, supplement_id INT DEFAULT NULL, registration_id INT DEFAULT NULL, price INT NOT NULL, INDEX IDX_200323977793FA21 (supplement_id), INDEX IDX_20032397833D8F43 (registration_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE tournament_game_registration_team ADD CONSTRAINT FK_207AFB244C3A3BB FOREIGN KEY (payment_id) REFERENCES payment (id)');
        $this->addSql('ALTER TABLE tournament_game_registration_team ADD CONSTRAINT FK_207AFB2433D1A3E7 FOREIGN KEY (tournament_id) REFERENCES tournament_game (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE tournament_game_registration_team ADD CONSTRAINT FK_207AFB24296CD8AE FOREIGN KEY (team_id) REFERENCES tournament_game_team (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE tournament_game_registration_team ADD CONSTRAINT FK_207AFB243346729B FOREIGN KEY (captain_id) REFERENCES customer (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE tournament_game_registration_team_supplement ADD CONSTRAINT FK_200323977793FA21 FOREIGN KEY (supplement_id) REFERENCES tournament_game_supplement (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE tournament_game_registration_team_supplement ADD CONSTRAINT FK_20032397833D8F43 FOREIGN KEY (registration_id) REFERENCES tournament_game_registration_team (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE tournament_game_registration_team_supplement DROP FOREIGN KEY FK_20032397833D8F43');
        $this->addSql('DROP TABLE tournament_game_registration_team');
        $this->addSql('DROP TABLE tournament_game_registration_team_supplement');
    }
}
