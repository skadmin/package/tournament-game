<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;
use Nette\Utils\DateTime;
use Skadmin\TournamentGame\Mail\CMailRegistrationTeamCreate;
use Skadmin\TournamentGame\Mail\CMailRegistrationTeamUpdate;
use Skadmin\TournamentGame\Mail\CMailRegistrationUserCreate;
use Skadmin\TournamentGame\Mail\CMailRegistrationUserUpdate;

use function serialize;
use function sprintf;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200419150951 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $mailTemplates = [
            [
                'content'            => '',
                'parameters'         => serialize(CMailRegistrationTeamCreate::getModelForSerialize()),
                'type'               => CMailRegistrationTeamCreate::TYPE,
                'class'              => CMailRegistrationTeamCreate::class,
                'name'               => sprintf('mail.%s.name', CMailRegistrationTeamCreate::TYPE),
                'subject'            => sprintf('mail.%s.subject', CMailRegistrationTeamCreate::TYPE),
                'last_update_author' => 'Cron',
                'last_update_at'     => new DateTime(),
                'recipients'         => '',
                'preheader'          => '',
            ],
            [
                'content'            => '',
                'parameters'         => serialize(CMailRegistrationTeamUpdate::getModelForSerialize()),
                'type'               => CMailRegistrationTeamUpdate::TYPE,
                'class'              => CMailRegistrationTeamUpdate::class,
                'name'               => sprintf('mail.%s.name', CMailRegistrationTeamUpdate::TYPE),
                'subject'            => sprintf('mail.%s.subject', CMailRegistrationTeamUpdate::TYPE),
                'last_update_author' => 'Cron',
                'last_update_at'     => new DateTime(),
                'recipients'         => '',
                'preheader'          => '',
            ],
        ];

        foreach ($mailTemplates as $mailTemplate) {
            $this->addSql(
                'INSERT INTO mail_template (content, parameters, type, class, name, subject, last_update_author, last_update_at) VALUES (:content, :parameters, :type, :class, :name, :subject, :last_update_author, :last_update_at)',
                $mailTemplate
            );
        }

        // update
        $mailTemplatesUpdate = [
            [
                'parameters'         => serialize(CMailRegistrationUserCreate::getModelForSerialize()),
                'type'               => CMailRegistrationUserCreate::TYPE,
                'last_update_author' => 'Cron',
                'last_update_at'     => new DateTime(),
            ],
            [
                'parameters'         => serialize(CMailRegistrationUserUpdate::getModelForSerialize()),
                'type'               => CMailRegistrationUserUpdate::TYPE,
                'last_update_author' => 'Cron',
                'last_update_at'     => new DateTime(),
            ],
        ];

        foreach ($mailTemplatesUpdate as $mailTemplate) {
            $this->addSql('UPDATE mail_template SET parameters = :parameters, last_update_author = :last_update_author, last_update_at = :last_update_at WHERE type = :type', $mailTemplate);
        }
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
    }
}
