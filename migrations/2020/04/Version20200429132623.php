<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200429132623 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE tournament_game_result (id INT AUTO_INCREMENT NOT NULL, tournament_id INT DEFAULT NULL, submitter_user_id INT DEFAULT NULL, opponent_user_id INT DEFAULT NULL, submitter_team_id INT DEFAULT NULL, opponent_team_id INT DEFAULT NULL, content LONGTEXT NOT NULL, identifier VARCHAR(255) DEFAULT NULL, size INT NOT NULL, mime_type VARCHAR(255) NOT NULL, last_update_at DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL, name VARCHAR(255) NOT NULL, hash VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL, is_valid TINYINT(1) DEFAULT \'0\' NOT NULL, INDEX IDX_E58BB49133D1A3E7 (tournament_id), INDEX IDX_E58BB491352F83EF (submitter_user_id), INDEX IDX_E58BB491AA25099D (opponent_user_id), INDEX IDX_E58BB491BB2D88D4 (submitter_team_id), INDEX IDX_E58BB491242702A6 (opponent_team_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE tournament_game_result ADD CONSTRAINT FK_E58BB49133D1A3E7 FOREIGN KEY (tournament_id) REFERENCES tournament_game (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE tournament_game_result ADD CONSTRAINT FK_E58BB491352F83EF FOREIGN KEY (submitter_user_id) REFERENCES customer (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE tournament_game_result ADD CONSTRAINT FK_E58BB491AA25099D FOREIGN KEY (opponent_user_id) REFERENCES customer (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE tournament_game_result ADD CONSTRAINT FK_E58BB491BB2D88D4 FOREIGN KEY (submitter_team_id) REFERENCES tournament_game_team (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE tournament_game_result ADD CONSTRAINT FK_E58BB491242702A6 FOREIGN KEY (opponent_team_id) REFERENCES tournament_game_team (id) ON DELETE SET NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE tournament_game_result');
    }
}
