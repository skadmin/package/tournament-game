<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;
use Skadmin\TournamentGame\BaseControl;

use function serialize;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200514171714 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE tournament_game ADD is_locked TINYINT(1) DEFAULT \'0\' NOT NULL');

        // new privilegies
        $data = [
            'additional_privilege' => serialize([
                BaseControl::PRIVILEGE_PUBLISH,
                BaseControl::PRIVILEGE_LOCK,
            ]),
            'name'                 => BaseControl::RESOURCE,
        ];
        $this->addSql('UPDATE resource SET additional_privilege = :additional_privilege WHERE name = :name', $data);

        // new translation
        $translations = [
            ['original' => 'privilege.publish', 'hash' => 'ffe416411b75e82f99c9dd681dea759f', 'module' => 'admin', 'language_id' => 1, 'singular' => 'publikování', 'plural1' => '', 'plural2' => ''],
            ['original' => 'privilege.lock', 'hash' => 'eee9a742273356c16dbbff1c85ad32af', 'module' => 'admin', 'language_id' => 1, 'singular' => 'zamykání', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.tournament-game.edit.is-locked', 'hash' => '3abce5a141af7c5734beceef5cc04e87', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Je zamčený', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.tournament-game.edit.flash.info.locked', 'hash' => '5f44089fa55ca0d24f3bd0f3a4e060d8', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Turnaj nemůžete upravovat, jelikož byl uzamčen.', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.tournament-game.edit.change-webalize', 'hash' => 'df98b17c5b1a9009d26660c4665fa185', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Znovu vygenerovat url', 'plural1' => '', 'plural2' => ''],
        ];

        foreach ($translations as $translation) {
            $this->addSql('DELETE FROM translation WHERE hash = :hash', $translation);
            $this->addSql('SELECT create_translation(:original, :hash, :module, :language_id, :singular, :plural1, :plural2)', $translation);
        }
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE tournament_game DROP is_locked');
    }
}
