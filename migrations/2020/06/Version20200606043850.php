<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;
use Skadmin\TournamentGame\Doctrine\Registration\RegistrationTeam;
use Skadmin\TournamentGame\Doctrine\Registration\RegistrationUser;

use function sprintf;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200606043850 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE tournament_game_registration_team ADD confirmed_at DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE tournament_game_registration_user ADD confirmed_at DATETIME DEFAULT NULL');

        // update rows
        $this->addSql(sprintf('UPDATE tournament_game_registration_team SET confirmed_at = paid_at WHERE STATUS = %d', RegistrationTeam::STATUS_CONFIRMED));
        $this->addSql(sprintf('UPDATE tournament_game_registration_user SET confirmed_at = paid_at WHERE STATUS = %d', RegistrationUser::STATUS_CONFIRMED));
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE tournament_game_registration_team DROP confirmed_at');
        $this->addSql('ALTER TABLE tournament_game_registration_user DROP confirmed_at');
    }
}
