<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201023180917 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE tournament_game_registration_team_list_of_rel_player (registration_team_id INT NOT NULL, customer_id INT NOT NULL, INDEX IDX_ABD2B400691D8D08 (registration_team_id), INDEX IDX_ABD2B4004A210C1D (user_front_id), PRIMARY KEY(registration_team_id, user_front_id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE tournament_game_registration_team_list_of_rel_player ADD CONSTRAINT FK_ABD2B400691D8D08 FOREIGN KEY (registration_team_id) REFERENCES tournament_game_registration_team (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE tournament_game_registration_team_list_of_rel_player ADD CONSTRAINT FK_ABD2B4004A210C1D FOREIGN KEY (customer_id) REFERENCES customer (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE tournament_game_registration_team ADD team_nickname VARCHAR(255) NOT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE tournament_game_registration_team_list_of_rel_player');
        $this->addSql('ALTER TABLE tournament_game_registration_team DROP team_nickname');
    }
}
