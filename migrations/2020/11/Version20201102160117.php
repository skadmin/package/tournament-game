<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201102160117 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE tournament_game ADD winner_registration_team_id INT DEFAULT NULL, ADD second_place_registration_team_id INT DEFAULT NULL, ADD third_place_registration_team_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE tournament_game ADD CONSTRAINT FK_14A683B24BEDF4EF FOREIGN KEY (winner_registration_team_id) REFERENCES tournament_game_registration_team (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE tournament_game ADD CONSTRAINT FK_14A683B246092CE4 FOREIGN KEY (second_place_registration_team_id) REFERENCES tournament_game_registration_team (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE tournament_game ADD CONSTRAINT FK_14A683B2602686F2 FOREIGN KEY (third_place_registration_team_id) REFERENCES tournament_game_registration_team (id) ON DELETE CASCADE');
        $this->addSql('CREATE INDEX IDX_14A683B24BEDF4EF ON tournament_game (winner_registration_team_id)');
        $this->addSql('CREATE INDEX IDX_14A683B246092CE4 ON tournament_game (second_place_registration_team_id)');
        $this->addSql('CREATE INDEX IDX_14A683B2602686F2 ON tournament_game (third_place_registration_team_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE tournament_game DROP FOREIGN KEY FK_14A683B24BEDF4EF');
        $this->addSql('ALTER TABLE tournament_game DROP FOREIGN KEY FK_14A683B246092CE4');
        $this->addSql('ALTER TABLE tournament_game DROP FOREIGN KEY FK_14A683B2602686F2');
        $this->addSql('DROP INDEX IDX_14A683B24BEDF4EF ON tournament_game');
        $this->addSql('DROP INDEX IDX_14A683B246092CE4 ON tournament_game');
        $this->addSql('DROP INDEX IDX_14A683B2602686F2 ON tournament_game');
        $this->addSql('ALTER TABLE tournament_game DROP winner_registration_team_id, DROP second_place_registration_team_id, DROP third_place_registration_team_id');
    }
}
