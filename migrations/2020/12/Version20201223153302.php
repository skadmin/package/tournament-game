<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;
use Skadmin\TournamentGame\BaseControl;

use function sprintf;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201223153302 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $resource = [
            'name'        => BaseControl::RESOURCE_TEAM,
            'title'       => sprintf('role-resource.%s.title', BaseControl::RESOURCE_TEAM),
            'description' => sprintf('role-resource.%s.description', BaseControl::RESOURCE_TEAM),
        ];
        $this->addSql('INSERT INTO core_role_resource (name, title, description) VALUES (:name, :title, :description)', $resource);
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
    }
}
