<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210618174804 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE tournament_bracket (id INT AUTO_INCREMENT NOT NULL, tournament_id INT DEFAULT NULL, stage INT NOT NULL, created_at DATETIME NOT NULL, last_update_at DATETIME DEFAULT NULL, name VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_2A01DD6433D1A3E7 (tournament_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE tournament_bracket_stage (id INT AUTO_INCREMENT NOT NULL, bracket_id INT DEFAULT NULL, will_play_in_the_finals TINYINT(1) DEFAULT \'1\' NOT NULL, number_of_output_players INT DEFAULT NULL, best_of_game INT NOT NULL, created_at DATETIME NOT NULL, last_update_at DATETIME DEFAULT NULL, name VARCHAR(255) NOT NULL, sequence INT NOT NULL, type VARCHAR(255) NOT NULL, INDEX IDX_27222D026E8D78 (bracket_id), INDEX tournament_bracket_stage_type (type), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE tournament_bracket_stage_double_elimination (id INT NOT NULL, best_of_game_loser INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE tournament_bracket_stage_round_robin (id INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE tournament_bracket_stage_single_elimination (id INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE tournament_bracket ADD CONSTRAINT FK_2A01DD6433D1A3E7 FOREIGN KEY (tournament_id) REFERENCES tournament_game (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE tournament_bracket_stage ADD CONSTRAINT FK_27222D026E8D78 FOREIGN KEY (bracket_id) REFERENCES tournament_bracket (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE tournament_bracket_stage_double_elimination ADD CONSTRAINT FK_D28512FBBF396750 FOREIGN KEY (id) REFERENCES tournament_bracket_stage (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE tournament_bracket_stage_round_robin ADD CONSTRAINT FK_F7D6E6D7BF396750 FOREIGN KEY (id) REFERENCES tournament_bracket_stage (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE tournament_bracket_stage_single_elimination ADD CONSTRAINT FK_35475827BF396750 FOREIGN KEY (id) REFERENCES tournament_bracket_stage (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE tournament_bracket_stage DROP FOREIGN KEY FK_27222D026E8D78');
        $this->addSql('ALTER TABLE tournament_bracket_stage_double_elimination DROP FOREIGN KEY FK_D28512FBBF396750');
        $this->addSql('ALTER TABLE tournament_bracket_stage_round_robin DROP FOREIGN KEY FK_F7D6E6D7BF396750');
        $this->addSql('ALTER TABLE tournament_bracket_stage_single_elimination DROP FOREIGN KEY FK_35475827BF396750');
        $this->addSql('DROP TABLE tournament_bracket');
        $this->addSql('DROP TABLE tournament_bracket_stage');
        $this->addSql('DROP TABLE tournament_bracket_stage_double_elimination');
        $this->addSql('DROP TABLE tournament_bracket_stage_round_robin');
        $this->addSql('DROP TABLE tournament_bracket_stage_single_elimination');
    }
}
