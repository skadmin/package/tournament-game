<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;
use Skadmin\TournamentGame\BaseControl;

use function serialize;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210627042913 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $data = [
            'additional_privilege' => serialize(BaseControl::PRIVILEGE_ADDITIONAL),
            'name'                 => BaseControl::RESOURCE,
        ];
        $this->addSql('UPDATE resource SET additional_privilege = :additional_privilege WHERE name = :name', $data);
    }

    public function down(Schema $schema): void
    {
    }
}
