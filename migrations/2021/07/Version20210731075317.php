<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210731075317 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE tournament_bracket_stage_match (id INT AUTO_INCREMENT NOT NULL, stage_id INT DEFAULT NULL, challenger_id INT DEFAULT NULL, opponent_id INT DEFAULT NULL, number INT NOT NULL, group_id INT NOT NULL, round_id INT NOT NULL, status INT NOT NULL, created_at DATETIME NOT NULL, last_update_at DATETIME DEFAULT NULL, INDEX IDX_B3C354F72298D193 (stage_id), INDEX IDX_B3C354F72D521FDF (challenger_id), INDEX IDX_B3C354F77F656CDC (opponent_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE tournament_bracket_stage_participant (id INT AUTO_INCREMENT NOT NULL, stage_id INT DEFAULT NULL, registration_user_id INT DEFAULT NULL, registration_team_id INT DEFAULT NULL, created_at DATETIME NOT NULL, last_update_at DATETIME DEFAULT NULL, code VARCHAR(255) NOT NULL, INDEX IDX_8529B9272298D193 (stage_id), INDEX IDX_8529B927E71F8633 (registration_user_id), INDEX IDX_8529B927691D8D08 (registration_team_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE tournament_bracket_stage_match ADD CONSTRAINT FK_B3C354F72298D193 FOREIGN KEY (stage_id) REFERENCES tournament_bracket_stage (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE tournament_bracket_stage_match ADD CONSTRAINT FK_B3C354F72D521FDF FOREIGN KEY (challenger_id) REFERENCES tournament_bracket_stage_participant (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE tournament_bracket_stage_match ADD CONSTRAINT FK_B3C354F77F656CDC FOREIGN KEY (opponent_id) REFERENCES tournament_bracket_stage_participant (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE tournament_bracket_stage_participant ADD CONSTRAINT FK_8529B9272298D193 FOREIGN KEY (stage_id) REFERENCES tournament_bracket_stage (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE tournament_bracket_stage_participant ADD CONSTRAINT FK_8529B927E71F8633 FOREIGN KEY (registration_user_id) REFERENCES tournament_game_registration_user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE tournament_bracket_stage_participant ADD CONSTRAINT FK_8529B927691D8D08 FOREIGN KEY (registration_team_id) REFERENCES tournament_game_registration_team (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE tournament_bracket_stage_match DROP FOREIGN KEY FK_B3C354F72D521FDF');
        $this->addSql('ALTER TABLE tournament_bracket_stage_match DROP FOREIGN KEY FK_B3C354F77F656CDC');
        $this->addSql('DROP TABLE tournament_bracket_stage_match');
        $this->addSql('DROP TABLE tournament_bracket_stage_participant');
    }
}
