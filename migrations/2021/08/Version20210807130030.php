<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210807130030 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE tournament_bracket_stage_match ADD match_winner_id INT DEFAULT NULL, ADD match_defeated_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE tournament_bracket_stage_match ADD CONSTRAINT FK_B3C354F7444D9813 FOREIGN KEY (match_winner_id) REFERENCES tournament_bracket_stage_match (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE tournament_bracket_stage_match ADD CONSTRAINT FK_B3C354F7F84B3076 FOREIGN KEY (match_defeated_id) REFERENCES tournament_bracket_stage_match (id) ON DELETE CASCADE');
        $this->addSql('CREATE INDEX IDX_B3C354F7444D9813 ON tournament_bracket_stage_match (match_winner_id)');
        $this->addSql('CREATE INDEX IDX_B3C354F7F84B3076 ON tournament_bracket_stage_match (match_defeated_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE tournament_bracket_stage_match DROP FOREIGN KEY FK_B3C354F7444D9813');
        $this->addSql('ALTER TABLE tournament_bracket_stage_match DROP FOREIGN KEY FK_B3C354F7F84B3076');
        $this->addSql('DROP INDEX IDX_B3C354F7444D9813 ON tournament_bracket_stage_match');
        $this->addSql('DROP INDEX IDX_B3C354F7F84B3076 ON tournament_bracket_stage_match');
        $this->addSql('ALTER TABLE tournament_bracket_stage_match DROP match_winner_id, DROP match_defeated_id');
    }
}
