<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210819144913 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $translations = [
            ['original' => 'grid.tournament-game.overview.action.overview-bracket', 'hash' => '64761a3c9504ea24d9be3da25b4895fa', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Bracket', 'plural1' => '', 'plural2' => ''],
            ['original' => 'tournament-game.bracket.title - %s', 'hash' => 'e49f75532a16f16ea5fce9ec2a95499f', 'module' => 'admin', 'language_id' => 1, 'singular' => '%s|Přehled bracketu', 'plural1' => '', 'plural2' => ''],
            ['original' => 'tournament-game.bracket.back', 'hash' => '47a794cfda6e9e3243bec18af3e8cd00', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Zpět na přehled turnajů', 'plural1' => '', 'plural2' => ''],
            ['original' => 'tournament-game.bracket.is-will-play-in-the-finals', 'hash' => 'ebecb66628913c8a7731921293a0c84f', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Hraje se do finále', 'plural1' => '', 'plural2' => ''],
            ['original' => 'tournament-game.bracket.number-of-output-players', 'hash' => '1631e5cc0169e185e1a59aa167d16927', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Počet zbývajících hráčů', 'plural1' => '', 'plural2' => ''],
            ['original' => 'tournament-game.bracket.best-of-game', 'hash' => '43e5f1932fe3b4aa3d971f3bf4ed49df', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Maximální počet her', 'plural1' => '', 'plural2' => ''],
            ['original' => 'tournament-game.bracket.bracket-stage-view', 'hash' => 'cdf6160c040169c26db38fa20222082c', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Zobrazit stage', 'plural1' => '', 'plural2' => ''],
            ['original' => 'tournament-game.stage-view.title - %s', 'hash' => '45cb7cada37189efbdc614c34c193711', 'module' => 'admin', 'language_id' => 1, 'singular' => '%s|Editace stage', 'plural1' => '', 'plural2' => ''],
            ['original' => 'tournament-game.bracket-stage-view.back', 'hash' => 'dd2f220a4f7c597595749a92f942b48a', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Zpět na bracket', 'plural1' => '', 'plural2' => ''],
            ['original' => 'tournament-game.bracket-stage-view.participants-collapse', 'hash' => '2e456322e13316137c03eb3b18c910c2', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Zobrazit / skrýt účastníky', 'plural1' => '', 'plural2' => ''],
            ['original' => 'tournament-game.bracket-stage-view.change-to-public', 'hash' => 'c07a23079fd06483a479c9c136b334e6', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Publikovat stage', 'plural1' => '', 'plural2' => ''],
            ['original' => 'tournament-game.bracket-stage-view.participants', 'hash' => '0baf6f55f5d5563084d38b856c3e61fb', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Účastníci stage', 'plural1' => '', 'plural2' => ''],
            ['original' => 'tournament-game.bracket.edit-stage-match.edit-title - %s', 'hash' => 'a5cbbb634ba55bbfbb61b6491c896f54', 'module' => 'admin', 'language_id' => 1, 'singular' => '%s|Editace zápasu', 'plural1' => '', 'plural2' => ''],
            ['original' => 'tournament-game.bracket.edit-stage-match.status', 'hash' => '69ab6b3b4bb8794e0d49f621bded6d46', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Status', 'plural1' => '', 'plural2' => ''],
            ['original' => 'tournament-bracket-stage-match.awaiting-participants', 'hash' => 'f8b9d302ff82cbe3b050e7ca09541928', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Čeká se na účastníky', 'plural1' => '', 'plural2' => ''],
            ['original' => 'tournament-bracket-stage-match.ready', 'hash' => 'bfe81da38ad8914afa4b8f0247f79cf8', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Připraveno', 'plural1' => '', 'plural2' => ''],
            ['original' => 'tournament-bracket-stage-match.running', 'hash' => '48dc9041de62125b5aa790648f904c35', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Probíhá', 'plural1' => '', 'plural2' => ''],
            ['original' => 'tournament-bracket-stage-match.awaiting-results', 'hash' => '5a1aa758518f8e54324bb4322655f0ae', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Čeká se na výsledky', 'plural1' => '', 'plural2' => ''],
            ['original' => 'tournament-bracket-stage-match.completed', 'hash' => '23da2d9721cf6630ebd85df95dd504c6', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Dohráno', 'plural1' => '', 'plural2' => ''],
            ['original' => 'tournament-bracket-stage-match.awaiting-admin', 'hash' => 'e018d93cbfdff26ebad62f763ea2c8cd', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Čeká se na admina', 'plural1' => '', 'plural2' => ''],
            ['original' => 'tournament-bracket-stage-match.maintenance', 'hash' => '3794ca653a6d4c7a91e842550cb50212', 'module' => 'admin', 'language_id' => 1, 'singular' => 'V řešení admina', 'plural1' => '', 'plural2' => ''],
            ['original' => 'tournament-game.bracket.edit-stage-match.challenger', 'hash' => '9dc07628297c9adf39e01844f87600c4', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Vyzyvatel', 'plural1' => '', 'plural2' => ''],
            ['original' => 'tournament-game.bracket.edit-stage-match.challenger-result', 'hash' => '4cb26523dae025bc80b653ead2abde96', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Vyzyvatel - výsledek', 'plural1' => '', 'plural2' => ''],
            ['original' => 'tournament-game.bracket.edit-stage-match.opponent', 'hash' => '731cc96313708cbc1ef90fa054915c1d', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Oponent', 'plural1' => '', 'plural2' => ''],
            ['original' => 'tournament-game.bracket.edit-stage-match.opponent-result', 'hash' => '013b7749fbed7632b5db74da13ca7ed0', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Oponent - výsledek', 'plural1' => '', 'plural2' => ''],
            ['original' => 'tournament-game.bracket.edit-stage-match.evaluate-the-result', 'hash' => 'd2b90a1155f34ac65cd078cb7b7040a1', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Vyhodnotit výsledek', 'plural1' => '', 'plural2' => ''],
            ['original' => 'tournament-game.bracket.edit-stage-match.send-back', 'hash' => '48a1310026873ae4f5752b7de99bb662', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Uložit a zpět', 'plural1' => '', 'plural2' => ''],
            ['original' => 'tournament-game.bracket-stage-view.change-to-dispublic', 'hash' => '9e9f722f8834de2ed5e234ab5c6b4bf5', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Zrušit publikování stage', 'plural1' => '', 'plural2' => ''],
            ['original' => 'tournament-game.stage-view.flash.success.change-to-dispublic', 'hash' => 'fb33ab7fae44601b9ac9a62a61f32143', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Stage byla odpublikována.', 'plural1' => '', 'plural2' => ''],
            ['original' => 'tournament-game.stage-view.flash.success.change-to-public', 'hash' => '73de08df64208d06ea363d07f47479c7', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Stage byla publikována.', 'plural1' => '', 'plural2' => ''],
            ['original' => 'tournament-game.bracket.edit-stage-match.flash.success.update', 'hash' => '76c684f72d99dfb7390df19e1720b488', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Zápas byl úspěšně upraven.', 'plural1' => '', 'plural2' => ''],
            ['original' => 'tournament-game.bracket.edit-stage-match.send', 'hash' => '67c92e8441fe0e42db8f287d11673b41', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Uložit', 'plural1' => '', 'plural2' => ''],
            ['original' => 'tournament-game.bracket.edit-stage-match.back', 'hash' => '5a292e894d6fa1ee0de79fef92a8d806', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Zpět', 'plural1' => '', 'plural2' => ''],
            ['original' => 'tournament-game.bracket.edit-bracket-stage', 'hash' => 'dd675aa130ee43719a3044fca751a947', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Upravit stage', 'plural1' => '', 'plural2' => ''],
            ['original' => 'tournament-game.bracket.bracket-stage-remove.confirm', 'hash' => 'd09e31a40b7b106827d19d21aedf4795', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Opravdu chcete smazat stage?', 'plural1' => '', 'plural2' => ''],
            ['original' => 'tournament-game.bracket.bracket-stage-remove', 'hash' => '0d19b868a8af114ce1fba5218dfc7ccb', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Smazat stage', 'plural1' => '', 'plural2' => ''],
            ['original' => 'tournament-game.bracket.flash.success.delete', 'hash' => 'fca4d93f07c5b47b780595e820155126', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Stage byla úspěšně smazána.', 'plural1' => '', 'plural2' => ''],
            ['original' => 'tournament-game.bracket.create-bracket-stage', 'hash' => 'e7282013c0e1899d1cba35c3d044a795', 'module' => 'admin', 'language_id' => 1, 'singular' => 'založit novou stage', 'plural1' => '', 'plural2' => ''],
            ['original' => 'tournament-game.bracket.edit-stage.create-title - %s', 'hash' => '0c891bb37d09c762f1a918d55c87aaf0', 'module' => 'admin', 'language_id' => 1, 'singular' => '%s|Založení stage', 'plural1' => '', 'plural2' => ''],
            ['original' => 'tournament-game.bracket.edit-stage.type', 'hash' => 'edf002343caa126c9d8cfec55bf53336', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Typ stage', 'plural1' => '', 'plural2' => ''],
            ['original' => 'tournament-game.bracket.edit-stage.type.req', 'hash' => 'f68af5365c9a4e0c2d4bcaf031e10b01', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Vyberte prosím stage', 'plural1' => '', 'plural2' => ''],
            ['original' => 'tournament-game.bracket.edit-stage.name', 'hash' => 'df626b217982fea29b60b4a3843af60e', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Název', 'plural1' => '', 'plural2' => ''],
            ['original' => 'tournament-game.bracket.edit-stage.name.req', 'hash' => '073d7c3a3741acbf8f0b0a2592ab6ddc', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Zadejte prosím název', 'plural1' => '', 'plural2' => ''],
            ['original' => 'tournament-game.bracket.edit-stage.best-of-game', 'hash' => 'fa7568ab7c9fa0fe6620f2b89e071426', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Maximální počet zápasů', 'plural1' => '', 'plural2' => ''],
            ['original' => 'tournament-game.bracket.edit-stage.best-of-game-loser', 'hash' => '8f7142fd8d1030ea09bb0f9af7826180', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Maximální počet zápasů (prohra)', 'plural1' => '', 'plural2' => ''],
            ['original' => 'tournament-game.bracket.edit-stage.will-play-in-the-finals', 'hash' => 'f4876dcbe22400b024973cd0e8db527d', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Hraje se do finále', 'plural1' => '', 'plural2' => ''],
            ['original' => 'tournament-game.bracket.edit-stage.number-of-output-players', 'hash' => '05b092ae316f0840818552c2181247bf', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Počet zbývajících účastníků', 'plural1' => '', 'plural2' => ''],
            ['original' => 'tournament-game.bracket.edit-stage.number-of-output-players.req', 'hash' => '54695daba43f79e4dc49fce7919c8647', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Zadejte prosím počet zbývajících účastníků', 'plural1' => '', 'plural2' => ''],
            ['original' => 'tournament-game.bracket.edit-stage.send-back', 'hash' => '9c20a89cd09d340306dce146a954e534', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Uložit a zpět', 'plural1' => '', 'plural2' => ''],
            ['original' => 'tournament-game.bracket.edit-stage.back', 'hash' => '060df64f7da1c8709d90c6b6477935f3', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Zpět', 'plural1' => '', 'plural2' => ''],
            ['original' => 'tournament-game.bracket.edit-stage.send', 'hash' => 'f70591b4a0331b955c87e872c864024e', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Uložit', 'plural1' => '', 'plural2' => ''],
            ['original' => 'tournament-game.bracket.edit-stage.flash.success.create', 'hash' => '97c3a77acdea0c815ef97fcc9001c86b', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Stage byla úspěšně založena.', 'plural1' => '', 'plural2' => ''],
            ['original' => 'tournament-game.bracket.edit-stage.title - %s', 'hash' => '8970bdd17383471acb641add51f65eb3', 'module' => 'admin', 'language_id' => 1, 'singular' => '%s|Editace stage', 'plural1' => '', 'plural2' => ''],
            ['original' => 'tournament-game.bracket.edit-stage.flash.success.update', 'hash' => 'ae7c54ea1f780fbe8ca3295e69b4d769', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Stage byla úspěšně upravena.', 'plural1' => '', 'plural2' => ''],
        ];

        foreach ($translations as $translation) {
            $this->addSql('DELETE FROM translation WHERE hash = :hash', $translation);
            $this->addSql('SELECT create_translation(:original, :hash, :module, :language_id, :singular, :plural1, :plural2)', $translation);
        }
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
    }
}
