<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210905082959 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE tournament_bracket_stage_match_rel_result (tournament_bracket_stage_match_id INT NOT NULL, tournament_result_id INT NOT NULL, INDEX IDX_5DCCFF4EA70AB8C (tournament_bracket_stage_match_id), INDEX IDX_5DCCFF4ED7271E52 (tournament_result_id), PRIMARY KEY(tournament_bracket_stage_match_id, tournament_result_id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE tournament_bracket_stage_match_rel_result ADD CONSTRAINT FK_5DCCFF4EA70AB8C FOREIGN KEY (tournament_bracket_stage_match_id) REFERENCES tournament_bracket_stage_match (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE tournament_bracket_stage_match_rel_result ADD CONSTRAINT FK_5DCCFF4ED7271E52 FOREIGN KEY (tournament_result_id) REFERENCES tournament_game_result (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE tournament_bracket_stage_round_robin ADD number_of_rounds INT NOT NULL');
        $this->addSql('ALTER TABLE tournament_game_result ADD submitter_result INT DEFAULT NULL, ADD opponent_result INT DEFAULT NULL, ADD status INT NOT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE tournament_bracket_stage_match_rel_result');
        $this->addSql('ALTER TABLE tournament_bracket_stage_round_robin DROP number_of_rounds');
        $this->addSql('ALTER TABLE tournament_game_result DROP submitter_result, DROP opponent_result, DROP status');
    }
}
