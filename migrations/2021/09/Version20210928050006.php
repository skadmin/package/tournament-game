<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210928050006 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $translations = [
            ['original' => 'tournament-game.bracket.best-of-game-loser', 'hash' => '5da755b66ecf3a1f559d72534616d12a', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Maximální počet zápasů (prohra)', 'plural1' => '', 'plural2' => ''],
            ['original' => 'tournament-game.bracket.detail-tournament', 'hash' => '685a29ffc50ce919aad9aba2c90b04ea', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Detail turnaje', 'plural1' => '', 'plural2' => ''],
            ['original' => 'tournament-game.bracket-stage-view.detail-tournament', 'hash' => 'dd98b70b5919bf06d3d8065860632a54', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Detail turnaje', 'plural1' => '', 'plural2' => ''],
            ['original' => 'homepage.default.tournament.bracet', 'hash' => 'a43d961e60e8c0c212d4ceb476ce5b69', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Bracket', 'plural1' => '', 'plural2' => ''],
            ['original' => 'tournament-game.edit.overview-bracket', 'hash' => 'a9b2189618c2ea53fc6b901b7e8bea81', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Bracket', 'plural1' => '', 'plural2' => ''],
            ['original' => 'tournament-game.bracket.edit-stage.number-of-rounds', 'hash' => '77c90d8231655fc7e74543a9fc089573', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Počet kol', 'plural1' => '', 'plural2' => ''],
            ['original' => 'tournament-game.bracket.edit-stage.number-of-rounds.title', 'hash' => 'd4004e15f06bbc4b96cd0781b30162c2', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Určuje kolik kol se bude hrát, nejvíce možných je  počet hráčů - 1.', 'plural1' => '', 'plural2' => ''],
            ['original' => 'tournament-game.bracket.edit-stage.number-of-rounds.number-zero', 'hash' => '09624085a954a44c1bb13638b1e51316', 'module' => 'admin', 'language_id' => 1, 'singular' => 'pokud ponecháte 0, vygeneruje se systém každý s každým', 'plural1' => '', 'plural2' => ''],
            ['original' => 'tournament-bracket-stage-match.awaiting-results-long', 'hash' => 'a0ed7382b076dd83a4e1034933332d12', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Čeká se na výsledky (dlouho)', 'plural1' => '', 'plural2' => ''],
        ];

        foreach ($translations as $translation) {
            $this->addSql('DELETE FROM translation WHERE hash = :hash', $translation);
            $this->addSql('SELECT create_translation(:original, :hash, :module, :language_id, :singular, :plural1, :plural2)', $translation);
        }
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
    }
}
