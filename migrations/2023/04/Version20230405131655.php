<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230405131655 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE tournament_bracket_stage_round_robin_duel (id INT NOT NULL, number_of_duels INT NOT NULL, number_of_round_duels INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE tournament_bracket_stage_round_robin_duel ADD CONSTRAINT FK_DCA9542CBF396750 FOREIGN KEY (id) REFERENCES tournament_bracket_stage (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE tournament_bracket_stage_match ADD allowed_from DATETIME DEFAULT NULL');

        $this->addSql('CREATE TABLE tournament_bracket_stage_match_duel (id INT AUTO_INCREMENT NOT NULL, match_id INT DEFAULT NULL, challenger_id INT DEFAULT NULL, opponent_id INT DEFAULT NULL, number_of_duel INT NOT NULL, challenger_result INT NOT NULL, opponent_result INT NOT NULL, challenger_type VARCHAR(255) NOT NULL, opponent_type VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL, last_update_at DATETIME DEFAULT NULL, INDEX IDX_24EB16A32ABEACD6 (match_id), INDEX IDX_24EB16A32D521FDF (challenger_id), INDEX IDX_24EB16A37F656CDC (opponent_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB;');
        $this->addSql('ALTER TABLE tournament_bracket_stage_match_duel ADD CONSTRAINT FK_24EB16A32ABEACD6 FOREIGN KEY (match_id) REFERENCES tournament_bracket_stage_match (id) ON DELETE CASCADE;');
        $this->addSql('ALTER TABLE tournament_bracket_stage_match_duel ADD CONSTRAINT FK_24EB16A32D521FDF FOREIGN KEY (challenger_id) REFERENCES customer (id) ON DELETE CASCADE;');
        $this->addSql('ALTER TABLE tournament_bracket_stage_match_duel ADD CONSTRAINT FK_24EB16A37F656CDC FOREIGN KEY (opponent_id) REFERENCES customer (id) ON DELETE CASCADE;');

        $this->addSql('ALTER TABLE tournament_bracket_stage_match ADD status_of_challenger_duels INT NOT NULL, ADD status_of_opponent_duels INT NOT NULL, ADD duels_challenger_ready_at DATETIME DEFAULT NULL, ADD duels_opponent_ready_at DATETIME DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE tournament_bracket_stage_round_robin_duel DROP FOREIGN KEY FK_DCA9542CBF396750');
        $this->addSql('DROP TABLE tournament_bracket_stage_round_robin_duel');
        $this->addSql('ALTER TABLE tournament_bracket_stage_match DROP allowed_from');

        $this->addSql('ALTER TABLE tournament_bracket_stage_match_duel DROP FOREIGN KEY FK_24EB16A32ABEACD6');
        $this->addSql('ALTER TABLE tournament_bracket_stage_match_duel DROP FOREIGN KEY FK_24EB16A32D521FDF');
        $this->addSql('ALTER TABLE tournament_bracket_stage_match_duel DROP FOREIGN KEY FK_24EB16A37F656CDC');
        $this->addSql('DROP TABLE tournament_bracket_stage_match_duel');

        $this->addSql('ALTER TABLE tournament_bracket_stage_match DROP status_of_challenger_duels, DROP status_of_opponent_duels, DROP duels_challenger_ready_at, DROP duels_opponent_ready_at');
    }
}
