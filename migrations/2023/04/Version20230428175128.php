<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230428175128 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE tournament_bracket_stage_match ADD allowed_from_draft_by_id INT DEFAULT NULL, ADD allowed_from_draft DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE tournament_bracket_stage_match ADD CONSTRAINT FK_B3C354F7B1395BEA FOREIGN KEY (allowed_from_draft_by_id) REFERENCES tournament_bracket_stage_participant (id) ON DELETE CASCADE');
        $this->addSql('CREATE INDEX IDX_B3C354F7B1395BEA ON tournament_bracket_stage_match (allowed_from_draft_by_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE tournament_bracket_stage_match DROP FOREIGN KEY FK_B3C354F7B1395BEA');
        $this->addSql('DROP INDEX IDX_B3C354F7B1395BEA ON tournament_bracket_stage_match');
        $this->addSql('ALTER TABLE tournament_bracket_stage_match DROP allowed_from_draft_by_id, DROP allowed_from_draft');
    }
}
