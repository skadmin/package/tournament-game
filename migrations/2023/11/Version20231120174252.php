<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20231120174252 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE tournament_bracket_stage ADD sequential_display_of_duels TINYINT(1) DEFAULT 0 NOT NULL');
        $this->addSql('ALTER TABLE tournament_game ADD is_result_file_required TINYINT(1) DEFAULT 1 NOT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE tournament_bracket_stage DROP sequential_display_of_duels');
        $this->addSql('ALTER TABLE tournament_game DROP is_result_file_required');
    }
}
