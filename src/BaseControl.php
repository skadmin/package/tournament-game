<?php

declare(strict_types=1);

namespace Skadmin\TournamentGame;

use App\Model\System\ABaseControl;
use Nette\Utils\ArrayHash;
use Nette\Utils\Html;

class BaseControl extends ABaseControl
{
    public const RESOURCE      = 'tournament-game';
    public const RESOURCE_TEAM = 'tournament-game-team';

    public const PRIVILEGE_PUBLISH = 'publish';
    public const PRIVILEGE_LOCK    = 'lock';
    public const PRIVILEGE_BRACKET = 'bracket';

    public const PRIVILEGE_ADDITIONAL = [
        self::PRIVILEGE_PUBLISH,
        self::PRIVILEGE_LOCK,
        self::PRIVILEGE_BRACKET,
    ];

    public const DIR_IMAGE      = 'tournament-game';
    public const DIR_IMAGE_TEAM = 'tournament-team';

    public const DIR_FILE_RESULT = 'tournament-game-result';

    public function getMenu(): ArrayHash
    {
        return ArrayHash::from([
            'control' => $this,
            'icon'    => Html::el('i', ['class' => 'fas fa-fw fa-trophy']),
            'items'   => ['overview'],
        ]);
    }
}
