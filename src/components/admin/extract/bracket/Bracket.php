<?php

declare(strict_types=1);

namespace Skadmin\TournamentGame\Components\Admin;

use App\Model\System\APackageControl;
use App\Model\System\Flash;
use Nette\ComponentModel\IContainer;
use Nette\Security\User;
use Skadmin\TournamentGame\BaseControl;
use Skadmin\TournamentGame\Doctrine\Tournament\Tournament;
use Skadmin\TournamentGame\Doctrine\Tournament\TournamentFacade;
use Skadmin\TournamentGame\Doctrine\TournamentBracketStage\ATournamentBracketStage;
use Skadmin\TournamentGame\Doctrine\TournamentBracketStage\TournamentBracketStageFacade;
use Skadmin\TournamentGame\Traits\TDownloadTournamentResult;
use Skadmin\Translator\SimpleTranslation;
use Skadmin\Translator\Translator;
use SkadminUtils\GridControls\UI\GridControl;
use WebLoader\Nette\CssLoader;
use WebLoader\Nette\JavaScriptLoader;
use WebLoader\Nette\LoaderFactory;

class Bracket extends GridControl
{
    use APackageControl;
    use TDownloadTournamentResult;

    private LoaderFactory                $webLoader;
    private TournamentBracketStageFacade $facadeTournamentBracketStage;
    private Tournament                   $tournament;

    public function __construct(int $id, TournamentFacade $facadeTournament, TournamentBracketStageFacade $facadeTournamentBracketStage, LoaderFactory $webLoader, Translator $translator, User $user)
    {
        parent::__construct($translator, $user);

        $this->webLoader = $webLoader;

        $this->tournament                   = $facadeTournament->get($id);
        $this->facadeTournamentBracketStage = $facadeTournamentBracketStage;
    }

    public function setParent(?IContainer $parent, ?string $name = null): static
    {
        parent::setParent($parent, $name);

        if (! $this->isAllowed(BaseControl::RESOURCE, BaseControl::PRIVILEGE_BRACKET)) {
            $this->getParent()->redirect(':Admin:Homepage:accessDenied');
        }

        return $this;
    }

    public function render(): void
    {
        $template = $this->getComponentTemplate();
        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/bracket.latte');

        $template->linkBack   = $this->getPresenter()
            ->link('Component:default', [
                'package' => new BaseControl(),
                'render'  => 'overview',
            ]);
        $template->tournament = $this->tournament;

        $template->render();
    }

    public function getTitle(): SimpleTranslation
    {
        return new SimpleTranslation('tournament-game.bracket.title - %s', $this->tournament->getName());
    }

    /**
     * @return CssLoader[]
     */
    public function getCss(): array
    {
        return [$this->webLoader->createCssLoader('jQueryUi')];
    }

    /**
     * @return JavaScriptLoader[]
     */
    public function getJs(): array
    {
        return [$this->webLoader->createJavaScriptLoader('jQueryUi')];
    }

    public function handleRemoveStage(int $stageId): void
    {
        $stage = $this->facadeTournamentBracketStage->get($stageId);
        if ($stage instanceof ATournamentBracketStage) {
            $this->facadeTournamentBracketStage->delete($stage);
            $this->onFlashmessage('tournament-game.bracket.flash.success.delete', Flash::SUCCESS);
        } else {
            $this->onFlashmessage('tournament-game.bracket.flash.danger.delete', Flash::DANGER);
        }

        $this->getPresenter()->redirect('Component:default', [
            'package' => new BaseControl(),
            'render'  => 'bracket',
            'id'      => $this->tournament->getId(),
        ]);
    }

    public function handleUpdateSequence(?string $jsonOrder = null): void
    {
        if ($jsonOrder === null) {
            return;
        }

        $this->facadeTournamentBracketStage->updateSequence(json_decode($jsonOrder, true));
        $this->onFlashmessage('tournament-game.bracket.flash.success.update-sequence', Flash::SUCCESS);
    }
}
