<?php

declare(strict_types=1);

namespace Skadmin\TournamentGame\Components\Admin;

interface IBracketFactory
{
    public function create(int $id): Bracket;
}
