<?php

declare(strict_types=1);

namespace Skadmin\TournamentGame\Components\Admin;

use App\Model\System\APackageControl;
use App\Model\System\Flash;
use Nette\ComponentModel\IContainer;
use Nette\Security\User;
use Nette\Utils\Arrays;
use Skadmin\TournamentGame\BaseControl;
use Skadmin\TournamentGame\Doctrine\TournamentBracketStage\ATournamentBracketStage;
use Skadmin\TournamentGame\Doctrine\TournamentBracketStage\TournamentBracketStageFacade;
use Skadmin\Translator\SimpleTranslation;
use Skadmin\Translator\Translator;
use SkadminUtils\GridControls\UI\GridControl;
use SkadminUtils\ImageStorage\ImageStorage;
use WebLoader\Nette\CssLoader;
use WebLoader\Nette\JavaScriptLoader;
use WebLoader\Nette\LoaderFactory;

use function count;
use function sprintf;

class BracketStageView extends GridControl
{
    use APackageControl;

    private LoaderFactory                $webLoader;
    private ImageStorage                 $imageStorage;
    private TournamentBracketStageFacade $facadeTournamentBracketStage;
    private ATournamentBracketStage      $stage;

    public function __construct(int $id, TournamentBracketStageFacade $facadeTournamentBracketStage, Translator $translator, LoaderFactory $webLoader, ImageStorage $imageStorage, User $user)
    {
        parent::__construct($translator, $user);
        $this->webLoader    = $webLoader;
        $this->imageStorage = $imageStorage;

        $this->facadeTournamentBracketStage = $facadeTournamentBracketStage;
        $this->stage                        = $this->facadeTournamentBracketStage->get($id);
    }

    public function setParent(?IContainer $parent, ?string $name = null): static
    {
        parent::setParent($parent, $name);

        $tournament = $this->stage->getBracket()->getTournament();
        if ($tournament->isOnline()) {
            $registrations = $tournament->getConfirmedRegistrations();
        } else {
            $registrations = $tournament->getPaidRegistrations();
        }

        if (count($registrations) < 3) {
            $this->onFlashmessage('tournament-game.stage-view.flash.warning.insufficient-number-of-participants', Flash::WARNING);
            $this->getPresenter()->redirect('Component:default', [
                'package' => new BaseControl(),
                'render'  => 'bracket',
                'id'      => $tournament->getId(),
            ]);
        }

        if (! $this->isAllowed(BaseControl::RESOURCE, BaseControl::PRIVILEGE_BRACKET)) {
            $this->getParent()->redirect(':Admin:Homepage:accessDenied');
        }

        return $this;
    }

    /**
     * @return CssLoader[]
     */
    public function getCss(): array
    {
        return [
            'bracketsViewer'           => $this->webLoader->createCssLoader('bracketsViewer'),
            'bracketsViewerThemeLight' => $this->webLoader->createCssLoader('bracketsViewerThemeLight'),
            'daterangePicker'          => $this->webLoader->createCssLoader('daterangePicker'),
        ];
    }

    /**
     * @return JavaScriptLoader[]
     */
    public function getJs(): array
    {
        return [
            'bracketsViewer'  => $this->webLoader->createJavaScriptLoader('bracketsViewer'),
            'moment'          => $this->webLoader->createJavaScriptLoader('moment'),
            'daterangePicker' => $this->webLoader->createJavaScriptLoader('daterangePicker'),
        ];
    }

    public function getTitle(): SimpleTranslation
    {
        return new SimpleTranslation('tournament-game.stage-view.title - %s', $this->stage->getName());
    }

    public function render(): void
    {
        $template = $this->getComponentTemplate();
        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/bracketStageView.latte');

        $template->imageStorage = $this->imageStorage;

        $template->linkBack = $this->getPresenter()
            ->link('Component:default', [
                'package' => new BaseControl(),
                'render'  => 'bracket',
                'id'      => $this->stage->getBracket()->getTournament()->getId(),
            ]);

        $stage                                 = $this->facadeTournamentBracketStage->prepareStage($this->stage->getId());
        $stageBracketViewerData                = $stage->getBracketViewerData();
        $stageBracketViewerData['participant'] = Arrays::map($stageBracketViewerData['participant'], function (array $participant): array {
            if ($participant['logo'] !== null) {
                $participantLogo     = $this->imageStorage->fromIdentifier([$participant['logo'], '100x100', 'exact']);
                $participant['logo'] = $participantLogo->createLink();
            }

            return $participant;
        });

        $template->stage                  = $stage;
        $template->stageBracketViewerData = $stageBracketViewerData;
        $template->render();
    }

    public function handleTogglePublic(): void
    {
        $stage = $this->facadeTournamentBracketStage->togglePublic($this->stage->getId());
        $this->onFlashmessage(sprintf('tournament-game.stage-view.flash.success.change-to-%s', $stage->isPublic() ? 'public' : 'dispublic'), Flash::SUCCESS);

        $this->getPresenter()->redirect('Component:default', [
            'package' => new BaseControl(),
            'render'  => 'bracket-stage-view',
            'id'      => $this->stage->getId(),
        ]);
    }

    public function handleOvergenerate(): void
    {
        $this->facadeTournamentBracketStage->clearMatchs($this->stage->getId());
        $this->redirect('this');
    }
}
