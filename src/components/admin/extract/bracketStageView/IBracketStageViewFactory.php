<?php

declare(strict_types=1);

namespace Skadmin\TournamentGame\Components\Admin;

interface IBracketStageViewFactory
{
    public function create(int $id): BracketStageView;
}
