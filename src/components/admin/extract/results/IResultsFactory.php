<?php

declare(strict_types=1);

namespace Skadmin\TournamentGame\Components\Admin;

interface IResultsFactory
{
    public function create(int $id): Results;
}
