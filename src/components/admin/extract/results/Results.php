<?php

declare(strict_types=1);

namespace Skadmin\TournamentGame\Components\Admin;

use App\Model\System\APackageControl;
use App\Model\System\Flash;
use App\Model\System\SystemDir;
use Nette\Application\UI\Multiplier;
use Nette\ComponentModel\IContainer;
use Nette\Security\User;
use Nette\Utils\ArrayHash;
use Nette\Utils\Strings;
use Skadmin\FileStorage\FileStorage;
use Skadmin\Role\Doctrine\Role\Privilege;
use Skadmin\TournamentGame\BaseControl;
use Skadmin\TournamentGame\Doctrine\Tournament\Tournament;
use Skadmin\TournamentGame\Doctrine\Tournament\TournamentFacade;
use Skadmin\TournamentGame\Doctrine\TournamentResult\TournamentResult;
use Skadmin\TournamentGame\Doctrine\TournamentResult\TournamentResultFacade;
use Skadmin\TournamentGame\Traits\TDownloadTournamentResult;
use Skadmin\Translator\SimpleTranslation;
use Skadmin\Translator\Translator;
use SkadminUtils\FormControls\UI\Form;
use SkadminUtils\GridControls\UI\GridControl;
use WebLoader\Nette\CssLoader;
use WebLoader\Nette\JavaScriptLoader;
use WebLoader\Nette\LoaderFactory;
use ZipArchive;

use function array_reverse;
use function assert;
use function filesize;
use function flush;
use function header;
use function intval;
use function readfile;
use function sprintf;
use function unlink;

use const DIRECTORY_SEPARATOR;

class Results extends GridControl
{
    use APackageControl;
    use TDownloadTournamentResult;

    private Tournament    $tournament;
    private LoaderFactory $webLoader;
    private SystemDir     $systemDir;

    public function __construct(int $id, TournamentResultFacade $facadeTournamentResult, TournamentFacade $facadeTournament, Translator $translator, User $user, FileStorage $fileStorage, LoaderFactory $webLoader, SystemDir $systemDir)
    {
        parent::__construct($translator, $user);

        $this->facadeTournamentResult = $facadeTournamentResult;
        $this->tournament             = $facadeTournament->get($id);
        $this->fileStorage            = $fileStorage;
        $this->webLoader              = $webLoader;
        $this->systemDir              = $systemDir;
    }

    public function setParent(?IContainer $parent, ?string $name = null): static
    {
        parent::setParent($parent, $name);

        if (! $this->isAllowed(BaseControl::RESOURCE, Privilege::READ)) {
            $this->getParent()->redirect(':Admin:Homepage:accessDenied');
        }

        return $this;
    }

    public function render(): void
    {
        $template = $this->getComponentTemplate();
        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/results.latte');

        $template->fileStorage = $this->fileStorage;
        $template->linkBack    = $this->getPresenter()
            ->link('Component:default', [
                'package' => new BaseControl(),
                'render'  => 'overview',
            ]);
        $template->canDelete   = $this->isAllowed(BaseControl::RESOURCE, Privilege::DELETE);

        $template->tournamentResults = $this->tournament->getResults();

        $template->render();
    }

    public function getTitle(): SimpleTranslation
    {
        return new SimpleTranslation('tournament-game.results.title - %s', $this->tournament->getName());
    }

    /**
     * @return CssLoader[]
     */
    public function getCss(): array
    {
        return [$this->webLoader->createCssLoader('fancyBox')];
    }

    /**
     * @return JavaScriptLoader[]
     */
    public function getJs(): array
    {
        return [$this->webLoader->createJavaScriptLoader('fancyBox')];
    }

    public function handleRemoveResult(string $hash): void
    {
        $tournamentResult = $this->facadeTournamentResult->findByHash($hash);

        if ($tournamentResult instanceof TournamentResult) {
            $tournamentResultName       = $tournamentResult->getName();
            $tournamentResultIdentifier = $tournamentResult->getIdentifier();

            $this->facadeTournamentResult->remove($tournamentResult);
            $this->fileStorage->remove($tournamentResultIdentifier);

            $this->onFlashmessage(new SimpleTranslation('tournament-game.results.flash.success.remove %s', $tournamentResultName), Flash::SUCCESS);
        }

        $this->redrawControl('snipOverview');
    }

    public function handledownloadAll(): void
    {
        $results = array_reverse($this->tournament->getResults()->toArray());

        $zipResults = [];
        foreach ($results as $result) {
            assert($result instanceof TournamentResult);
            $zipResults[$result->getFileName(null, false)][] = $result;
        }

        $zip     = new ZipArchive();
        $zipPath = $this->systemDir->getPathWww(['..', 'temp', 'zip']);
        $this->systemDir->createDir($zipPath);

        $zipName = sprintf('%s_%s.zip', $this->tournament->getId(), Strings::webalize($this->tournament->getWebalize()));

        $zipPath .= sprintf('%s%s', DIRECTORY_SEPARATOR, $zipName);

        @unlink($zipPath);
        if ($zip->open($zipPath, ZipArchive::CREATE) !== true) {
            $this->onFlashmessage('tournament-game.results.flash.danger.download-all', Flash::DANGER);
            $this->redirect('this');
        }

        foreach ($zipResults as $_zipResults) {
            $iterator = 1;

            foreach ($_zipResults as $zipResult) {
                assert($zipResult instanceof TournamentResult);
                if ($zipResult->getIdentifier() === null) {
                    continue;
                }

                $filePreview = $this->fileStorage->getFilePreview($zipResult->getIdentifier(), $zipResult->getFileName($iterator++), (string) $zipResult->getSize());
                $zip->addFile($filePreview->getPath(), $filePreview->getName());
            }
        }

        $zip->close();

        header('Content-Description: File Transfer');
        header('Content-Type: application/zip');
        header(sprintf('Content-Disposition: attachment; filename="%s"', $zipName));
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        header(sprintf('Content-Length: %d', filesize($zipPath)));
        flush();
        readfile($zipPath);
        exit;
    }

    protected function createComponentFormUpdateContent(): Multiplier
    {
        return new Multiplier(function ($id): Form {
            $form = new Form();

            // INPUTS
            $form->addHidden('id', $id);
            $form->addText('content', 'tournament-game.results.form.update-content.content');

            // SUBMITS
            $form->addSubmit('send', 'tournament-game.results.form.update-content.send');

            // CALLBACK
            $form->onSuccess[] = [$this, 'formUpdateContentOnSuccess'];

            return $form;
        });
    }

    public function formUpdateContentOnSuccess(Form $form, ArrayHash $values): void
    {
        $this->facadeTournamentResult->updateContent(intval($values->id), $values->content);
        $this->redrawControl('snipOverview');
    }
}
