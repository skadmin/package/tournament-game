<?php

declare(strict_types=1);

namespace Skadmin\TournamentGame\Components\Admin;

use App\Model\Doctrine\Customer\CustomerFacade;
use App\Model\Doctrine\User\User;
use App\Model\System\APackageControl;
use App\Model\System\Constant;
use App\Model\System\Flash;
use App\Model\System\FormDefaults;
use Doctrine\Common\Collections\Criteria;
use Nette\ComponentModel\IContainer;
use Nette\Security\User as LoggedUser;
use Nette\Utils\ArrayHash;
use Nette\Utils\Arrays;
use Nette\Utils\DateTime;
use Nette\Utils\Html;
use Skadmin\Game\Doctrine\Game\GameFacade;
use Skadmin\Payment\Doctrine\Payment\PaymentFacade;
use Skadmin\Role\Doctrine\Role\Privilege;
use Skadmin\TournamentGame\BaseControl;
use Skadmin\TournamentGame\Doctrine\Registration\RegistrationTeamFacade;
use Skadmin\TournamentGame\Doctrine\Supplement\Supplement;
use Skadmin\TournamentGame\Doctrine\Supplement\SupplementFacade;
use Skadmin\TournamentGame\Doctrine\Tournament\Tournament;
use Skadmin\TournamentGame\Doctrine\Tournament\TournamentFacade;
use Skadmin\TournamentGame\Doctrine\TournamentPayment\TournamentPaymentBox;
use Skadmin\TournamentGame\Doctrine\TournamentType\TournamentTypeFacade;
use Skadmin\Translator\SimpleTranslation;
use Skadmin\Translator\Translator;
use SkadminUtils\FormControls\Rules\FormRuleValidator;
use SkadminUtils\FormControls\UI\Form;
use SkadminUtils\FormControls\UI\FormWithUserControl;
use SkadminUtils\FormControls\Utils\UtilsFormControl;
use SkadminUtils\ImageStorage\ImageStorage;
use WebLoader\Nette\CssLoader;
use WebLoader\Nette\JavaScriptLoader;
use WebLoader\Nette\LoaderFactory;

use function array_combine;
use function assert;
use function class_exists;
use function count;
use function defined;
use function explode;
use function intval;
use function is_bool;
use function range;
use function sprintf;

class Edit extends FormWithUserControl
{
    use APackageControl;

    private LoaderFactory          $webLoader;
    private TournamentFacade       $facade;
    private TournamentTypeFacade   $facadeTournamentType;
    private CustomerFacade         $facadeCustomer;
    private GameFacade             $facadeGame;
    private PaymentFacade          $facadePayment;
    private SupplementFacade       $facadeSupplement;
    private RegistrationTeamFacade $facadeRegistrationTeam;
    private Tournament             $tournament;
    private ImageStorage           $imageStorage;
    private User                   $user;

    public function __construct(?int $id, TournamentFacade $facade, TournamentTypeFacade $facadeTournamentType, CustomerFacade $facadeCustomer, GameFacade $facadeGame, PaymentFacade $facadePayment, SupplementFacade $facadeSupplement, RegistrationTeamFacade $facadeRegistrationTeam, Translator $translator, LoaderFactory $webLoader, LoggedUser $user, ImageStorage $imageStorage)
    {
        parent::__construct($translator, $user);
        $this->user = $this->loggedUser->getIdentity(); //@phpstan-ignore-line

        $this->facade                 = $facade;
        $this->facadeTournamentType   = $facadeTournamentType;
        $this->facadeCustomer         = $facadeCustomer;
        $this->facadeGame             = $facadeGame;
        $this->facadePayment          = $facadePayment;
        $this->facadeSupplement       = $facadeSupplement;
        $this->facadeRegistrationTeam = $facadeRegistrationTeam;

        $this->webLoader    = $webLoader;
        $this->imageStorage = $imageStorage;

        $this->tournament = $this->facade->get($id);
    }

    public function setParent(?IContainer $parent, ?string $name = null): static
    {
        parent::setParent($parent, $name);

        if (! $this->isAllowed(BaseControl::RESOURCE, BaseControl::PRIVILEGE_LOCK) && $this->tournament->isLocked()) {
            $this->onFlashmessage('form.tournament-game.edit.flash.info.locked', Flash::INFO);
            $this->processOnBack();
        }

        $canEdit = true;
        if ($this->tournament->isLoaded()) {
            $canEdit = $this->user->getAllowedGames()->count() === 0;

            $criteria = Criteria::create();
            $criteria->where(Criteria::expr()->eq('id', $this->tournament->getGame()->getId()));
            $canEdit = $canEdit || $this->user->getAllowedGames()->matching($criteria)->count() > 0;
        }

        if (! $this->isAllowed(BaseControl::RESOURCE, Privilege::WRITE) && $canEdit) {
            $this->getParent()->redirect(':Admin:Homepage:accessDenied');
        }

        return $this;
    }

    public function processOnBack(): void
    {
        $this->getPresenter()->redirect('Component:default', [
            'package' => new BaseControl(),
            'render'  => 'overview',
        ]);
    }

    public function getTitle(): SimpleTranslation|string
    {
        if ($this->tournament->isLoaded()) {
            return new SimpleTranslation('tournament-game.edit.title - %s', $this->tournament->getName());
        }

        return 'tournament-game.edit.title';
    }

    /**
     * @return CssLoader[]
     */
    public function getCss(): array
    {
        return [
            $this->webLoader->createCssLoader('daterangePicker'),
            $this->webLoader->createCssLoader('customFileInput'),
            $this->webLoader->createCssLoader('fancyBox'), // responsive file manager
        ];
    }

    /**
     * @return JavaScriptLoader[]
     */
    public function getJs(): array
    {
        return [
            $this->webLoader->createJavaScriptLoader('adminTinyMce'),
            $this->webLoader->createJavaScriptLoader('moment'),
            $this->webLoader->createJavaScriptLoader('daterangePicker'),
            $this->webLoader->createJavaScriptLoader('customFileInput'),
            $this->webLoader->createJavaScriptLoader('fancyBox'), // responsive file manager
        ];
    }

    public function processOnSuccess(Form $form, ArrayHash $values): void
    {
        [$termFrom, $termTo] = Arrays::map(explode(' - ', $values->term), static function (string $date): DateTime {
            $date = DateTime::createFromFormat('d.m.Y H:i', $date);

            return is_bool($date) ? new DateTime() : $date;
        });
        assert($termFrom instanceof DateTime);
        assert($termTo instanceof DateTime);

        // IDENTIFIER
        $identifier = UtilsFormControl::getImagePreview($values->image_preview, BaseControl::DIR_IMAGE);

        $payments = [];
        if ($values->is_unpaid === false) {
            foreach ($values->payment as $paymentId) {
                $payment    = $this->facadePayment->get($paymentId);
                $payments[] = new TournamentPaymentBox($payment, intval($values->payment_price[$paymentId]->price));
            }
        }

        $supplements = [];
        foreach ($values->supplement as $supplement) {
            $supplements[] = $this->facadeSupplement->get($supplement);
        }

        $winner = null;
        if ($values->winner_id) {
            if ($this->tournament->isOneVsOne()) {
                $winner = $this->facadeCustomer->get($values->winner_id);
            } else {
                $winner = $this->facadeRegistrationTeam->get($values->winner_id);
            }
        }

        $secondPlace = null;
        if ($values->second_place_id) {
            if ($this->tournament->isOneVsOne()) {
                $secondPlace = $this->facadeCustomer->get($values->second_place_id);
            } else {
                $secondPlace = $this->facadeRegistrationTeam->get($values->second_place_id);
            }
        }

        $thirdPlace = null;
        if ($values->third_place_id) {
            if ($this->tournament->isOneVsOne()) {
                $thirdPlace = $this->facadeCustomer->get($values->third_place_id);
            } else {
                $thirdPlace = $this->facadeRegistrationTeam->get($values->third_place_id);
            }
        }

        if ($this->tournament->isLoaded()) {
            if ($identifier !== null && $this->tournament->getImagePreview() !== null) {
                $this->imageStorage->delete($this->tournament->getImagePreview());
            }

            $tournamentGame = $this->facade->update(
                $this->tournament->getId(),
                $values->name,
                $values->change_webalize,
                intval($values->number_of_participants),
                $values->is_online ? $values->confirmation_from : null,
                $values->is_online ? $values->confirmation_to : null,
                $values->content,
                $values->description,
                $values->prize_pool,
                $values->rules,
                $values->stream,
                $values->discord,
                $values->schedule,
                $termFrom,
                $termTo,
                $values->place_name,
                $values->place_address,
                $values->place_gps_lat,
                $values->place_gps_lng,
                $identifier,
                $this->facadeGame->get($values->game_id),
                $this->facadeTournamentType->get($values->tournament_type_id)
            );
            $this->onFlashmessage('form.tournament-game.edit.flash.success.update', Flash::SUCCESS);
        } else {
            $tournamentGame = $this->facade->create(
                $values->name,
                $values->change_webalize,
                intval($values->number_of_participants),
                $values->is_online ? $values->confirmation_from : null,
                $values->is_online ? $values->confirmation_to : null,
                $values->content,
                $values->description,
                $values->prize_pool,
                $values->rules,
                $values->stream,
                $values->discord,
                $values->schedule,
                $termFrom,
                $termTo,
                $values->place_name,
                $values->place_address,
                $values->place_gps_lat,
                $values->place_gps_lng,
                $identifier,
                $this->facadeGame->get($values->game_id),
                $this->facadeTournamentType->get($values->tournament_type_id)
            );
            $this->onFlashmessage('form.tournament-game.edit.flash.success.create', Flash::SUCCESS);
        }

        $this->facade->updateStates(
            $tournamentGame,
            $values->is_active ?? $this->tournament->isActive(),
            $values->is_online,
            $values->is_live_chat,
            $values->is_result,
            $values->is_result_opponent_required,
            $values->is_result_file_required,
            $values->is_locked ?? false,
            $values->is_required_game_id,
            $values->is_open_registration,
            $values->is_confirmation_required
        );

        $this->facade->updatePriceSetting($tournamentGame, $values->is_unpaid, $payments, $supplements);

        if ($this->tournament->isLoaded() && ! $this->tournament->isInFuture()) {
            $this->facade->updateWinner($tournamentGame, $winner, $secondPlace, $thirdPlace);
        }

        if ($form->isSubmitted()->name === 'send_back') {
            $this->processOnBack();
        }

        $this->getPresenter()->redirect('Component:default', [
            'package' => new BaseControl(),
            'render'  => 'edit',
            'id'      => $tournamentGame->getId(),
        ]);
    }

    public function render(): void
    {
        $template               = $this->getComponentTemplate();
        $template->imageStorage = $this->imageStorage;
        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/edit.latte');

        $template->tournament = $this->tournament;
        $template->render();
    }

    protected function createComponentForm(): Form
    {
        $form = new Form();
        $form->setTranslator($this->translator);

        // DATA
        $dataTournamentType = $this->facadeTournamentType->getPairs('id', 'name');
        $dataGame           = [];

        $activeGame   = [];
        $inactiveGame = [];
        $games        = $this->user->getAllowedGames();
        if (count($games) === 0) {
            $games = $this->facadeGame->getAll(true, ['name' => 'ASC']);
        }

        foreach ($games as $game) {
            if ($game->isActive()) {
                $activeGame[$game->getId()] = $game->getName();
            } else {
                $inactiveGame[$game->getId()] = $game->getName();
            }
        }

        if (count($activeGame) !== 0) {
            $dataGame[$this->translator->translate('form.tournament-game.edit.game-id.active')] = $activeGame;
        }

        if (count($inactiveGame) !== 0) {
            $dataGame[$this->translator->translate('form.tournament-game.edit.game-id.inactive')] = $inactiveGame;
        }

        $dataWinner = [];

        if ($this->tournament->isOnline()) {
            $registrations = $this->tournament->getConfirmedRegistrations();
        } else {
            $registrations = $this->tournament->getPaidRegistrations();
        }

        foreach ($registrations as $registration) {
            if ($this->tournament->isOneVsOne()) {
                $user                       = $registration->getUser(); //@phpstan-ignore-line
                $dataWinner[$user->getId()] = $user->getFullName();
            } else {
                $team = $registration->getTeam();
                if ($registration->getTeamNickname() !== '') { //@phpstan-ignore-line
                    $dataWinner[$registration->getId()] = sprintf('%s - %s [%s]', $team->getName(), $registration->getTeamNickname(), $team->getNickname()); //@phpstan-ignore-line
                } elseif ($team !== null) {
                    $dataWinner[$registration->getId()] = sprintf('%s [%s]', $team->getName(), $team->getNickname());
                }
            }
        }

        $dataPayment         = [];
        $dataPaymentInactive = [];
        foreach ($this->facadePayment->getAll() as $payment) {
            if ($payment->isActive()) {
                $dataPayment[$payment->getId()] = $payment->getName();
            } else {
                $dataPaymentInactive[$payment->getId()] = sprintf('[X] %s', $payment->getName());
            }
        }

        $dataPayment += $dataPaymentInactive;

        $dataSupplement         = [];
        $dataSupplementInactive = [];
        foreach ($this->facadeSupplement->getAll() as $supplement) {
            if ($supplement->isActive()) {
                $dataSupplement[$supplement->getId()] = $supplement->getName();
            } else {
                $dataSupplementInactive[$supplement->getId()] = sprintf('[X] %s', $supplement->getName());
            }
        }

        $dataSupplement += $dataSupplementInactive;

        // INPUT
        $form->addText('name', 'form.tournament-game.edit.name')
            ->setRequired('form.tournament-game.edit.name.req');
        $form->addCheckbox('change_webalize', Html::el('sup', [
            'title'          => $this->translator->translate('form.tournament-game.edit.change-webalize'),
            'class'          => 'far fa-fw fa-question-circle',
            'data-toggle'    => 'tooltip',
            'data-placement' => 'left',
        ]))->setTranslator(null);

        $form->addImageWithRFM('image_preview', 'form.tournament-game.edit.image-preview');

        $form->addText('number_of_participants', 'form.tournament-game.edit.number-of-participants')
            ->setHtmlType('number')
            ->setHtmlAttribute('min', 0);

        if ($this->isAllowed(BaseControl::RESOURCE, BaseControl::PRIVILEGE_PUBLISH)) {
            $form->addCheckbox('is_active', 'form.tournament-game.edit.is-active')
                ->setDefaultValue(true);
        }

        if ($this->isAllowed(BaseControl::RESOURCE, BaseControl::PRIVILEGE_LOCK)) {
            $form->addCheckbox('is_locked', 'form.tournament-game.edit.is-locked')
                ->setDefaultValue(false);
        }

        $form->addCheckbox('is_required_game_id', 'form.tournament-game.edit.is-required-game-id')
            ->setDefaultValue(false);

        $formInputIsConfirmationRequired = $form->addCheckbox('is_confirmation_required', 'form.tournament-game.edit.is-confirmation-required')
            ->setDefaultValue(true);
        $formInputIsOpenRegistration     = $form->addCheckbox('is_open_registration', 'form.tournament-game.edit.is-open-registration')
            ->setDefaultValue(false);

        // WINNER
        $form->addSelect('winner_id', 'form.tournament-game.edit.winner-id', $dataWinner)
            ->setPrompt(Constant::PROMTP)
            ->setTranslator(null);
        $form->addSelect('second_place_id', 'form.tournament-game.edit.second-place-id', $dataWinner)
            ->setPrompt(Constant::PROMTP)
            ->setTranslator(null);
        $form->addSelect('third_place_id', 'form.tournament-game.edit.third-place-id', $dataWinner)
            ->setPrompt(Constant::PROMTP)
            ->setTranslator(null);

        // JOIN DATA
        $form->addSelect('tournament_type_id', 'form.tournament-game.edit.tournament-type-id', $dataTournamentType)
            ->setTranslator(null)
            ->setPrompt(Constant::PROMTP)
            ->setRequired('form.tournament-game.edit.tournament-type-id.req');
        $form->addSelect('game_id', 'form.tournament-game.edit.game-id', $dataGame)
            ->setTranslator(null)
            ->setPrompt(Constant::PROMTP)
            ->setRequired('form.tournament-game.edit.game-id.req');

        // DATE
        $form->addText('term', 'form.tournament-game.edit.term')
            ->setRequired('form.tournament-game.edit.term.req')
            ->setHtmlAttribute('data-daterange', 'DD.MM.YYYY HH:mm')
            ->setHtmlAttribute('data-daterange-timepicker');//->setHtmlAttribute('data-daterange-mindate', date('d.m.Y H:i'));

        // ONLINE
        $form->addCheckbox('is_online', 'form.tournament-game.edit.is-online')
            ->addCondition(Form::EQUAL, false)
            ->toggle('is-offline')
            ->endCondition()
            ->addCondition(Form::EQUAL, true)
            ->addConditionOn($formInputIsOpenRegistration, Form::EQUAL, false)
            ->addConditionOn($formInputIsConfirmationRequired, Form::EQUAL, true)
            ->toggle('is-online');

        // CONFIRMATION
        $range0to120minutes  = range(0, 120, 5);
        $range10to120minutes = range(10, 120, 5);
        $range3hoursTo2days = range(180, 60 * 48, 60);

        $rangeFrom = array_merge($range0to120minutes, $range3hoursTo2days);
        $rangeTo = array_merge($range10to120minutes, $range3hoursTo2days);

        $fncTrasformMinutesToText = function ($minutes): array {
            $modifyMinutes = [];

            foreach ($minutes as $key => $totalMinute) {
                $hours = floor($totalMinute / 60);
                $minutes = $totalMinute % 60;

                if ($minutes === 0) {
                    $modifyMinutes[$key] = sprintf('%s%s',
                        $hours,
                        $this->translator->translate('form.tournament-game.edit.confirmation-hours')
                    );
                } else {
                    $modifyMinutes[$key] = sprintf('%s%s %s%s',
                        $hours,
                        $this->translator->translate('form.tournament-game.edit.confirmation-hours'),
                        $minutes,
                        $this->translator->translate('form.tournament-game.edit.confirmation-minutes')
                    );
                }
            }

            return $modifyMinutes;
        };

        $dataFrom = array_combine($rangeFrom, $fncTrasformMinutesToText($rangeFrom));
        $dataTo = array_combine($rangeTo, $fncTrasformMinutesToText($rangeTo));
        $form->addSelect('confirmation_from', 'form.tournament-game.edit.confirmation-from', $dataFrom)
            ->setTranslator(null);
        $form->addSelect('confirmation_to', 'form.tournament-game.edit.confirmation-to', $dataTo)
            ->setTranslator(null);

        // TEXT
        $form->addTextArea('content', 'form.tournament-game.edit.content');
        $form->addTextArea('description', 'form.tournament-game.edit.description');
        $form->addTextArea('prize_pool', 'form.tournament-game.edit.prize-pool');
        $form->addTextArea('rules', 'form.tournament-game.edit.rules');
        $form->addTextArea('stream', 'form.tournament-game.edit.stream');
        $form->addUrl('discord', 'form.tournament-game.edit.discord', ['discord.com', 'discordapp.com', 'discord.gg']);
        $form->addTextArea('schedule', 'form.tournament-game.edit.schedule');

        // PLACE
        $form->addText('place_name', 'form.tournament-game.edit.place-name');
        $form->addText('place_address', 'form.tournament-game.edit.place-address');
        $form->addText('place_gps_lat', 'form.tournament-game.edit.place-gps-lat');
        $form->addText('place_gps_lng', 'form.tournament-game.edit.place-gps-lng');

        // SUPPLEMENT
        $form->addMultiSelect('supplement', 'form.tournament-game.edit.supplement', $dataSupplement)
            ->setTranslator(null);

        // TABS
        $form->addCheckbox('is_live_chat', 'form.tournament-game.edit.is-live-chat');
        $form->addCheckbox('is_result', 'form.tournament-game.edit.is-result')
            ->addCondition(Form::EQUAL, true)
            ->toggle('is-result-required');
        $form->addCheckbox('is_result_opponent_required', 'form.tournament-game.edit.is-result-opponent-required');
        $form->addCheckbox('is_result_file_required', 'form.tournament-game.edit.is-result-file-required');

        // PAYMENT
        $form->addCheckbox('is_unpaid', 'form.tournament-game.edit.is-unpaid')
            ->addCondition(Form::EQUAL, false)
            ->toggle('input-payment');
        $inputPayment = $form->addMultiSelect('payment', 'form.tournament-game.edit.payment', $dataPayment)
            ->setTranslator(null);

        $containerPayment = $form->addContainer('payment_price');
        foreach ($dataPayment as $paymentId => $paymentName) {
            $containerPaymentPrice = $containerPayment->addContainer($paymentId);
            $containerPaymentPrice->addText('price', new SimpleTranslation('form.tournament-game.edit.payment-price - %s', $paymentName))
                ->setTranslator(null)
                ->setHtmlType('number')
                ->setHtmlAttribute('min', 0)
                ->addConditionOn($inputPayment, FormRuleValidator::IS_SELECTED, $paymentId)
                ->setRequired('form.tournament-game.edit.payment-price.req');

            $inputPayment->addCondition(FormRuleValidator::IS_SELECTED, $paymentId)
                ->toggle(sprintf('payment-price-%d', $paymentId));
        }

        // BUTTON
        $form->addSubmit('send', 'form.tournament-game.edit.send');
        $form->addSubmit('send_back', 'form.tournament-game.edit.send-back');
        $form->addSubmit('back', 'form.tournament-game.edit.back')
            ->setValidationScope([])
            ->onClick[] = [$this, 'processOnBack'];

        // DEFAULT
        $form->setDefaults($this->getDefaults());

        // CALLBACK
        $form->onSuccess[] = [$this, 'processOnSuccess'];

        return $form;
    }

    /**
     * @return mixed[]
     */
    private function getDefaults(): array
    {
        if (! $this->tournament->isLoaded()) {
            if (class_exists(FormDefaults::class) && defined('App\Model\System\FormDefaults::TOURNAMENT_GAME__ADMIN__EDIT')) {
                return FormDefaults::TOURNAMENT_GAME__ADMIN__EDIT;
            }

            return [];
        }

        $payments      = [];
        $paymentsPrice = [];
        foreach ($this->tournament->getPayments() as $payment) {
            $payments[]                                              = $payment->getPayment()->getId();
            $paymentsPrice[$payment->getPayment()->getId()]['price'] = $payment->getPrice();
        }

        $supplements = $this->tournament->getSupplements()->map(static function (Supplement $supplement): ?int {
            return $supplement->getId();
        })->toArray();

        return [
            'name'                        => $this->tournament->getName(),
            'term'                        => sprintf('%s - %s', $this->tournament->getTermFrom()->format('d.m.Y H:i'), $this->tournament->getTermTo()->format('d.m.Y H:i')),
            'game_id'                     => $this->tournament->getGame()->getId(),
            'winner_id'                   => $this->tournament->getWinner() !== null ? $this->tournament->getWinner()->getId() : null,
            'second_place_id'             => $this->tournament->getSecondPlace() !== null ? $this->tournament->getSecondPlace()->getId() : null,
            'third_place_id'              => $this->tournament->getThirdPlace() !== null ? $this->tournament->getThirdPlace()->getId() : null,
            'tournament_type_id'          => $this->tournament->getTournamentType() !== null ? $this->tournament->getTournamentType()->getId() : null,
            'number_of_participants'      => $this->tournament->getNumberOfParticipants(),
            'place_name'                  => $this->tournament->getPlaceName(),
            'place_address'               => $this->tournament->getPlaceAddress(),
            'place_gps_lat'               => $this->tournament->getPlaceGpsLat(),
            'place_gps_lng'               => $this->tournament->getPlaceGpsLng(),
            'content'                     => $this->tournament->getContent(),
            'description'                 => $this->tournament->getDescription(),
            'prize_pool'                  => $this->tournament->getPrizepool(),
            'rules'                       => $this->tournament->getRules(),
            'stream'                      => $this->tournament->getStream(),
            'discord'                     => $this->tournament->getDiscord(),
            'schedule'                    => $this->tournament->getSchedule(),
            'is_active'                   => $this->tournament->isActive(),
            'is_online'                   => $this->tournament->isOnline(),
            'confirmation_from'           => $this->tournament->getConfirmationFrom(),
            'confirmation_to'             => $this->tournament->getConfirmationTo(),
            'payment'                     => $payments,
            'payment_price'               => $paymentsPrice,
            'supplement'                  => $supplements,
            'is_unpaid'                   => $this->tournament->isUnpaid(),
            'is_live_chat'                => $this->tournament->isLiveChat(),
            'is_result'                   => $this->tournament->isResult(),
            'is_result_opponent_required' => $this->tournament->isResultOpponentRequired(),
            'is_result_file_required'     => $this->tournament->isResultFileRequired(),
            'is_locked'                   => $this->tournament->isLocked(),
            'is_required_game_id'         => $this->tournament->isRequiredGameId(),
            'is_open_registration'        => $this->tournament->isOpenRegistration(),
            'is_confirmation_required'    => $this->tournament->isConfirmationRequired(),
        ];
    }
}
