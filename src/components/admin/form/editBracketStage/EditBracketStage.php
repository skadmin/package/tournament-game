<?php

declare(strict_types=1);

namespace Skadmin\TournamentGame\Components\Admin;

use App\Model\System\APackageControl;
use App\Model\System\Constant;
use App\Model\System\Flash;
use Nette\ComponentModel\IContainer;
use Nette\Security\User as LoggedUser;
use Nette\Utils\ArrayHash;
use Skadmin\TournamentGame\BaseControl;
use Skadmin\TournamentGame\Doctrine\Tournament\Tournament;
use Skadmin\TournamentGame\Doctrine\Tournament\TournamentFacade;
use Skadmin\TournamentGame\Doctrine\TournamentBracket\TournamentBracket;
use Skadmin\TournamentGame\Doctrine\TournamentBracket\TournamentBracketFacade;
use Skadmin\TournamentGame\Doctrine\TournamentBracketStage\ATournamentBracketStage;
use Skadmin\TournamentGame\Doctrine\TournamentBracketStage\TournamentBracketStageFacade;
use Skadmin\TournamentGame\Doctrine\TournamentBracketStage\Type\TournamentBracketStageDoubleElimination;
use Skadmin\TournamentGame\Doctrine\TournamentBracketStage\Type\TournamentBracketStageRoundRobin;
use Skadmin\TournamentGame\Doctrine\TournamentBracketStage\Type\TournamentBracketStageRoundRobinDuel;
use Skadmin\Translator\SimpleTranslation;
use Skadmin\Translator\Translator;
use SkadminUtils\FormControls\UI\Form;
use SkadminUtils\FormControls\UI\FormWithUserControl;

use function array_flip;
use function intval;

class EditBracketStage extends FormWithUserControl
{
    use APackageControl;

    private TournamentBracketStageFacade $facade;
    private TournamentBracketFacade $facadeBracket;
    private TournamentFacade $facadeTournament;
    private ?Tournament $tournament = null;
    private ?ATournamentBracketStage $bracketStage = null;

    public function __construct(?int $id, TournamentBracketStageFacade $facade, TournamentBracketFacade $facadeBracket, TournamentFacade $facadeTournament, Translator $translator, LoggedUser $user)
    {
        parent::__construct($translator, $user);
        $this->facade = $facade;
        $this->facadeBracket = $facadeBracket;
        $this->facadeTournament = $facadeTournament;

        $this->bracketStage = $this->facade->get($id);
        if (!($this->bracketStage instanceof ATournamentBracketStage)) {
            return;
        }

        $this->tournament = $this->bracketStage->getBracket()->getTournament();
    }

    public function setParent(?IContainer $parent, ?string $name = null): static
    {
        parent::setParent($parent, $name);

        if (!$this->isAllowed(BaseControl::RESOURCE, BaseControl::PRIVILEGE_BRACKET)) {
            $this->getParent()->redirect(':Admin:Homepage:accessDenied');
        }

        if ($this->tournament === null) {
            $this->tournament = $this->facadeTournament->get(intval($this->getPresenter()->getParameter('tournamentId')));
        }

        if ($this->getPresenter()->isAjax()) {
            $this->drawBox = false;
            $this->isModal = true;
        }

        return $this;
    }

    public function getTitle(): SimpleTranslation|string
    {
        if ($this->bracketStage instanceof ATournamentBracketStage) {
            return new SimpleTranslation('tournament-game.bracket.edit-stage.title - %s', $this->bracketStage->getName());
        }

        return new SimpleTranslation('tournament-game.bracket.edit-stage.create-title - %s', $this->tournament->getName());
    }

    public function render(): void
    {
        $template = $this->getComponentTemplate();
        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/editBracketStage.latte');

        $template->drawBox = $this->drawBox;
        $template->isModal = $this->isModal;
        $template->countStages = $this->tournament->getBracket() instanceof TournamentBracket ? $this->tournament->getBracket()->getStages()->count() + 1 : 1;

        $template->render();
    }

    protected function createComponentForm(): Form
    {
        $dataBestOfGame = [1 => 1, 2 => 2, 3 => 3, 5 => 5, 7 => 7];
        $dataType = array_flip(ATournamentBracketStage::generateAllowedTypes());
        $dataParticipants = [];

        $tournament = $this->tournament->isLoaded() ? $this->tournament : $this->facadeTournament->get(intval($_POST['tournamentId']));

        if ($tournament->isOnline()) {
            $registrations = $tournament->getConfirmedRegistrations();
        } else {
            $registrations = $tournament->getPaidRegistrations();
        }

        foreach ($registrations as $registration) {
            $dataParticipants[$registration->getId()] = $registration->getFullName();
        }

        $form = new Form();
        $form->setTranslator($this->translator);

        $isEdit = $this->bracketStage !== null && $this->bracketStage->isLoaded();

        // INPUT
        $form->addHidden('tournamentId', $this->tournament->getId());
        $form->addSelect('type', 'tournament-game.bracket.edit-stage.type', $dataType)
            ->setPrompt(Constant::PROMTP)
            ->setDisabled($isEdit)
            ->setRequired('tournament-game.bracket.edit-stage.type.req')
            ->addCondition(Form::EQUAL, TournamentBracketStageDoubleElimination::class)
            ->toggle('toggle-best-of-game-loser')
            ->endCondition()
            ->addCondition(Form::EQUAL, TournamentBracketStageRoundRobinDuel::class)
            ->toggle('toggle-round-robin-duel')
            ->toggle('toggle-sequential-display-of-duels')
            ->endCondition()
            ->addCondition(Form::NOT_EQUAL, TournamentBracketStageRoundRobinDuel::class)
            ->toggle('toggle-best-of-game')
            ->endCondition();
        //->addCondition(Form::EQUAL, TournamentBracketStageRoundRobin::class)

        $form->addText('name', 'tournament-game.bracket.edit-stage.name')
            ->setRequired('tournament-game.bracket.edit-stage.name.req');

        $form->addCheckbox('willPlayInTheFinals', 'tournament-game.bracket.edit-stage.will-play-in-the-finals')
            ->setDefaultValue(true)
            ->addCondition(Form::EQUAL, false)
            ->toggle('toggle-number-of-rounds');

        $form->addInteger('numberOfRounds', 'tournament-game.bracket.edit-stage.number-of-rounds')
            ->setDefaultValue(2)
            ->setHtmlAttribute('min', 2)
            ->addConditionOn($form['willPlayInTheFinals'], Form::EQUAL, false)
            ->setRequired('tournament-game.bracket.edit-stage.number-of-rounds.req');

        $form->addSelect('bestOfGame', 'tournament-game.bracket.edit-stage.best-of-game', $dataBestOfGame)
            ->setTranslator(null)
            ->setDefaultValue(1);

        $form->addSelect('bestOfGameLoser', 'tournament-game.bracket.edit-stage.best-of-game-loser', $dataBestOfGame)
            ->setTranslator(null)
            ->setDefaultValue(1);

        $form->addInteger('numberOfDuels', 'tournament-game.bracket.edit-stage.number-of-duels')
            ->setDefaultValue(2)
            ->setHtmlAttribute('min', 1);

        $form->addInteger('numberOfRoundDuels', 'tournament-game.bracket.edit-stage.number-of-round-duels')
            ->setDefaultValue(1)
            ->setHtmlAttribute('min', 1);

        $form->addCheckbox('sequentialDisplayOfDuels', 'tournament-game.bracket.edit-stage.sequential-display-of-duels')
            ->setDefaultValue(false);

        if ($this->bracketStage === null || !$this->bracketStage->isPublic()) {
            $form->addMultiSelect('participants', 'tournament-game.bracket.edit-stage.participants', $dataParticipants)
                ->setTranslator(null);
        } else {
            $form['willPlayInTheFinals']->setDisabled();
            $form['numberOfRounds']->setDisabled();
        }

        // BUTTON
        $form->addSubmit('send', 'tournament-game.bracket.edit-stage.send');
        $form->addSubmit('send_back', 'tournament-game.bracket.edit-stage.send-back');
        $form->addSubmit('back', 'tournament-game.bracket.edit-stage.back')
            ->setValidationScope([])
            ->onClick[] = [$this, 'processOnBack'];

        // DEFAULT
        $form->setDefaults($this->getDefaults());

        // CALLBACK
        $form->onSuccess[] = [$this, 'processOnSuccess'];

        return $form;
    }

    public function processOnSuccess(Form $form, ArrayHash $values): void
    {
        $this->tournament = $this->tournament->isLoaded() ? $this->tournament : $this->facadeTournament->get(intval($values->tournamentId));

        $bracket = $this->tournament->getBracket();
        if (!$bracket instanceof TournamentBracket) {
            $bracket = $this->facadeBracket->create($this->tournament);
        }

        $numberOfRounds = intval($values->numberOfRounds ?? $this->bracketStage->getNumberOfRounds());
        $willPlayInTheFinals = $values->willPlayInTheFinals ?? $this->bracketStage->isWillPlayInTheFinals();
        $sequentialDisplayOfDuels = $values->sequentialDisplayOfDuels ?? $this->bracketStage->isSequentialDisplayOfDuels();

        if (isset($values->numberOfRoundDuels)) {
            $bestOfGame = intval($values->numberOfRoundDuels);
        } else {
            $bestOfGame = intval($values->bestOfGame);
        }

        if ($this->bracketStage instanceof ATournamentBracketStage) {
            if ($this->bracketStage->getNumberOfRounds() !== $numberOfRounds) {
                $this->facade->clearMatchs($this->bracketStage->getId());
            }

            $bracketStage = $this->facade->update(
                $this->bracketStage->getId(),
                $values->name,
                $willPlayInTheFinals,
                $sequentialDisplayOfDuels,
                $numberOfRounds,
                $bestOfGame
            );
            $this->onFlashmessage('tournament-game.bracket.edit-stage.flash.success.update', Flash::SUCCESS);
        } else {
            $bracketStage = $this->facade->create(
                $bracket,
                $values->type,
                $values->name,
                $willPlayInTheFinals,
                $sequentialDisplayOfDuels,
                $numberOfRounds,
                $bestOfGame
            );
            $this->onFlashmessage('tournament-game.bracket.edit-stage.flash.success.create', Flash::SUCCESS);
        }

        if (isset($values->participants) && count($values->participants) >= 4) {
            $this->facade->createParticipants($bracketStage, $values->participants);
        }

        if ($bracketStage instanceof TournamentBracketStageDoubleElimination) {
            $this->facade->updateDoubleElimination($bracketStage, $values->bestOfGameLoser);
        }

        if ($bracketStage instanceof TournamentBracketStageRoundRobin) {
            $this->facade->updateRoundRobin($bracketStage);
        }

        if ($bracketStage instanceof TournamentBracketStageRoundRobinDuel) {
            $this->facade->updateRoundRobinDuel($bracketStage, $values->numberOfDuels, $values->numberOfRoundDuels);
        }

        if ($form->isSubmitted()->name === 'send_back') {
            $this->processOnBack();
        }

        $this->getPresenter()->redirect('Component:default', [
            'package' => new BaseControl(),
            'render' => 'edit-bracket-stage',
            'id' => $bracketStage->getId(),
        ]);
    }

    public function processOnBack(): void
    {
        $this->getPresenter()->redirect('Component:default', [
            'package' => new BaseControl(),
            'render' => 'bracket',
            'id' => $this->tournament->getId(),
        ]);
    }

    /**
     * @return mixed[]
     */
    private function getDefaults(): array
    {
        if ($this->bracketStage === null) {
            return [];
        }

        $participants = [];

        if ($this->bracketStage->getParticipants()->count() !== $this->bracketStage->getBracket()->getTournament()->getCountRegistrations()) {
            foreach ($this->bracketStage->getParticipants() as $participant) {
                $participants[] = $participant->getRegistration()->getId();
            }
        }

        $default = [
            'type' => ATournamentBracketStage::generateAllowedTypes()[$this->bracketStage->getType()],
            'name' => $this->bracketStage->getName(),
            'willPlayInTheFinals' => $this->bracketStage->isWillPlayInTheFinals(),
            'numberOfRounds' => $this->bracketStage->getNumberOfRounds(),
            'bestOfGame' => $this->bracketStage->getBestOfGame(),
            'participants' => $participants,
        ];

        if ($this->bracketStage instanceof TournamentBracketStageDoubleElimination) {
            $default['bestOfGameLoser'] = $this->bracketStage->getBestOfGameLoser();
        }

        if ($this->bracketStage instanceof TournamentBracketStageRoundRobinDuel) {
            $default['numberOfDuels'] = $this->bracketStage->getNumberOfDuels();
            $default['numberOfRoundDuels'] = $this->bracketStage->getNumberOfRoundDuels();
            $default['sequentialDisplayOfDuels'] = $this->bracketStage->isSequentialDisplayOfDuels();
        }

        //if ($this->bracketStage instanceof TournamentBracketStageRoundRobin) {}

        return $default;
    }
}
