<?php

declare(strict_types=1);

namespace Skadmin\TournamentGame\Components\Admin;

interface IEditBracketStageFactory
{
    public function create(?int $id = null): EditBracketStage;
}
