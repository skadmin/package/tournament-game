<?php

declare(strict_types=1);

namespace Skadmin\TournamentGame\Components\Admin;

use App\Model\System\APackageControl;
use App\Model\System\Constant;
use App\Model\System\Flash;
use Nette\ComponentModel\IContainer;
use Nette\Security\User as LoggedUser;
use Nette\Utils\ArrayHash;
use Nette\Utils\DateTime;
use Skadmin\TournamentGame\BaseControl;
use Skadmin\TournamentGame\Doctrine\TournamentBracketStage\Type\TournamentBracketStageRoundRobinDuel;
use Skadmin\TournamentGame\Doctrine\TournamentBracketStageMatch\TournamentBracketStageMatch;
use Skadmin\TournamentGame\Doctrine\TournamentBracketStageMatch\TournamentBracketStageMatchFacade;
use Skadmin\TournamentGame\Doctrine\TournamentBracketStageParticipant\TournamentBracketStageParticipantFacade;
use Skadmin\Translator\SimpleTranslation;
use Skadmin\Translator\Translator;
use SkadminUtils\FormControls\UI\Form;
use SkadminUtils\FormControls\UI\FormWithUserControl;

use WebLoader\Nette\CssLoader;
use WebLoader\Nette\JavaScriptLoader;
use WebLoader\Nette\LoaderFactory;
use function intval;
use function sprintf;
use function trim;

class EditBracketStageMatch extends FormWithUserControl
{
    use APackageControl;

    private TournamentBracketStageMatchFacade       $facade;
    private TournamentBracketStageParticipantFacade $facadeBracketStageParticipant;
    private TournamentBracketStageMatch             $bracketStageMatch;
    private LoaderFactory                           $webLoader;

    public function __construct(?int $id, TournamentBracketStageMatchFacade $facade, TournamentBracketStageParticipantFacade $facadeBracketStageParticipant, Translator $translator, LoaderFactory $webLoader, LoggedUser $user)
    {
        parent::__construct($translator, $user);
        $this->facade                        = $facade;
        $this->facadeBracketStageParticipant = $facadeBracketStageParticipant;
        $this->webLoader                     = $webLoader;

        $this->bracketStageMatch = $this->facade->get($id);
    }

    public function setParent(?IContainer $parent, ?string $name = null): static
    {
        parent::setParent($parent, $name);

        if (! $this->isAllowed(BaseControl::RESOURCE, BaseControl::PRIVILEGE_BRACKET)) {
            $this->getParent()->redirect(':Admin:Homepage:accessDenied');
        }

        if ($this->getPresenter()->isAjax()) {
            $this->drawBox = false;
            $this->isModal = true;
        }

        return $this;
    }

    public function getTitle(): SimpleTranslation
    {
        $challenger = $this->bracketStageMatch->getChallenger();
        $opponent   = $this->bracketStageMatch->getOpponent();
        $title      = sprintf('%s vs %s', $challenger === null ? '--' : $challenger->getName(), $opponent === null ? '--' : $opponent->getName());

        return new SimpleTranslation('tournament-game.bracket.edit-stage-match.edit-title - %s', $title);
    }


    /**
     * @return CssLoader[]
     */
    public function getCss(): array
    {
        return [
            $this->webLoader->createCssLoader('daterangePicker'),
        ];
    }

    /**
     * @return JavaScriptLoader[]
     */
    public function getJs(): array
    {
        return [
            $this->webLoader->createJavaScriptLoader('moment'),
            $this->webLoader->createJavaScriptLoader('daterangePicker'),
        ];
    }

    public function render(): void
    {
        $template = $this->getComponentTemplate();
        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/editBracketStageMatch.latte');

        $template->drawBox           = $this->drawBox;
        $template->isModal           = $this->isModal;
        $template->bracketStageMatch = $this->bracketStageMatch;

        $template->render();
    }

    protected function createComponentForm(): Form
    {
        // DATA
        $dataStatus       = TournamentBracketStageMatch::STATUTES;
        $dataParticipants = [];
        foreach ($this->bracketStageMatch->getStage()->getParticipants() as $participant) {
            $dataParticipants[$participant->getId()] = $participant->getName();
        }

        // FORM
        $form = new Form();
        $form->setTranslator($this->translator);

        // INPUT
        $form->addSelect('status', 'tournament-game.bracket.edit-stage-match.status', $dataStatus);
        $form->addSelect('challenger', 'tournament-game.bracket.edit-stage-match.challenger', $dataParticipants)
            ->setTranslator(null)
            ->setPrompt(Constant::PROMTP);
        $form->addSelect('opponent', 'tournament-game.bracket.edit-stage-match.opponent', $dataParticipants)
            ->setTranslator(null)
            ->setPrompt(Constant::PROMTP);

        $form->addText('challengerResult', 'tournament-game.bracket.edit-stage-match.challenger-result')
            ->setHtmlType('number')
            ->setHtmlAttribute('min', 0)
            ->setHtmlAttribute('step', 1);
        $form->addText('opponentResult', 'tournament-game.bracket.edit-stage-match.opponent-result')
            ->setHtmlType('number')
            ->setHtmlAttribute('min', 0)
            ->setHtmlAttribute('step', 1);

        $form->addCheckbox('evaluateTheResult', 'tournament-game.bracket.edit-stage-match.evaluate-the-result')
            ->addCondition(Form::EQUAL, false)
            ->toggle('toggle-status');

        if ($this->bracketStageMatch->getStage() instanceof TournamentBracketStageRoundRobinDuel) {
            $form->addText('allowedFrom', 'tournament-game.bracket.allowed-from')
                ->setHtmlAttribute('data-date');
        }

        // BUTTON
        $form->addSubmit('send', 'tournament-game.bracket.edit-stage-match.send');
        $form->addSubmit('send_back', 'tournament-game.bracket.edit-stage-match.send-back');
        $form->addSubmit('back', 'tournament-game.bracket.edit-stage-match.back')
            ->setValidationScope([])
            ->onClick[] = [$this, 'processOnBack'];

        // DEFAULT
        $form->setDefaults($this->getDefaults());

        // CALLBACK
        $form->onSuccess[] = [$this, 'processOnSuccess'];

        return $form;
    }

    public function processOnSuccess(Form $form, ArrayHash $values): void
    {
        $bracketStageMatch = $this->facade->updateMatch(
            $this->bracketStageMatch->getId(),
            intval($values->status),
            $this->facadeBracketStageParticipant->get($values->challenger),
            $this->facadeBracketStageParticipant->get($values->opponent),
            trim($values->challengerResult) === '' ? null : intval($values->challengerResult),
            trim($values->opponentResult) === '' ? null : intval($values->opponentResult),
            $values->evaluateTheResult
        );

        if ($this->bracketStageMatch->getStage() instanceof TournamentBracketStageRoundRobinDuel) {
            $allowedFrom = DateTime::createFromFormat('d.m.Y', $values->allowedFrom);
            $allowedFrom->setTime(0, 0, 0, 0);
            $bracketStageMatch = $this->facade->updateAllowedFrom($bracketStageMatch->getId(), $allowedFrom);
        }

        $this->onFlashmessage('tournament-game.bracket.edit-stage-match.flash.success.update', Flash::SUCCESS);

        if ($form->isSubmitted()->name === 'send_back') {
            $this->processOnBack();
        }

        $this->getPresenter()->redirect('Component:default', [
            'package' => new BaseControl(),
            'render'  => 'edit-bracket-stage-match',
            'id'      => $bracketStageMatch->getId(),
        ]);
    }

    public function processOnBack(): void
    {
        $this->getPresenter()->redirect('Component:default', [
            'package' => new BaseControl(),
            'render'  => 'bracket-stage-view',
            'id'      => $this->bracketStageMatch->getStage()->getId(),
        ]);
    }

    /**
     * @return mixed[]
     */
    private function getDefaults(): array
    {
        if (! $this->bracketStageMatch->isLoaded()) {
            return [];
        }

        $challenger = $this->bracketStageMatch->getChallenger();
        $opponent   = $this->bracketStageMatch->getOpponent();

        $data = [
            'status'           => $this->bracketStageMatch->getStatus(),
            'challenger'       => $challenger === null ? null : $challenger->getId(),
            'opponent'         => $opponent === null ? null : $opponent->getId(),
            'challengerResult' => $this->bracketStageMatch->getChallengerResult(),
            'opponentResult'   => $this->bracketStageMatch->getOpponentResult(),
        ];

        if ($this->bracketStageMatch->getStage() instanceof TournamentBracketStageRoundRobinDuel) {
            $data['allowedFrom'] = $this->bracketStageMatch->getAllowedFrom() === null ? null : $this->bracketStageMatch->getAllowedFrom()->format('d.m.Y');
        }

        return $data;
    }
}
