<?php

declare(strict_types=1);

namespace Skadmin\TournamentGame\Components\Admin;

use App\Model\Doctrine\Customer\Customer;
use App\Model\Doctrine\Customer\CustomerFacade;
use App\Model\Doctrine\User\User;
use App\Model\System\APackageControl;
use App\Model\System\Constant;
use App\Model\System\Flash;
use Doctrine\Common\Collections\Criteria;
use Exception;
use Nette\Application\Attributes\Persistent;
use Nette\ComponentModel\IContainer;
use Nette\Http\Request;
use Nette\Security\User as LoggedUser;
use Nette\Utils\ArrayHash;
use Nette\Utils\Html;
use Skadmin\Payment\Doctrine\Payment\Payment;
use Skadmin\Payment\Doctrine\Payment\PaymentFacade;
use Skadmin\Role\Doctrine\Role\Privilege;
use Skadmin\TournamentGame\BaseControl;
use Skadmin\TournamentGame\Doctrine\Registration\RegistrationTeam;
use Skadmin\TournamentGame\Doctrine\Registration\RegistrationTeamFacade;
use Skadmin\TournamentGame\Doctrine\RegistrationSupplement\RegistrationTeamSupplement;
use Skadmin\TournamentGame\Doctrine\Supplement\Supplement;
use Skadmin\TournamentGame\Doctrine\Supplement\SupplementFacade;
use Skadmin\TournamentGame\Doctrine\Team\Team;
use Skadmin\TournamentGame\Doctrine\Team\TeamFacade;
use Skadmin\TournamentGame\Doctrine\Tournament\Tournament;
use Skadmin\TournamentGame\Doctrine\Tournament\TournamentFacade;
use Skadmin\TournamentGame\Doctrine\TournamentPayment\TournamentPayment;
use Skadmin\Translator\SimpleTranslation;
use Skadmin\Translator\Translator;
use SkadminUtils\FormControls\UI\Form;
use SkadminUtils\FormControls\UI\FormWithUserControl;
use SkadminUtils\ImageStorage\ImageStorage;
use WebLoader\Nette\JavaScriptLoader;
use WebLoader\Nette\LoaderFactory;

use function array_filter;
use function array_merge;
use function chr;
use function count;
use function intval;
use function is_string;
use function number_format;
use function sprintf;
use function trim;

class EditRegistrationTeam extends FormWithUserControl
{
    use APackageControl;

    private LoaderFactory          $webLoader;
    private RegistrationTeamFacade $facade;
    private TournamentFacade       $facadeTournament;
    private CustomerFacade         $facadeCustomer;
    private PaymentFacade          $facadePayment;
    private TeamFacade             $facadeTeam;
    private SupplementFacade       $facadeSupplement;
    private Request                $httpRequest;
    private ImageStorage           $imageStorage;
    private RegistrationTeam       $registrationTeam;
    private ?Tournament            $tournament = null;
    #[Persistent]
    public ?int                    $tid        = null;
    private User                   $user;

    /** @var TournamentPayment[] */
    private array $paymentMethods = [];

    public function __construct(?int $id, RegistrationTeamFacade $facade, TournamentFacade $facadeTournament, PaymentFacade $facadePayment, CustomerFacade $facadeCustomer, TeamFacade $facadeTeam, SupplementFacade $facadeSupplement, Request $httpRequest, ImageStorage $imageStorage, Translator $translator, LoaderFactory $webLoader, LoggedUser $user)
    {
        parent::__construct($translator, $user);
        $this->user = $this->loggedUser->getIdentity(); //@phpstan-ignore-line

        $this->facade           = $facade;
        $this->facadeTournament = $facadeTournament;
        $this->facadeCustomer   = $facadeCustomer;
        $this->facadePayment    = $facadePayment;
        $this->facadeTeam       = $facadeTeam;
        $this->facadeSupplement = $facadeSupplement;
        $this->imageStorage     = $imageStorage;

        $this->webLoader   = $webLoader;
        $this->httpRequest = $httpRequest;

        $this->registrationTeam = $this->facade->get($id);
        if (! $this->registrationTeam->isLoaded()) {
            return;
        }

        $this->tournament = $this->registrationTeam->getTournament();
    }

    public function setParent(?IContainer $parent, ?string $name = null): static
    {
        parent::setParent($parent, $name);

        if (! $this->isAllowed(BaseControl::RESOURCE, BaseControl::PRIVILEGE_LOCK) && $this->tournament->isLocked()) {
            $this->onFlashmessage('form.tournament-game.edit.flash.info.locked', Flash::INFO);
            $this->getPresenter()->redirect('Component:default', [
                'package' => new BaseControl(),
                'render'  => 'overview',
            ]);
        }

        if (! $this->registrationTeam->isLoaded()) {
            $tid = $this->getParameter('tid', $this->httpRequest->getQuery('tid'));
            if ($tid !== null) {
                $this->tid        = intval($tid);
                $this->tournament = $this->facadeTournament->get($this->tid);
            }
        }

        if (! $this->tournament instanceof Tournament) {
            $this->getParent()->redirect(':Admin:Homepage:accessDenied');
        }

        $canEdit = true;
        if ($this->tournament->isLoaded()) {
            $canEdit = $this->user->getAllowedGames()->count() === 0;

            $criteria = Criteria::create();
            $criteria->where(Criteria::expr()->eq('id', $this->tournament->getGame()->getId()));
            $canEdit = $canEdit || $this->user->getAllowedGames()->matching($criteria)->count() > 0;
        }

        if (! $this->isAllowed(BaseControl::RESOURCE, Privilege::WRITE) && $canEdit) {
            $this->getParent()->redirect(':Admin:Homepage:accessDenied');
        }

        return $this;
    }

    /**
     * @return JavaScriptLoader[]
     */
    public function getJs(): array
    {
        return [$this->webLoader->createJavaScriptLoader('choosen')];
    }

    public function getTitle(): SimpleTranslation|string
    {
        if ($this->registrationTeam->isLoaded()) {
            $team = $this->registrationTeam->getTeam();
            if ($this->registrationTeam->getTeamNickname() !== '' && $team !== null) {
                $title = sprintf('%s - %s [%s]', $team->getName(), $this->registrationTeam->getTeamNickname(), $team->getNickname());
            } elseif ($team !== null) {
                $title = sprintf('%s [%s]', $team->getName(), $team->getNickname());
            } else {
                $title = 'team name not found';
            }

            return new SimpleTranslation('registration-team.edit.title - %s', $title);
        }

        return 'registration-team.edit.title';
    }

    public function processOnSuccess(Form $form, ArrayHash $values): void
    {
        if ($this->tournament->isUnpaid() && $this->tournament !== null) {
            $tournamentPayment = new TournamentPayment();
            $tournamentPayment->create($this->tournament, $this->facadePayment->getForUnpaid(), 0);
        } elseif ($this->tournament !== null) {
            $payment           = $this->facadePayment->get($values->payment);
            $tournamentPayment = $this->tournament->findPayment($payment);
        } else {
            $tournamentPayment = null;
        }

        if ($tournamentPayment === null) {
            throw new Exception('Payment for tournament is not set');
        }

        $supplements = [];
        foreach ($values->supplement as $supplement) {
            $supplements[] = $this->facadeSupplement->get($supplement);
        }

        $captain = $this->facadeCustomer->get(intval($values->captain));

        if ($this->registrationTeam->isLoaded()) {
            $registration = $this->facade->update(
                $this->registrationTeam->getId(), //@phpstan-ignore-line
                $captain,
                $values->email,
                $values->phone,
                $values->facebook,
                $tournamentPayment,
                $values->team_nickname,
                $values->list_of_players,
                $supplements
            );
            $this->onFlashmessage('form.registration-team.edit.flash.success.update', Flash::SUCCESS);
        } else {
            $team = $this->facadeTeam->get($values->team);

            $registration = $this->facade->create(
                $this->tournament, //@phpstan-ignore-line
                $team,
                $captain,
                $values->email,
                $values->phone,
                $values->facebook,
                $tournamentPayment,
                $values->team_nickname,
                $values->list_of_players,
                $supplements,
                true
            );
            $this->onFlashmessage('form.registration-team.edit.flash.success.create', Flash::SUCCESS);
        }

        if ($registration === null) {
            throw new Exception('Registration not found!');
        }

        if ($values->status !== $registration->getStatus()) {
            $this->facade->updateStatus($registration, $values->status);
        } else {
            if ($registration->getStatus() !== RegistrationTeam::STATUS_CANCALED) {
                if ($this->tournament->isUnpaid() && $registration->getPrice() === 0 && $this->tournament->isOnline()) {
                    $this->facade->updateStatus($registration, RegistrationTeam::STATUS_PAID);
                }

                //$this->sendEmailCreate($registration);

                // pokud je registrace uděláne v čase pro potvrzení, tak jí rovnou potvrdíme
                if ($registration->getStatus() === RegistrationTeam::STATUS_PAID && $this->tournament->isTimeToConfirm()) {
                    $this->facade->updateStatus($registration, RegistrationTeam::STATUS_CONFIRMED);
                    //$this->sendEmailUpdate($registration);
                }
            }
        }

        if ($form->isSubmitted()->name === 'send_back') {
            $this->processOnBack();
        }

        $this->getPresenter()->redirect('Component:default', [
            'package' => new BaseControl(),
            'render'  => 'edit-registration-team',
            'id'      => $registration->getId(),
        ]);
    }

    public function processOnBack(): void
    {
        $this->getPresenter()->redirect('Component:default', [
            'package' => new BaseControl(),
            'render'  => 'overview-registrations',
            'id'      => $this->tournament instanceof Tournament ? $this->tournament->getId() : $this->tid,
        ]);
    }

    public function render(): void
    {
        $template = $this->getComponentTemplate();
        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/editRegistrationTeam.latte');

        $this->preparePaymentMethods();
        $template->_form        = $this['form'];
        $template->payments     = $this->paymentMethods;
        $template->tournament   = $this->tournament;
        $template->imageStorage = $this->imageStorage;

        $template->render();
    }

    protected function createComponentForm(): Form
    {
        $form = new Form();

        $this->initTournament();

        // DATA
        $disableTeam       = false;
        $dataTeams         = [];
        $dataCaptain       = [];
        $dataListOfPlayers = [];
        $dataStatus        = RegistrationTeam::STATUS;

        if ($this->registrationTeam->isLoaded()) {
            $disableTeam = true;
            $team        = $this->registrationTeam->getTeam();

            if ($team === null) {
                $dataTeams         = [];
                $dataCaptain       = [];
                $dataListOfPlayers = [];
            } else {
                $dataTeams         = [$team->getId() => $team->getName()];
                $dataCaptain       = $this->prepareDataCaptain($team, $this->registrationTeam);
                $dataListOfPlayers = $this->prepareDataListOfPlayers($team, $this->registrationTeam);
            }
        } else {
            if ($this->tournament->getTournamentType() !== null) {
                $numberOfParticipantsFrom = $this->tournament->getTournamentType()->getNumberOfParticipantsFrom();
            } else {
                $numberOfParticipantsFrom = 1;
            }

            foreach ($this->facadeTeam->findWithMinimumPlayers($numberOfParticipantsFrom) as $team) {
                $dataTeams[$team->getId()] = sprintf('%s [%s]', $team->getName(), $team->getNickname());
            }

            $team              = $this->facadeTeam->get(intval($this->httpRequest->getPost('team')));
            $dataCaptain       = $this->prepareDataCaptain($team);
            $dataListOfPlayers = $this->prepareDataListOfPlayers($team);
        }

        $this->preparePaymentMethods();
        $dataPayments = [];
        foreach ($this->paymentMethods as $tournamentPayment) {
            $label = sprintf('%s - [%s,- Kč]', $tournamentPayment->getPayment()->getName(), number_format($tournamentPayment->getPrice(), 0, ' ', ' '));

            $option = Html::el('option', [
                'data-price' => $tournamentPayment->getPrice(),
            ])->setText($label);

            $dataPayments[$tournamentPayment->getPayment()->getId()] = $option;
        }

        $dataSupplements         = [];
        $availableSupplements    = $this->tournament->getSupplements()->toArray();
        $registrationSupplements = $this->registrationTeam->getSupplements()->map(static function (RegistrationTeamSupplement $rts): Supplement {
            return $rts->getSupplement();
        })->toArray();
        $availableSupplements    = array_merge($availableSupplements, $registrationSupplements);
        foreach ($availableSupplements as $supplement) {
            if (! $supplement->isActive()) {
                continue;
            }

            $text  = sprintf('%s - [%s,- Kč]', $supplement->getName(), number_format($supplement->getPrice(), 0, ' ', ' '));
            $label = Html::el('span', [
                'data-price' => $supplement->getPrice(),
            ])->setText($text);

            if ($supplement->getContent() !== '') {
                $info = Html::el('sup', [
                    'class'          => 'far fa-fw fa-sm fa-question-circle text-orange ml-1',
                    'title'          => $supplement->getContent(),
                    'data-toggle'    => 'tooltip',
                    'data-placement' => 'right',
                    'data-html'      => 'true',
                ]);

                $label->addHtml($info);
            }

            $dataSupplements[$supplement->getId()] = $label;
        }

        $form->setTranslator($this->translator);

        // INPUT
        $inputTeam = $form->addSelect('team', 'form.registration-team.edit.team', $dataTeams)
            ->setPrompt(Constant::PROMTP)
            ->setTranslator(null)
            ->setHtmlAttribute('data-chosen-placeholder-text', 'form.registration-team.edit.team.chosen-placeholder-text')
            ->setHtmlAttribute('data-chosen-no-result-text', 'form.registration-team.edit.team.chosen-no-result-text');

        if ($this->registrationTeam->isLoaded()) {
            $inputTeam->setDisabled($disableTeam);
        } else {
            $inputTeam->setRequired('form.registration-team.edit.team.req');
        }

        $form->addSelect('captain', 'form.registration-team.edit.captain', $dataCaptain)
            ->setPrompt(Constant::PROMTP)
            ->setTranslator(null)
            ->setRequired('form.registration-team.edit.captain.req');

        $form->addText('team_nickname', 'form.registration-team.edit.team-nickname')
            ->setRequired('form.registration-team.edit.team-nickname.req');

        $form->addCheckboxList('list_of_players', 'form.registration-team.edit.list-of-players', $dataListOfPlayers)
            ->setTranslator(null)
            ->setRequired('form.registration-team.edit.list-of-players.req');

        $form->addSelect('status', 'form.registration-team.edit.status', $dataStatus)
            ->setPrompt(Constant::PROMTP)
            ->setRequired('form.registration-team.edit.status.req');

        // CONTACT
        $form->addEmail('email', 'form.registration-team.edit.email')
            ->setRequired('form.registration-team.edit.email.req');
        $inputPhone = $form->addText('phone', 'form.registration-team.edit.phone');
        if (! $this->tournament->isOnline()) {
            $inputPhone->setRequired('form.registration-team.edit.phone.req');
        }

        $form->addText('facebook', 'form.registration-team.edit.facebook');

        // PAYMENT
        if (! $this->tournament->isUnpaid()) {
            $inputPayment = $form->addSelect('payment', 'form.registration-team.edit.payment', $dataPayments)
                ->setTranslator(null)
                ->setRequired('form.registration-team.edit.payment.req')
                ->setPrompt(Constant::PROMTP)
                ->addCondition(Form::FILLED)
                ->toggle('payment-info');

            foreach ($this->paymentMethods as $tournamentPayment) {
                $inputPayment->addCondition(Form::EQUAL, $tournamentPayment->getPayment()->getId())
                    ->toggle(sprintf('payment-%d', $tournamentPayment->getPayment()->getId()));
            }
        }

        // SUPPLEMENT
        $form->addCheckboxList('supplement', 'form.registration-team.edit.supplement', $dataSupplements)
            ->setTranslator(null);

        // BUTTON
        $form->addSubmit('send', 'form.registration-team.edit.send');
        $form->addSubmit('send_back', 'form.registration-team.edit.send-back');
        $form->addSubmit('back', 'form.registration-team.edit.back')
            ->setValidationScope([])
            ->onClick[] = [$this, 'processOnBack'];

        // DEFAULT
        $form->setDefaults($this->getDefaults($form));

        // CALLBACK
        $form->onSuccess[] = [$this, 'processOnSuccess'];

        return $form;
    }

    public function handleUpdateFormCaptain(?string $val = null): void
    {
        $team = $this->facadeTeam->get(intval($val));
        $this->initTournament();

        $countRegistrationsFromTeam = $this->tournament->getCountRegistrationsForTeam($team);
        $listName                   = $team->getName();
        if ($countRegistrationsFromTeam !== 0) {
            $listName .= sprintf(' %s', chr(65 + $countRegistrationsFromTeam));
        }

        $this['form']['captain']->setItems($this->prepareDataCaptain($team));
        $this['form']['team_nickname']->setValue($listName);
        $this['form']['list_of_players']->setItems($this->prepareDataListOfPlayers($team));
        $this['form']['email']->setValue('');
        $this['form']['phone']->setValue('');
        $this['form']['facebook']->setValue('');

        $this->redrawControl('snipForm');
        $this->redrawControl('snipFormCaptain');
        $this->redrawControl('snipFormListName');
        $this->redrawControl('snipFormListOfPlayers');
        $this->redrawControl('snipFormContact');
    }

    public function handleUpdateFormContact(?string $val = null): void
    {
        $user = $this->facadeCustomer->get(intval($val));
        $this->initTournament();

        $this['form']['email']->setValue($user->getEmail());
        $this['form']['phone']->setValue($user->getPhone());
        $this['form']['facebook']->setValue($user->getFacebook(true));

        $this->redrawControl('snipForm');
        $this->redrawControl('snipFormContact');
    }

    /**
     * @return array<string, array<Html>>
     */
    private function prepareDataCaptain(Team $team, ?RegistrationTeam $registration = null): array
    {
        $dataValid   = [];
        $dataInvalid = [];

        $registrations = $this->tournament->getValidRegistrations();
        foreach ($team->getPlayers(true) as $_player) {
            $player = $_player->getUser();
            $option = Html::el('option', [
                'value' => $player->getId(),
            ]);

            $alreadyInTeam = false;
            foreach ($registrations as $_registration) {
                if (! $_registration->isPlayerIn($player) || $_registration === $registration) { //@phpstan-ignore-line
                    continue;
                }

                $alreadyInTeam = true;
            }

            $option->setText(sprintf('%s [%s]', $player->getFullName(), $this->translator->translate($_player->getRoleName())));

            $playerGameId = $_player->getUser()->getGameId($this->tournament->getGame()->getId());
            if (($this->tournament->isRequiredGameId() && is_string($playerGameId) && trim($playerGameId) === '') || $alreadyInTeam) {
                $option->addAttributes(['disabled' => 'disabled']);
                $dataInvalid[$player->getId()] = $option;
            } else {
                $dataValid[$player->getId()] = $option;
            }
        }

        $data = [];
        if (count($dataValid) > 0) {
            $data[$this->translator->translate('form.registration-team.edit.captain.valid')] = $dataValid;
        }

        if (count($dataInvalid) > 0) {
            $data[$this->translator->translate('form.registration-team.edit.captain.invalid')] = $dataInvalid;
        }

        return $data;
    }

    /**
     * @return array<Html>
     */
    private function prepareDataListOfPlayers(Team $team, ?RegistrationTeam $registration = null): array
    {
        $dataValid   = [];
        $dataInvalid = [];

        $registrations = $this->tournament->getValidRegistrations();
        foreach ($team->getPlayers() as $_player) {
            $player = $_player->getUser();
            $option = Html::el('label', [
                'data-role-name'     => $this->translator->translate($_player->getRoleName()),
                'data-image-preview' => $_player->getUser()->getImagePreview(),
            ]);

            $option->setText($player->getFullName());

            $playerGameId = $_player->getUser()->getGameId($this->tournament->getGame()->getId());

            $alreadyInTeamFormat = 'form.registration-team.edit.list-of-players.invalid-reason.already-in-tournament %s';
            $alreadyInTeam       = false;
            foreach ($registrations as $_registration) {
                if (! $_registration->isPlayerIn($player) || $_registration === $registration) { //@phpstan-ignore-line
                    continue;
                }

                $alreadyInTeam = $_registration->getTeamNickname(); //@phpstan-ignore-line
            }

            if ($this->tournament->isRequiredGameId() && is_string($playerGameId) && trim($playerGameId) === '') {
                $option->addAttributes([
                    'data-disabled' => 'disabled',
                    'data-reason'   => $this->translator->translate('form.registration-team.edit.list-of-players.invalid-reason.missing-game-id'),
                ]);
                $dataInvalid[$player->getId()] = $option;
            } elseif ($alreadyInTeam !== false) { // zde bude pokud už je v turnaji...
                $option->addAttributes([
                    'data-disabled' => 'disabled',
                    'data-reason'   => $this->translator->translate(new SimpleTranslation($alreadyInTeamFormat, $alreadyInTeam)),
                ]);
                $dataInvalid[$player->getId()] = $option;
            } else {
                $dataValid[$player->getId()] = $option;
            }
        }

        return $dataValid + $dataInvalid;
    }

    private function preparePaymentMethods(): void
    {
        if (count($this->paymentMethods) !== 0 || $this->tournament->isUnpaid()) {
            return;
        }

        foreach ($this->tournament->getPayments() as $tournamentPayment) {
            $payment = $tournamentPayment->getPayment();
            if (! $payment->isActive()) {
                continue;
            }

            $this->paymentMethods[$payment->getId()] = $tournamentPayment;
        }
    }

    private function initTournament(): void
    {
        if ($this->tournament !== null || $this->tid === null) {
            return;
        }

        $this->tournament = $this->facadeTournament->get(intval($this->tid));
    }

    /**
     * @return mixed[]
     */
    private function getDefaults(Form $form): array
    {
        if (! $this->registrationTeam->isLoaded()) {
            return [];
        }

        $currentPlayers = $form->getComponent('list_of_players')->getItems();
        $listOfPlayers  = array_filter(
            $this->registrationTeam->getListOfPlayers()
                ->map(static fn (Customer $player): int => $player->getId()) //@phpstan-ignore-line
                ->toArray(),
            static fn ($id): bool => isset($currentPlayers[$id])
        );

        $captain = $this->registrationTeam->getCaptain()->getId();
        if (!array_key_exists($captain, $form->getComponent('captain')->getItems())) {
            $captain = null;
        }
        
        $data = [
            'team'            => $this->registrationTeam->getTeam()->getId(), //@phpstan-ignore-line
            'captain'         => $captain,
            'team_nickname'   => $this->registrationTeam->getTeamNickname(),
            'status'          => $this->registrationTeam->getStatus(),
            'list_of_players' => $listOfPlayers,
            'email'           => $this->registrationTeam->getEmail(),
            'phone'           => $this->registrationTeam->getPhone(),
            'facebook'        => $this->registrationTeam->getFacebook(),
            'supplement'      => $this->registrationTeam->getSupplements()
                ->map(static fn (RegistrationTeamSupplement $supplement): int => $supplement->getSupplement()->getId()) //@phpstan-ignore-line
                ->toArray(),
        ];

        if (! $this->tournament->isUnpaid() && $this->registrationTeam->getPayment() instanceof Payment) { //@phpstan-ignore-line
            $paymentOptions = $form->getComponent('payment')->getItems();
            if (isset($paymentOptions[$this->registrationTeam->getPayment()->getId()])) {
                $data['payment'] = $this->registrationTeam->getPayment()->getId();
            }
        }

        return $data;
    }
}
