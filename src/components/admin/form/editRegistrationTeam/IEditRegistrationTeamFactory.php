<?php

declare(strict_types=1);

namespace Skadmin\TournamentGame\Components\Admin;

interface IEditRegistrationTeamFactory
{
    public function create(?int $id = null): EditRegistrationTeam;
}
