<?php

declare(strict_types=1);

namespace Skadmin\TournamentGame\Components\Admin;

use App\Model\System\APackageControl;
use App\Model\System\Flash;
use Nette\ComponentModel\IContainer;
use Nette\Security\User as LoggedUser;
use Nette\Utils\ArrayHash;
use Skadmin\Role\Doctrine\Role\Privilege;
use Skadmin\TournamentGame\BaseControl;
use Skadmin\TournamentGame\Doctrine\Supplement\Supplement;
use Skadmin\TournamentGame\Doctrine\Supplement\SupplementFacade;
use Skadmin\Translator\SimpleTranslation;
use Skadmin\Translator\Translator;
use SkadminUtils\FormControls\UI\Form;
use SkadminUtils\FormControls\UI\FormWithUserControl;
use WebLoader\Nette\JavaScriptLoader;
use WebLoader\Nette\LoaderFactory;

use function intval;

class EditSupplement extends FormWithUserControl
{
    use APackageControl;

    private LoaderFactory    $webLoader;
    private SupplementFacade $facade;
    private Supplement       $supplement;

    public function __construct(?int $id, SupplementFacade $facade, Translator $translator, LoaderFactory $webLoader, LoggedUser $user)
    {
        parent::__construct($translator, $user);
        $this->facade    = $facade;
        $this->webLoader = $webLoader;

        $this->supplement = $this->facade->get($id);
    }

    public function setParent(?IContainer $parent, ?string $name = null): static
    {
        parent::setParent($parent, $name);

        if (! $this->isAllowed(BaseControl::RESOURCE, Privilege::WRITE)) {
            $this->getParent()->redirect(':Admin:Homepage:accessDenied');
        }

        return $this;
    }

    public function getTitle(): SimpleTranslation|string
    {
        if ($this->supplement->isLoaded()) {
            return new SimpleTranslation('supplement.edit.title - %s', $this->supplement->getName());
        }

        return 'supplement.edit.title';
    }

    /**
     * @return JavaScriptLoader[]
     */
    public function getJs(): array
    {
        return [$this->webLoader->createJavaScriptLoader('adminTinyMce')];
    }

    public function processOnSuccess(Form $form, ArrayHash $values): void
    {
        if ($this->supplement->isLoaded()) {
            $supplement = $this->facade->update(
                $this->supplement->getId(),
                $values->name,
                $values->is_active,
                $values->content,
                intval($values->price)
            );
            $this->onFlashmessage('form.supplement.edit.flash.success.update', Flash::SUCCESS);
        } else {
            $supplement = $this->facade->create(
                $values->name,
                $values->is_active,
                $values->content,
                intval($values->price)
            );
            $this->onFlashmessage('form.supplement.edit.flash.success.create', Flash::SUCCESS);
        }

        if ($form->isSubmitted()->name === 'send_back') {
            $this->processOnBack();
        }

        $this->getPresenter()->redirect('Component:default', [
            'package' => new BaseControl(),
            'render'  => 'edit-supplement',
            'id'      => $supplement->getId(),
        ]);
    }

    public function processOnBack(): void
    {
        $this->getPresenter()->redirect('Component:default', [
            'package' => new BaseControl(),
            'render'  => 'overview-supplement',
        ]);
    }

    public function render(): void
    {
        $template = $this->getComponentTemplate();
        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/editSupplement.latte');

        $template->render();
    }

    protected function createComponentForm(): Form
    {
        $form = new Form();
        $form->setTranslator($this->translator);

        // INPUT
        $form->addText('name', 'form.supplement.edit.name')
            ->setRequired('form.supplement.edit.name.req');
        $form->addTextArea('content', 'form.supplement.edit.content');

        $form->addCheckbox('is_active', 'form.tournament-game.edit.is-active')
            ->setDefaultValue(true);

        $form->addText('price', 'form.supplement.edit.price')
            ->setRequired('form.supplement.edit.price.req')
            ->setHtmlType('number');

        // BUTTON
        $form->addSubmit('send', 'form.supplement.edit.send');
        $form->addSubmit('send_back', 'form.supplement.edit.send-back');
        $form->addSubmit('back', 'form.supplement.edit.back')
            ->setValidationScope([])
            ->onClick[] = [$this, 'processOnBack'];

        // DEFAULT
        $form->setDefaults($this->getDefaults());

        // CALLBACK
        $form->onSuccess[] = [$this, 'processOnSuccess'];

        return $form;
    }

    /**
     * @return mixed[]
     */
    private function getDefaults(): array
    {
        if (! $this->supplement->isLoaded()) {
            return [];
        }

        return [
            'name'      => $this->supplement->getName(),
            'content'   => $this->supplement->getContent(),
            'price'     => $this->supplement->getPrice(),
            'is_active' => $this->supplement->isActive(),
        ];
    }
}
