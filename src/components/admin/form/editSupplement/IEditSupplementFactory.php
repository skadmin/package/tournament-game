<?php

declare(strict_types=1);

namespace Skadmin\TournamentGame\Components\Admin;

interface IEditSupplementFactory
{
    public function create(?int $id = null): EditSupplement;
}
