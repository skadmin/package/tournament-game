<?php

declare(strict_types=1);

namespace Skadmin\TournamentGame\Components\Admin;

use Nette\ComponentModel\IContainer;
use Nette\Utils\ArrayHash;
use Skadmin\Role\Doctrine\Role\Privilege;
use Skadmin\TournamentGame\BaseControl;
use Skadmin\TournamentGame\Components\Front\EditTeam as FrontEditTeam;
use SkadminUtils\FormControls\UI\Form;

class EditTeam extends FrontEditTeam
{
    public function setParent(?IContainer $parent, ?string $name = null): static
    {
        parent::setParent($parent, $name);

        if (! $this->isAllowed(BaseControl::RESOURCE_TEAM, Privilege::WRITE)) {
            $this->getParent()->redirect(':Admin:Homepage:accessDenied');
        }

        return $this;
    }

    public function processOnSuccess(Form $form, ArrayHash $values): void
    {
        parent::processOnSuccess($form, $values);

        $this->getPresenter()->redirect('Component:default', [
            'package' => new BaseControl(),
            'render'  => 'edit-team',
            'id'      => $this->team->getId(),
        ]);
    }
}
