<?php

declare(strict_types=1);

namespace Skadmin\TournamentGame\Components\Admin;

use App\Model\System\APackageControl;
use App\Model\System\Flash;
use Nette\ComponentModel\IContainer;
use Nette\Security\User as LoggedUser;
use Nette\Utils\ArrayHash;
use Skadmin\Role\Doctrine\Role\Privilege;
use Skadmin\TournamentGame\BaseControl;
use Skadmin\TournamentGame\Doctrine\TournamentType\TournamentType;
use Skadmin\TournamentGame\Doctrine\TournamentType\TournamentTypeFacade;
use Skadmin\Translator\SimpleTranslation;
use Skadmin\Translator\Translator;
use SkadminUtils\FormControls\UI\Form;
use SkadminUtils\FormControls\UI\FormWithUserControl;
use WebLoader\Nette\JavaScriptLoader;
use WebLoader\Nette\LoaderFactory;

use function intval;

class EditType extends FormWithUserControl
{
    use APackageControl;

    private LoaderFactory        $webLoader;
    private TournamentTypeFacade $facade;
    private TournamentType       $tournamentType;

    public function __construct(?int $id, TournamentTypeFacade $facade, Translator $translator, LoaderFactory $webLoader, LoggedUser $user)
    {
        parent::__construct($translator, $user);
        $this->facade    = $facade;
        $this->webLoader = $webLoader;

        $this->tournamentType = $this->facade->get($id);
    }

    public function setParent(?IContainer $parent, ?string $name = null): static
    {
        parent::setParent($parent, $name);

        if (! $this->isAllowed(BaseControl::RESOURCE, Privilege::WRITE)) {
            $this->getParent()->redirect(':Admin:Homepage:accessDenied');
        }

        return $this;
    }

    public function getTitle(): SimpleTranslation|string
    {
        if ($this->tournamentType->isLoaded()) {
            return new SimpleTranslation('tournament-type.edit.title - %s', $this->tournamentType->getName());
        }

        return 'tournament-type.edit.title';
    }

    /**
     * @return JavaScriptLoader[]
     */
    public function getJs(): array
    {
        return [$this->webLoader->createJavaScriptLoader('adminTinyMce')];
    }

    public function processOnSuccess(Form $form, ArrayHash $values): void
    {
        if ($this->tournamentType->isLoaded()) {
            $tournamentType = $this->facade->update(
                $this->tournamentType->getId(),
                $values->name,
                $values->content,
                intval($values->number_of_participants_from),
                intval($values->number_of_participants_to)
            );
            $this->onFlashmessage('form.tournament-type.edit.flash.success.update', Flash::SUCCESS);
        } else {
            $tournamentType = $this->facade->create(
                $values->name,
                $values->content,
                intval($values->number_of_participants_from),
                intval($values->number_of_participants_to)
            );
            $this->onFlashmessage('form.tournament-type.edit.flash.success.create', Flash::SUCCESS);
        }

        if ($form->isSubmitted()->name === 'send_back') {
            $this->processOnBack();
        }

        $this->getPresenter()->redirect('Component:default', [
            'package' => new BaseControl(),
            'render'  => 'edit-type',
            'id'      => $tournamentType->getId(),
        ]);
    }

    public function processOnBack(): void
    {
        $this->getPresenter()->redirect('Component:default', [
            'package' => new BaseControl(),
            'render'  => 'overview-type',
        ]);
    }

    public function render(): void
    {
        $template = $this->getComponentTemplate();
        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/editType.latte');

        $template->render();
    }

    protected function createComponentForm(): Form
    {
        $form = new Form();
        $form->setTranslator($this->translator);

        // INPUT
        $form->addText('name', 'form.tournament-type.edit.name')
            ->setRequired('form.tournament-type.edit.name.req');
        $form->addTextArea('content', 'form.tournament-type.edit.content');

        $form->addText('number_of_participants_from', 'form.tournament-type.edit.number-of-participants-from')
            ->setRequired('form.tournament-type.edit.number-of-participants-from.req')
            ->setHtmlType('number');
        $form->addText('number_of_participants_to', 'form.tournament-type.edit.number-of-participants-to')
            ->setRequired('form.tournament-type.edit.number-of-participants-to.req')
            ->setHtmlType('number');

        // BUTTON
        $form->addSubmit('send', 'form.tournament-type.edit.send');
        $form->addSubmit('send_back', 'form.tournament-type.edit.send-back');
        $form->addSubmit('back', 'form.tournament-type.edit.back')
            ->setValidationScope([])
            ->onClick[] = [$this, 'processOnBack'];

        // DEFAULT
        $form->setDefaults($this->getDefaults());

        // CALLBACK
        $form->onSuccess[] = [$this, 'processOnSuccess'];

        return $form;
    }

    /**
     * @return mixed[]
     */
    private function getDefaults(): array
    {
        if (! $this->tournamentType->isLoaded()) {
            return [];
        }

        return [
            'name'                        => $this->tournamentType->getName(),
            'content'                     => $this->tournamentType->getContent(),
            'number_of_participants_from' => $this->tournamentType->getNumberOfParticipantsFrom(),
            'number_of_participants_to'   => $this->tournamentType->getNumberOfParticipantsTo(),
        ];
    }
}
