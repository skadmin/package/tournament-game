<?php

declare(strict_types=1);

namespace Skadmin\TournamentGame\Components\Admin;

interface IEditTypeFactory
{
    public function create(?int $id = null): EditType;
}
