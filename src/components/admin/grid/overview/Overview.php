<?php

declare(strict_types=1);

namespace Skadmin\TournamentGame\Components\Admin;

use App\Model\Doctrine\User\User;
use App\Model\Grid\Traits\IsActive;
use App\Model\System\APackageControl;
use App\Model\System\Constant;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\QueryBuilder;
use Nette\ComponentModel\IContainer;
use Nette\Security\User as LoggedUser;
use Nette\Utils\DateTime;
use Nette\Utils\Html;
use Skadmin\Game\Doctrine\Game\GameFacade;
use Skadmin\Role\Doctrine\Role\Privilege;
use Skadmin\TournamentGame\BaseControl;
use Skadmin\TournamentGame\Doctrine\Tournament\Tournament;
use Skadmin\TournamentGame\Doctrine\Tournament\TournamentFacade;
use Skadmin\Translator\Translator;
use SkadminUtils\GridControls\UI\GridControl;
use SkadminUtils\GridControls\UI\GridDoctrine;

use function count;
use function sprintf;

class Overview extends GridControl
{
    use APackageControl;
    use IsActive;

    private TournamentFacade $facade;
    private GameFacade       $facadeGame;
    private User             $user;

    public function __construct(TournamentFacade $facade, GameFacade $facadeGame, Translator $translator, LoggedUser $user)
    {
        parent::__construct($translator, $user);
        $this->user = $this->loggedUser->getIdentity(); //@phpstan-ignore-line

        $this->facade     = $facade;
        $this->facadeGame = $facadeGame;
    }

    public function setParent(?IContainer $parent, ?string $name = null): static
    {
        parent::setParent($parent, $name);

        if (! $this->isAllowed(BaseControl::RESOURCE, Privilege::READ)) {
            $this->getParent()->redirect(':Admin:Homepage:accessDenied');
        }

        return $this;
    }

    public function render(): void
    {
        $template = $this->getComponentTemplate();
        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/overview.latte');
        $template->render();
    }

    public function getTitle(): string
    {
        return 'tournament-game.overview.title';
    }

    protected function createComponentGrid(string $name): GridDoctrine
    {
        $grid = new GridDoctrine($this->getPresenter());

        // DATA
        $translator = $this->translator;
        $_dataGame  = $this->user->getAllowedGames();
        if (count($_dataGame) === 0) {
            $dataGame = $this->facadeGame->getPairs('id', 'name');
            $games    = null;
        } else {
            $dataGame = [];
            $games    = [];
            foreach ($_dataGame as $game) {
                $dataGame[$game->getId()] = $game->getName();
                $games[]                  = $game;
            }
        }

        // DEFAULT
        $grid->setPrimaryKey('id');
        $grid->setDataSource($this->facade->getModelByGameIds($games)
            ->orderBy('a.termFrom', 'DESC'));

        // COLUMNS
        $grid->addColumnText('name', 'grid.tournament-game.overview.name')
            ->setRenderer(function (Tournament $tournament): Html {
                if ((! $tournament->isLocked() || $this->isAllowed(BaseControl::RESOURCE, BaseControl::PRIVILEGE_LOCK)) && $this->isAllowed(BaseControl::RESOURCE, 'write')) {
                    $link = $this->getPresenter()->link('Component:default', [
                        'package' => new BaseControl(),
                        'render'  => 'edit',
                        'id'      => $tournament->getId(),
                    ]);

                    $name = Html::el('a', [
                        'class' => 'text-primary font-weight-bold text-nowrap',
                        'href'  => $link,
                    ]);
                } else {
                    $name = Html::el('span', ['class' => 'text-primary font-weight-bold text-nowrap']);
                }

                if ($tournament->isLocked()) {
                    $name->addHtml(Html::el('i', ['class' => 'fas fa-fw fa-lock mr-1']));
                }

                $name->addText($tournament->getName());

                $webalize = Html::el('code', ['class' => 'text-muted d-block small'])->setText($tournament->getWebalize());
                $code     = Html::el('code', ['class' => 'text-muted d-block small'])->setText($tournament->getCode());

                $render = new Html();

                $render->addHtml($name)
                    ->addHtml($webalize)
                    ->addHtml($code);

                return $render;
            });
        $grid->addColumnText('game', 'grid.tournament-game.overview.game')
            ->setRenderer(static function (Tournament $tournament): Html {
                $name = Html::el('span', ['class' => 'text-primary'])
                    ->setText($tournament->getGame()->getName());

                $type = Html::el('code', ['class' => 'text-muted small']);
                if ($tournament->getTournamentType() !== null) {
                    $type->setText($tournament->getTournamentType()->getName());
                }

                $render = new Html();
                $render->addHtml($name)
                    ->addHtml('<br/>')
                    ->addHtml($type);

                return $render;
            });
        $grid->addColumnText('count', 'grid.tournament-game.overview.count')
            ->setAlign('center')
            ->setRenderer(static function (Tournament $tournament) use ($translator): Html {
                $text = sprintf(
                    '<span class="font-weight-bold text-primary">%d</span> / %d / %d',
                    $tournament->isOnline() ? $tournament->getCountConfirmedRegistrations() : $tournament->getCountPaidRegistrations(),
                    $tournament->getCountValidRegistrations(),
                    $tournament->getNumberOfParticipants()
                );

                return Html::el('span', [
                    'title'       => $translator->translate('grid.tournament-game.overview.count.title'),
                    'data-toggle' => 'tooltip',
                ])->addHtml($text);
            });
        $grid->addColumnDateTime('termFrom', 'grid.tournament-game.overview.term-from')
            ->setFormat('d.m.Y H:i');
        $grid->addColumnText('statuses', 'grid.tournament-game.overview.statuses')
            ->setRenderer(function (Tournament $tournament): Html {
                $statuses = new Html();

                $inActiveText = sprintf('grid.tournament-game.overview.is-active.%s', $tournament->isActive() ? 'yes' : 'no');
                $statuses->addHtml(Html::el('span', [
                    'class' => sprintf('d-block mt-1 badge badge-%s', $tournament->isActive() ? 'success' : 'danger'),
                ])->setText($this->translator->translate($inActiveText)));

                $statuses->addHtml(Html::el('span', [
                    'class' => sprintf('d-block mt-1 badge badge-%s', $tournament->isOnline() ? 'success' : 'danger'),
                ])->setText($tournament->isOnline() ? 'online' : 'offline'));

                $inFutureText = sprintf('grid.tournament-game.overview.is-in-future.%s', $tournament->isInFuture() ? 'yes' : 'no');
                $statuses->addHtml(Html::el('span', [
                    'class' => sprintf('d-block mt-1 badge badge-%s', $tournament->isInFuture() ? 'success' : 'danger'),
                ])->setText($this->translator->translate($inFutureText)));

                return $statuses;
            });

        // FILTER
        $grid->setOuterFilterRendering();
        $grid->setOuterFilterColumnsCount(3);
        $grid->addFilterText('name', 'grid.tournament-game.overview.name', ['name', 'code']);
        $grid->addFilterSelect('game', 'grid.tournament-game.overview.game', $dataGame)
            ->setTranslateOptions(false)
            ->setPrompt(Constant::PROMTP);
        $this->addFilterIsActive($grid, 'tournament-game.overview');
        $grid->addFilterSelect('isOnline', 'grid.tournament-game.overview.is-online', $this->getReplacementIsActive())
            ->setPrompt(Constant::PROMTP);
        $grid->addFilterSelect('isInFuture', 'grid.tournament-game.overview.is-in-future', $this->getReplacementIsActive())
            ->setPrompt(Constant::PROMTP)
            ->setCondition(static function (QueryBuilder $qb, $value): void {
                if ($value === Constant::NO) {
                    $criteria = new Criteria();
                    $criteria->andWhere(Criteria::expr()->lt('termTo', new DateTime()));
                    $qb->addCriteria($criteria);
                } elseif ($value === Constant::YES) {
                    $criteria = new Criteria();
                    $criteria->andWhere(Criteria::expr()->gte('termTo', new DateTime()));
                    $qb->addCriteria($criteria);
                }
            });
        $grid->addFilterSelect('isLocked', 'grid.tournament-game.overview.is-locked', $this->getReplacementIsActive())
            ->setPrompt(Constant::PROMTP);

        // ACTION
        $grid->addAction('results', 'grid.tournament-game.overview.action.results', 'Component:default', ['id' => 'id'])->addParameters([
            'package' => new BaseControl(),
            'render'  => 'results',
        ])->setIcon('upload')
            ->setClass('btn btn-xs btn-default btn-outline-primary');

        if ($this->isAllowed(BaseControl::RESOURCE, BaseControl::PRIVILEGE_BRACKET)) {
            $grid->addAction('bracket', 'grid.tournament-game.overview.action.overview-bracket', 'Component:default', ['id' => 'id'])->addParameters([
                'package' => new BaseControl(),
                'render'  => 'bracket',
            ])->setIcon('sitemap')
                ->setClass('btn btn-xs btn-default btn-outline-primary');
        }

        $grid->addAction('overview-registrations', 'grid.tournament-game.overview.action.overview-registrations', 'Component:default', ['id' => 'id'])->addParameters([
            'package' => new BaseControl(),
            'render'  => 'overview-registrations',
        ])->setIcon('users')
            ->setClass('btn btn-xs btn-default btn-outline-primary');

        if ($this->isAllowed(BaseControl::RESOURCE, 'write')) {
            $grid->addAction('edit', 'grid.tournament-game.overview.action.edit', 'Component:default', ['id' => 'id'])->addParameters([
                'package' => new BaseControl(),
                'render'  => 'edit',
            ])->setIcon('pencil-alt')
                ->setClass('btn btn-xs btn-default btn-primary');
        }

        // TOOLBAR
        $grid->addToolbarButton('Component:default#1', 'grid.tournament-game.overview.action.tournament-type', [
            'package' => new BaseControl(),
            'render'  => 'overview-type',
        ])->setIcon('list')
            ->setClass('btn btn-xs btn-outline-primary');

        $grid->addToolbarButton('Component:default#2', 'grid.tournament-game.overview.action.tournament-supplement', [
            'package' => new BaseControl(),
            'render'  => 'overview-supplement',
        ])->setIcon('list')
            ->setClass('btn btn-xs btn-outline-primary');

        if ($this->isAllowed(BaseControl::RESOURCE, 'write')) {
            $grid->addToolbarButton('Component:default', 'grid.tournament-game.overview.action.new', [
                'package' => new BaseControl(),
                'render'  => 'edit',
            ])->setIcon('plus')
                ->setClass('btn btn-xs btn-primary');
        }

        $grid->addToolbarSeo(BaseControl::RESOURCE, $this->getTitle());

        // ALLOW
        $grid->allowRowsAction('results', static function (Tournament $tournament): bool {
            return ! $tournament->isInFuture() && $tournament->getResults()->count() !== 0;
        });

        $grid->allowRowsAction('edit', function (Tournament $tournament): bool {
            return ! $this->isLocked($tournament);
        });

        // OTHER
        $grid->setDefaultFilter([
            'isActive'   => Constant::YES,
            'isInFuture' => Constant::YES,
        ]);

        return $grid;
    }

    private function isLocked(Tournament $tournament): bool
    {
        return $tournament->isLocked() && ! $this->isAllowed(BaseControl::RESOURCE, BaseControl::PRIVILEGE_LOCK);
    }
}
