<?php

declare(strict_types=1);

namespace Skadmin\TournamentGame\Components\Admin;

interface IOverviewRegistrationsFactory
{
    public function create(int $id): OverviewRegistrations;
}
