<?php

declare(strict_types=1);

namespace Skadmin\TournamentGame\Components\Admin;

use App\Model\Doctrine\Customer\Customer;
use App\Model\Doctrine\Customer\CustomerFacade;
use App\Model\Doctrine\User\User;
use App\Model\Mail\MailClassBox;
use App\Model\System\APackageControl;
use App\Model\System\Constant;
use App\Model\System\Flash;
use App\Model\System\Utils;
use Doctrine\Common\Collections\Criteria;
use Nette\ComponentModel\IContainer;
use Nette\Forms\Container;
use Nette\Security\User as LoggedUser;
use Nette\Utils\ArrayHash;
use Nette\Utils\Html;
use Skadmin\Payment\Doctrine\Payment\PaymentFacade;
use Skadmin\Role\Doctrine\Role\Privilege;
use Skadmin\TournamentGame\BaseControl;
use Skadmin\TournamentGame\Doctrine\Registration\RegistrationTeam;
use Skadmin\TournamentGame\Doctrine\Registration\RegistrationTeamFacade;
use Skadmin\TournamentGame\Doctrine\Registration\RegistrationUser;
use Skadmin\TournamentGame\Doctrine\Registration\RegistrationUserFacade;
use Skadmin\TournamentGame\Doctrine\Tournament\Tournament;
use Skadmin\TournamentGame\Doctrine\Tournament\TournamentFacade;
use Skadmin\TournamentGame\Doctrine\TournamentPayment\TournamentPayment;
use Skadmin\Translator\SimpleTranslation;
use Skadmin\Translator\Translator;
use SkadminUtils\GridControls\UI\GridControl;
use SkadminUtils\GridControls\UI\GridDoctrine;

use function implode;
use function intval;
use function method_exists;
use function number_format;
use function sprintf;

class OverviewRegistrations extends GridControl
{
    use APackageControl;

    private Tournament             $tournament;
    private TournamentFacade       $facade;
    private RegistrationUserFacade $facadeRegistrationUser;
    private RegistrationTeamFacade $facadeRegistrationTeam;
    private CustomerFacade         $facadeCustomer;
    private PaymentFacade          $facadePayment;
    private MailClassBox           $mailClassBox;
    private User                   $user;

    public function __construct(int $id, TournamentFacade $facade, RegistrationUserFacade $facadeRegistrationUser, RegistrationTeamFacade $facadeRegistrationTeam, Translator $translator, LoggedUser $user, MailClassBox $mailClassBox, CustomerFacade $facadeCustomer, PaymentFacade $facadePayment)
    {
        parent::__construct($translator, $user);
        $this->user = $this->loggedUser->getIdentity(); //@phpstan-ignore-line

        $this->facade       = $facade;
        $this->mailClassBox = $mailClassBox;

        $this->tournament             = $this->facade->get($id);
        $this->facadeRegistrationUser = $facadeRegistrationUser;
        $this->facadeRegistrationTeam = $facadeRegistrationTeam;
        $this->facadeCustomer         = $facadeCustomer;
        $this->facadePayment          = $facadePayment;
    }

    public function setParent(?IContainer $parent, ?string $name = null): static
    {
        parent::setParent($parent, $name);

        if (! $this->isAllowed(BaseControl::RESOURCE, BaseControl::PRIVILEGE_LOCK) && $this->tournament->isLocked()) {
            $this->onFlashmessage('form.tournament-game.edit.flash.info.locked', Flash::INFO);
            $this->getPresenter()->redirect('Component:default', [
                'package' => new BaseControl(),
                'render'  => 'overview',
            ]);
        }

        $canView = true;
        if ($this->tournament->isLoaded()) {
            $canView = $this->user->getAllowedGames()->count() === 0;

            $criteria = Criteria::create();
            $criteria->where(Criteria::expr()->eq('id', $this->tournament->getGame()->getId()));
            $canView = $canView || $this->user->getAllowedGames()->matching($criteria)->count() > 0;
        }

        if (! $this->isAllowed(BaseControl::RESOURCE, 'read') && $canView) {
            $this->getParent()->redirect(':Admin:Homepage:accessDenied');
        }

        return $this;
    }

    public function render(): void
    {
        $template = $this->getComponentTemplate();
        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/overviewRegistrations.latte');

        $template->tournament = $this->tournament;

        $template->render();
    }

    public function getTitle(): SimpleTranslation
    {
        return new SimpleTranslation('tournament-game.overview-registrations.title - %s', $this->tournament->getName());
    }

    public function overviewRegistrationsUserOnChangeStatus(string $id, string $newStatus): void
    {
        $id        = intval($id);
        $newStatus = intval($newStatus);

        $registration = $this->facadeRegistrationUser->updateStatus($id, $newStatus);

        $presenter = $this->getPresenterIfExists();
        if ($presenter !== null) {
            $newStatusText = $this->translator->translate($registration->getStatusText());
            $message       = new SimpleTranslation('grid.tournament-game.overview-regitration.flash.change-status %s %s', [$registration->getVariableSymbol(), $newStatusText]);
            $presenter->flashMessage($message, Flash::SUCCESS);
        }

        // zkusíme odeslat e-mail
        if (method_exists($this->mailClassBox, 'overviewRegistrationUserSendEmailUpdate')) {
            $mailSend = $this->mailClassBox->overviewRegistrationUserSendEmailUpdate($registration); // @phpstan-ignore-line
            if ($presenter !== null && $mailSend) {
                $message = new SimpleTranslation('grid.tournament-game.overview-regitration.flash.registration-update.success %s', $registration->getEmail());
                $presenter->flashMessage($message, Flash::SUCCESS);
            } else {
                $message = new SimpleTranslation('grid.tournament-game.overview-regitration.flash.registration-update.danger %s', $registration->getEmail());
                $presenter->flashMessage($message, Flash::DANGER);
            }
        }

        $this['gridUser']->redrawItem($id);
    }

    public function overviewRegistrationsTeamOnChangeStatus(string $id, string $newStatus): void
    {
        $id        = intval($id);
        $newStatus = intval($newStatus);

        $registration = $this->facadeRegistrationTeam->updateStatus($id, $newStatus);

        $presenter = $this->getPresenterIfExists();
        if ($presenter !== null) {
            $newStatusText = $this->translator->translate($registration->getStatusText());
            $message       = new SimpleTranslation('grid.tournament-game.overview-regitration.flash.change-status %s %s', [$registration->getVariableSymbol(), $newStatusText]);
            $presenter->flashMessage($message, Flash::SUCCESS);
        }

        // zkusíme odeslat e-mail
        if (method_exists($this->mailClassBox, 'overviewRegistrationTeamSendEmailUpdate')) {
            $mailSend = $this->mailClassBox->overviewRegistrationTeamSendEmailUpdate($registration);
            if ($presenter !== null && $mailSend) {
                $message = new SimpleTranslation('grid.tournament-game.overview-regitration.flash.registration-update.success %s', $registration->getEmail());
                $presenter->flashMessage($message, Flash::SUCCESS);
            } else {
                $message = new SimpleTranslation('grid.tournament-game.overview-regitration.flash.registration-update.danger %s', $registration->getEmail());
                $presenter->flashMessage($message, Flash::DANGER);
            }
        }

        $this['gridTeam']->redrawItem($id);
    }

    protected function createComponentGridUser(string $name): GridDoctrine
    {
        $grid = new GridDoctrine($this->getPresenter());

        // DEFAULT
        $grid->setPrimaryKey('id');
        $grid->setDataSource($this->facadeRegistrationUser->getModelForTournament($this->tournament));

        // DATA
        $translator    = $this->translator;
        $dataStatus    = RegistrationTeam::STATUS;
        $dataStatusBtn = [
            RegistrationUser::STATUS_NEW       => 'btn-light',
            RegistrationUser::STATUS_PAID      => 'btn-primary',
            RegistrationUser::STATUS_CANCALED  => 'btn-danger',
            RegistrationUser::STATUS_CONFIRMED => 'btn-success',
        ];

        // COLUMNS
        $grid->addColumnText('name', 'grid.tournament-game.overview-regitration.name')
            ->setRenderer(static function (RegistrationUser $registration): Html {
                $name = Html::el('span', ['class' => 'text-primary font-weight-bold'])->setText($registration->getFullName());

                $variableSymbol = Html::el('code', ['class' => 'text-muted small'])->setText($registration->getVariableSymbol());

                $render = new Html();
                $render->addHtml($name)
                    ->addHtml('<br/>')
                    ->addHtml($variableSymbol);

                return $render;
            });

        if (method_exists(Customer::class, 'getAdditionalInfoForTournament')) { //@phpstan-ignore-line
            $grid->addColumnText('userInfo', 'grid.tournament-game.overview-regitration.user-info')
                ->setRenderer(static function (RegistrationUser $registration): Html {
                    return $registration->getUser()->getAdditionalInfoForTournament($registration->getTournament());
                });
        }

        $grid->addColumnText('contact', 'grid.tournament-game.overview-regitration.contact')
            ->setRenderer(static function (RegistrationUser $registration): Html {
                $contact = new Html();

                $contacts = [];
                if ($registration->getEmail() !== '') {
                    $contacts[] = Utils::createHtmlContact($registration->getEmail());
                }

                if ($registration->getPhone() !== '') {
                    $contacts[] = Utils::createHtmlContact($registration->getPhone());
                }

                if ($registration->getFacebook() !== '') {
                    $contacts[] = Utils::createFacebookContact($registration->getFacebook(), $registration->getFullName());
                }

                $contact->addHtml(implode('<br>', $contacts));

                return $contact;
            });
        $grid->addColumnText('payment', 'grid.tournament-game.overview-regitration.payment')
            ->setAlign('right')
            ->setRenderer(static function (RegistrationUser $registration): Html {
                $price = Html::el('span', ['class' => 'font-weight-bold']);

                if ($registration->getSupplements()->count() !== 0) {
                    $supplements = new Html();

                    $supplementsRow = Html::el('div', ['class' => 'row'])
                        ->addHtml(Html::el('div', ['class' => 'col-7'])
                            ->setText($registration->getPayment()->getName()))
                        ->addHtml(Html::el('div', ['class' => 'col-5 text-right'])
                            ->setText(sprintf('%s,- Kč', number_format($registration->getPricePayment(), 0, ',', ' '))));
                    $supplements->addHtml($supplementsRow);

                    foreach ($registration->getSupplements() as $supplement) {
                        $supplementsRow = Html::el('div', ['class' => 'row'])
                            ->addHtml(Html::el('div', ['class' => 'col-7'])
                                ->setText($supplement->getSupplement()->getName()))
                            ->addHtml(Html::el('div', ['class' => 'col-5 text-right'])
                                ->setText(sprintf('%s,- Kč', number_format($supplement->getPrice(), 0, ',', ' '))));
                        $supplements->addHtml($supplementsRow);
                    }

                    $supplementsIcon = Html::el('i', [
                        'class'          => 'far fa-sm fa-question-circle text-primary mr-2',
                        'data-toggle'    => 'popover',
                        'data-content'   => $supplements,
                        'data-placement' => 'left',
                        'data-html'      => 'true',
                        'data-trigger'   => 'click hover',
                    ]);
                    $price->setHtml($supplementsIcon);
                }

                $price->addText(sprintf('%s,- Kč', number_format($registration->getPrice(), 0, ',', ' ')));

                $render = new Html();
                $render->addHtml($registration->getPayment()->getName())
                    ->addHtml('<br/>')
                    ->addHtml($price);

                return $render;
            });
        $griColumnStatus = $grid->addColumnStatus('status', 'grid.tournament-game.overview-regitration.status')
            ->setAlign('center')
            ->setCaret(false);
        foreach ($dataStatus as $statusId => $status) {
            $griColumnStatus
                ->addOption($statusId, $status)
                ->setClass($dataStatusBtn[$statusId])
                ->endOption();
        }

        $griColumnStatus->onChange[] = [$this, 'overviewRegistrationsUserOnChangeStatus'];

        if ($this->tournament->isOnline()) {
            $grid->addColumnDateTime('confirmedAt', 'grid.tournament-game.overview-regitration.confirmed-at')
                ->setFormat('d.m.Y H:i')
                ->setSortable();
        }

        $grid->addColumnDateTime('paidAt', 'grid.tournament-game.overview-regitration.paid-at')
            ->setFormat('d.m.Y H:i')
            ->setSortable();
        $grid->addColumnDateTime('createdAt', 'grid.tournament-game.overview-regitration.created-at')
            ->setFormat('d.m.Y H:i')
            ->setSortable();

        // FILTER
        $grid->addFilterText('name', 'grid.tournament-game.overview-regitration.name', ['name', 'surname', 'nickname', 'variableSymbol']);
        $grid->addFilterText('contact', 'grid.tournament-game.overview-regitration.contact', ['email', 'phone', 'facebook']);
        $grid->addFilterSelect('status', 'grid.tournament-game.overview-regitration.status', $dataStatus)
            ->setTranslateOptions()
            ->setPrompt(Constant::PROMTP);

        // ACTION
        if ($this->isAllowed(BaseControl::RESOURCE, Privilege::WRITE)) {
            // INLINE ADD
            $grid->addInlineAdd()
                ->setPositionTop()
                ->setText($this->translator->translate('grid.tournament-game.overview-regitration.action.inline-add'))
                ->onControlAdd[] = [$this, 'onInlineAddUser'];
            $grid->getInlineAddPure()
                ->onSubmit[]     = [$this, 'onInlineAddUserSubmit'];
        }

        // TOOLBAR
        $grid->addToolbarButton('Component:default#1', 'grid.tournament-game.overview-regitration.action.tournament', [
            'package' => new BaseControl(),
            'render'  => 'overview',
        ])->setIcon('trophy')
            ->setClass('btn btn-xs btn-outline-primary');

        // DETAIL
        $grid->setItemsDetail(__DIR__ . '/detail.latte');

        // OTHER
        $grid->setDefaultSort([$this->tournament->isOnline() ? 'confirmedAt' : 'paidAt' => 'DESC']);

        return $grid;
    }

    public function onInlineAddUser(Container $container): void
    {
        $users = [];
        foreach ($this->facadeCustomer->find(true) as $user) {
            $users[$user->getId()] = $user->getFullName();
        }

        foreach ($this->tournament->getValidRegistrations() as $registration) {
            unset($users[$registration->getUser()->getId()]); //@phpstan-ignore-line
        }

        $container->addSelect('name', 'grid.tournament-game.overview-regitration.name', $users)
            ->setTranslator(null)
            ->setRequired('grid.tournament-game.overview-regitration.name.req')
            ->setHtmlAttribute('class', 'chosen-select')
            ->setHtmlAttribute('data-chosen-placeholder-text', $this->translator->translate('grid.tournament-game.overview-regitration.name.placeholder'))
            ->setHtmlAttribute('data-chosen-no-result-text', $this->translator->translate('grid.tournament-game.overview-regitration.name.no-result'));
    }

    /**
     * @param ArrayHash<mixed> $values
     */
    public function onInlineAddUserSubmit(ArrayHash $values): void
    {
        $tournamentPayment = $this->tournament->getPayments()->first();
        if ($tournamentPayment === false) {
            $tournamentPayment = new TournamentPayment();
            $tournamentPayment->create($this->tournament, $this->facadePayment->getForUnpaid(), 0);
        }

        $user = $this->facadeCustomer->get($values->name);
        $this->facadeRegistrationUser->create(
            $this->tournament,
            $user,
            $user->getEmail(),
            $user->getPhone(),
            $user->getFacebook(),
            $tournamentPayment
        );

        $presenter = $this->getPresenterIfExists();
        if ($presenter !== null) {
            $message = new SimpleTranslation('grid.tournament-game.overview-regitration.action.flash.inline-add-user.success "%s"', [$user->getFullName()]);
            $presenter->flashMessage($message, Flash::SUCCESS);
        }

        $this['gridUser']->reload();
    }

    protected function createComponentGridTeam(string $name): GridDoctrine
    {
        $grid = new GridDoctrine($this->getPresenter());

        // DEFAULT
        $grid->setPrimaryKey('id');
        $grid->setDataSource($this->facadeRegistrationTeam->getModelForTournament($this->tournament));

        // DATA
        $translator    = $this->translator;
        $dataStatus    = RegistrationTeam::STATUS;
        $dataStatusBtn = [
            RegistrationTeam::STATUS_NEW       => 'btn-light',
            RegistrationTeam::STATUS_PAID      => 'btn-primary',
            RegistrationTeam::STATUS_CANCALED  => 'btn-danger',
            RegistrationTeam::STATUS_CONFIRMED => 'btn-success',
        ];

        // COLUMNS
        $grid->addColumnText('name', 'grid.tournament-game.overview-regitration.name')
            ->setRenderer(static function (RegistrationTeam $registration): Html {
                if ($registration->getTeam() !== null) {
                    $teamName = sprintf('%s [%s]', $registration->getTeam()->getName(), $registration->getTeam()->getNickname());
                } else {
                    $teamName = 'team name not found';
                }

                $name = Html::el('span', ['class' => 'text-primary font-weight-bold'])->setText($teamName);

                $render = new Html();
                $render->addHtml($name);

                if ($registration->getTeamNickname() !== '') {
                    $teamNickname = Html::el('code', ['class' => 'text-primary small'])->setText($registration->getTeamNickname());
                    $render->addHtml('<br/>')
                        ->addHtml($teamNickname);
                }

                $variableSymbol = Html::el('code', ['class' => 'text-muted small'])->setText($registration->getVariableSymbol());
                $render->addHtml('<br/>')
                    ->addHtml($variableSymbol);

                return $render;
            });

        $grid->addColumnText('contact', 'grid.tournament-game.overview-regitration.contact')
            ->setRenderer(static function (RegistrationTeam $registration): Html {
                $contact = new Html();

                $contacts = [];
                if ($registration->getEmail() !== '') {
                    $contacts[] = Utils::createHtmlContact($registration->getEmail());
                }

                if ($registration->getPhone() !== '') {
                    $contacts[] = Utils::createHtmlContact($registration->getPhone());
                }

                if ($registration->getFacebook() !== '') {
                    $contacts[] = Utils::createFacebookContact($registration->getFacebook(), $registration->getFullName());
                }

                $contact->addHtml(implode('<br>', $contacts));

                return $contact;
            });
        $grid->addColumnText('payment', 'grid.tournament-game.overview-regitration.payment')
            ->setAlign('right')
            ->setRenderer(static function (RegistrationTeam $registration): Html {
                $price = Html::el('span', ['class' => 'font-weight-bold']);

                if ($registration->getSupplements()->count() !== 0) {
                    $supplements = new Html();

                    $supplementsRow = Html::el('div', ['class' => 'row'])
                        ->addHtml(Html::el('div', ['class' => 'col-7'])
                            ->setText($registration->getPayment()->getName()))
                        ->addHtml(Html::el('div', ['class' => 'col-5 text-right'])
                            ->setText(sprintf('%s,- Kč', number_format($registration->getPricePayment(), 0, ',', ' '))));
                    $supplements->addHtml($supplementsRow);

                    foreach ($registration->getSupplements() as $supplement) {
                        $supplementsRow = Html::el('div', ['class' => 'row'])
                            ->addHtml(Html::el('div', ['class' => 'col-7'])
                                ->setText($supplement->getSupplement()->getName()))
                            ->addHtml(Html::el('div', ['class' => 'col-5 text-right'])
                                ->setText(sprintf('%s,- Kč', number_format($supplement->getPrice(), 0, ',', ' '))));
                        $supplements->addHtml($supplementsRow);
                    }

                    $supplementsIcon = Html::el('i', [
                        'class'          => 'far fa-sm fa-question-circle text-primary mr-2',
                        'data-toggle'    => 'popover',
                        'data-content'   => $supplements,
                        'data-placement' => 'left',
                        'data-html'      => 'true',
                        'data-trigger'   => 'click hover',
                    ]);
                    $price->setHtml($supplementsIcon);
                }

                $price->addText(sprintf('%s,- Kč', number_format($registration->getPrice(), 0, ',', ' ')));

                $render = new Html();
                $render->addHtml($registration->getPayment()->getName())
                    ->addHtml('<br/>')
                    ->addHtml($price);

                return $render;
            });
        $griColumnStatus = $grid->addColumnStatus('status', 'grid.tournament-game.overview-regitration.status')
            ->setAlign('center')
            ->setCaret(false);
        foreach ($dataStatus as $statusId => $status) {
            $griColumnStatus
                ->addOption($statusId, $status)
                ->setClass($dataStatusBtn[$statusId])
                ->endOption();
        }

        $griColumnStatus->onChange[] = [$this, 'overviewRegistrationsTeamOnChangeStatus'];

        if ($this->tournament->isOnline()) {
            $grid->addColumnDateTime('confirmedAt', 'grid.tournament-game.overview-regitration.confirmed-at')
                ->setFormat('d.m.Y H:i')
                ->setSortable();
        }

        $grid->addColumnDateTime('paidAt', 'grid.tournament-game.overview-regitration.paid-at')
            ->setFormat('d.m.Y H:i')
            ->setSortable();
        $grid->addColumnDateTime('createdAt', 'grid.tournament-game.overview-regitration.created-at')
            ->setFormat('d.m.Y H:i')
            ->setSortable();

        // FILTER
        $grid->addFilterText('name', 'grid.tournament-game.overview-type.name', ['team.name', 'team.code', 'team.nickname', 'teamNickname', 'variableSymbol']);
        $grid->addFilterText('contact', 'grid.tournament-game.overview-regitration.contact', ['email', 'phone', 'facebook']);
        $grid->addFilterSelect('status', 'grid.tournament-game.overview-regitration.status', $dataStatus)
            ->setTranslateOptions()
            ->setPrompt(Constant::PROMTP);

        // TOOLBAR
        $grid->addToolbarButton('Component:default#1', 'grid.tournament-game.overview-regitration.action.tournament', [
            'package' => new BaseControl(),
            'render'  => 'overview',
        ])->setIcon('trophy')
            ->setClass('btn btn-xs btn-outline-primary');

        if ($this->isAllowed(BaseControl::RESOURCE, Privilege::WRITE)) {
            $grid->addToolbarButton('Component:default', 'grid.tournament-game.overview-regitration.action.create', [
                'package' => new BaseControl(),
                'render'  => 'edit-registration-team',
                'tid'     => $this->tournament->getId(),
            ])->setIcon('pencil-alt')
                ->setClass('btn btn-xs btn-primary');

            $grid->addAction('edit', 'grid.tournament-game.overview-regitration.action.edit', 'Component:default', ['id' => 'id'])
                ->addParameters([
                    'package' => new BaseControl(),
                    'render'  => 'edit-registration-team',
                ])->setIcon('pencil-alt')
                ->setClass('btn btn-xs  btn-primary');
        }

        // DETAIL
        $grid->setItemsDetail(__DIR__ . '/detail.latte');

        // OTHER
        $grid->setDefaultSort([$this->tournament->isOnline() ? 'confirmedAt' : 'paidAt' => 'DESC']);

        return $grid;
    }
}
