<?php

declare(strict_types=1);

namespace Skadmin\TournamentGame\Components\Admin;

interface IOverviewSupplementFactory
{
    public function create(): OverviewSupplement;
}
