<?php

declare(strict_types=1);

namespace Skadmin\TournamentGame\Components\Admin;

use App\Model\Grid\Traits\IsActive;
use App\Model\System\APackageControl;
use Nette\ComponentModel\IContainer;
use Nette\Security\User;
use Skadmin\Role\Doctrine\Role\Privilege;
use Skadmin\TournamentGame\BaseControl;
use Skadmin\TournamentGame\Doctrine\Supplement\SupplementFacade;
use Skadmin\Translator\Translator;
use SkadminUtils\GridControls\UI\GridControl;
use SkadminUtils\GridControls\UI\GridDoctrine;

class OverviewSupplement extends GridControl
{
    use APackageControl;
    use IsActive;

    private SupplementFacade $facade;

    public function __construct(SupplementFacade $facade, Translator $translator, User $user)
    {
        parent::__construct($translator, $user);

        $this->facade = $facade;
    }

    public function setParent(?IContainer $parent, ?string $name = null): static
    {
        parent::setParent($parent, $name);

        if (! $this->isAllowed(BaseControl::RESOURCE, Privilege::READ)) {
            $this->getParent()->redirect(':Admin:Homepage:accessDenied');
        }

        return $this;
    }

    public function render(): void
    {
        $template = $this->getComponentTemplate();
        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/overviewSupplement.latte');
        $template->render();
    }

    public function getTitle(): string
    {
        return 'tournament-game.supplement.title';
    }

    protected function createComponentGrid(string $name): GridDoctrine
    {
        $grid = new GridDoctrine($this->getPresenter());

        // DEFAULT
        $grid->setPrimaryKey('id');
        $grid->setDataSource($this->facade->getModel());

        // DATA

        // COLUMNS
        $grid->addColumnText('name', 'grid.tournament-game.supplement.name');
        $this->addColumnIsActive($grid, 'tournament-game.supplement');
        $grid->addColumnText('price', 'grid.tournament-game.supplement.price')
            ->setAlign('right');

        // FILTER
        $grid->addFilterText('name', 'grid.tournament-game.supplement.name');
        $this->addFilterIsActive($grid, 'tournament-game.supplement');

        // ACTION
        if ($this->isAllowed(BaseControl::RESOURCE, 'write')) {
            $grid->addAction('edit', 'grid.tournament-game.supplement.action.edit', 'Component:default', ['id' => 'id'])->addParameters([
                'package' => new BaseControl(),
                'render'  => 'edit-supplement',
            ])->setIcon('pencil-alt')
                ->setClass('btn btn-xs btn-default btn-primary');
        }

        // TOOLBAR
        $grid->addToolbarButton('Component:default#1', 'grid.tournament-game.overview.action.tournament', [
            'package' => new BaseControl(),
            'render'  => 'overview',
        ])->setIcon('trophy')
            ->setClass('btn btn-xs btn-outline-primary');

        if ($this->isAllowed(BaseControl::RESOURCE, 'write')) {
            $grid->addToolbarButton('Component:default', 'grid.tournament-game.supplement.action.new', [
                'package' => new BaseControl(),
                'render'  => 'edit-supplement',
            ])->setIcon('plus')
                ->setClass('btn btn-xs btn-default btn-primary');
        }

        // OTHER
        $grid->setDefaultSort(['name' => 'ASC']);

        return $grid;
    }
}
