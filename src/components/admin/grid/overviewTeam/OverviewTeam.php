<?php

declare(strict_types=1);

namespace Skadmin\TournamentGame\Components\Admin;

use App\Model\System\APackageControl;
use App\Model\System\Utils;
use Nette\Application\Responses\FileResponse;
use Nette\ComponentModel\IContainer;
use Nette\Security\User;
use Nette\Utils\Html;
use Nette\Utils\Strings;
use Skadmin\Game\Doctrine\Game\GameFacade;
use Skadmin\Role\Doctrine\Role\Privilege;
use Skadmin\TournamentGame\BaseControl;
use Skadmin\TournamentGame\Doctrine\Team\Team;
use Skadmin\TournamentGame\Doctrine\Team\TeamFacade;
use Skadmin\Translator\Translator;
use SkadminUtils\GridControls\UI\GridControl;
use SkadminUtils\GridControls\UI\GridDoctrine;
use SkadminUtils\ImageStorage\ImageStorage;
use SplFileInfo;

use function implode;
use function intval;
use function sprintf;

class OverviewTeam extends GridControl
{
    use APackageControl;

    private TeamFacade   $facade;
    private GameFacade   $facadeGame;
    private ImageStorage $imageStorage;

    public function __construct(TeamFacade $facade, GameFacade $facadeGame, Translator $translator, ImageStorage $imageStorage, User $user)
    {
        parent::__construct($translator, $user);

        $this->facade       = $facade;
        $this->facadeGame   = $facadeGame;
        $this->imageStorage = $imageStorage;
    }

    public function setParent(?IContainer $parent, ?string $name = null): static
    {
        parent::setParent($parent, $name);

        if (! $this->isAllowed(BaseControl::RESOURCE_TEAM, Privilege::READ)) {
            $this->getParent()->redirect(':Admin:Homepage:accessDenied');
        }

        return $this;
    }

    public function render(): void
    {
        $template = $this->getComponentTemplate();
        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/overviewTeam.latte');
        $template->render();
    }

    public function getTitle(): string
    {
        return 'tournament-team.overview.title';
    }

    protected function createComponentGrid(string $name): GridDoctrine
    {
        $grid = new GridDoctrine($this->getPresenter());

        // DEFAULT
        $grid->setPrimaryKey('id');
        $grid->setDataSource($this->facade->getModelForGrid());

        // DATA
        $translator = $this->translator;

        // COLUMNS
        $grid->addColumnText('imagePreview', '')
            ->setRenderer(function (Team $team): ?Html {
                if ($team->getImagePreview() !== null) {
                    $imageSrc = $this->imageStorage->fromIdentifier([$team->getImagePreview(), '60x60', 'exact']);

                    return Html::el('img', [
                        'src'   => sprintf('/%s', $imageSrc->createLink()),
                        'style' => 'max-width: none;',
                    ]);
                }

                return null;
            })->setAlign('center');
        $grid->addColumnText('name', 'grid.tournament-team.overview.name')
            ->setRenderer(static function (Team $team): Html {
                $teamName = sprintf('%s [%s]', $team->getName(), $team->getNickname());
                $name     = Html::el('span', ['class' => 'text-primary font-weight-bold'])->setText($teamName);

                $code = Html::el('code', ['class' => 'text-muted small'])->setText($team->getCode());

                $render = new Html();
                $render->addHtml($name)
                    ->addHtml('<br/>')
                    ->addHtml($code);

                return $render;
            });
        $grid->addColumnText('contact', 'grid.tournament-team.overview.contact')
            ->setRenderer(static function (Team $team): Html {
                $contact = new Html();
                $captain = $team->getCaptain();

                if ($captain === null) {
                    return $contact;
                }

                $captain = $captain->getUser();

                $contacts = [];
                if ($captain->getEmail() !== '') {
                    $contacts[] = Utils::createHtmlContact($captain->getEmail());
                }

                if ($captain->getPhone() !== '') {
                    $contacts[] = Utils::createHtmlContact($captain->getPhone());
                }

                if ($captain->getFacebook() !== '') {
                    $contacts[] = Utils::createFacebookContact($captain->getFacebook(), $captain->getFullName());
                }

                if ($team->getFacebook() !== '') {
                    $contacts[] = Utils::createFacebookContact($team->getFacebook(), $team->getName());
                }

                $contact->addHtml(implode('<br>', $contacts));

                return $contact;
            });
        $grid->addColumnText('playersCount', 'grid.tournament-team.overview.players-count')
            ->setAlign('center')
            ->setRenderer(static function (Team $team): int {
                return $team->getPlayers()->count();
            });
        $grid->addColumnText('website', 'grid.tournament-team.overview.website')
            ->setRenderer(static function (Team $team): ?Html {
                if ($team->getWebsite() === '') {
                    return null;
                }

                $icon = Html::el('small', ['class' => 'fas fa-external-link-alt mr-1']);

                return Html::el('a', [
                    'href'   => $team->getWebsite(),
                    'target' => '_blank',
                ])->setHtml($icon)
                    ->addText($team->getWebsite());
            });

        // STYLE
        $grid->getColumn('imagePreview')
            ->getElementPrototype('th')
            ->setAttribute('style', 'width: 1px');

        // FILTER
        $grid->addFilterText('name', 'grid.tournament-team.overview.name', ['t.name', 't.nickname', 't.code']);
        $grid->addFilterText('contact', 'grid.tournament-team.overview.contact', ['u.name', 'u.nickname', 'u.surname', 'u.email', 'u.phone']);

        // ACTION
        if ($this->isAllowed(BaseControl::RESOURCE_TEAM, Privilege::WRITE)) {
            $grid->addAction('editTeam', 'grid.tournament-game.overview.action.edit-team', 'Component:default', ['id' => 'id'])->addParameters([
                'package' => new BaseControl(),
                'render'  => 'edit-team',
            ])->setIcon('pencil-alt')
                ->setClass('btn btn-xs btn-primary');
        }

        $grid->addActionCallback('downloadImagePreview', 'grid.tournament-game.overview.action.download-image-preview', function (string $teamId): void {
            $team         = $this->facade->get(intval($teamId));
            $imagePreview = $this->imageStorage->fromIdentifier($team->getImagePreview());

            $fileInfo = new SplFileInfo($imagePreview->getPath());
            $fileName = sprintf('%s.%s', Strings::webalize($team->getName()), $fileInfo->getExtension());

            $response = new FileResponse($imagePreview->getPath(), $fileName);
            $this->getPresenter()->sendResponse($response);
        })->setIcon('save')
            ->setClass('btn btn-xs btn-outline-primary');

        // allowed
        $grid->allowRowsAction('downloadImagePreview', static function (Team $team): bool {
            return $team->getImagePreview() !== null;
        });

        // DETAIL
        $grid->setItemsDetail(__DIR__ . '/detail.latte');
        $grid->getItemsDetail()->setTemplateParameters([
            'games' => $this->facadeGame->getPairs('id', 'name'),
        ]);

        // OTHER
        $grid->setDefaultSort(['name' => 'DESC']);
        //$grid->setDefaultFilter(['isActive' => 1]);

        return $grid;
    }
}
