<?php

declare(strict_types=1);

namespace Skadmin\TournamentGame\Components\Admin;

interface IOverviewTypeFactory
{
    public function create(): OverviewType;
}
