<?php

declare(strict_types=1);

namespace Skadmin\TournamentGame\Components\Admin;

use App\Model\System\APackageControl;
use Nette\ComponentModel\IContainer;
use Nette\Security\User;
use Skadmin\Role\Doctrine\Role\Privilege;
use Skadmin\TournamentGame\BaseControl;
use Skadmin\TournamentGame\Doctrine\TournamentType\TournamentTypeFacade;
use Skadmin\Translator\Translator;
use SkadminUtils\GridControls\UI\GridControl;
use SkadminUtils\GridControls\UI\GridDoctrine;

class OverviewType extends GridControl
{
    use APackageControl;

    private TournamentTypeFacade $facade;

    public function __construct(TournamentTypeFacade $facade, Translator $translator, User $user)
    {
        parent::__construct($translator, $user);

        $this->facade = $facade;
    }

    public function setParent(?IContainer $parent, ?string $name = null): static
    {
        parent::setParent($parent, $name);

        if (! $this->isAllowed(BaseControl::RESOURCE, Privilege::READ)) {
            $this->getParent()->redirect(':Admin:Homepage:accessDenied');
        }

        return $this;
    }

    public function render(): void
    {
        $template = $this->getComponentTemplate();
        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/overviewType.latte');
        $template->render();
    }

    public function getTitle(): string
    {
        return 'tournament-game.overview-type.title';
    }

    protected function createComponentGrid(string $name): GridDoctrine
    {
        $grid = new GridDoctrine($this->getPresenter());

        // DEFAULT
        $grid->setPrimaryKey('id');
        $grid->setDataSource($this->facade->getModel());

        // DATA

        // COLUMNS
        $grid->addColumnText('name', 'grid.tournament-game.overview-type.name');
        $grid->addColumnText('numberOfParticipantsFrom', 'grid.tournament-game.overview-type.number-of-participants-from')
            ->setAlign('center');
        $grid->addColumnText('numberOfParticipantsTo', 'grid.tournament-game.overview-type.number-of-participants-to')
            ->setAlign('center');

        // FILTER
        $grid->addFilterText('name', 'grid.tournament-game.overview-type.name', ['name']);
        $grid->addFilterText('numberOfParticipantsFrom', 'grid.tournament-game.overview-type.number-of-participants-from');
        $grid->addFilterText('numberOfParticipantsTo', 'grid.tournament-game.overview-type.number-of-participants-to');

        // ACTION
        if ($this->isAllowed(BaseControl::RESOURCE, 'write')) {
            $grid->addAction('edit', 'grid.tournament-game.overview-type.action.edit', 'Component:default', ['id' => 'id'])->addParameters([
                'package' => new BaseControl(),
                'render'  => 'edit-type',
            ])->setIcon('pencil-alt')
                ->setClass('btn btn-xs btn-default btn-primary');
        }

        // TOOLBAR
        $grid->addToolbarButton('Component:default#1', 'grid.tournament-game.overview.action.tournament', [
            'package' => new BaseControl(),
            'render'  => 'overview',
        ])->setIcon('trophy')
            ->setClass('btn btn-xs btn-outline-primary');

        if ($this->isAllowed(BaseControl::RESOURCE, 'write')) {
            $grid->addToolbarButton('Component:default', 'grid.tournament-game.overview-type.action.new', [
                'package' => new BaseControl(),
                'render'  => 'edit-type',
            ])->setIcon('plus')
                ->setClass('btn btn-xs btn-default btn-primary');
        }

        // OTHER
        $grid->setDefaultSort(['createdAt' => 'DESC']);

        return $grid;
    }
}
