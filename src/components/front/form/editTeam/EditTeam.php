<?php

declare(strict_types=1);

namespace Skadmin\TournamentGame\Components\Front;

use App\Model\Doctrine\Customer\Customer;
use App\Model\Doctrine\Customer\CustomerFacade;
use App\Model\System\APackageControl;
use App\Model\System\Constant;
use App\Model\System\Flash;
use Nette\Http\FileUpload;
use Nette\Security\User as LoggedUser;
use Nette\Utils\ArrayHash;
use Skadmin\Nationality\Doctrine\Nationality\NationalityFacade;
use Skadmin\TournamentGame\BaseControl;
use Skadmin\TournamentGame\Doctrine\Team\Team;
use Skadmin\TournamentGame\Doctrine\Team\TeamFacade;
use Skadmin\Translator\SimpleTranslation;
use Skadmin\Translator\Translator;
use SkadminUtils\FormControls\UI\Form;
use SkadminUtils\FormControls\UI\FormWithUserControl;
use SkadminUtils\ImageStorage\ImageStorage;
use WebLoader\Nette\CssLoader;
use WebLoader\Nette\JavaScriptLoader;
use WebLoader\Nette\LoaderFactory;

use function assert;

class EditTeam extends FormWithUserControl
{
    use APackageControl;

    private LoaderFactory     $webLoader;
    private TeamFacade        $facade;
    private NationalityFacade $facadeNationality;
    protected Team            $team;
    private Customer          $user;
    private ImageStorage      $imageStorage;

    public function __construct(?int $id, TeamFacade $facade, NationalityFacade $facadeNationality, CustomerFacade $facadeCustomer, Translator $translator, ImageStorage $imageStorage, LoaderFactory $webLoader, LoggedUser $user)
    {
        parent::__construct($translator, $user);
        $this->facade            = $facade;
        $this->facadeNationality = $facadeNationality;
        $this->imageStorage      = $imageStorage;
        $this->webLoader         = $webLoader;

        $this->team = $this->facade->get($id);
        $this->user = $facadeCustomer->get($this->loggedUser->id);
    }

    public function getTitle(): SimpleTranslation|string
    {
        if ($this->team->isLoaded()) {
            return new SimpleTranslation('tournament-team.front.edit.title - %s', $this->team->getName());
        }

        return 'tournament-team.front.edit.title';
    }

    /**
     * @return CssLoader[]
     */
    public function getCss(): array
    {
        return [
            $this->webLoader->createCssLoader('customFileInput'),
            $this->webLoader->createCssLoader('simpleMde'),
        ];
    }

    /**
     * @return JavaScriptLoader[]
     */
    public function getJs(): array
    {
        return [
            $this->webLoader->createJavaScriptLoader('choosen'),
            $this->webLoader->createJavaScriptLoader('customFileInput'),
            $this->webLoader->createJavaScriptLoader('simpleMde'),
        ];
    }

    public function processOnSuccess(Form $form, ArrayHash $values): void
    {
        $nationality = $this->facadeNationality->get($values->nationality_id);

        $identifier = null;

        $imagePreview = $values->image_preview;
        assert($imagePreview instanceof FileUpload);
        if ($imagePreview->isOk() && $imagePreview->isImage()) {
            $identifier = $this->imageStorage->saveUpload($imagePreview, BaseControl::DIR_IMAGE_TEAM)->identifier;
        }

        if ($this->team->isLoaded()) {
            if ($identifier !== null && $this->team->getImagePreview() !== null) {
                $this->imageStorage->delete($this->team->getImagePreview());
            }

            $team = $this->facade->update(
                $this->team->getId(),
                $values->name,
                $values->nickname,
                $nationality,
                $values->content,
                $values->website,
                $values->facebook,
                $values->is_accepting_invitations,
                $identifier
            );
            $this->onFlashmessage('form.tournament-team.front.edit.flash.success.update', Flash::SUCCESS);
        } else {
            $team = $this->facade->create(
                $values->name,
                $values->nickname,
                $nationality,
                $values->content,
                $values->website,
                $values->facebook,
                $values->is_accepting_invitations,
                $identifier,
                $this->user
            );
            $this->onFlashmessage('form.tournament-team.front.edit.flash.success.create', Flash::SUCCESS);
        }

        if ($form->isSubmitted()->name === 'send_back') {
            $this->processOnBack();
        }

        $this->onSuccess($form, $values, $form->isSubmitted()->name);
        $this->onSubmit($form, $team, $form->isSubmitted()->name);
    }

    public function processOnBack(): void
    {
        $this->onBack();
    }

    public function render(): void
    {
        $template = $this->getComponentTemplate();
        $template->setTranslator($this->translator);
        $template->setFile($this->getControlTemplate(__DIR__ . '/editTeam.latte'));
        $template->imageStorage = $this->imageStorage;
        $template->team         = $this->team;

        $template->render();
    }

    protected function createComponentForm(): Form
    {
        $form = new Form();
        $form->setTranslator($this->translator);

        // DATA
        $namePreferred   = $this->translator->translate('nationality.name.preferred');
        $nameUnpreferred = $this->translator->translate('nationality.name.unpreferred');
        $dataNationality = $this->facadeNationality->getPairsPreferred($namePreferred, $nameUnpreferred);

        // INPUT
        $form->addText('name', 'form.tournament-team.front.edit.name')
            ->setRequired('form.tournament-team.front.edit.name.req');
        $form->addText('nickname', 'form.tournament-team.front.edit.nickname')
            ->setRequired('form.tournament-team.front.edit.nickname.req');
        $form->addText('website', 'form.tournament-team.front.edit.website');
        $form->addUrl('facebook', 'form.tournament-team.front.edit.facebook', 'facebook.com');

        // NATIONALITY
        $form->addSelect('nationality_id', $this->translator->translate('form.tournament-team.front.edit.nationality-id'), $dataNationality)
            ->setPrompt(Constant::PROMTP)
            ->setTranslator(null)
            ->setRequired($this->translator->translate('form.tournament-team.front.edit.nationality-id.req'))
            ->setHtmlAttribute('data-chosen-placeholder-text', $this->translator->translate('form.tournament-team.front.edit.nationality-id.chosen-placeholder-text'))
            ->setHtmlAttribute('data-chosen-no-result-text', $this->translator->translate('form.tournament-team.front.edit.nationality-id.chosen-no-result-text'));

        // FILE
        $form->addUpload('image_preview', 'form.tournament-team.front.edit.image-preview')
            ->addRule(Form::IMAGE, 'form.tournament-team.front.edit.image-preview.rule-image');

        // TEXT
        $form->addTextArea('content', 'form.tournament-team.front.edit.content');

        // CHECKBOX
        $form->addCheckbox('is_accepting_invitations', 'form.tournament-team.front.edit.is-accepting-invitations')
            ->setDefaultValue(true);

        // BUTTON
        if ($this->team->isLoaded()) {
            $form->addSubmit('send', 'form.tournament-team.front.edit.send');
            $form->addSubmit('send_back', 'form.tournament-team.front.edit.send-back');
        } else {
            $form->addSubmit('send', 'form.tournament-team.front.edit.create');
            $form->addSubmit('send_back', 'form.tournament-team.front.edit.create-back');
        }

        $form->addSubmit('back', 'form.tournament-team.front.edit.back')
            ->setValidationScope([])
            ->onClick[] = [$this, 'processOnBack'];

        // DEFAULT
        $form->setDefaults($this->getDefaults());

        // CALLBACK
        $form->onSuccess[]  = [$this, 'processOnSuccess'];
        $form->onValidate[] = [$this, 'processOnValidate'];

        return $form;
    }

    public function processOnValidate(Form $form, ArrayHash $values): void
    {
        if ($form->isSubmitted()->name === 'back') {
            return;
        }

        $teamByName = $this->facade->findByName($values->name);
        if ($teamByName !== null && $teamByName->getId() !== $this->team->getId()) {
            $form->addError('form.tournament-team.front.edit.error.name-not-unique');
        }

        $teamByNickname = $this->facade->findByNickname($values->nickname);
        if ($teamByNickname === null || $teamByNickname->getId() === $this->team->getId()) {
            return;
        }

        $form->addError('form.tournament-team.front.edit.error.nickname-not-unique');
    }

    /**
     * @return mixed[]
     */
    private function getDefaults(): array
    {
        if (! $this->team->isLoaded()) {
            return [];
        }

        return [
            'name'                     => $this->team->getName(),
            'content'                  => $this->team->getContent(),
            'nickname'                 => $this->team->getNickname(),
            'website'                  => $this->team->getWebsite(),
            'facebook'                 => $this->team->getFacebook(),
            'nationality_id'           => $this->team->getNationality()->getId(),
            'is_accepting_invitations' => $this->team->isAcceptingInvitations(),
        ];
    }
}
