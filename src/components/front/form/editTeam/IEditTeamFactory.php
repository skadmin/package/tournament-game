<?php

declare(strict_types=1);

namespace Skadmin\TournamentGame\Components\Front;

interface IEditTeamFactory
{
    public function create(?int $id = null): EditTeam;
}
