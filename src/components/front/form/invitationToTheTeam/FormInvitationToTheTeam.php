<?php

declare(strict_types=1);

namespace Skadmin\TournamentGame\Components\Front;

use App\Model\System\APackageControl;
use Nette\Utils\ArrayHash;
use Skadmin\Translator\SimpleTranslation;
use Skadmin\Translator\Translator;
use SkadminUtils\FormControls\UI\Form;
use SkadminUtils\FormControls\UI\FormControl;

class FormInvitationToTheTeam extends FormControl
{
    use APackageControl;

    public function __construct(Translator $translator)
    {
        parent::__construct($translator);
    }

    public function getTitle(): SimpleTranslation|string
    {
        return 'tournament-team.front.invitation-to-the-team.title';
    }

    public function processOnSuccess(Form $form, ArrayHash $values): void
    {
        $this->onSuccess($form, $values, $form->isSubmitted()->name);
        $form->reset();
        $this->onRedraw();
    }

    public function render(): void
    {
        $template = $this->getComponentTemplate();
        $template->setTranslator($this->translator);
        $template->setFile($this->getControlTemplate(__DIR__ . '/formInvitationToTheTeam.latte'));

        $template->render();
    }

    protected function createComponentForm(): Form
    {
        $form = new Form();
        $form->setTranslator($this->translator);

        // INPUT
        $form->addText('phrase', 'form.tournament-team.front.invitation-to-the-team.phrase')
            ->setRequired('form.tournament-team.front.invitation-to-the-team.email.req');

        // BUTTON
        $form->addSubmit('send', 'form.tournament-team.front.invitation-to-the-team.send');

        // CALLBACK
        $form->onSuccess[] = [$this, 'processOnSuccess'];

        return $form;
    }
}
