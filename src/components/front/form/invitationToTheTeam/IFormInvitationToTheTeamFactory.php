<?php

declare(strict_types=1);

namespace Skadmin\TournamentGame\Components\Front;

interface IFormInvitationToTheTeamFactory
{
    public function create(): FormInvitationToTheTeam;
}
