<?php

declare(strict_types=1);

namespace Skadmin\TournamentGame\Components\Front;

use App\Model\Doctrine\Customer\Customer;
use App\Model\Doctrine\Customer\CustomerFacade;
use App\Model\System\APackageControl;
use App\Model\System\Constant;
use App\Model\System\Flash;
use Exception;
use Nette\Security\User as LoggedUser;
use Nette\Utils\ArrayHash;
use Nette\Utils\Html;
use Skadmin\Mailing\Doctrine\Mail\MailQueue;
use Skadmin\Mailing\Model\MailService;
use Skadmin\Payment\Doctrine\Payment\PaymentFacade;
use Skadmin\TournamentGame\Doctrine\Registration\RegistrationTeam;
use Skadmin\TournamentGame\Doctrine\Registration\RegistrationTeamFacade;
use Skadmin\TournamentGame\Doctrine\Supplement\SupplementFacade;
use Skadmin\TournamentGame\Doctrine\Team\Team;
use Skadmin\TournamentGame\Doctrine\Team\TeamFacade;
use Skadmin\TournamentGame\Doctrine\Tournament\Tournament;
use Skadmin\TournamentGame\Doctrine\TournamentPayment\TournamentPayment;
use Skadmin\TournamentGame\Mail\CMailRegistrationTeamCreate;
use Skadmin\Translator\SimpleTranslation;
use Skadmin\Translator\Translator;
use SkadminUtils\FormControls\UI\Form;
use SkadminUtils\FormControls\UI\FormWithUserControl;
use SkadminUtils\ImageStorage\ImageStorage;
use WebLoader\Nette\CssLoader;
use WebLoader\Nette\JavaScriptLoader;
use WebLoader\Nette\LoaderFactory;

use function chr;
use function count;
use function intval;
use function is_string;
use function number_format;
use function sprintf;
use function trim;

class FormRegistrationTournamentTeam extends FormWithUserControl
{
    use APackageControl;

    /** @var callable[] */
    public array $onProcessRegistration;

    private LoaderFactory          $webLoader;
    private RegistrationTeamFacade $facade;
    private CustomerFacade         $facadeCustomer;
    private PaymentFacade          $facadePayment;
    private SupplementFacade       $facadeSupplement;
    private TeamFacade             $facadeTeam;
    private Customer               $user;
    private Tournament             $tournament;
    private MailService            $mailService;
    private ImageStorage           $imageStorage;

    /** @var TournamentPayment[] */
    private array $paymentMethods = [];

    public function __construct(Tournament $tournament, RegistrationTeamFacade $facade, CustomerFacade $facadeCustomer, PaymentFacade $facadePayment, SupplementFacade $facadeSupplement, TeamFacade $facadeTeam, Translator $translator, LoaderFactory $webLoader, LoggedUser $user, MailService $mailService, ImageStorage $imageStorage)
    {
        parent::__construct($translator, $user);
        $this->tournament       = $tournament;
        $this->facade           = $facade;
        $this->facadeCustomer   = $facadeCustomer;
        $this->facadePayment    = $facadePayment;
        $this->facadeSupplement = $facadeSupplement;
        $this->facadeTeam       = $facadeTeam;
        $this->webLoader        = $webLoader;
        $this->mailService      = $mailService;
        $this->imageStorage     = $imageStorage;

        $this->user = $this->facadeCustomer->get($this->loggedUser->id);
    }

    public function getTitle(): SimpleTranslation|string
    {
        return 'tournament-registration-team.front.create.title';
    }

    /**
     * @return CssLoader[]
     */
    public function getCss(): array
    {
        return [];
    }

    /**
     * @return JavaScriptLoader[]
     */
    public function getJs(): array
    {
        return [];
    }

    public function processOnSuccess(Form $form, ArrayHash $values): void
    {
        if ($this->tournament->isUnpaid()) {
            $tournamentPayment = new TournamentPayment();
            $tournamentPayment->create($this->tournament, $this->facadePayment->getForUnpaid(), 0);
        } else {
            $payment           = $this->facadePayment->get($values->payment);
            $tournamentPayment = $this->tournament->findPayment($payment);

            if ($tournamentPayment === null) {
                throw new Exception('Payment for tournament is not set');
            }
        }

        $supplements = [];
        foreach ($values->supplement as $supplement) {
            $supplements[] = $this->facadeSupplement->get($supplement);
        }

        $team    = $this->facadeTeam->get(intval($values->team));
        $captain = $this->facadeCustomer->get(intval($values->captain));

        $registration = $this->facade->create(
            $this->tournament,
            $team,
            $captain,
            $values->email,
            $values->phone,
            $values->facebook,
            $tournamentPayment,
            $values->list_name,
            $values->list_of_players,
            $supplements
        );

        if ($registration !== null) {
            $this->onFlashmessage('form.tournament-registration-team.front.create.flash.success.create', Flash::SUCCESS);

            if ($this->tournament->isUnpaid() && $registration->getPrice() === 0 && $this->tournament->isOnline()) {
                $this->facade->updateStatus($registration, RegistrationTeam::STATUS_PAID);
            }

            // pokud je registrace uděláne v čase pro potvrzení, tak jí rovnou potvrdíme
            if ($registration->getStatus() === RegistrationTeam::STATUS_PAID && $this->tournament->isTimeToConfirm()) {
                $this->facade->updateStatus($registration, RegistrationTeam::STATUS_CONFIRMED);
            }

            $this->sendEmailCreate($registration);
        } else {
            $this->onFlashmessage('form.tournament-registration-team.front.create.flash.danger.create', Flash::DANGER);
        }

        $this->onProcessRegistration($registration);
        $this->onSuccess($form, $values, $form->isSubmitted()->name);
    }

    private function sendEmailCreate(RegistrationTeam $registration): void
    {
        $cMailRegistrationTeamUpdate = new CMailRegistrationTeamCreate($registration, $this->translator);

        $mailQueue = $this->mailService->addByTemplateType(
            CMailRegistrationTeamCreate::TYPE,
            $cMailRegistrationTeamUpdate,
            [$registration->getEmail()],
            true
        );

        $presenter = $this->getPresenterIfExists();
        if ($presenter !== null && $mailQueue !== null && $mailQueue->getStatus() === MailQueue::STATUS_SENT) {
            $message = new SimpleTranslation('form.tournament-registration-team.front.create.flash.registration-update.success %s', $registration->getEmail());
            $this->onFlashmessage($message, Flash::SUCCESS);
        } else {
            $message = new SimpleTranslation('form.tournament-registration-team.front.create.flash.registration-update.danger %s', $registration->getEmail());
            $this->onFlashmessage($message, Flash::DANGER);
        }
    }

    //private function sendEmailUpdate(RegistrationTeam $registration) : void
    //{
    //    $cMailRegistrationTeamUpdate = new CMailRegistrationTeamUpdate($registration, $this->translator);
    //
    //    $mailQueue = $this->mailService->addByTemplateType(
    //        CMailRegistrationTeamUpdate::TYPE,
    //        $cMailRegistrationTeamUpdate,
    //        [$registration->getEmail()],
    //        true
    //    );
    //
    //    $presenter = $this->getPresenterIfExists();
    //    if ($presenter && $mailQueue !== null && $mailQueue->getStatus() === MailQueue::STATUS_SENT) {
    //        $message = new SimpleTranslation('form.tournament-registration-team.front.create.flash.registration-update.success %s', $registration->getEmail());
    //        $this->onFlashmessage($message, Flash::SUCCESS);
    //    } else {
    //        $message = new SimpleTranslation('form.tournament-registration-team.front.create.flash.registration-update.danger %s', $registration->getEmail());
    //        $this->onFlashmessage($message, Flash::DANGER);
    //    }
    //}

    /**
     * render template...
     */
    public function render(): void
    {
        $template = $this->getComponentTemplate();
        $template->setTranslator($this->translator);
        $template->setFile($this->getControlTemplate(__DIR__ . '/formRegistrationTournamentUser.latte'));

        $this->preparePaymentMethods();
        $template->imageStorage = $this->imageStorage;
        $template->payments     = $this->paymentMethods;
        $template->tournament   = $this->tournament;
        $template->_form        = $this['form'];

        $template->render();
    }

    private function preparePaymentMethods(): void
    {
        if (count($this->paymentMethods) !== 0 || $this->tournament->isUnpaid()) {
            return;
        }

        foreach ($this->tournament->getPayments() as $tournamentPayment) {
            $payment = $tournamentPayment->getPayment();
            if (! $payment->isActive()) {
                continue;
            }

            $this->paymentMethods[$payment->getId()] = $tournamentPayment;
        }
    }

    protected function createComponentForm(): Form
    {
        $form = new Form();
        $form->setTranslator($this->translator);

        // DATA
        if ($this->tournament->getTournamentType() === null) {
            $numberOfParticipantsFrom = 1;
        } else {
            $numberOfParticipantsFrom = $this->tournament->getTournamentType()->getNumberOfParticipantsFrom();
        }

        $minNumberOfParticipants = $numberOfParticipantsFrom;
        $teams                   = $this->user->getTeams(true, $minNumberOfParticipants);
        $dataTeams               = [];
        foreach ($teams as $_team) {
            $team                      = $_team->getTeam();
            $dataTeams[$team->getId()] = sprintf('%s [%s]', $team->getName(), $team->getNickname());
        }

        $dataCaptain       = [];
        $dataListOfPlayers = [];
        if (isset($_POST['team']) && isset($dataTeams[$_POST['team']])) {
            $team              = $this->facadeTeam->get(intval($_POST['team']));
            $dataCaptain       = $this->prepareDataCaptain($team, true);
            $dataListOfPlayers = $this->prepareDataListOfPlayers($team, true);
        }

        $this->preparePaymentMethods();
        $dataPayments = [];
        foreach ($this->paymentMethods as $tournamentPayment) {
            $label = sprintf('%s - [%s,- Kč]', $tournamentPayment->getPayment()->getName(), number_format($tournamentPayment->getPrice(), 0, ' ', ' '));

            $option = Html::el('option', [
                'data-price' => $tournamentPayment->getPrice(),
            ])->setText($label);

            $dataPayments[$tournamentPayment->getPayment()->getId()] = $option;
        }

        $dataSupplements = [];
        foreach ($this->tournament->getSupplements() as $supplement) {
            if (! $supplement->isActive()) {
                continue;
            }

            $text  = sprintf('%s - [%s,- Kč]', $supplement->getName(), number_format($supplement->getPrice(), 0, ' ', ' '));
            $label = Html::el('span', [
                'data-price' => $supplement->getPrice(),
            ])->setText($text);

            if ($supplement->getContent() !== '') {
                $info = Html::el('sup', [
                    'class'          => 'far fa-fw fa-sm fa-question-circle text-orange ml-1',
                    'title'          => $supplement->getContent(),
                    'data-toggle'    => 'tooltip',
                    'data-placement' => 'right',
                    'data-html'      => 'true',
                ]);

                $label->addHtml($info);
            }

            $dataSupplements[$supplement->getId()] = $label;
        }

        // INPUT EDIT
        $form->addSelect('team', 'form.tournament-registration-team.front.create.team', $dataTeams)
            ->setPrompt(Constant::PROMTP)
            ->setTranslator(null)
            ->setRequired('form.tournament-registration-team.front.create.team.req');

        $form->addSelect('captain', 'form.tournament-registration-team.front.create.captain', $dataCaptain)
            ->setPrompt(Constant::PROMTP)
            ->setTranslator(null)
            ->setRequired('form.tournament-registration-team.front.create.captain.req');

        $form->addText('list_name', 'form.tournament-registration-team.front.create.list-name')
            ->setRequired('form.tournament-registration-team.front.create.list-name.req');

        $form->addCheckboxList('list_of_players', 'form.tournament-registration-team.front.create.list-of-players', $dataListOfPlayers)
            ->setTranslator(null)
            ->setRequired('form.tournament-registration-team.front.create.list-of-players.req');

        $form->addEmail('email', 'form.tournament-registration-team.front.create.email')
            ->setRequired('form.tournament-registration-team.front.create.email.req');
        $inputPhone = $form->addText('phone', 'form.tournament-registration-team.front.create.phone');
        if (! $this->tournament->isOnline()) {
            $inputPhone->setRequired('form.tournament-registration-team.front.create.phone.req');
        }

        $form->addText('facebook', 'form.tournament-registration-team.front.create.facebook');

        // PAYMENT
        if (! $this->tournament->isUnpaid()) {
            $inputPayment = $form->addSelect('payment', 'form.tournament-registration-team.front.create.payment', $dataPayments)
                ->setTranslator(null)
                ->setRequired('form.tournament-registration-team.front.create.payment.req')
                ->setPrompt(Constant::PROMTP)
                ->addCondition(Form::FILLED)
                ->toggle('payment-info');

            foreach ($this->paymentMethods as $tournamentPayment) {
                $inputPayment->addCondition(Form::EQUAL, $tournamentPayment->getPayment()->getId())
                    ->toggle(sprintf('payment-%d', $tournamentPayment->getPayment()->getId()));
            }
        }

        // SUPPLEMENT
        $form->addCheckboxList('supplement', 'form.tournament-registration-team.front.create.supplement', $dataSupplements)
            ->setTranslator(null);

        // BUTTON
        $form->addSubmit('send', 'form.tournament-registration-team.front.create.send');
        $form->addSubmit('send_back', 'form.tournament-registration-team.front.create.send-back');
        $form->addSubmit('back', 'form.tournament-registration-team.front.create.back')
            ->setValidationScope([])
            ->onClick[] = [$this, 'processOnBack'];

        // DEFAULT
        //$form->setDefaults($this->getDefaults());

        // CALLBACK
        $form->onSuccess[] = [$this, 'processOnSuccess'];
        $this->onModifyForm($form);

        return $form;
    }

    public function handleUpdateFormCaptain(?string $id = null): void
    {
        $team = $this->facadeTeam->get(intval($id));

        $countRegistrationsFromTeam = $this->tournament->getCountRegistrationsForTeam($team);
        $listName                   = $team->getName();
        if ($countRegistrationsFromTeam !== 0) {
            $listName .= sprintf(' %s', chr(65 + $countRegistrationsFromTeam));
        }

        $this['form']['captain']->setItems($this->prepareDataCaptain($team));
        $this['form']['list_name']->setValue($listName);
        $this['form']['list_of_players']->setItems($this->prepareDataListOfPlayers($team));
        $this['form']['email']->setValue('');
        $this['form']['phone']->setValue('');
        $this['form']['facebook']->setValue('');

        $this->redrawControl('snipForm');
        $this->redrawControl('snipFormCaptain');
        $this->redrawControl('snipFormListName');
        $this->redrawControl('snipFormListOfPlayers');
        $this->redrawControl('snipFormContact');
    }

    /**
     * @return array<string, array<Html>>
     */
    private function prepareDataCaptain(Team $team, bool $onlyValid = false): array
    {
        $dataValid   = [];
        $dataInvalid = [];

        $registrations = $this->tournament->getValidRegistrations();
        foreach ($team->getPlayers(true) as $_player) {
            $player = $_player->getUser();
            $option = Html::el('option', [
                'value' => $player->getId(),
            ]);

            $alreadyInTeam = false;
            foreach ($registrations as $registration) {
                if (! $registration->isPlayerIn($player)) { //@phpstan-ignore-line
                    continue;
                }

                $alreadyInTeam = true;
            }

            $option->setText(sprintf('%s [%s]', $player->getFullName(), $this->translator->translate($_player->getRoleName())));

            $playerGameId = $_player->getUser()->getGameId($this->tournament->getGame()->getId());
            if (($this->tournament->isRequiredGameId() && is_string($playerGameId) && trim($playerGameId) === '') || $alreadyInTeam) {
                $option->addAttributes(['disabled' => 'disabled']);
                $dataInvalid[$player->getId()] = $option;
            } else {
                $dataValid[$player->getId()] = $option;
            }
        }

        $data = [];
        if (count($dataValid) > 0) {
            $data[$this->translator->translate('form.tournament-registration-team.front.create.captain.valid')] = $dataValid;
        }

        if (count($dataInvalid) > 0 && ! $onlyValid) {
            $data[$this->translator->translate('form.tournament-registration-team.front.create.captain.invalid')] = $dataInvalid;
        }

        return $data;
    }

    /**
     * @return array<Html>
     */
    private function prepareDataListOfPlayers(Team $team, bool $onlyValid = false): array
    {
        $dataValid   = [];
        $dataInvalid = [];

        $registrations = $this->tournament->getValidRegistrations();
        foreach ($team->getPlayers() as $_player) {
            $player = $_player->getUser();
            $option = Html::el('label', [
                'data-role-name'     => $this->translator->translate($_player->getRoleName()),
                'data-image-preview' => $_player->getUser()->getImagePreview(),
            ]);

            $option->setText($player->getFullName());

            $playerGameId = $_player->getUser()->getGameId($this->tournament->getGame()->getId());

            $alreadyInTeamFormat = 'form.tournament-registration-team.front.create.list-of-players.invalid-reason.already-in-tournament %s';
            $alreadyInTeam       = false;
            foreach ($registrations as $registration) {
                if (! $registration->isPlayerIn($player)) { //@phpstan-ignore-line
                    continue;
                }

                $alreadyInTeam = $registration->getTeamNickname(); //@phpstan-ignore-line
            }

            if ($this->tournament->isRequiredGameId() && is_string($playerGameId) && trim($playerGameId) === '') {
                $option->addAttributes([
                    'data-disabled' => 'disabled',
                    'data-reason'   => $this->translator->translate('form.tournament-registration-team.front.create.list-of-players.invalid-reason.missing-game-id'),
                ]);
                $dataInvalid[$player->getId()] = $option;
            } elseif ($alreadyInTeam !== false) { // zde bude pokud už je v turnaji...
                $option->addAttributes([
                    'data-disabled' => 'disabled',
                    'data-reason'   => $this->translator->translate(new SimpleTranslation($alreadyInTeamFormat, $alreadyInTeam)),
                ]);
                $dataInvalid[$player->getId()] = $option;
            } else {
                $dataValid[$player->getId()] = $option;
            }
        }

        if ($onlyValid) {
            return $dataValid;
        }

        return $dataValid + $dataInvalid;
    }

    public function handleUpdateFormContact(?string $id = null): void
    {
        $user = $this->facadeCustomer->get(intval($id));

        $this['form']['email']->setValue($user->getEmail());
        $this['form']['phone']->setValue($user->getPhone());
        $this['form']['facebook']->setValue($user->getFacebook(true));

        $this->redrawControl('snipForm');
        $this->redrawControl('snipFormContact');
    }
}
