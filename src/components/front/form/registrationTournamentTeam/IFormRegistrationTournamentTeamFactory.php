<?php

declare(strict_types=1);

namespace Skadmin\TournamentGame\Components\Front;

use Skadmin\TournamentGame\Doctrine\Tournament\Tournament;

interface IFormRegistrationTournamentTeamFactory
{
    public function create(Tournament $tournament): FormRegistrationTournamentTeam;
}
