<?php

declare(strict_types=1);

namespace Skadmin\TournamentGame\Components\Front;

use App\Model\Doctrine\Customer\Customer;
use App\Model\Doctrine\Customer\CustomerFacade;
use App\Model\System\APackageControl;
use App\Model\System\Constant;
use App\Model\System\Flash;
use Exception;
use Nette\Security\User as LoggedUser;
use Nette\Utils\ArrayHash;
use Nette\Utils\Html;
use Skadmin\Mailing\Doctrine\Mail\MailQueue;
use Skadmin\Mailing\Model\MailService;
use Skadmin\Payment\Doctrine\Payment\PaymentFacade;
use Skadmin\TournamentGame\Doctrine\Registration\RegistrationUser;
use Skadmin\TournamentGame\Doctrine\Registration\RegistrationUserFacade;
use Skadmin\TournamentGame\Doctrine\Supplement\SupplementFacade;
use Skadmin\TournamentGame\Doctrine\Tournament\Tournament;
use Skadmin\TournamentGame\Doctrine\TournamentPayment\TournamentPayment;
use Skadmin\TournamentGame\Mail\CMailRegistrationUserCreate;
use Skadmin\TournamentGame\Mail\CMailRegistrationUserUpdate;
use Skadmin\Translator\SimpleTranslation;
use Skadmin\Translator\Translator;
use SkadminUtils\FormControls\UI\Form;
use SkadminUtils\FormControls\UI\FormWithUserControl;
use WebLoader\Nette\LoaderFactory;

use function count;
use function number_format;
use function sprintf;

class FormRegistrationTournamentUser extends FormWithUserControl
{
    use APackageControl;

    /** @var callable[] */
    public array $onProcessRegistration;

    private LoaderFactory          $webLoader;
    private RegistrationUserFacade $facade;
    private PaymentFacade          $facadePayment;
    private SupplementFacade       $facadeSupplement;
    private Customer               $user;
    private Tournament             $tournament;
    private MailService            $mailService;

    /** @var TournamentPayment[] */
    private array $paymentMethods = [];

    public function __construct(Tournament $tournament, RegistrationUserFacade $facade, CustomerFacade $facadeCustomer, PaymentFacade $facadePayment, SupplementFacade $facadeSupplement, Translator $translator, LoaderFactory $webLoader, LoggedUser $user, MailService $mailService)
    {
        parent::__construct($translator, $user);
        $this->tournament       = $tournament;
        $this->facade           = $facade;
        $this->facadePayment    = $facadePayment;
        $this->facadeSupplement = $facadeSupplement;
        $this->webLoader        = $webLoader;
        $this->mailService      = $mailService;

        $this->user = $facadeCustomer->get($this->loggedUser->id);
    }

    public function getTitle(): SimpleTranslation|string
    {
        return 'tournament-registration-user.front.create.title';
    }

    public function processOnSuccess(Form $form, ArrayHash $values): void
    {
        $team = null;

        if ($this->tournament->isUnpaid()) {
            $tournamentPayment = new TournamentPayment();
            $tournamentPayment->create($this->tournament, $this->facadePayment->getForUnpaid(), 0);
        } else {
            $payment           = $this->facadePayment->get($values->payment);
            $tournamentPayment = $this->tournament->findPayment($payment);

            if ($tournamentPayment === null) {
                throw new Exception('Payment for tournament is not set');
            }
        }

        $supplements = [];
        foreach ($values->supplement as $supplement) {
            $supplements[] = $this->facadeSupplement->get($supplement);
        }

        $registration = $this->facade->create(
            $this->tournament,
            $this->user,
            $values->email,
            $values->phone,
            $values->facebook,
            $tournamentPayment,
            $supplements,
            $team,
        );

        if ($registration !== null) {
            $this->onFlashmessage('form.tournament-registration-user.front.create.flash.success.create', Flash::SUCCESS);

            if ($this->tournament->isUnpaid() && $registration->getPrice() === 0 && $this->tournament->isOnline()) {
                $this->facade->updateStatus($registration, RegistrationUser::STATUS_PAID);
            }

            // pokud je registrace uděláne v čase pro potvrzení, tak jí rovnou potvrdíme
            if ($registration->getStatus() === RegistrationUser::STATUS_PAID && $this->tournament->isTimeToConfirm()) {
                $registration = $this->facade->updateStatus($registration, RegistrationUser::STATUS_CONFIRMED);
            }

            $this->sendEmailCreate($registration);
        } else {
            $this->onFlashmessage('form.tournament-registration-user.front.create.flash.danger.create', Flash::DANGER);
        }

        $this->onProcessRegistration($registration);
        $this->onSuccess($form, $values, $form->isSubmitted()->name);
    }

    private function sendEmailCreate(RegistrationUser $registration): void
    {
        $cMailRegistrationUserUpdate = new CMailRegistrationUserCreate($registration, $this->translator);

        $mailQueue = $this->mailService->addByTemplateType(
            CMailRegistrationUserCreate::TYPE,
            $cMailRegistrationUserUpdate,
            [$registration->getEmail()],
            true
        );

        $presenter = $this->getPresenterIfExists();
        if ($presenter !== null && $mailQueue !== null && $mailQueue->getStatus() === MailQueue::STATUS_SENT) {
            $message = new SimpleTranslation('form.tournament-registration-user.front.create.flash.registration-update.success %s', $registration->getEmail());
            $this->onFlashmessage($message, Flash::SUCCESS);
        } else {
            $message = new SimpleTranslation('form.tournament-registration-user.front.create.flash.registration-update.danger %s', $registration->getEmail());
            $this->onFlashmessage($message, Flash::DANGER);
        }
    }

    private function sendEmailUpdate(RegistrationUser $registration): void
    {
        $cMailRegistrationUserUpdate = new CMailRegistrationUserUpdate($registration, $this->translator);

        $mailQueue = $this->mailService->addByTemplateType(
            CMailRegistrationUserUpdate::TYPE,
            $cMailRegistrationUserUpdate,
            [$registration->getEmail()],
            true
        );

        $presenter = $this->getPresenterIfExists();
        if ($presenter !== null && $mailQueue !== null && $mailQueue->getStatus() === MailQueue::STATUS_SENT) {
            $message = new SimpleTranslation('form.tournament-registration-user.front.create.flash.registration-update.success %s', $registration->getEmail());
            $this->onFlashmessage($message, Flash::SUCCESS);
        } else {
            $message = new SimpleTranslation('form.tournament-registration-user.front.create.flash.registration-update.danger %s', $registration->getEmail());
            $this->onFlashmessage($message, Flash::DANGER);
        }
    }

    public function render(): void
    {
        $template = $this->getComponentTemplate();
        $template->setTranslator($this->translator);
        $template->setFile($this->getControlTemplate(__DIR__ . '/formRegistrationTournamentUser.latte'));

        $this->preparePaymentMethods();
        $template->payments = $this->paymentMethods;

        $template->render();
    }

    private function preparePaymentMethods(): void
    {
        if (count($this->paymentMethods) !== 0 || $this->tournament->isUnpaid()) {
            return;
        }

        foreach ($this->tournament->getPayments() as $tournamentPayment) {
            $payment = $tournamentPayment->getPayment();
            if (! $payment->isActive()) {
                continue;
            }

            $this->paymentMethods[$payment->getId()] = $tournamentPayment;
        }
    }

    protected function createComponentForm(): Form
    {
        $form = new Form();
        $form->setTranslator($this->translator);

        // DATA
        $this->preparePaymentMethods();
        $dataPayments = [];
        foreach ($this->paymentMethods as $tournamentPayment) {
            $label = sprintf('%s - [%s,- Kč]', $tournamentPayment->getPayment()->getName(), number_format($tournamentPayment->getPrice(), 0, ' ', ' '));

            $option = Html::el('option', [
                'data-price' => $tournamentPayment->getPrice(),
            ])->setText($label);

            $dataPayments[$tournamentPayment->getPayment()->getId()] = $option;
        }

        $dataSupplements = [];
        foreach ($this->tournament->getSupplements() as $supplement) {
            if (! $supplement->isActive()) {
                continue;
            }

            $text  = sprintf('%s - [%s,- Kč]', $supplement->getName(), number_format($supplement->getPrice(), 0, ' ', ' '));
            $label = Html::el('span', [
                'data-price' => $supplement->getPrice(),
            ])->setText($text);

            if ($supplement->getContent() !== '') {
                $info = Html::el('sup', [
                    'class'          => 'far fa-fw fa-sm fa-question-circle text-orange ml-1',
                    'title'          => $supplement->getContent(),
                    'data-toggle'    => 'tooltip',
                    'data-placement' => 'right',
                    'data-html'      => 'true',
                ]);

                $label->addHtml($info);
            }

            $dataSupplements[$supplement->getId()] = $label;
        }

        // INPUT NOEDIT
        $form->addText('name', 'form.tournament-registration-user.front.create.name')
            ->setDisabled(true);
        $form->addText('nickname', 'form.tournament-registration-user.front.create.nickname')
            ->setDisabled(true);
        $form->addText('surname', 'form.tournament-registration-user.front.create.surname')
            ->setDisabled(true);

        // INPUT EDIT
        $form->addEmail('email', 'form.tournament-registration-user.front.create.email')
            ->setRequired('form.tournament-registration-user.front.create.email.req');
        $inputPhone = $form->addText('phone', 'form.tournament-registration-user.front.create.phone');
        if (! $this->tournament->isOnline()) {
            $inputPhone->setRequired('form.tournament-registration-user.front.create.phone.req');
        }

        $form->addText('facebook', 'form.tournament-registration-user.front.create.facebook');

        // PAYMENT
        if (! $this->tournament->isUnpaid()) {
            $inputPayment = $form->addSelect('payment', $this->translator->translate('form.tournament-registration-user.front.create.payment'), $dataPayments)
                ->setTranslator(null)
                ->setRequired($this->translator->translate('form.tournament-registration-user.front.create.payment.req'))
                ->setPrompt(Constant::PROMTP)
                ->addCondition(Form::FILLED)
                ->toggle('payment-info');

            foreach ($this->paymentMethods as $tournamentPayment) {
                $inputPayment->addCondition(Form::EQUAL, $tournamentPayment->getPayment()->getId())
                    ->toggle(sprintf('payment-%d', $tournamentPayment->getPayment()->getId()));
            }
        }

        // SUPPLEMENT
        $form->addCheckboxList('supplement', $this->translator->translate('form.tournament-registration-user.front.create.supplement'), $dataSupplements)
            ->setTranslator(null);

        // BUTTON
        $form->addSubmit('send', 'form.tournament-registration-user.front.create.send');
        $form->addSubmit('send_back', 'form.tournament-registration-user.front.create.send-back');
        $form->addSubmit('back', 'form.tournament-registration-user.front.create.back')
            ->setValidationScope([])
            ->onClick[] = [$this, 'processOnBack'];

        // DEFAULT
        $form->setDefaults($this->getDefaults());

        // CALLBACK
        $form->onSuccess[] = [$this, 'processOnSuccess'];
        $this->onModifyForm($form);

        return $form;
    }

    /**
     * @return mixed[]
     */
    private function getDefaults(): array
    {
        return [
            'name'     => $this->user->getName(),
            'nickname' => $this->user->getNickname(),
            'surname'  => $this->user->getSurname(),
            'email'    => $this->user->getEmail(),
            'phone'    => $this->user->getPhone(),
            'facebook' => $this->user->getFacebook(),
        ];
    }
}
