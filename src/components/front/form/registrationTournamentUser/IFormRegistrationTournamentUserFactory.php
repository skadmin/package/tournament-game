<?php

declare(strict_types=1);

namespace Skadmin\TournamentGame\Components\Front;

use Skadmin\TournamentGame\Doctrine\Tournament\Tournament;

interface IFormRegistrationTournamentUserFactory
{
    public function create(Tournament $tournament): FormRegistrationTournamentUser;
}
