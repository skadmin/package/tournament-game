<?php

declare(strict_types=1);

namespace Skadmin\TournamentGame\Components\Front;

interface ITournamentMatchSettingsFactory
{
    public function create(?int $id = null): TournamentMatchSettings;
}
