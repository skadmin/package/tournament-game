<?php

declare(strict_types=1);

namespace Skadmin\TournamentGame\Components\Front;

use App\Model\Doctrine\Customer\CustomerFacade;
use App\Model\System\APackageControl;
use App\Model\System\Flash;
use Nette\Http\FileUpload;
use Nette\Security\User as LoggedUser;
use Nette\Utils\ArrayHash;
use Nette\Utils\DateTime;
use Skadmin\FileStorage\FileStorage;
use Skadmin\LiveChat\Doctrine\LiveChat\LiveChat;
use Skadmin\LiveChat\Doctrine\LiveChat\LiveChatFacade;
use Skadmin\LiveChat\Doctrine\LiveChatMessage\LiveChatMessageFacade;
use Skadmin\TournamentGame\BaseControl;
use Skadmin\TournamentGame\Doctrine\Team\TeamFacade;
use Skadmin\TournamentGame\Doctrine\Tournament\Tournament;
use Skadmin\TournamentGame\Doctrine\TournamentBracketStage\Type\TournamentBracketStageRoundRobinDuel;
use Skadmin\TournamentGame\Doctrine\TournamentBracketStageMatch\TournamentBracketStageMatch;
use Skadmin\TournamentGame\Doctrine\TournamentBracketStageMatch\TournamentBracketStageMatchFacade;
use Skadmin\TournamentGame\Doctrine\TournamentBracketStageMatch\TournamentBracketStageMatchService;
use Skadmin\TournamentGame\Doctrine\TournamentBracketStageMatchDuel\TournamentBracketStageMatchDuel;
use Skadmin\TournamentGame\Doctrine\TournamentBracketStageParticipant\TournamentBracketStageParticipant;
use Skadmin\TournamentGame\Doctrine\TournamentResult\TournamentResult as EntityTournamentResult;
use Skadmin\TournamentGame\Doctrine\TournamentResult\TournamentResultFacade;
use Skadmin\TournamentGame\Traits\TDownloadTournamentResult;
use Skadmin\Translator\SimpleTranslation;
use Skadmin\Translator\Translator;
use SkadminUtils\FormControls\UI\Form;
use SkadminUtils\FormControls\UI\FormWithUserControl;
use SkadminUtils\ImageStorage\ImageStorage;
use WebLoader\Nette\CssLoader;
use WebLoader\Nette\JavaScriptLoader;
use WebLoader\Nette\LoaderFactory;

use function array_combine;
use function array_reverse;
use function assert;
use function intval;
use function max;
use function min;
use function range;

class TournamentMatchSettings extends FormWithUserControl
{
    use APackageControl;

    private TournamentBracketStageMatchFacade $facadeTournamentBracketStageMatch;
    private LiveChatFacade $facadeLiveChat;
    private LiveChatMessageFacade $facadeLiveChatMessage;
    private LoaderFactory $webLoader;
    private ?TournamentBracketStageMatch $match;

    public function __construct(?int $id, TournamentBracketStageMatchFacade $facadeTournamentBracketStageMatch, LiveChatFacade $facadeLiveChat, LiveChatMessageFacade $facadeLiveChatMessage, Translator $translator, LoggedUser $user, LoaderFactory $webLoader)
    {
        parent::__construct($translator, $user);

        $this->facadeTournamentBracketStageMatch = $facadeTournamentBracketStageMatch;
        $this->facadeLiveChat = $facadeLiveChat;
        $this->facadeLiveChatMessage = $facadeLiveChatMessage;

        $this->webLoader = $webLoader;

        $this->match = $this->facadeTournamentBracketStageMatch->get($id);
        if (!$this->match instanceof TournamentBracketStageMatch) {
            return;
        }
    }

    public function getTitle(): SimpleTranslation|string
    {
        return 'tournament-team.front.tournament-match-settings.title';
    }

    /**
     * @return CssLoader[]
     */
    public function getCss(): array
    {
        return [
            $this->webLoader->createCssLoader('daterangePicker'),
        ];
    }

    /**
     * @return JavaScriptLoader[]
     */
    public function getJs(): array
    {
        return [
            $this->webLoader->createJavaScriptLoader('moment'),
            $this->webLoader->createJavaScriptLoader('daterangePicker'),
        ];
    }

    public function render(): void
    {
        $template = $this->getComponentTemplate();
        $template->setTranslator($this->translator);
        $template->setFile($this->getControlTemplate(__DIR__ . '/tournamentMatchSettings.latte'));

        $template->drawBox = $this->drawBox;
        $template->match = $this->match;
        $template->render();
    }

    protected function createComponentForm(): Form
    {
        $form = new Form();
        $form->setTranslator($this->translator);

        // INPUT

        $form->addText('allowedFromDraft', 'form.tournament-team.front.tournament-match-settings.allowed-from-draft')
            ->setHtmlAttribute('data-date');

        // BUTTON
        $form->addSubmit('send', 'form.tournament-team.front.tournament-match-settings.send');

        // DEFAULT
        $form->setDefaults($this->getDefaults());

        // CALLBACK
        $form->onSuccess[] = [$this, 'processOnSuccess'];

        return $form;
    }

    public function processOnSuccess(Form $form, ArrayHash $values): void
    {
        $allowedFromDraft = DateTime::createFromFormat('d.m.Y', $values->allowedFromDraft);

        if ($this->match->isParticipant($this->loggedUser->getIdentity())) {
            if ($this->match->isParticipant($this->loggedUser->getIdentity(), true)) {
                $participant = $this->match->getChallenger();
            } else {
                $participant = $this->match->getOpponent();
            }
        }

        if ($participant instanceof TournamentBracketStageParticipant) {
            $this->facadeTournamentBracketStageMatch->updateAllowedFromDraft($this->match->getId(), $participant, $allowedFromDraft);
        }

        $this->addMessageToLiveChat(SimpleTranslation::create('form.tournament-team.front.tournament-match-settings.message-create %s', [$allowedFromDraft->format('d.m.Y')]));
        $this->getPresenter()->redrawControl('snipModal', false);
        $this->redrawControl('snipForm');
    }

    /**
     * @return mixed[]
     */
    private function getDefaults(): array
    {
        if (!$this->match->isLoaded()) {
            return [];
        }

        return [
            'allowedFromDraft' => $this->match->getAllowedFrom() === null ? null : $this->match->getAllowedFrom()->format('d.m.Y'),
        ];
    }

    public function handleConfirm(): void
    {
        $this->facadeTournamentBracketStageMatch->confirmAllowedFromDraft($this->match->getId());
        $this->addMessageToLiveChat($this->translator->translate('form.tournament-team.front.tournament-match-settings.message-confirm'));
    }

    public function handleReject(): void
    {
        $this->facadeTournamentBracketStageMatch->rejectAllowedFromDraft($this->match->getId());

        $this->getPresenter()->redrawControl('snipModal', false);
        $this->addMessageToLiveChat($this->translator->translate('form.tournament-team.front.tournament-match-settings.message-reject'));
        $this->redrawControl('snipForm');
    }

    protected function addMessageToLiveChat(SimpleTranslation|string $message): void
    {
        if (!$this->match instanceof TournamentBracketStageMatch) {
            return;
        }

        $tournament = $this->match->getTournament();
        $match = $this->match;

        $liveChageCode = sprintf('%s::%s', $tournament->getCode(), $match->getId());
        $liveChat = $this->facadeLiveChat->findByWebalize($liveChageCode);

        if (!$liveChat instanceof LiveChat) {
            return;
        }

        if ($message instanceof SimpleTranslation) {
            $message = $this->translator->translate($message);
        }

        if ($this->match->isParticipant($this->loggedUser->getIdentity())) {
            if ($this->match->isParticipant($this->loggedUser->getIdentity(), true)) {
                $name = $this->match->getChallenger()->getName();
            } else {
                $name = $this->match->getOpponent()->getName();
            }
        } else {
            $name = 'SYSTEM';
        }

        $this->facadeLiveChatMessage->create($liveChat, $name, $message, true);
    }
}
