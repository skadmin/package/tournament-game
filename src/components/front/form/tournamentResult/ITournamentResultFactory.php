<?php

declare(strict_types=1);

namespace Skadmin\TournamentGame\Components\Front;

interface ITournamentResultFactory
{
    public function create(int $id): TournamentResult;
}
