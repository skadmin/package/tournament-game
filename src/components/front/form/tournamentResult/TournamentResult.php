<?php

declare(strict_types=1);

namespace Skadmin\TournamentGame\Components\Front;

use App\Model\Doctrine\Customer\CustomerFacade;
use App\Model\System\APackageControl;
use App\Model\System\Constant;
use App\Model\System\Flash;
use Nette\Http\FileUpload;
use Nette\Security\User as LoggedUser;
use Nette\Utils\ArrayHash;
use Skadmin\FileStorage\FileStorage;
use Skadmin\TournamentGame\BaseControl;
use Skadmin\TournamentGame\Doctrine\Team\TeamFacade;
use Skadmin\TournamentGame\Doctrine\Tournament\Tournament;
use Skadmin\TournamentGame\Doctrine\Tournament\TournamentFacade;
use Skadmin\TournamentGame\Doctrine\TournamentResult\TournamentResultFacade;
use Skadmin\TournamentGame\Traits\TDownloadTournamentResult;
use Skadmin\Translator\SimpleTranslation;
use Skadmin\Translator\Translator;
use SkadminUtils\FormControls\UI\Form;
use SkadminUtils\FormControls\UI\FormWithUserControl;
use WebLoader\Nette\CssLoader;
use WebLoader\Nette\JavaScriptLoader;
use WebLoader\Nette\LoaderFactory;

use function asort;
use function assert;
use function count;
use function explode;
use function intval;
use function sprintf;

class TournamentResult extends FormWithUserControl
{
    use APackageControl;
    use TDownloadTournamentResult;

    private TeamFacade     $facadeTeam;
    private CustomerFacade $facadeCustomer;
    private Tournament     $tournament;
    private LoaderFactory  $webLoader;

    public function __construct(int $id, TournamentResultFacade $facadeTournamentResult, TournamentFacade $facadeTournament, TeamFacade $facadeTeam, CustomerFacade $facadeCustomer, Translator $translator, LoggedUser $user, FileStorage $fileStorage, LoaderFactory $webLoader)
    {
        parent::__construct($translator, $user);

        $this->facadeTournamentResult = $facadeTournamentResult;
        $this->facadeTeam             = $facadeTeam;
        $this->facadeCustomer         = $facadeCustomer;
        $this->fileStorage            = $fileStorage;
        $this->webLoader              = $webLoader;

        $this->tournament = $facadeTournament->get($id);
    }

    public function getTitle(): SimpleTranslation|string
    {
        return 'tournament-team.front.tournament-result.title';
    }

    /**
     * @return CssLoader[]
     */
    public function getCss(): array
    {
        return [
            $this->webLoader->createCssLoader('customFileInput'),
            $this->webLoader->createCssLoader('fancyBox'),
        ];
    }

    /**
     * @return JavaScriptLoader[]
     */
    public function getJs(): array
    {
        return [
            $this->webLoader->createJavaScriptLoader('customFileInput'),
            $this->webLoader->createJavaScriptLoader('fancyBox'),
        ];
    }

    public function processOnSuccess(Form $form, ArrayHash $values): void
    {
        if ($this->tournament->isOneVsOne()) {
            $submitter = $this->loggedUser->getIdentity(); //@phpstan-ignore-line
        } else {
            if (isset($values->submitter_id)) {
                [$teamId, $registrationId] = explode('__', $values->submitter_id);
                $submitter                 = $this->facadeTeam->get(intval($teamId));
                $submitterRegistration     = $this->tournament->findRegistration(intval($registrationId));
            } else {
                $playerRegistrations = $this->tournament->getRegistrationsForTeam($this->loggedUser->getIdentity()); // @phpstan-ignore-line

                $submitterRegistration = null;
                foreach ($playerRegistrations as $_registration) {
                    if (! $_registration->isPlayerIn($this->loggedUser->getIdentity())) {
                        continue;
                    }

                    $submitterRegistration = $_registration;
                }

                $submitter = $submitterRegistration->getTeam(); //@phpstan-ignore-line
            }
        }

        if ($values->opponent_id === null && ! $this->tournament->isResultOpponentRequired()) {
            $opponent = null;
        } elseif ($this->tournament->isOneVsOne()) {
            $opponent = $this->facadeCustomer->get($values->opponent_id);
        } else {
            [$teamId, $registrationId] = explode('__', $values->opponent_id);
            $opponent                  = $this->facadeTeam->get(intval($teamId));
            $opponentRegistration      = $this->tournament->findRegistration(intval($registrationId));
        }

        foreach ($values->file as $file) {
            assert($file instanceof FileUpload);
            if ($file->isOk()) {
                $identifier = $this->fileStorage->save($file, BaseControl::DIR_FILE_RESULT);
                $this->facadeTournamentResult->create(
                    $this->tournament,
                    $submitter,
                    $opponent,
                    $file->getName(),
                    $identifier,
                    $file->getSize(),
                    $file->getContentType(),
                    $values->note,
                    $submitterRegistration ?? null,
                    $opponentRegistration ?? null
                );

                $this->onFlashmessage(new SimpleTranslation('form.tournament-team.front.tournament-result.flash.success %s', $file->getName()), Flash::SUCCESS);
            } else {
                $this->onFlashmessage(new SimpleTranslation('form.tournament-team.front.tournament-result.flash.danger %s', $file->getName()), Flash::DANGER);
            }
        }

        $form->reset();
        $this->redrawControl('snipForm');
        $this->redrawControl('snipOverview');
    }

    public function render(): void
    {
        $template = $this->getComponentTemplate();
        $template->setTranslator($this->translator);
        $template->setFile($this->getControlTemplate(__DIR__ . '/tournamentResult.latte'));

        $template->drawBox           = $this->drawBox;
        $template->fileStorage       = $this->fileStorage;
        $template->tournamentResults = $this->tournament->getResults();
        $template->tournament        = $this->tournament;

        $template->render();
    }

    protected function createComponentForm(): Form
    {
        $form = new Form();
        $form->setTranslator($this->translator);

        // DATA
        $dataOpponents = [];
        $dataSubmitter = [];

        if ($this->tournament->isOnline()) {
            $registrations = $this->tournament->getConfirmedRegistrations();
        } else {
            $registrations = $this->tournament->getPaidRegistrations();
        }

        $isOneVsOne = $this->tournament->isOneVsOne();

        // zjistíme si tým za který aktuálně hraje
        $currentTeamId             = null;
        $currentTeamRegistrationId = null;
        if (! $isOneVsOne) {
            $playerRegistrations = $this->tournament->getRegistrationsForTeam($this->loggedUser->getIdentity()); // @phpstan-ignore-line

            $registration = null;
            foreach ($playerRegistrations as $_registration) {
                if (! $_registration->isPlayerIn($this->loggedUser->getIdentity())) {
                    continue;
                }

                $registration = $_registration;
            }

            if ($registration !== null && $registration->getTeam() !== null) {
                $currentTeamId             = $registration->getTeam()->getId();
                $currentTeamRegistrationId = sprintf('%s__%s', $registration->getTeam()->getId(), $registration->getId());
            }
        }

        foreach ($registrations as $registration) {
            if ($isOneVsOne) {
                $user                          = $registration->getUser(); //@phpstan-ignore-line
                $dataOpponents[$user->getId()] = $user->isPublicName() ? $user->getFullName() : $user->getNickname();
            } elseif ($registration->getTeam() !== null) {
                $team               = $registration->getTeam();
                $teamRegistrationId = sprintf('%s__%s', $team->getId(), $registration->getId());

                if ($registration->getTeamNickname() !== '') { //@phpstan-ignore-line
                    $registrationName = sprintf('%s [%s] - %s', $team->getName(), $team->getNickname(), $registration->getTeamNickname()); //@phpstan-ignore-line
                } else {
                    $registrationName = sprintf('%s [%s]', $team->getName(), $team->getNickname());
                }

                $dataOpponents[$teamRegistrationId] = $registrationName;

                if ($currentTeamId === $team->getId()) {
                    $dataSubmitter[$teamRegistrationId] = $registrationName;
                }
            }
        }

        if ($isOneVsOne) {
            unset($dataOpponents[$this->loggedUser->getId()]);
        }

        asort($dataOpponents);
        asort($dataSubmitter);

        // INPUT
        if (count($dataSubmitter) > 1) {
            $inputSubmitterId = $form->addSelect('submitter_id', 'form.tournament-team.front.tournament-result.submitter-id', $dataSubmitter)
                ->setRequired($this->translator->translate('form.tournament-team.front.tournament-result.submitter-id.req'))
                ->setTranslator(null)
                ->setDefaultValue($currentTeamRegistrationId);
        }

        $inputOpponentId = $form->addSelect('opponent_id', $this->translator->translate('form.tournament-team.front.tournament-result.opponent-id'), $dataOpponents)
            ->setTranslator(null)
            ->setPrompt(Constant::PROMTP);

        if ($this->tournament->isResultOpponentRequired()) {
            $inputOpponentId->setRequired($this->translator->translate('form.tournament-team.front.tournament-result.opponent-id.req'));
        }

        if (isset($inputSubmitterId)) {
            $inputOpponentId->addRule(Form::NOT_EQUAL, 'form.tournament-team.front.tournament-result.opponent-id.not-equal.submitter-id', $inputSubmitterId);
        }

        $form->addMultiUpload('file', 'form.tournament-team.front.tournament-result.file')
            ->setRequired('form.tournament-team.front.tournament-result.file.req');

        $form->addTextArea('note', 'form.tournament-team.front.tournament-result.note', null, 4);

        // BUTTON
        $form->addSubmit('send', 'form.tournament-team.front.tournament-result.send');

        // CALLBACK
        $form->onSuccess[] = [$this, 'processOnSuccess'];

        return $form;
    }
}
