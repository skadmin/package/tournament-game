<?php

declare(strict_types=1);

namespace Skadmin\TournamentGame\Components\Front;

interface ITournamentResultMatchFactory
{
    public function create(?int $id = null): TournamentResultMatch;
}
