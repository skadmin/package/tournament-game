<?php

declare(strict_types=1);

namespace Skadmin\TournamentGame\Components\Front;

use App\Model\Doctrine\Customer\CustomerFacade;
use App\Model\System\APackageControl;
use App\Model\System\Flash;
use Nette\Http\FileUpload;
use Nette\Security\User as LoggedUser;
use Nette\Utils\ArrayHash;
use Skadmin\FileStorage\FileStorage;
use Skadmin\TournamentGame\BaseControl;
use Skadmin\TournamentGame\Doctrine\Team\TeamFacade;
use Skadmin\TournamentGame\Doctrine\Tournament\Tournament;
use Skadmin\TournamentGame\Doctrine\TournamentBracketStage\Type\TournamentBracketStageRoundRobinDuel;
use Skadmin\TournamentGame\Doctrine\TournamentBracketStageMatch\TournamentBracketStageMatch;
use Skadmin\TournamentGame\Doctrine\TournamentBracketStageMatch\TournamentBracketStageMatchFacade;
use Skadmin\TournamentGame\Doctrine\TournamentBracketStageMatch\TournamentBracketStageMatchService;
use Skadmin\TournamentGame\Doctrine\TournamentBracketStageMatchDuel\TournamentBracketStageMatchDuel;
use Skadmin\TournamentGame\Doctrine\TournamentBracketStageParticipant\TournamentBracketStageParticipant;
use Skadmin\TournamentGame\Doctrine\TournamentResult\TournamentResult as EntityTournamentResult;
use Skadmin\TournamentGame\Doctrine\TournamentResult\TournamentResultFacade;
use Skadmin\TournamentGame\Traits\TDownloadTournamentResult;
use Skadmin\Translator\SimpleTranslation;
use Skadmin\Translator\Translator;
use SkadminUtils\FormControls\UI\Form;
use SkadminUtils\FormControls\UI\FormWithUserControl;
use SkadminUtils\ImageStorage\ImageStorage;
use WebLoader\Nette\CssLoader;
use WebLoader\Nette\JavaScriptLoader;
use WebLoader\Nette\LoaderFactory;

use function array_combine;
use function array_reverse;
use function assert;
use function intval;
use function max;
use function min;
use function range;

class TournamentResultMatch extends FormWithUserControl
{
    use APackageControl;
    use TDownloadTournamentResult;

    private TournamentBracketStageMatchFacade $facadeTournamentBracketStageMatch;
    private TeamFacade $facadeTeam;
    private CustomerFacade $facadeCustomer;
    private TournamentBracketStageMatchService $serviceTournamentBracketStageMatch;
    private ?TournamentBracketStageMatch $match;
    private Tournament $tournament;
    private LoaderFactory $webLoader;
    private ImageStorage $imageStorage;

    public function __construct(?int $id, TournamentBracketStageMatchFacade $facadeTournamentBracketStageMatch, TournamentResultFacade $facadeTournamentResult, TeamFacade $facadeTeam, CustomerFacade $facadeCustomer, Translator $translator, LoggedUser $user, FileStorage $fileStorage, LoaderFactory $webLoader, ImageStorage $imageStorage, TournamentBracketStageMatchService $serviceTournamentBracketStageMatch)
    {
        parent::__construct($translator, $user);

        $this->facadeTournamentBracketStageMatch = $facadeTournamentBracketStageMatch;
        $this->facadeTournamentResult = $facadeTournamentResult;
        $this->facadeTeam = $facadeTeam;
        $this->facadeCustomer = $facadeCustomer;

        $this->serviceTournamentBracketStageMatch = $serviceTournamentBracketStageMatch;

        $this->fileStorage = $fileStorage;
        $this->webLoader = $webLoader;
        $this->imageStorage = $imageStorage;

        $this->match = $this->facadeTournamentBracketStageMatch->get($id);
        if (!($this->match instanceof TournamentBracketStageMatch)) {
            return;
        }

        $this->tournament = $this->match->getStage()->getBracket()->getTournament();
    }

    public function getTitle(): SimpleTranslation|string
    {
        return 'tournament-team.front.tournament-result-match.title';
    }

    /**
     * @return CssLoader[]
     */
    public function getCss(): array
    {
        return [
            $this->webLoader->createCssLoader('customFileInput'),
            $this->webLoader->createCssLoader('fancyBox'),
        ];
    }

    /**
     * @return JavaScriptLoader[]
     */
    public function getJs(): array
    {
        return [
            $this->webLoader->createJavaScriptLoader('customFileInput'),
            $this->webLoader->createJavaScriptLoader('fancyBox'),
            //$this->webLoader->createJavaScriptLoader('prtToBlob'),
        ];
    }

    public function render(): void
    {
        $template = $this->getComponentTemplate();
        $template->setTranslator($this->translator);
        $template->setFile($this->getControlTemplate(__DIR__ . '/tournamentResult.latte'));

        $customer = $this->facadeCustomer->get($this->loggedUser->getId());

        $template->drawBox = $this->drawBox;
        $template->fileStorage = $this->fileStorage;
        $template->imageStorage = $this->imageStorage;
        $template->results = array_reverse($this->match->getResults()->toArray());
        $template->tournament = $this->tournament;
        $template->match = $this->match;
        $template->canUploadResult = $this->match->isParticipant($customer);
        $template->customer = $customer;
        $template->userAdmin = $this->getPresenter()->template->userAdmin;

        $template->render();
    }

    protected function createComponentForm(): Form
    {
        // DATA
        if ($this->match->getStage() instanceof TournamentBracketStageRoundRobinDuel) {
            $range = range(1, $this->match->getStage()->getNumberOfRoundDuels());

            $rangeDules = range(1, $this->match->getStage()->getNumberOfDuels() + 1);
            $dataNumberOfDules = array_combine($rangeDules, $rangeDules);

            if ($this->match->isNeedAce()) {
                $dataNumberOfDules[count($dataNumberOfDules)] = 'ACE';
            } else {
                unset($dataNumberOfDules[count($dataNumberOfDules)]);
            }
        } else {
            $range = range(1, $this->match->getStage()->getBestOfGame());
        }
        $dataNumberOfMatch = array_combine($range, $range);

        $duelByResults = [];
        foreach ($this->match->getResults(EntityTournamentResult::STATUS_CONFIRMED) as $result) {
            $duelByResults[$result->getNumberOfDuel()][intval($result->getNumberOfMatch())] = 1;
        }
        $currentNumberOfMatch = count($duelByResults[$this->match->getCurrentNumberOfDuel()] ?? []) + 1;

        $form = new Form();
        $form->setTranslator($this->translator);

        // INPUT
        $form->addInteger('submitterResult', 'form.tournament-team.front.submitter-result')
            ->setHtmlAttribute('min', 0)
            ->setDefaultValue(0);
        $form->addInteger('opponentResult', 'form.tournament-team.front.opponent-result')
            ->setHtmlAttribute('min', 0)
            ->setDefaultValue(0);

        if (isset($dataNumberOfDules)) {
            $form->addSelect('numberOfDuel', 'form.tournament-team.front.number-of-duel', $dataNumberOfDules)
                ->setDefaultValue(min($this->match->getCurrentNumberOfDuel(), max($dataNumberOfDules)))
                ->setTranslator(null);
        }

        $form->addSelect('numberOfMatch', 'form.tournament-team.front.number-of-match', $dataNumberOfMatch)
            ->setDefaultValue(min($currentNumberOfMatch, max($dataNumberOfMatch)))
            ->setTranslator(null);

        $inputFile = $form->addMultiUpload('file', 'form.tournament-team.front.tournament-result-match.file');
        if ($this->tournament->isResultFileRequired()) {
            $inputFile->setRequired('form.tournament-team.front.tournament-result-match.file.req');
        }

        $form->addTextArea('note', 'form.tournament-team.front.tournament-result-match.note', null, 4);

        // BUTTON
        $form->addSubmit('send', 'form.tournament-team.front.tournament-result-match.send');

        // CALLBACK
        $form->onSuccess[] = [$this, 'processOnSuccess'];

        return $form;
    }

    public function processOnSuccess(Form $form, ArrayHash $values): void
    {
        $loggedCustomer = $this->facadeCustomer->get($this->loggedUser->getId());

        $submitter = null;
        $opponent = null;

        $submitterIsChallenger = false;
        if ($this->tournament->isOneVsOne()) {
            if ($this->match->getChallenger()->getRegistration()->getUser() === $loggedCustomer) {
                $submitterIsChallenger = true;
                $submitter = $this->match->getChallenger();
                $opponent = $this->match->getOpponent();
            } elseif ($this->match->getOpponent()->getRegistration()->getUser() === $loggedCustomer) {
                $submitter = $this->match->getOpponent();
                $opponent = $this->match->getChallenger();
            }

            if ($submitter instanceof TournamentBracketStageParticipant) {
                $submitter = $submitter->getRegistration()->getUser();
            }

            if ($opponent instanceof TournamentBracketStageParticipant) {
                $opponent = $opponent->getRegistration()->getUser();
            }
        } else {
            if ($this->match->getChallenger()->getRegistration()->isPlayerIn($loggedCustomer)) {
                $submitterIsChallenger = true;
                $submitter = $this->match->getChallenger();
                $opponent = $this->match->getOpponent();
            } elseif ($this->match->getOpponent()->getRegistration()->isPlayerIn($loggedCustomer)) {
                $submitter = $this->match->getOpponent();
                $opponent = $this->match->getChallenger();
            }

            if ($submitter instanceof TournamentBracketStageParticipant) {
                $submitter = $submitter->getRegistration()->getTeam();
            }

            if ($opponent instanceof TournamentBracketStageParticipant) {
                $opponent = $opponent->getRegistration()->getTeam();
            }
        }

        if (count($values->file) > 0) {
            foreach ($values->file as $file) {
                assert($file instanceof FileUpload);
                if ($file->isOk()) {
                    $identifier = $this->fileStorage->save($file, BaseControl::DIR_FILE_RESULT);
                    $this->facadeTournamentResult->create(
                        $this->tournament,
                        $submitter,
                        $opponent,
                        $file->getName(),
                        $identifier,
                        $file->getSize(),
                        $file->getContentType(),
                        $values->note,
                        $submitterRegistration ?? null,
                        $opponentRegistration ?? null,
                        $values->numberOfMatch,
                        $values->submitterResult,
                        $values->opponentResult,
                        $this->match,
                        $values->numberOfDuel ?? null
                    );

                    $this->onFlashmessage(new SimpleTranslation('form.tournament-team.front.tournament-result-match.flash.success %s', $file->getName()), Flash::SUCCESS);
                } else {
                    $this->onFlashmessage(new SimpleTranslation('form.tournament-team.front.tournament-result-match.flash.danger %s', $file->getName()), Flash::DANGER);
                }
            }
        } else {
            $fileName = 'dot.png';
            $identifier = $this->fileStorage->saveContent(file_get_contents($fileName), $fileName, BaseControl::DIR_FILE_RESULT);
            $this->facadeTournamentResult->create(
                $this->tournament,
                $submitter,
                $opponent,
                $fileName,
                $identifier,
                filesize($fileName),
                mime_content_type($fileName),
                $values->note,
                $submitterRegistration ?? null,
                $opponentRegistration ?? null,
                $values->numberOfMatch,
                $values->submitterResult,
                $values->opponentResult,
                $this->match,
                $values->numberOfDuel ?? null
            );

            $this->onFlashmessage(new SimpleTranslation('form.tournament-team.front.tournament-result-match.flash.success %s', $file->getName()), Flash::SUCCESS);
        }

        $this->updateMatch();

        $form->reset();
        $this->getPresenter()->redrawControl('snipModal', false);
        $this->redrawControl('snipForm');
        $this->redrawControl('snipOverview');
    }

    public function handleSnipMatch(): void
    {
        $this->getPresenter()->redrawControl('snipModal', false);
        $this->redrawControl('snipOverview');
    }

    public function handleConfirmed(string $resultId): void
    {
        $this->updateStatus($resultId, EntityTournamentResult::STATUS_CONFIRMED);
        $this->getPresenter()->redrawControl('snipModal', false);
        $this->updateMatch();

        $this->redrawControl('snipForm');
        $this->redrawControl('snipOverview');
    }

    public function handleNotConfirmed(string $resultId): void
    {
        $this->updateStatus($resultId, EntityTournamentResult::STATUS_NOT_CONFIRMED);
        $this->getPresenter()->redrawControl('snipModal', false);
        $this->updateMatch();

        $this->redrawControl('snipForm');
        $this->redrawControl('snipOverview');
    }

    private function updateStatus(string $resultId, int $status): void
    {
        $result = $this->match->getResult(intval($resultId));
        $userAdmin = $this->getPresenter()->template->userAdmin;
        $customer = $this->facadeCustomer->get($this->loggedUser->getId());

        if ($this->tournament->isOneVsOne()) {
            if ($userAdmin->isLoggedIn() || ($result->getSubmitter() !== $customer && $result->getStatus() === EntityTournamentResult::STATUS_NEW)) {
                $this->facadeTournamentResult->updateStatus($result->getId(), $status);
            }
        } else {
            if ($userAdmin->isLoggedIn() || (!$result->getSubmitter()->isPlayerIn($customer) && $result->getStatus() === EntityTournamentResult::STATUS_NEW)) {
                $this->facadeTournamentResult->updateStatus($result->getId(), $status);
            }
        }

        //$duel = $this->match->getDuel($values->numberOfDuel);
        //if ($duel instanceof TournamentBracketStageMatchDuel) {
        //    if ($submitterIsChallenger) {
        //        $challengerResult = $values->submitterResult;
        //        $opponentResult = $values->opponentResult;
        //    } else {
        //        $challengerResult = $values->opponentResult;
        //        $opponentResult = $values->submitterResult;
        //    }
//
        //    $duel->setChallengerResult($duel->getChallengerResult() + $challengerResult);
        //    $duel->setOpponentResult($duel->getOpponentResult() + $opponentResult);
        //}
    }

    private function updateMatch(): void
    {
        $this->serviceTournamentBracketStageMatch->updateMatch($this->match, $this->getPresenter());
    }
}
