<?php

declare(strict_types=1);

namespace Skadmin\TournamentGame\Doctrine\Registration;

use App\Model\Doctrine\Customer\Customer;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Nette\Utils\DateTime;
use Skadmin\Payment\Doctrine\Payment\Payment;
use Skadmin\TournamentGame\Doctrine\RegistrationSupplement\RegistrationTeamSupplement;
use Skadmin\TournamentGame\Doctrine\Team\Team;
use Skadmin\TournamentGame\Doctrine\TeamPlayer\TeamPlayer;
use Skadmin\TournamentGame\Doctrine\Tournament\Tournament;
use Skadmin\TournamentGame\Doctrine\TournamentPayment\TournamentPayment;
use SkadminUtils\DoctrineTraits\Entity;

use function in_array;
use function sprintf;

#[ORM\Entity]
#[ORM\Table(name: 'tournament_game_registration_team')]
#[ORM\HasLifecycleCallbacks]
class RegistrationTeam
{
    use Entity\Id;
    use Entity\HumanName {
        getFullName as getTraitFullName;
    }
    use Entity\Nickname;
    use Entity\Contact;
    use Entity\Facebook;
    use Entity\Status;
    use Entity\Created;
    use Entity\VariableSymbol;

    public const STATUS_NEW       = 1;
    public const STATUS_PAID      = 2;
    public const STATUS_CANCALED  = 3;
    public const STATUS_CONFIRMED = 4;
    public const STATUS           = [
        self::STATUS_NEW       => 'registration-user.status.new',
        self::STATUS_CANCALED  => 'registration-user.status.cancaled',
        self::STATUS_PAID      => 'registration-user.status.paid',
        self::STATUS_CONFIRMED => 'registration-user.status.confirmed',
    ];

    #[ORM\Column]
    private string $teamNickname = '';

    #[ORM\ManyToOne(targetEntity: Payment::class)]
    private Payment $payment;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    private ?DateTimeInterface $paidAt = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    private ?DateTimeInterface $confirmedAt = null;

    #[ORM\Column]
    private int $price = 0;

    #[ORM\Column]
    private int $pricePayment = 0;

    #[ORM\ManyToOne(targetEntity: Tournament::class, inversedBy: 'registrationsTeam')]
    #[ORM\JoinColumn(onDelete: 'cascade')]
    private Tournament $tournament;

    #[ORM\ManyToOne(targetEntity: Team::class, inversedBy: 'registrationsTeam')]
    #[ORM\JoinColumn(onDelete: 'SET NULL')]
    private ?Team $team;

    #[ORM\ManyToOne(targetEntity: Customer::class)]
    #[ORM\JoinColumn(onDelete: 'cascade')]
    private Customer $captain;

    /** @var ArrayCollection|Collection|RegistrationTeamSupplement[] */
    #[ORM\OneToMany(targetEntity: RegistrationTeamSupplement::class, mappedBy: 'registration')]
    private $supplements;

    /** @var ArrayCollection|Collection|Customer[] */
    #[ORM\ManyToMany(targetEntity: Customer::class)]
    #[ORM\JoinTable(name: 'tournament_game_registration_team_list_of_rel_player')]
    private $listOfPlayers;

    /** @var ArrayCollection|Collection|Tournament[] */
    #[ORM\OneToMany(targetEntity: Tournament::class, mappedBy: 'winnerRegistrationTeam')]
    #[ORM\OrderBy(['termFrom' => 'DESC'])]
    private $wonTournaments;

    /** @var ArrayCollection|Collection|Tournament[] */
    #[ORM\OneToMany(targetEntity: Tournament::class, mappedBy: 'secondPlaceRegistrationTeam')]
    #[ORM\OrderBy(['termFrom' => 'DESC'])]
    private $secondPlaces;

    /** @var ArrayCollection|Collection|Tournament[] */
    #[ORM\OneToMany(targetEntity: Tournament::class, mappedBy: 'thirdPlaceRegistrationTeam')]
    #[ORM\OrderBy(['termFrom' => 'DESC'])]
    private $thirdPlaces;

    public function __construct()
    {
        $this->status        = self::STATUS_NEW;
        $this->supplements   = new ArrayCollection();
        $this->listOfPlayers = new ArrayCollection();
    }

    /**
     * @param Customer[]|array $listOfPlayers
     */
    public function create(Tournament $tournament, Team $team, Customer $captain, string $email, string $phone, string $facebook, TournamentPayment $tournamentPayment, string $teamNickname, array $listOfPlayers): void
    {
        $this->tournament = $tournament;
        $this->team       = $team;
        $this->captain    = $captain;

        $this->name     = $captain->getName();
        $this->surname  = $captain->getSurname();
        $this->nickname = $captain->getNickname();

        $this->payment      = $tournamentPayment->getPayment();
        $this->pricePayment = $tournamentPayment->getPrice();

        $this->email    = $email;
        $this->phone    = $phone;
        $this->facebook = $facebook;

        $this->teamNickname = $teamNickname;
        $this->setListOfPlayers($listOfPlayers);

        $this->recalculate();
    }

    /**
     * @param Customer[]|array $listOfPlayers
     */
    public function update(Customer $captain, string $email, string $phone, string $facebook, TournamentPayment $tournamentPayment, string $teamNickname, array $listOfPlayers): void
    {
        $this->captain = $captain;

        $this->name     = $captain->getName();
        $this->surname  = $captain->getSurname();
        $this->nickname = $captain->getNickname();

        $this->payment      = $tournamentPayment->getPayment();
        $this->pricePayment = $tournamentPayment->getPrice();

        $this->email    = $email;
        $this->phone    = $phone;
        $this->facebook = $facebook;

        $this->teamNickname = $teamNickname;
        $this->setListOfPlayers($listOfPlayers);

        $this->recalculate();
    }

    public function recalculate(): int
    {
        $this->price = $this->pricePayment;

        foreach ($this->supplements as $supplement) {
            $this->price += $supplement->getPrice();
        }

        return $this->price;
    }

    public function getTeamNickname(): string
    {
        return $this->teamNickname;
    }

    public function getPayment(): Payment
    {
        return $this->payment;
    }

    public function getPaymentText(): string
    {
        return $this->payment->getName();
    }

    public function getPaidAt(): ?DateTimeInterface
    {
        return $this->paidAt;
    }

    public function getConfirmedAt(): ?DateTimeInterface
    {
        return $this->confirmedAt;
    }

    public function getPrice(): int
    {
        return $this->price;
    }

    public function getPricePayment(): int
    {
        return $this->pricePayment;
    }

    public function getTournament(): Tournament
    {
        return $this->tournament;
    }

    public function getTeam(): ?Team
    {
        return $this->team;
    }

    public function setStatus(int $status): void
    {
        if ($this->status === $status) {
            return;
        }

        $this->status = $status;

        if (! in_array($this->status, [self::STATUS_PAID, self::STATUS_CONFIRMED], true)) {
            $this->paidAt      = null;
            $this->confirmedAt = null;

            return;
        }

        if ($this->status === self::STATUS_PAID || ($this->status === self::STATUS_CONFIRMED && $this->paidAt === null)) { //@phpstan-ignore-line
            $this->paidAt = new DateTime();

            if ($this->status === self::STATUS_PAID) {
                $this->confirmedAt = null;
            }
        }

        if ($this->status !== self::STATUS_CONFIRMED) {
            return;
        }

        $this->confirmedAt = new DateTime();
    }

    public function getStatusText(): string
    {
        return self::STATUS[$this->getStatus()];
    }

    /**
     * @return ArrayCollection|Collection|RegistrationTeamSupplement[]
     */
    public function getSupplements()
    {
        return $this->supplements;
    }

    /**
     * First "name" OR with $reverse[true] first "surname"
     */
    public function getFullName(bool $reverse = false): string
    {
        if ($this->getNickname() === '') {
            return $this->getTraitFullName($reverse);
        }

        if ($reverse) {
            return sprintf('%s "%s" %s', $this->getSurname(), $this->getNickname(), $this->getName());
        }

        return sprintf('%s "%s" %s', $this->getName(), $this->getNickname(), $this->getSurname());
    }

    /**
     * @param Customer[] $listOfPlayers
     */
    public function setListOfPlayers(array $listOfPlayers): void
    {
        $this->listOfPlayers->clear();

        foreach ($listOfPlayers as $player) {
            $this->addPlayerToList($player);
        }
    }

    public function addPlayerToList(Customer $player): void
    {
        if ($this->listOfPlayers->contains($player)) {
            return;
        }

        $this->listOfPlayers->add($player);
    }

    /**
     * @return ArrayCollection|Collection|Customer[]
     */
    public function getListOfPlayers()
    {
        return $this->listOfPlayers;
    }

    public function isPlayerIn(Customer $player): bool
    {
        return $this->listOfPlayers->contains($player);
    }

    public function getCaptain(): Customer
    {
        return $this->captain;
    }

    public function isCaptionOrManager(Customer $player): bool
    {
        foreach ($this->getTeam()->getPlayers() as $teamPlayer) {
            if ($player === $teamPlayer->getUser() && in_array($teamPlayer->getRole(), [TeamPlayer::ROLE_CAPTAIN, TeamPlayer::ROLE_MANAGER])) {
                return true;
            }
        }

        return false;
    }

    public function isInRole(Customer $player, array $roles): bool
    {
        foreach ($this->getTeam()->getPlayers() as $teamPlayer) {
            if ($player === $teamPlayer->getUser() && in_array($teamPlayer->getRole(), $roles, true)) {
                return true;
            }
        }

        return false;
    }
}
