<?php

declare(strict_types=1);

namespace Skadmin\TournamentGame\Doctrine\Registration;

use App\Model\Doctrine\Customer\Customer;
use App\Model\Doctrine\Customer\CustomerFacade;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\QueryBuilder;
use Nette\Utils\ArrayHash;
use Nettrine\ORM\EntityManagerDecorator;
use Skadmin\TournamentGame\Doctrine\RegistrationSupplement\RegistrationTeamSupplementFacade;
use Skadmin\TournamentGame\Doctrine\Supplement\Supplement;
use Skadmin\TournamentGame\Doctrine\Team\Team;
use Skadmin\TournamentGame\Doctrine\Tournament\Tournament;
use Skadmin\TournamentGame\Doctrine\TournamentPayment\TournamentPayment;
use SkadminUtils\DoctrineTraits\Facade;

use function date;
use function intval;
use function sprintf;

final class RegistrationTeamFacade extends Facade
{
    use Facade\VariableSymbol;

    private RegistrationTeamSupplementFacade $facadeRegistrationTeamSupplement;
    private CustomerFacade                   $facadeCustomer;

    public function __construct(EntityManagerDecorator $em, RegistrationTeamSupplementFacade $facadeRegistrationTeamSupplement, CustomerFacade $facadeCustomer)
    {
        parent::__construct($em);
        $this->table                            = RegistrationTeam::class;
        $this->facadeRegistrationTeamSupplement = $facadeRegistrationTeamSupplement;
        $this->facadeCustomer                   = $facadeCustomer;
    }

    /**
     * @param int[]        $playerIds
     * @param Supplement[] $supplements
     */
    public function create(Tournament $tournament, Team $team, Customer $captain, string $email, string $phone, string $facebook, TournamentPayment $payment, string $teamNickname, array $playerIds = [], array $supplements = [], bool $ignoreFullCapacity = false): ?RegistrationTeam
    {
        if (! $tournament->isFull() || $ignoreFullCapacity) {
            $registration = $this->get();

            $listOfPlayers = [];
            foreach ($playerIds as $playerId) {
                $player = $this->facadeCustomer->get($playerId);

                if (! $player->isLoaded()) {
                    continue;
                }

                $listOfPlayers[] = $player;
            }

            $registration->create($tournament, $team, $captain, $email, $phone, $facebook, $payment, $teamNickname, $listOfPlayers);
            $prefix  = intval(date('Ym'));
            $prefix += 20; // VS for team registration
            $registration->setVariableSymbol($this->getValidVariableSymbol($prefix));

            $this->em->persist($registration);
            $this->em->flush();

            $registrationSupplements = [];
            foreach ($supplements as $supplement) {
                $registrationSupplements[] = $this->facadeRegistrationTeamSupplement->create($registration, $supplement);
            }

            $registrationId = $registration->getId();
            $this->em->detach($registration);

            $registration = $this->get($registrationId);
            $registration->recalculate();

            $this->em->flush();

            return $registration;
        }

        return null;
    }

    /**
     * @param int[]        $playerIds
     * @param Supplement[] $supplements
     */
    public function update(int $id, Customer $captain, string $email, string $phone, string $facebook, TournamentPayment $payment, string $teamNickname, array $playerIds = [], array $supplements = []): ?RegistrationTeam
    {
        $registration = $this->get($id);

        if (! $registration->isLoaded()) {
            return null;
        }

        $listOfPlayers = [];
        foreach ($playerIds as $playerId) {
            $player = $this->facadeCustomer->get($playerId);

            if (! $player->isLoaded()) {
                continue;
            }

            $listOfPlayers[] = $player;
        }

        $registration->update($captain, $email, $phone, $facebook, $payment, $teamNickname, $listOfPlayers);

        $this->em->persist($registration);
        $this->em->flush();

        $registrationSupplements = [];
        $currentSupplements      = $registration->getSupplements();

        // remove no selected supplements
        $criteria = Criteria::create();
        $criteria->where(Criteria::expr()->notIn('supplement', $supplements));
        foreach ($currentSupplements->matching($criteria)->toArray() as $supplementForRemove) {
            $this->em->remove($supplementForRemove);
        }

        // add new supplement
        foreach ($supplements as $supplement) {
            $criteria = Criteria::create();
            $criteria->where(Criteria::expr()->eq('supplement', $supplement));

            // when this supplement not yet in registration
            if ($currentSupplements->matching($criteria)->count() === 0) {
                $registrationSupplements[] = $this->facadeRegistrationTeamSupplement->create($registration, $supplement);
            } else {
                // tato část se může časem odebrat...
                $fixRemove = $currentSupplements->matching($criteria)->toArray();
                unset($fixRemove[0]);

                foreach ($fixRemove as $supplementForRemove) {
                    $this->em->remove($supplementForRemove);
                }
            }
        }

        $this->em->flush();

        $this->em->detach($registration);
        $registration = $this->get($id);
        $registration->recalculate();

        $this->em->flush();

        return $registration;
    }

    public function get(?int $id = null): RegistrationTeam
    {
        if ($id === null) {
            return new RegistrationTeam();
        }

        $registration = parent::get($id);

        if ($registration === null) {
            return new RegistrationTeam();
        }

        return $registration;
    }

    public function getModelForTournament(Tournament $tournament): QueryBuilder
    {
        $joinTables = [
            'team' => ArrayHash::from([
                'table'         => Team::class,
                'conditionType' => Join::WITH,
                'condition'     => sprintf('%s.team = %s.id', 'registration', 'team'),
            ]),
        ];

        return $this->getModelWithJoinTable($this->table, 'registration', $joinTables)
            ->where('registration.tournament = ?1')
            ->setParameters([1 => $tournament]);
    }

    /**
     * @param RegistrationTeam|int $registration
     */
    public function updateStatus($registration, int $status): RegistrationTeam
    {
        if (! $registration instanceof RegistrationTeam) {
            $registration = $this->get($registration);
        }

        $registration->setStatus($status);

        $this->em->persist($registration);
        $this->em->flush();

        return $registration;
    }

    public function findByVariableSymbol(string $variableSymbol): ?RegistrationTeam
    {
        $criteria = ['variableSymbol' => $variableSymbol];

        return $this->em
            ->getRepository($this->table)
            ->findOneBy($criteria);
    }
}
