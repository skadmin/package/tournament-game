<?php

declare(strict_types=1);

namespace Skadmin\TournamentGame\Doctrine\Registration;

use App\Model\Doctrine\Customer\Customer;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Nette\Utils\DateTime;
use Skadmin\Payment\Doctrine\Payment\Payment;
use Skadmin\TournamentGame\Doctrine\RegistrationSupplement\RegistrationUserSupplement;
use Skadmin\TournamentGame\Doctrine\Team\Team;
use Skadmin\TournamentGame\Doctrine\Tournament\Tournament;
use Skadmin\TournamentGame\Doctrine\TournamentPayment\TournamentPayment;
use SkadminUtils\DoctrineTraits\Entity;

use function in_array;
use function sprintf;

#[ORM\Entity]
#[ORM\Table(name: 'tournament_game_registration_user')]
#[ORM\HasLifecycleCallbacks]
class RegistrationUser
{
    use Entity\Id;
    use Entity\HumanName {
        getFullName as getTraitFullName;
    }
    use Entity\Nickname;
    use Entity\Contact;
    use Entity\Facebook;
    use Entity\Status;
    use Entity\Created;
    use Entity\VariableSymbol;

    public const STATUS_NEW       = 1;
    public const STATUS_PAID      = 2;
    public const STATUS_CANCALED  = 3;
    public const STATUS_CONFIRMED = 4;
    public const STATUS           = [
        self::STATUS_NEW       => 'registration-user.status.new',
        self::STATUS_CANCALED  => 'registration-user.status.cancaled',
        self::STATUS_PAID      => 'registration-user.status.paid',
        self::STATUS_CONFIRMED => 'registration-user.status.confirmed',
    ];

    #[ORM\ManyToOne(targetEntity: Payment::class)]
    private Payment $payment;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    private ?DateTimeInterface $paidAt = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    private ?DateTimeInterface $confirmedAt = null;

    #[ORM\Column]
    private int $price = 0;

    #[ORM\Column]
    private int $pricePayment = 0;

    #[ORM\ManyToOne(targetEntity: Tournament::class, inversedBy: 'registrationsUser')]
    #[ORM\JoinColumn(onDelete: 'cascade')]
    private Tournament $tournament;

    #[ORM\ManyToOne(targetEntity: Team::class)]
    #[ORM\JoinColumn(onDelete: 'SET NULL')]
    private ?Team $team;

    #[ORM\ManyToOne(targetEntity: Customer::class, inversedBy: 'registrationsUser')]
    #[ORM\JoinColumn(onDelete: 'cascade')]
    private Customer $user;

    /** @var ArrayCollection|Collection|RegistrationUserSupplement[] */
    #[ORM\OneToMany(targetEntity: RegistrationUserSupplement::class, mappedBy: 'registration')]
    private $supplements;

    public function __construct()
    {
        $this->status      = self::STATUS_NEW;
        $this->supplements = new ArrayCollection();
    }

    public function create(Tournament $tournament, Customer $user, string $email, string $phone, string $facebook, TournamentPayment $tournamentPayment, ?Team $team): void
    {
        $this->tournament = $tournament;
        $this->user       = $user;
        $this->team       = $team;

        $this->name     = $user->getName();
        $this->surname  = $user->getSurname();
        $this->nickname = $user->getNickname();

        $this->payment      = $tournamentPayment->getPayment();
        $this->pricePayment = $tournamentPayment->getPrice();

        $this->email    = $email;
        $this->phone    = $phone;
        $this->facebook = $facebook;

        $this->recalculate();
    }

    public function recalculate(): int
    {
        $this->price = $this->pricePayment;

        foreach ($this->supplements as $supplement) {
            $this->price += $supplement->getPrice();
        }

        return $this->price;
    }

    public function getPayment(): Payment
    {
        return $this->payment;
    }

    public function getPaymentText(): string
    {
        return $this->payment->getName();
    }

    public function getPaidAt(): ?DateTimeInterface
    {
        return $this->paidAt;
    }

    public function getConfirmedAt(): ?DateTimeInterface
    {
        return $this->confirmedAt;
    }

    public function getPrice(): int
    {
        return $this->price;
    }

    public function getPricePayment(): int
    {
        return $this->pricePayment;
    }

    public function getTournament(): Tournament
    {
        return $this->tournament;
    }

    public function getTeam(): ?Team
    {
        return $this->team;
    }

    public function getUser(): Customer
    {
        return $this->user;
    }

    public function setStatus(int $status): void
    {
        if ($this->status === $status) {
            return;
        }

        $this->status = $status;

        if (! in_array($this->status, [self::STATUS_PAID, self::STATUS_CONFIRMED], true)) {
            $this->paidAt      = null;
            $this->confirmedAt = null;

            return;
        }

        if ($this->status === self::STATUS_PAID || ($this->status === self::STATUS_CONFIRMED && $this->paidAt === null)) { //@phpstan-ignore-line
            $this->paidAt = new DateTime();

            if ($this->status === self::STATUS_PAID) {
                $this->confirmedAt = null;
            }
        }

        if ($this->status !== self::STATUS_CONFIRMED) {
            return;
        }

        $this->confirmedAt = new DateTime();
    }

    public function getStatusText(): string
    {
        return self::STATUS[$this->getStatus()];
    }

    /**
     * @return ArrayCollection|Collection|RegistrationUserSupplement[]
     */
    public function getSupplements()
    {
        return $this->supplements;
    }

    /**
     * First "name" OR with $reverse[true] first "surname"
     */
    public function getFullName(bool $reverse = false): string
    {
        if ($this->getNickname() === '') {
            return $this->getTraitFullName($reverse);
        }

        if ($reverse) {
            return sprintf('%s "%s" %s', $this->getSurname(), $this->getNickname(), $this->getName());
        }

        return sprintf('%s "%s" %s', $this->getName(), $this->getNickname(), $this->getSurname());
    }
}
