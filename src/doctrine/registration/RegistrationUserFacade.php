<?php

declare(strict_types=1);

namespace Skadmin\TournamentGame\Doctrine\Registration;

use App\Model\Doctrine\Customer\Customer;
use Doctrine\ORM\QueryBuilder;
use Nettrine\ORM\EntityManagerDecorator;
use Skadmin\TournamentGame\Doctrine\RegistrationSupplement\RegistrationUserSupplementFacade;
use Skadmin\TournamentGame\Doctrine\Supplement\Supplement;
use Skadmin\TournamentGame\Doctrine\Team\Team;
use Skadmin\TournamentGame\Doctrine\Tournament\Tournament;
use Skadmin\TournamentGame\Doctrine\TournamentPayment\TournamentPayment;
use SkadminUtils\DoctrineTraits\Facade;

final class RegistrationUserFacade extends Facade
{
    use Facade\VariableSymbol;

    private RegistrationUserSupplementFacade $facadeRegistrationUserSupplement;

    public function __construct(EntityManagerDecorator $em, RegistrationUserSupplementFacade $facadeRegistrationUserSupplement)
    {
        parent::__construct($em);
        $this->table                            = RegistrationUser::class;
        $this->facadeRegistrationUserSupplement = $facadeRegistrationUserSupplement;
    }

    /**
     * @param Supplement[] $supplements
     */
    public function create(Tournament $tournament, Customer $user, string $email, string $phone, string $facebook, TournamentPayment $payment, array $supplements = [], ?Team $team = null): ?RegistrationUser
    {
        if (! $tournament->isUserIn($user, true) && ! $tournament->isFull()) {
            $registration = $this->get();

            $registration->create($tournament, $user, $email, $phone, $facebook, $payment, $team);
            $registration->setVariableSymbol($this->getValidVariableSymbol());

            $this->em->persist($registration);
            $this->em->flush();

            $registrationSupplements = [];
            foreach ($supplements as $supplement) {
                $registrationSupplements[] = $this->facadeRegistrationUserSupplement->create($registration, $supplement);
            }

            $registrationId = $registration->getId();
            $this->em->detach($registration);

            $registration = $this->get($registrationId);
            $registration->recalculate();

            $this->em->persist($registration);
            $this->em->flush();

            return $registration;
        }

        return null;
    }

    public function get(?int $id = null): RegistrationUser
    {
        if ($id === null) {
            return new RegistrationUser();
        }

        $registration = parent::get($id);

        if ($registration === null) {
            return new RegistrationUser();
        }

        return $registration;
    }

    public function getModelForTournament(Tournament $tournament): QueryBuilder
    {
        return $this->getModel()
            ->where('a.tournament = ?1')
            ->setParameters([1 => $tournament]);
    }

    public function updateStatus(RegistrationUser|int $registration, int $status): RegistrationUser
    {
        if (! $registration instanceof RegistrationUser) {
            $registration = $this->get($registration);
        }

        $registration->setStatus($status);

        $this->em->persist($registration);
        $this->em->flush();

        return $registration;
    }

    public function findByVariableSymbol(string $variableSymbol): ?RegistrationUser
    {
        $criteria = ['variableSymbol' => $variableSymbol];

        return $this->em
            ->getRepository($this->table)
            ->findOneBy($criteria);
    }
}
