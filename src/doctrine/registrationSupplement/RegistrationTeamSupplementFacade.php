<?php

declare(strict_types=1);

namespace Skadmin\TournamentGame\Doctrine\RegistrationSupplement;

use Nettrine\ORM\EntityManagerDecorator;
use Skadmin\TournamentGame\Doctrine\Registration\RegistrationTeam;
use Skadmin\TournamentGame\Doctrine\Supplement\Supplement;
use SkadminUtils\DoctrineTraits\Facade;

final class RegistrationTeamSupplementFacade extends Facade
{
    use Facade\Webalize;

    public function __construct(EntityManagerDecorator $em)
    {
        parent::__construct($em);
        $this->table = RegistrationTeamSupplement::class;
    }

    public function create(RegistrationTeam $registration, Supplement $supplement): RegistrationTeamSupplement
    {
        $registrationSupplement = $this->get();
        $registrationSupplement->create($registration, $supplement);

        $this->em->persist($registrationSupplement);
        $this->em->flush();

        return $registrationSupplement;
    }

    public function get(?int $id = null): RegistrationTeamSupplement
    {
        if ($id === null) {
            return new RegistrationTeamSupplement();
        }

        $registrationSupplement = parent::get($id);

        if ($registrationSupplement === null) {
            return new RegistrationTeamSupplement();
        }

        return $registrationSupplement;
    }
}
