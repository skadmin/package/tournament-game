<?php

declare(strict_types=1);

namespace Skadmin\TournamentGame\Doctrine\RegistrationSupplement;

use Doctrine\ORM\Mapping as ORM;
use Skadmin\TournamentGame\Doctrine\Registration\RegistrationUser;
use Skadmin\TournamentGame\Doctrine\Supplement\Supplement;
use SkadminUtils\DoctrineTraits\Entity;

#[ORM\Entity]
#[ORM\Table(name: 'tournament_game_registration_user_supplement')]
#[ORM\HasLifecycleCallbacks]
class RegistrationUserSupplement
{
    use Entity\Id;

    #[ORM\Column]
    private int $price = 0;

    #[ORM\ManyToOne(targetEntity: Supplement::class)]
    #[ORM\JoinColumn(onDelete: 'cascade')]
    private Supplement $supplement;

    #[ORM\ManyToOne(targetEntity: RegistrationUser::class, inversedBy: 'supplements', cascade: ['persist'])]
    #[ORM\JoinColumn(onDelete: 'cascade')]
    private RegistrationUser $registration;

    public function create(RegistrationUser $registration, Supplement $supplement): void
    {
        $this->registration = $registration;
        $this->supplement   = $supplement;
        $this->price        = $supplement->getPrice();
    }

    public function update(int $price): void
    {
        $this->price = $price;
    }

    public function getRegistration(): RegistrationUser
    {
        return $this->registration;
    }

    public function getPrice(): int
    {
        return $this->price;
    }

    public function getSupplement(): Supplement
    {
        return $this->supplement;
    }
}
