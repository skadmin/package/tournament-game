<?php

declare(strict_types=1);

namespace Skadmin\TournamentGame\Doctrine\RegistrationSupplement;

use Nettrine\ORM\EntityManagerDecorator;
use Skadmin\TournamentGame\Doctrine\Registration\RegistrationUser;
use Skadmin\TournamentGame\Doctrine\Supplement\Supplement;
use SkadminUtils\DoctrineTraits\Facade;

final class RegistrationUserSupplementFacade extends Facade
{
    use Facade\Webalize;

    public function __construct(EntityManagerDecorator $em)
    {
        parent::__construct($em);
        $this->table = RegistrationUserSupplement::class;
    }

    public function create(RegistrationUser $registration, Supplement $supplement): RegistrationUserSupplement
    {
        $registrationSupplement = $this->get();
        $registrationSupplement->create($registration, $supplement);

        $this->em->persist($registrationSupplement);
        $this->em->flush();

        return $registrationSupplement;
    }

    public function get(?int $id = null): RegistrationUserSupplement
    {
        if ($id === null) {
            return new RegistrationUserSupplement();
        }

        $registrationSupplement = parent::get($id);

        if ($registrationSupplement === null) {
            return new RegistrationUserSupplement();
        }

        return $registrationSupplement;
    }
}
