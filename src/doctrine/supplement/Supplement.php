<?php

declare(strict_types=1);

namespace Skadmin\TournamentGame\Doctrine\Supplement;

use Doctrine\ORM\Mapping as ORM;
use SkadminUtils\DoctrineTraits\Entity;

#[ORM\Entity]
#[ORM\Table(name: 'tournament_game_supplement')]
#[ORM\HasLifecycleCallbacks]
class Supplement
{
    use Entity\Id;
    use Entity\Name;
    use Entity\Content;
    use Entity\IsActive;

    #[ORM\Column]
    private int $price = 0;

    public function getPrice(): int
    {
        return $this->price;
    }

    public function update(string $name, bool $isActive, string $content, int $price): void
    {
        $this->name     = $name;
        $this->isActive = $isActive;
        $this->content  = $content;
        $this->price    = $price;
    }
}
