<?php

declare(strict_types=1);

namespace Skadmin\TournamentGame\Doctrine\Supplement;

use Nettrine\ORM\EntityManagerDecorator;
use SkadminUtils\DoctrineTraits\Facade;

final class SupplementFacade extends Facade
{
    public function __construct(EntityManagerDecorator $em)
    {
        parent::__construct($em);
        $this->table = Supplement::class;
    }

    public function create(string $name, bool $isActive, string $content, int $price): Supplement
    {
        return $this->update(null, $name, $isActive, $content, $price);
    }

    public function update(?int $id, string $name, bool $isActive, string $content, int $price): Supplement
    {
        $supplement = $this->get($id);
        $supplement->update($name, $isActive, $content, $price);

        $this->em->persist($supplement);
        $this->em->flush();

        return $supplement;
    }

    public function get(?int $id = null): Supplement
    {
        if ($id === null) {
            return new Supplement();
        }

        $supplement = parent::get($id);

        if ($supplement === null) {
            return new Supplement();
        }

        return $supplement;
    }

    /**
     * @return Supplement[]
     */
    public function getAll(bool $onlyActive = false): array
    {
        $criteria = [];

        if ($onlyActive) {
            $criteria['isActive'] = true;
        }

        return $this->em
            ->getRepository($this->table)
            ->findBy($criteria);
    }
}
