<?php

declare(strict_types=1);

namespace Skadmin\TournamentGame\Doctrine\Team;

use App\Model\Doctrine\Customer\Customer;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping as ORM;
use Nette\Utils\Arrays;
use Skadmin\Nationality\Doctrine\Nationality\Nationality;
use Skadmin\TournamentGame\Doctrine\Registration\RegistrationTeam;
use Skadmin\TournamentGame\Doctrine\TeamInvitation\TeamInvitation;
use Skadmin\TournamentGame\Doctrine\TeamPlayer\TeamPlayer;
use Skadmin\TournamentGame\Doctrine\Tournament\Tournament;
use SkadminUtils\DoctrineTraits\Entity;

use function in_array;
use function krsort;
use function sprintf;

#[ORM\Entity]
#[ORM\Table(name: 'tournament_game_team')]
#[ORM\HasLifecycleCallbacks]
class Team
{
    use Entity\Id;
    use Entity\Code;
    use Entity\Created;
    use Entity\WebalizeName;
    use Entity\Nickname;
    use Entity\ImagePreview;
    use Entity\Website;
    use Entity\Facebook;
    use Entity\Content;

    #[ORM\Column(options: ['default' => true])]
    private bool $isAcceptingInvitations = true;

    #[ORM\ManyToOne(targetEntity: Nationality::class)]
    #[ORM\JoinColumn(onDelete: 'cascade')]
    private Nationality $nationality;

    /** @var ArrayCollection|Collection|TeamPlayer[] */
    #[ORM\OneToMany(targetEntity: TeamPlayer::class, mappedBy: 'team')]
    private $players;

    /** @var ArrayCollection|Collection|TeamInvitation[] */
    #[ORM\OneToMany(targetEntity: TeamInvitation::class, mappedBy: 'team')]
    #[ORM\OrderBy(['isByUser' => 'ASC'])]
    private $invitations;

    /** @var ArrayCollection|Collection|RegistrationTeam[] */
    #[ORM\OneToMany(targetEntity: RegistrationTeam::class, mappedBy: 'team')]
    private $registrationsTeam;

    public function __construct()
    {
        $this->players     = new ArrayCollection();
        $this->invitations = new ArrayCollection();
    }

    public function update(string $name, string $nickname, Nationality $nationality, string $content, string $website, string $facebook, bool $isAcceptingInvitations, ?string $imagePreview): void
    {
        $this->name                   = $name;
        $this->nickname               = $nickname;
        $this->content                = $content;
        $this->website                = $website;
        $this->facebook               = $facebook;
        $this->isAcceptingInvitations = $isAcceptingInvitations;

        $this->nationality = $nationality;

        if ($imagePreview === null || $imagePreview === '') {
            return;
        }

        $this->imagePreview = $imagePreview;
    }

    public function setCaptain(TeamPlayer $captain): void
    {
        if (! $captain->isCaptain()) {
            return;
        }

        $this->players->add($captain);
    }

    public function addPlayer(TeamPlayer $player): void
    {
        $this->players->add($player);
    }

    public function getNationality(): Nationality
    {
        return $this->nationality;
    }

    /**
     * @return ArrayCollection|Collection|TeamPlayer[]
     */
    public function getPlayers(bool $onlyCaptain = false)
    {
        $criteria = Criteria::create();

        if ($onlyCaptain) {
            $criteria->where(Criteria::expr()->in('role', [TeamPlayer::ROLE_CAPTAIN, TeamPlayer::ROLE_DEPUTY_CAPTAIN]));
        }

        $criteria->orderBy([
            'role'      => 'ASC',
            'createdAt' => 'ASC',
        ]);

        return $this->players->matching($criteria);
    }

    public function isPlayerIn(Customer $customer): bool
    {
        $players = Arrays::map($this->getPlayers()->toArray(), static function (TeamPlayer $teamPlayer): Customer {
            return $teamPlayer->getUser();
        });

        return in_array($customer, $players, true);
    }

    public function getCaptain(): ?TeamPlayer
    {
        $criteria = Criteria::create();
        $criteria->where(Criteria::expr()->eq('role', TeamPlayer::ROLE_CAPTAIN));

        $captain = $this->players->matching($criteria)->first();

        return $captain === false ? null : $captain;
    }

    public function getUserTeamPlayer(Customer $user): ?TeamPlayer
    {
        $criteria = Criteria::create()
            ->where(Criteria::expr()->eq('user', $user));

        $teamPlayer = $this->players->matching($criteria)->first();

        return $teamPlayer ? $teamPlayer : null;
    }

    /**
     * @return ArrayCollection|Collection|TeamInvitation[]
     */
    public function getInvitations()
    {
        return $this->invitations;
    }

    /**
     * @return ArrayCollection|Collection|RegistrationTeam[]
     */
    public function getRegistrationsTeam()
    {
        return $this->registrationsTeam;
    }

    /**
     * @return ArrayCollection|Collection|Tournament[]
     */
    public function getWonTournaments()
    {
        $registrations = $this->registrationsTeam->filter(static function (RegistrationTeam $registration): bool {
            return $registration->getTournament()->getWinner() === $registration;
        });

        return $registrations->map(static function (RegistrationTeam $registration): Tournament {
            return $registration->getTournament();
        });
    }

    /**
     * @return ArrayCollection|Collection|Tournament[]
     */
    public function getSecondPlaces()
    {
        $registrations = $this->registrationsTeam->filter(static function (RegistrationTeam $registration): bool {
            return $registration->getTournament()->getSecondPlace() === $registration;
        });

        return $registrations->map(static function (RegistrationTeam $registration): Tournament {
            return $registration->getTournament();
        });
    }

    /**
     * @return ArrayCollection|Collection|Tournament[]
     */
    public function getThirdPlaces()
    {
        $registrations = $this->registrationsTeam->filter(static function (RegistrationTeam $registration): bool {
            return $registration->getTournament()->getThirdPlace() === $registration;
        });

        return $registrations->map(static function (RegistrationTeam $registration): Tournament {
            return $registration->getTournament();
        });
    }

    /**
     * @return Tournament[]
     */
    public function getAllTournamentsWithPlacement(): array
    {
        $tournaments = [];
        foreach ($this->getWonTournaments() as $tournament) {
            $tournamentId      = $tournament->getId() ?? 0;
            $key               = sprintf('%d%05d1', $tournament->getTermFrom()->format('Ymd'), $tournamentId % 9999);
            $tournaments[$key] = $tournament;
        }

        foreach ($this->getSecondPlaces() as $tournament) {
            $tournamentId      = $tournament->getId() ?? 0;
            $key               = sprintf('%d%05d2', $tournament->getTermFrom()->format('Ymd'), $tournamentId % 9999);
            $tournaments[$key] = $tournament;
        }

        foreach ($this->getThirdPlaces() as $tournament) {
            $tournamentId      = $tournament->getId() ?? 0;
            $key               = sprintf('%d%05d3', $tournament->getTermFrom()->format('Ymd'), $tournamentId % 9999);
            $tournaments[$key] = $tournament;
        }

        krsort($tournaments);

        return $tournaments;
    }

    public function isAcceptingInvitations(): bool
    {
        return $this->isAcceptingInvitations;
    }
}
