<?php

declare(strict_types=1);

namespace Skadmin\TournamentGame\Doctrine\Team;

use App\Model\Doctrine\Customer\Customer;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;
use Nettrine\ORM\EntityManagerDecorator;
use Skadmin\Nationality\Doctrine\Nationality\Nationality;
use Skadmin\TournamentGame\Doctrine\TeamInvitation\TeamInvitation;
use Skadmin\TournamentGame\Doctrine\TeamPlayer\TeamPlayer;
use Skadmin\TournamentGame\Doctrine\TeamPlayer\TeamPlayerFacade;
use SkadminUtils\DoctrineTraits\Facade;

use function array_filter;
use function assert;
use function intval;

final class TeamFacade extends Facade
{
    use Facade\Webalize;
    use Facade\Code;

    private TeamPlayerFacade $facadeTeamPlayer;

    public function __construct(EntityManagerDecorator $em, TeamPlayerFacade $facadeTeamPlayer)
    {
        parent::__construct($em);
        $this->table = Team::class;

        $this->facadeTeamPlayer = $facadeTeamPlayer;
    }

    public function getModelForGrid(): QueryBuilder
    {
        $repository = $this->em->getRepository($this->table);
        assert($repository instanceof EntityRepository);

        return $repository->createQueryBuilder('t')
            ->leftJoin('t.players', 'p')
            ->leftJoin('p.user', 'u');
    }

    public function create(string $name, string $nickname, Nationality $nationality, string $content, string $website, string $facebook, bool $isAcceptingInvitations, ?string $imagePreview, Customer $user): Team
    {
        $team = $this->update(null, $name, $nickname, $nationality, $content, $website, $facebook, $isAcceptingInvitations, $imagePreview);

        $captain = $this->facadeTeamPlayer->create($team, $user, TeamPlayer::ROLE_CAPTAIN);
        $team->setCaptain($captain);

        $this->em->persist($team);
        $this->em->flush();

        return $team;
    }

    public function addPlayerByInvitation(TeamInvitation $teamInvitation): Team
    {
        return $this->addPlayer($teamInvitation->getTeam(), $teamInvitation->getUser());
    }

    public function addPlayer(Team $team, Customer $user): Team
    {
        $player = $this->facadeTeamPlayer->create($team, $user);
        $team->addPlayer($player);
        $this->em->flush();

        return $team;
    }

    public function removePlayer(Team $team, Customer $user): bool
    {
        return $this->facadeTeamPlayer->removeUserFromTeam($team, $user);
    }

    public function update(?int $id, string $name, string $nickname, Nationality $nationality, string $content, string $website, string $facebook, bool $isAcceptingInvitations, ?string $imagePreview): Team
    {
        $team = $this->get($id);

        $team->update($name, $nickname, $nationality, $content, $website, $facebook, $isAcceptingInvitations, $imagePreview);

        if (! $team->isLoaded()) {
            $team->setCode($this->getValidCode());
            $team->setWebalize($this->getValidWebalize($name));
        }

        $this->em->persist($team);
        $this->em->flush();

        return $team;
    }

    public function get(?int $id = null): Team
    {
        if ($id === null) {
            return new Team();
        }

        $team = parent::get($id);

        if ($team === null) {
            return new Team();
        }

        return $team;
    }

    public function findByWebalize(string $webalize): ?Team
    {
        $criteria = ['webalize' => $webalize];

        return $this->em
            ->getRepository($this->table)
            ->findOneBy($criteria);
    }

    public function findByName(string $name): ?Team
    {
        $criteria = ['name' => $name];

        return $this->em
            ->getRepository($this->table)
            ->findOneBy($criteria);
    }

    public function findByNickname(string $nickname): ?Team
    {
        $criteria = ['nickname' => $nickname];

        return $this->em
            ->getRepository($this->table)
            ->findOneBy($criteria);
    }

    public function findByCode(string $code): ?Team
    {
        $criteria = ['code' => $code];

        return $this->em
            ->getRepository($this->table)
            ->findOneBy($criteria);
    }

    /**
     * @return Team[]|array
     */
    public function findWithMinimumPlayers(int $minPlayers): array
    {
        $teams = $this->em
            ->getRepository($this->table)
            ->findBy([], ['name' => Criteria::ASC]);

        return array_filter($teams, static function (Team $team) use ($minPlayers): bool {
            return $team->getPlayers()->count() >= $minPlayers;
        });
    }

    public function getCount(?TeamFilter $filter = null): int
    {
        $qb = $this->findQb($filter);
        $qb->select('count(a.id)');

        return intval($qb->getQuery()
            ->getSingleScalarResult());
    }

    /**
     * @return Customer[]
     */
    public function find(?TeamFilter $filter = null, ?int $limit = null, ?int $offset = null): array
    {
        $qb = $this->findQb($filter, $limit, $offset);

        return $qb->getQuery()
            ->getResult();
    }

    private function findQb(?TeamFilter $filter, ?int $limit = null, ?int $offset = null): QueryBuilder
    {
        $qb = $this->em
            ->getRepository($this->table)
            ->createQueryBuilder('a');
        assert($qb instanceof QueryBuilder);

        $qb->setMaxResults($limit)
            ->setFirstResult($offset)
            ->orderBy('a.name', 'ASC');

        $criteria = Criteria::create();

        /*
        bool $onlyActive = false
        if ($onlyActive) {
            $criteria->andWhere(Criteria::expr()->eq('a.isActive', true));
        }
         */
        if ($filter !== null) {
            $filter->modifyCriteria($criteria);
        }

        $qb->addCriteria($criteria);

        return $qb;
    }
}
