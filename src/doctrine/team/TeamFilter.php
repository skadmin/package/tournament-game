<?php

declare(strict_types=1);

namespace Skadmin\TournamentGame\Doctrine\Team;

use Doctrine\Common\Collections\Criteria;
use Nette\SmartObject;
use SkadminUtils\DoctrineTraits\ACriteriaFilter;

use function trim;

final class TeamFilter extends ACriteriaFilter
{
    use SmartObject;

    private string $phrase = '';

    public function __construct(string $phrase)
    {
        $this->phrase = $phrase;
    }

    public function getPhrase(): string
    {
        return trim($this->phrase);
    }

    public function setPhrase(string $phrase): self
    {
        $this->phrase = $phrase;

        return $this;
    }

    public function modifyCriteria(Criteria &$criteria, string $alias = 'a'): void
    {
        $expr = Criteria::expr();

        if ($this->getPhrase() === '') {
            return;
        }

        $criteria->andWhere($expr->orX(
            $expr->contains($this->getEntityName($alias, 'name'), $this->getPhrase()),
            $expr->contains($this->getEntityName($alias, 'nickname'), $this->getPhrase()),
            $expr->contains($this->getEntityName($alias, 'code'), $this->getPhrase())
        ));
    }
}
