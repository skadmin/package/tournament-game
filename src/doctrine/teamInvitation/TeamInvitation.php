<?php

declare(strict_types=1);

namespace Skadmin\TournamentGame\Doctrine\TeamInvitation;

use App\Model\Doctrine\Customer\Customer;
use Doctrine\ORM\Mapping as ORM;
use Skadmin\TournamentGame\Doctrine\Team\Team;
use SkadminUtils\DoctrineTraits\Entity;

#[ORM\Entity]
#[ORM\Table(name: 'tournament_game_team_invitation')]
#[ORM\HasLifecycleCallbacks]
class TeamInvitation
{
    use Entity\Id;
    use Entity\Created;
    use Entity\Hash;

    #[ORM\Column(options: ['default' => false])]
    private bool $isByUser = false;

    #[ORM\ManyToOne(targetEntity: Customer::class)]
    #[ORM\JoinColumn(onDelete: 'cascade')]
    private Customer $user;

    #[ORM\ManyToOne(targetEntity: Team::class, inversedBy: 'invitations')]
    #[ORM\JoinColumn(onDelete: 'cascade')]
    private Team $team;

    public function create(Team $team, Customer $customer): void
    {
        $this->team = $team;
        $this->user = $customer;
    }

    public function createByUser(Team $team, Customer $customer): void
    {
        $this->create($team, $customer);
        $this->isByUser = true;
    }

    public function isByUser(): bool
    {
        return $this->isByUser;
    }

    public function getUser(): Customer
    {
        return $this->user;
    }

    public function getTeam(): Team
    {
        return $this->team;
    }
}
