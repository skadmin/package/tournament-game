<?php

declare(strict_types=1);

namespace Skadmin\TournamentGame\Doctrine\TeamInvitation;

use App\Model\Doctrine\Customer\Customer;
use Nettrine\ORM\EntityManagerDecorator;
use Skadmin\TournamentGame\Doctrine\Team\Team;
use SkadminUtils\DoctrineTraits\Facade;

final class TeamInvitationFacade extends Facade
{
    use Facade\Hash;

    public function __construct(EntityManagerDecorator $em)
    {
        parent::__construct($em);
        $this->table = TeamInvitation::class;
    }

    public function create(Team $team, Customer $user, bool $byUser = false): TeamInvitation
    {
        $teamInvitation = $this->findInvitation($team, $user);

        if ($teamInvitation !== null) {
            return $teamInvitation;
        }

        $teamInvitation = $this->get();

        if ($byUser) {
            $teamInvitation->createByUser($team, $user);
        } else {
            $teamInvitation->create($team, $user);
        }

        if (! $teamInvitation->isLoaded()) {
            $teamInvitation->setHash($this->getValidHash());
        }

        $this->em->persist($teamInvitation);
        $this->em->flush();

        return $teamInvitation;
    }

    public function get(?int $id = null): TeamInvitation
    {
        if ($id === null) {
            return new TeamInvitation();
        }

        $teamInvitation = parent::get($id);

        if ($teamInvitation === null) {
            return new TeamInvitation();
        }

        return $teamInvitation;
    }

    private function findInvitation(Team $team, Customer $user): ?TeamInvitation
    {
        $criteria = [
            'team' => $team,
            'user' => $user,
        ];

        return $this->em
            ->getRepository($this->table)
            ->findOneBy($criteria);
    }

    public function findByHash(string $hash): ?TeamInvitation
    {
        $criteria = ['hash' => $hash];

        return $this->em
            ->getRepository($this->table)
            ->findOneBy($criteria);
    }

    public function delete(TeamInvitation $teamInvitation): void
    {
        $this->em->remove($teamInvitation);
        $this->em->flush();
    }
}
