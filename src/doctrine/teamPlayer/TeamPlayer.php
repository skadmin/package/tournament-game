<?php

declare(strict_types=1);

namespace Skadmin\TournamentGame\Doctrine\TeamPlayer;

use App\Model\Doctrine\Customer\Customer;
use Doctrine\ORM\Mapping as ORM;
use Skadmin\TournamentGame\Doctrine\Team\Team;
use SkadminUtils\DoctrineTraits\Entity;

use function in_array;

#[ORM\Entity]
#[ORM\Table(name: 'tournament_game_team_player')]
#[ORM\HasLifecycleCallbacks]
class TeamPlayer
{
    public const ROLE_CAPTAIN        = 1;
    public const ROLE_DEPUTY_CAPTAIN = 2;
    public const ROLE_PLAYER         = 3;
    public const ROLE_MANAGER        = 4;
    public const ROLE_COACH          = 5;

    public const ROLES = [
        self::ROLE_CAPTAIN        => 'tournament-game.player.captain',
        self::ROLE_DEPUTY_CAPTAIN => 'tournament-game.player.deputy-captain',
        self::ROLE_PLAYER         => 'tournament-game.player.player',
        self::ROLE_MANAGER        => 'tournament-game.player.manager',
        self::ROLE_COACH          => 'tournament-game.player.coach',
    ];
    use Entity\Id;
    use Entity\Created;

    #[ORM\Column(nullable: false)]
    private int $role = self::ROLE_PLAYER;

    #[ORM\ManyToOne(targetEntity: Customer::class, inversedBy: 'teams')]
    #[ORM\JoinColumn(onDelete: 'cascade')]
    private Customer $user;

    #[ORM\ManyToOne(targetEntity: Team::class, inversedBy: 'players')]
    #[ORM\JoinColumn(onDelete: 'cascade')]
    private Team $team;

    public function create(Team $team, Customer $customer, int $role): void
    {
        $this->team = $team;
        $this->user = $customer;
        $this->role = $role;
    }

    public function getRole(): int
    {
        return $this->role;
    }

    public function getRoleName(): string
    {
        return self::ROLES[$this->role];
    }

    public function getUser(): Customer
    {
        return $this->user;
    }

    public function getTeam(): Team
    {
        return $this->team;
    }

    public function isCaptain(): bool
    {
        return $this->getRole() === self::ROLE_CAPTAIN;
    }

    public function isDeputyCaptain(): bool
    {
        return $this->getRole() === self::ROLE_DEPUTY_CAPTAIN;
    }

    public function isAnyCaptain(): bool
    {
        return in_array($this->getRole(), [self::ROLE_CAPTAIN, self::ROLE_DEPUTY_CAPTAIN, self::ROLE_MANAGER], true);
    }

    public function isPlayer(): bool
    {
        return $this->getRole() === self::ROLE_PLAYER;
    }

    public function isManager(): bool
    {
        return $this->getRole() === self::ROLE_MANAGER;
    }

    public function isCoach(): bool
    {
        return $this->getRole() === self::ROLE_COACH;
    }

    public function promoteToCaptain(): void
    {
        $this->role = self::ROLE_CAPTAIN;
    }

    public function setRole(int $role): void
    {
        $this->role = $role;
    }
}
