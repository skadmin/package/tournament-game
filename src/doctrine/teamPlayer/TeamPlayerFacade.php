<?php

declare(strict_types=1);

namespace Skadmin\TournamentGame\Doctrine\TeamPlayer;

use App\Model\Doctrine\Customer\Customer;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\QueryBuilder;
use Nettrine\ORM\EntityManagerDecorator;
use Skadmin\TournamentGame\Doctrine\Team\Team;
use SkadminUtils\DoctrineTraits\Facade;

use function assert;

final class TeamPlayerFacade extends Facade
{
    public function __construct(EntityManagerDecorator $em)
    {
        parent::__construct($em);
        $this->table = TeamPlayer::class;
    }

    public function create(Team $team, Customer $customer, int $role = TeamPlayer::ROLE_PLAYER): TeamPlayer
    {
        $teamPlayer = $this->get();

        $teamPlayer->create($team, $customer, $role);

        $this->em->persist($teamPlayer);
        $this->em->flush();

        return $teamPlayer;
    }

    public function get(?int $id = null): TeamPlayer
    {
        if ($id === null) {
            return new TeamPlayer();
        }

        $teamPlayer = parent::get($id);

        if ($teamPlayer === null) {
            return new TeamPlayer();
        }

        return $teamPlayer;
    }

    public function removeUserFromTeam(Team $team, Customer $user): bool
    {
        $criteria = [
            'team' => $team,
            'user' => $user,
        ];

        $player = $this->em
            ->getRepository($this->table)
            ->findOneBy($criteria);
        assert($player instanceof TeamPlayer || $player === null);

        if ($player === null) {
            return false;
        }

        if ($player->isCaptain()) {
            $newCaptain = $this->findNewCaptain($team);

            if ($newCaptain instanceof TeamPlayer) {
                $newCaptain->promoteToCaptain();
            }
        }

        $this->em->remove($player);
        $this->em->flush();

        return true;
    }

    private function findNewCaptain(Team $team): ?TeamPlayer
    {
        $qb = $this->em
            ->getRepository($this->table)
            ->createQueryBuilder('a');
        assert($qb instanceof QueryBuilder);

        $criteria = Criteria::create();
        $criteria->where(Criteria::expr()->eq('a.team', $team));
        $criteria->andWhere(Criteria::expr()->neq('a.role', TeamPlayer::ROLE_CAPTAIN));
        $criteria->orderBy(['a.role' => Criteria::ASC]);

        $qb->addCriteria($criteria);

        $teamPlayer = $qb->getQuery()
            ->setMaxResults(1)
            ->getOneOrNullResult();

        if (! $teamPlayer instanceof TeamPlayer) {
            return null;
        }

        return $teamPlayer;
    }

    public function changeRole(TeamPlayer $teamPlayer, int $role): TeamPlayer
    {
        if ($role === TeamPlayer::ROLE_CAPTAIN) {
            $captain = $teamPlayer->getTeam()->getCaptain();

            if ($captain !== null) {
                $captain->setRole(TeamPlayer::ROLE_DEPUTY_CAPTAIN);
            }
        }

        $teamPlayer->setRole($role);
        $this->em->flush();

        return $teamPlayer;
    }
}
