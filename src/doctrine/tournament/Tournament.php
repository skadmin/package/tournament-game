<?php

declare(strict_types=1);

namespace Skadmin\TournamentGame\Doctrine\Tournament;

use App\Model\Doctrine\Customer\Customer;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use JetBrains\PhpStorm\Deprecated;
use Nette\Utils\DateTime;
use Nette\Utils\Strings;
use Skadmin\Game\Doctrine\Game\Game;
use Skadmin\Payment\Doctrine\Payment\Payment;
use Skadmin\TournamentGame\Doctrine\Registration\RegistrationTeam;
use Skadmin\TournamentGame\Doctrine\Registration\RegistrationUser;
use Skadmin\TournamentGame\Doctrine\Supplement\Supplement;
use Skadmin\TournamentGame\Doctrine\Team\Team;
use Skadmin\TournamentGame\Doctrine\TournamentBracket\TournamentBracket;
use Skadmin\TournamentGame\Doctrine\TournamentBracketStage\ATournamentBracketStage;
use Skadmin\TournamentGame\Doctrine\TournamentPayment\TournamentPayment;
use Skadmin\TournamentGame\Doctrine\TournamentResult\TournamentResult;
use Skadmin\TournamentGame\Doctrine\TournamentType\TournamentType;
use SkadminUtils\DoctrineTraits\Entity;

use function assert;
use function intval;
use function ksort;
use function sprintf;

use const PHP_INT_MAX;

#[ORM\Entity]
#[ORM\Table(name: 'tournament_game')]
#[ORM\HasLifecycleCallbacks]
class Tournament
{
    use Entity\Id;
    use Entity\WebalizeName;
    use Entity\IsActive;
    use Entity\Created;
    use Entity\Code;
    use Entity\Term;
    use Entity\Content;
    use Entity\Description;
    use Entity\PlaceOnTheMap;
    use Entity\ImagePreview;
    use Entity\IsLocked;

    #[ORM\Column(options: ['default' => false])]
    private bool $isOnline = false;

    #[ORM\Column(options: ['default' => false])]
    private bool $isUnpaid = false;

    #[ORM\Column(options: ['default' => false])]
    private bool $isRequiredGameId = false;

    #[ORM\Column(options: ['default' => false])]
    private bool $isOpenRegistration = false;

    #[ORM\Column(options: ['default' => true])]
    private bool $isConfirmationRequired = true;

    #[ORM\Column(nullable: true)]
    private ?int $confirmationFrom = null;

    #[ORM\Column(nullable: true)]
    private ?int $confirmationTo = null;

    #[ORM\Column]
    private int $numberOfParticipants = 0;

    #[ORM\Column(type: Types::TEXT)]
    private string $prizepool;

    #[ORM\Column(type: Types::TEXT)]
    private string $rules;

    #[ORM\Column(type: Types::TEXT)]
    private string $stream;

    #[ORM\Column(type: Types::TEXT)]
    private string $discord;

    #[ORM\Column(type: Types::TEXT)]
    private string $schedule;

    #[ORM\Column(options: ['default' => false])]
    private bool $isLiveChat = false;

    #[ORM\Column(options: ['default' => false])]
    private bool $isResult = false;

    #[ORM\Column(options: ['default' => false])]
    private bool $isResultOpponentRequired = false;

    #[ORM\Column(options: ['default' => true])]
    private bool $isResultFileRequired = true;

    #[ORM\ManyToOne(targetEntity: Game::class, inversedBy: 'tournaments')]
    #[ORM\JoinColumn(onDelete: 'cascade')]
    private Game $game;

    /** @var ArrayCollection|Collection|TournamentPayment[] */
    #[ORM\OneToMany(targetEntity: TournamentPayment::class, mappedBy: 'tournament')]
    private $payments;

    #[ORM\ManyToOne(targetEntity: TournamentType::class, inversedBy: 'tournaments')]
    #[ORM\JoinColumn(onDelete: 'cascade')]
    private ?TournamentType $tournamentType = null;

    /** @var ArrayCollection|Collection|RegistrationUser[] */
    #[ORM\OneToMany(targetEntity: RegistrationUser::class, mappedBy: 'tournament')]
    private $registrationsUser;

    /** @var ArrayCollection|Collection|RegistrationTeam[] */
    #[ORM\OneToMany(targetEntity: RegistrationTeam::class, mappedBy: 'tournament')]
    private $registrationsTeam;

    /** @var ArrayCollection|Collection|TournamentResult[] */
    #[ORM\OneToMany(targetEntity: TournamentResult::class, mappedBy: 'tournament')]
    #[ORM\OrderBy(['createdAt' => 'DESC'])]
    private $results;

    /** @var ArrayCollection|Collection|Supplement[] */
    #[ORM\ManyToMany(targetEntity: Supplement::class)]
    #[ORM\JoinTable(name: 'tournament_game_rel_supplement')]
    private $supplements;

    #[ORM\ManyToOne(targetEntity: Customer::class, inversedBy: 'wonTournaments')]
    #[ORM\JoinColumn(onDelete: 'cascade')]
    private ?Customer $winner;

    #[ORM\ManyToOne(targetEntity: Customer::class, inversedBy: 'secondPlaces')]
    #[ORM\JoinColumn(onDelete: 'cascade')]
    private ?Customer $secondPlace;

    #[ORM\ManyToOne(targetEntity: Customer::class, inversedBy: 'thirdPlaces')]
    #[ORM\JoinColumn(onDelete: 'cascade')]
    private ?Customer $thirdPlace;

    #[ORM\ManyToOne(targetEntity: RegistrationTeam::class, inversedBy: 'wonTournaments')]
    #[ORM\JoinColumn(onDelete: 'cascade')]
    private ?RegistrationTeam $winnerRegistrationTeam;

    #[ORM\ManyToOne(targetEntity: RegistrationTeam::class, inversedBy: 'secondPlaces')]
    #[ORM\JoinColumn(onDelete: 'cascade')]
    private ?RegistrationTeam $secondPlaceRegistrationTeam;

    #[ORM\ManyToOne(targetEntity: RegistrationTeam::class, inversedBy: 'thirdPlaces')]
    #[ORM\JoinColumn(onDelete: 'cascade')]
    private ?RegistrationTeam $thirdPlaceRegistrationTeam;

    #[ORM\OneToOne(targetEntity: TournamentBracket::class, mappedBy: 'tournament')]
    private ?TournamentBracket $bracket;

    public function __construct()
    {
        $this->registrationsUser = new ArrayCollection();
        $this->payments          = new ArrayCollection();
        $this->supplements       = new ArrayCollection();
        $this->results           = new ArrayCollection();
    }

    public function updateWebalize(string $webalize): void
    {
        $this->webalize = $webalize;
    }

    public function update(string $name, int $numberOfParticipants, ?int $confirmationFrom, ?int $confirmationFo, string $content, string $description, string $prizepool, string $rules, string $stream, string $discord, string $schedule, DateTimeInterface $termFrom, DateTimeInterface $termTo, string $placeName, string $placeAddress, string $placeGpsLat, string $placeGpsLng, ?string $imagePreview, Game $game, TournamentType $tournamentType): void
    {
        $this->name = $name;

        $this->numberOfParticipants = $numberOfParticipants;
        $this->confirmationFrom     = $confirmationFrom;
        $this->confirmationTo       = $confirmationFo;

        $this->content     = $content;
        $this->description = $description;
        $this->prizepool   = $prizepool;
        $this->rules       = $rules;
        $this->stream      = $stream;
        $this->discord     = $discord;
        $this->schedule    = $schedule;

        $this->termFrom = $termFrom;
        $this->termTo   = $termTo;

        $this->placeName    = $placeName;
        $this->placeAddress = $placeAddress;
        $this->placeGpsLat  = $placeGpsLat;
        $this->placeGpsLng  = $placeGpsLng;

        $this->game           = $game;
        $this->tournamentType = $tournamentType;

        if ($imagePreview === null || $imagePreview === '') {
            return;
        }

        $this->imagePreview = $imagePreview;
    }

    public function updateStates(bool $isActive, bool $isOnline, bool $isLiveChat, bool $isResult, bool $isResultOpponentRequired, bool $isResultFileRequired, bool $isLocked, bool $isRequiredGameId, bool $isOpenRegistration, bool $isConfirmationRequired): void
    {
        $this->isActive                 = $isActive;
        $this->isOnline                 = $isOnline;
        $this->isLiveChat               = $isLiveChat;
        $this->isResult                 = $isResult;
        $this->isResultOpponentRequired = $isResult ? $isResultOpponentRequired : false;
        $this->isResultFileRequired     = $isResultFileRequired;
        $this->isLocked                 = $isLocked;
        $this->isRequiredGameId         = $isRequiredGameId;
        $this->isOpenRegistration       = $isOpenRegistration;
        $this->isConfirmationRequired   = $isConfirmationRequired;
    }

    /**
     * @param Supplement[]|array $supplements
     */
    public function updatePriceSetting(bool $isUnpaid, array $supplements): void
    {
        $this->isUnpaid = $isUnpaid;

        $this->supplements->clear();
        foreach ($supplements as $supplement) {
            $this->supplements->add($supplement);
        }
    }

    public function updateWinner(Customer|RegistrationTeam|null $winner, Customer|RegistrationTeam|null $secondPlace, Customer|RegistrationTeam|null $thirdPlace): void
    {
        if ($this->isOneVsOne()) {
            $this->winner      = $winner;
            $this->secondPlace = $secondPlace;
            $this->thirdPlace  = $thirdPlace;

            $this->winnerRegistrationTeam      = null;
            $this->secondPlaceRegistrationTeam = null;
            $this->thirdPlaceRegistrationTeam  = null;
        } else {
            $this->winnerRegistrationTeam      = $winner;
            $this->secondPlaceRegistrationTeam = $secondPlace;
            $this->thirdPlaceRegistrationTeam  = $thirdPlace;

            $this->winner      = null;
            $this->secondPlace = null;
            $this->thirdPlace  = null;
        }
    }

    public function isOneVsOne(): bool
    {
        return $this->getTournamentType() === null || $this->getTournamentType()->getNumberOfParticipantsTo() === 1;
    }

    public function getTournamentType(): ?TournamentType
    {
        return $this->tournamentType;
    }

    public function getWinner(): Customer|RegistrationTeam|null
    {
        if ($this->isOneVsOne()) {
            return $this->winner;
        }

        return $this->winnerRegistrationTeam;
    }

    public function getSecondPlace(): Customer|RegistrationTeam|null
    {
        if ($this->isOneVsOne()) {
            return $this->secondPlace;
        }

        return $this->secondPlaceRegistrationTeam;
    }

    public function getThirdPlace(): Customer|RegistrationTeam|null
    {
        if ($this->isOneVsOne()) {
            return $this->thirdPlace;
        }

        return $this->thirdPlaceRegistrationTeam;
    }

    /**
     * @return ArrayCollection|Collection|Supplement[]
     */
    public function getSupplements()
    {
        return $this->supplements;
    }

    /**
     * @return ArrayCollection|Collection|TournamentResult[]
     */
    public function getResults()
    {
        return $this->results;
    }

    public function getPrizepool(): string
    {
        return $this->prizepool;
    }

    public function getRules(): string
    {
        return $this->rules;
    }

    public function getStream(): string
    {
        return $this->stream;
    }

    public function getDiscord(): string
    {
        return $this->discord;
    }

    public function getSchedule(): string
    {
        return $this->schedule;
    }

    public function getGame(): Game
    {
        return $this->game;
    }

    public function isUnpaid(): bool
    {
        return $this->isUnpaid;
    }

    public function isRequiredGameId(): bool
    {
        return $this->isRequiredGameId;
    }

    public function isOpenRegistration(): bool
    {
        return $this->isOpenRegistration;
    }

    public function isConfirmationRequired(): bool
    {
        return $this->isConfirmationRequired;
    }

    public function isLiveChat(): bool
    {
        return $this->isLiveChat;
    }

    public function isResult(): bool
    {
        return $this->isResult;
    }

    public function isResultOpponentRequired(): bool
    {
        return $this->isResultOpponentRequired;
    }

    public function isResultFileRequired(): bool
    {
        return $this->isResultFileRequired;
    }

    public function isUserIn(?Customer $user, bool $onlyValid = false): bool
    {
        if ($user === null) {
            return false;
        }

        $criteria = Criteria::create()
            ->where(Criteria::expr()->eq('user', $user));

        if ($onlyValid) {
            $criteria->andWhere(Criteria::expr()->neq('status', RegistrationUser::STATUS_CANCALED));
        }

        $registration = $this->registrationsUser->matching($criteria)->count();

        return $registration !== 0;
    }

    public function isParticipant(?Customer $user, bool $onlyValid = false): bool
    {
        if ($user === null) {
            return false;
        }

        if ($this->isOneVsOne()) {
            return $this->isUserIn($user, $onlyValid);
        }

        $inTeam = false;
        foreach ($this->getRegistrationsForTeam($user) as $registrationTeam) {
            if ($onlyValid && $registrationTeam->getStatus() === RegistrationTeam::STATUS_CANCALED) {
                continue;
            }

            if (! $registrationTeam->isPlayerIn($user)) {
                continue;
            }

            $inTeam = true;
        }

        return $inTeam;
    }

    public function isTeamIn(?Team $team, bool $onlyValid = false): bool
    {
        if ($team === null) {
            return false;
        }

        $criteria = Criteria::create()
            ->where(Criteria::expr()->eq('team', $team));

        if ($onlyValid) {
            $criteria->andWhere(Criteria::expr()->neq('status', RegistrationUser::STATUS_CANCALED));
        }

        $registration = $this->registrationsTeam->matching($criteria)->count();

        return $registration !== 0;
    }

    public function getRegistrationForUser(?Customer $user): ?RegistrationUser
    {
        if ($user !== null) {
            $criteria = Criteria::create()
                ->where(Criteria::expr()->eq('user', $user));

            $registration = $this->findValidRegistrations()->matching($criteria)->first();

            return $registration ? $registration : null;
        }

        return null;
    }

    private function findValidRegistrations(): ArrayCollection
    {
        if ($this->isOneVsOne()) {
            $criteria = Criteria::create()->where(Criteria::expr()->neq('status', RegistrationUser::STATUS_CANCALED));

            return $this->registrationsUser->matching($criteria);
        }

        $criteria = Criteria::create()->where(Criteria::expr()->neq('status', RegistrationTeam::STATUS_CANCALED));

        return $this->registrationsTeam->matching($criteria);
    }

    public function findRegistrationByVariableSymbol(string $variableSymbol): RegistrationUser|RegistrationTeam
    {
        $criteria = Criteria::create()->where(Criteria::expr()->eq('variableSymbol', $variableSymbol));
        if ($this->isOneVsOne()) {
            return $this->registrationsUser->matching($criteria)->first();
        }

        return $this->registrationsTeam->matching($criteria)->first();
    }

    public function findRegistration(int $registrationId): RegistrationUser|RegistrationTeam
    {
        $criteria = Criteria::create()->where(Criteria::expr()->eq('id', $registrationId));
        if ($this->isOneVsOne()) {
            return $this->registrationsUser->matching($criteria)->first();
        }

        return $this->registrationsTeam->matching($criteria)->first();
    }

    #[Deprecated]
    public function getRegistrationForTeam(?Customer $user): ?RegistrationTeam
    {
        if ($user !== null) {
            foreach ($user->getTeams() as $_team) {
                $criteria = Criteria::create()
                    ->where(Criteria::expr()->eq('team', $_team->getTeam()));

                $registration = $this->findValidRegistrations()->matching($criteria)->first();

                if ($registration !== null && $registration !== false) {
                    return $registration;
                }
            }
        }

        return null;
    }

    /**
     * @return RegistrationTeam[]|array
     */
    public function getRegistrationsForTeam(?Customer $player): array
    {
        $registrations = [];
        if ($player !== null) {
            foreach ($player->getTeams() as $teamPlayer) {
                $criteria = Criteria::create()
                    ->where(Criteria::expr()->eq('team', $teamPlayer->getTeam()));

                foreach ($this->findValidRegistrations()->matching($criteria) as $registration) {
                    assert($registration instanceof RegistrationTeam);
                    if (! $teamPlayer->isCaptain() && ! $teamPlayer->isDeputyCaptain() && ! $registration->isPlayerIn($player)) {
                        continue;
                    }

                    $registrations[] = $registration;
                }
            }
        }

        return $registrations;
    }

    public function getCountRegistrations(): int
    {
        if ($this->isOneVsOne()) {
            return $this->registrationsUser->count();
        }

        return $this->registrationsTeam->count();
    }

    public function getCountRegistrationsForTeam(Team $team): int
    {
        if ($this->isOneVsOne()) {
            return 0;
        }

        $criteria = Criteria::create();
        $criteria->where(Criteria::expr()->eq('team', $team));

        return $this->registrationsTeam->matching($criteria)->count();
    }

    public function getCountValidRegistrations(): int
    {
        return $this->findValidRegistrations()->count();
    }

    /**
     * @return RegistrationUser[]|RegistrationTeam[]
     */
    public function getValidRegistrations(): array
    {
        return $this->findValidRegistrations()->toArray();
    }

    public function cancaleRegistrationByUser(Customer $user): RegistrationUser
    {
        $criteria = Criteria::create()
            ->where(Criteria::expr()->eq('user', $user))
            ->andWhere(Criteria::expr()->neq('status', RegistrationUser::STATUS_CANCALED));

        $registration = $this->registrationsUser->matching($criteria)->first();
        assert($registration instanceof RegistrationUser);
        $registration->setStatus(RegistrationUser::STATUS_CANCALED);

        return $registration;
    }

    #[Deprecated]
    public function cancelRegistrationForTeam(Team $team): RegistrationTeam
    {
        $criteria = Criteria::create()
            ->where(Criteria::expr()->eq('team', $team))
            ->andWhere(Criteria::expr()->neq('status', RegistrationTeam::STATUS_CANCALED));

        $registration = $this->registrationsTeam->matching($criteria)->first();
        assert($registration instanceof RegistrationTeam);
        $registration->setStatus(RegistrationUser::STATUS_CANCALED);

        return $registration;
    }

    public function confirmRegistrationByUser(Customer $user): RegistrationUser
    {
        $criteria = Criteria::create()
            ->where(Criteria::expr()->eq('user', $user))
            ->andWhere(Criteria::expr()->eq('status', RegistrationUser::STATUS_PAID));

        $registration = $this->registrationsUser->matching($criteria)->first();
        assert($registration instanceof RegistrationUser);
        $registration->setStatus(RegistrationUser::STATUS_CONFIRMED);

        return $registration;
    }

    #[Deprecated]
    public function confirmRegistrationByTeam(Team $team): RegistrationTeam
    {
        $criteria = Criteria::create()
            ->where(Criteria::expr()->eq('team', $team))
            ->andWhere(Criteria::expr()->eq('status', RegistrationTeam::STATUS_PAID));

        $registration = $this->registrationsTeam->matching($criteria)->first();
        assert($registration instanceof RegistrationTeam);
        $registration->setStatus(RegistrationUser::STATUS_CONFIRMED);

        return $registration;
    }

    public function restoreRegistrationByUserId(Customer $user): ?RegistrationUser
    {
        if (! $this->isFull()) {
            $criteria = Criteria::create()
                ->where(Criteria::expr()->eq('user', $user))
                ->andWhere(Criteria::expr()->eq('status', RegistrationUser::STATUS_CANCALED));

            $registration = $this->registrationsUser->matching($criteria)->first();
            assert($registration instanceof RegistrationUser);
            $registration->setStatus(RegistrationUser::STATUS_NEW);

            return $registration;
        }

        return null;
    }

    public function isFull(): bool
    {
        if ($this->isOnline()) {
            return $this->getCountConfirmedRegistrations() >= $this->getNumberOfParticipants();
        }

        return $this->getCountPaidRegistrations() >= $this->getNumberOfParticipants();
    }

    public function isOnline(): bool
    {
        return $this->isOnline;
    }

    public function getCountConfirmedRegistrations(): int
    {
        return $this->findConfirmedRegistrations()->count();
    }

    private function findConfirmedRegistrations(): ArrayCollection
    {
        if ($this->isOneVsOne()) {
            $criteria = Criteria::create()->where(Criteria::expr()->eq('status', RegistrationUser::STATUS_CONFIRMED));

            return $this->registrationsUser->matching($criteria);
        }

        $criteria = Criteria::create()->where(Criteria::expr()->eq('status', RegistrationUser::STATUS_CONFIRMED));

        return $this->registrationsTeam->matching($criteria);
    }

    public function getNumberOfParticipants(): int
    {
        return $this->numberOfParticipants;
    }

    public function getCountPaidRegistrations(): int
    {
        return $this->findPaidRegistrations()->count();
    }

    private function findPaidRegistrations(): ArrayCollection
    {
        if ($this->isOneVsOne()) {
            $criteria = Criteria::create()->where(Criteria::expr()->in('status', [RegistrationUser::STATUS_PAID, RegistrationUser::STATUS_CONFIRMED]));

            return $this->registrationsUser->matching($criteria);
        }

        $criteria = Criteria::create()->where(Criteria::expr()->in('status', [RegistrationUser::STATUS_PAID, RegistrationUser::STATUS_CONFIRMED]));

        return $this->registrationsTeam->matching($criteria);
    }

    /**
     * @return RegistrationUser[]|RegistrationTeam[]
     */
    public function getPaidRegistrations(): array
    {
        return $this->findPaidRegistrations()->toArray();
    }

    /**
     * @return RegistrationUser[]|RegistrationTeam[]
     */
    public function getConfirmedRegistrations(): array
    {
        return $this->findConfirmedRegistrations()->toArray();
    }

    public function findPayment(Payment $payment): ?TournamentPayment
    {
        $criteria          = Criteria::create()->where(Criteria::expr()->eq('payment', $payment));
        $tournamentPayment = $this->payments->matching($criteria)->first();

        return $tournamentPayment ? $tournamentPayment : null;
    }

    public function getMinimumPrice(): int
    {
        $minimumPrice = PHP_INT_MAX;

        foreach ($this->getPayments() as $payment) {
            if ($payment->getPrice() >= $minimumPrice) {
                continue;
            }

            $minimumPrice = $payment->getPrice();
        }

        return $minimumPrice === PHP_INT_MAX ? 0 : $minimumPrice;
    }

    /**
     * @return ArrayCollection|Collection|TournamentPayment[]
     */
    public function getPayments()
    {
        $paymentsCollection = new ArrayCollection();

        $payments = [];
        foreach ($this->payments as $i => $payment) {
            $payments[Strings::webalize($payment->getPayment()->getName())] = $payment;
        }

        ksort($payments);

        foreach ($payments as $payment) {
            $paymentsCollection->add($payment);
        }

        return $paymentsCollection;
    }

    public function isInFuture(): bool
    {
        return ! $this->isLoaded() || $this->getTermFrom() >= new DateTime();
    }

    public function isNow(): bool
    {
        return $this->getTermFrom() <= new DateTime() && $this->getTermTo() >= new DateTime();
    }

    public function isTimeToConfirm(): bool
    {
        if ($this->isOpenRegistration() || ! $this->isConfirmationRequired()) {
            return true;
        }

        $now      = new DateTime();
        $termFrom = DateTime::from($this->getTermFrom()->getTimestamp());
        $termTo   = DateTime::from($this->getTermFrom()->getTimestamp());

        $termFrom->modify(sprintf('- %d minutes', $this->getConfirmationTo()));
        $termTo->modify(sprintf('- %d minutes', $this->getConfirmationFrom()));

        return $termFrom <= $now && $termTo >= $now && $this->isOnline();
    }

    public function getConfirmationTo(bool $forceInt = false): ?int
    {
        if ($forceInt) {
            return intval($this->confirmationTo);
        }

        return $this->confirmationTo;
    }

    public function getConfirmationFrom(bool $forceInt = false): ?int
    {
        if ($forceInt) {
            return intval($this->confirmationFrom);
        }

        return $this->confirmationFrom;
    }

    public function getConfirmationTerm(): string
    {
        $confirmationFrom = DateTime::createFromFormat('YmdHis', $this->getTermFrom()->format('YmdHis'));
        $confirmationTo   = DateTime::createFromFormat('YmdHis', $this->getTermFrom()->format('YmdHis'));

        $confirmationFrom->modify(sprintf('-%dminutes', $this->confirmationTo));
        $confirmationTo->modify(sprintf('-%dminutes', $this->confirmationFrom));
        return sprintf('%s - %s', $confirmationFrom->format('d.m.Y H:i'), $confirmationTo->format('H:i'));
    }

    public function isRegistrationEnable(): bool
    {
        $now      = new DateTime();
        $termFrom = $this->getRegistrationDeadline();

        return ($termFrom >= $now || ($this->isOpenRegistration() && $this->isNow())) && ! $this->isFull();
    }

    public function getRegistrationDeadline(): DateTimeInterface
    {
        $termFrom = DateTime::from($this->getTermFrom()->getTimestamp());

        if (! $this->isOpenRegistration() && $this->isConfirmationRequired()) {
            $termFrom->modify(sprintf('- %d minutes', $this->getConfirmationFrom(true)));
        }

        return $termFrom;
    }

    public function getBracket(): ?TournamentBracket
    {
        return $this->bracket;
    }

    public function existPublicStage(): bool
    {
        return $this->getBracket() instanceof TournamentBracket && $this->getPublicStages()->count() > 0;
    }

    /**
     * @return ArrayCollection<ATournamentBracketStage>|array<ATournamentBracketStage>
     */
    public function getPublicStages(): ArrayCollection
    {
        $criteria = Criteria::create();
        $criteria->where(Criteria::expr()->eq('isPublic', true));

        return $this->getBracket()->getStages()->matching($criteria);
    }

    public function findStage(int $stageId): ?ATournamentBracketStage
    {
        $criteria = Criteria::create();
        $criteria->where(Criteria::expr()->eq('id', $stageId));

        $stage = $this->getBracket()->getStages()->matching($criteria)->first();

        return $stage instanceof ATournamentBracketStage ? $stage : null;
    }
}
