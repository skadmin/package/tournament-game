<?php

declare(strict_types=1);

namespace Skadmin\TournamentGame\Doctrine\Tournament;

use App\Model\Doctrine\Customer\Customer;
use DateTimeInterface;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;
use Nette\Utils\DateTime;
use Nettrine\ORM\EntityManagerDecorator;
use Skadmin\Game\Doctrine\Game\Game;
use Skadmin\TournamentGame\Doctrine\Registration\RegistrationTeam;
use Skadmin\TournamentGame\Doctrine\Registration\RegistrationUser;
use Skadmin\TournamentGame\Doctrine\Supplement\Supplement;
use Skadmin\TournamentGame\Doctrine\TournamentPayment\TournamentPayment;
use Skadmin\TournamentGame\Doctrine\TournamentPayment\TournamentPaymentBox;
use Skadmin\TournamentGame\Doctrine\TournamentPayment\TournamentPaymentFacade;
use Skadmin\TournamentGame\Doctrine\TournamentType\TournamentType;
use SkadminUtils\DoctrineTraits\Facade;

use function assert;
use function count;
use function intval;

final class TournamentFacade extends Facade
{
    use Facade\Webalize;
    use Facade\Code;

    private TournamentPaymentFacade $facadeTournamentPayment;

    public function __construct(EntityManagerDecorator $em, TournamentPaymentFacade $facadeTournamentPayment)
    {
        parent::__construct($em);
        $this->table = Tournament::class;

        $this->facadeTournamentPayment = $facadeTournamentPayment;
    }

    public function create(string $name, bool $changeWebalize, int $numberOfParticipants, ?int $confirmationFrom, ?int $confirmationFo, string $content, string $description, string $prizepool, string $rules, string $stream, string $discord, string $schedule, DateTimeInterface $termFrom, DateTimeInterface $termTo, string $placeName, string $placeAddress, string $placeGpsLat, string $placeGpsLng, ?string $imagePreview, Game $game, TournamentType $tournamentType): Tournament
    {
        return $this->update(
            null,
            $name,
            $changeWebalize,
            $numberOfParticipants,
            $confirmationFrom,
            $confirmationFo,
            $content,
            $description,
            $prizepool,
            $rules,
            $stream,
            $discord,
            $schedule,
            $termFrom,
            $termTo,
            $placeName,
            $placeAddress,
            $placeGpsLat,
            $placeGpsLng,
            $imagePreview,
            $game,
            $tournamentType
        );
    }

    public function update(?int $id, string $name, bool $changeWebalize, int $numberOfParticipants, ?int $confirmationFrom, ?int $confirmationFo, string $content, string $description, string $prizepool, string $rules, string $stream, string $discord, string $schedule, DateTimeInterface $termFrom, DateTimeInterface $termTo, string $placeName, string $placeAddress, string $placeGpsLat, string $placeGpsLng, ?string $imagePreview, Game $game, TournamentType $tournamentType): Tournament
    {
        $tournament = $this->get($id);

        $tournament->update(
            $name,
            $numberOfParticipants,
            $confirmationFrom,
            $confirmationFo,
            $content,
            $description,
            $prizepool,
            $rules,
            $stream,
            $discord,
            $schedule,
            $termFrom,
            $termTo,
            $placeName,
            $placeAddress,
            $placeGpsLat,
            $placeGpsLng,
            $imagePreview,
            $game,
            $tournamentType
        );

        if (! $tournament->isLoaded()) {
            $tournament->setCode($this->getValidCode());
            $tournament->setWebalize($this->getValidWebalize($name));
        } elseif ($changeWebalize) {
            $tournament->updateWebalize($this->getValidWebalize($name));
        }

        $this->em->persist($tournament);
        $this->em->flush();

        return $tournament;
    }

    public function updateStates(Tournament $tournament, bool $isActive, bool $isOnline, bool $isLiveChat, bool $isResult, bool $isResultOpponentRequired, bool $isResultFileRequired, bool $isLocked, bool $isRequiredGameId, bool $isOpenRegistration, bool $isConfirmationRequired): Tournament
    {
        $tournament->updateStates(
            $isActive,
            $isOnline,
            $isLiveChat,
            $isResult,
            $isResultOpponentRequired,
            $isResultFileRequired,
            $isLocked,
            $isRequiredGameId,
            $isOpenRegistration,
            $isConfirmationRequired
        );

        $this->em->persist($tournament);
        $this->em->flush();

        return $tournament;
    }

    /**
     * @param TournamentPaymentBox[] $paymentsBox
     * @param Supplement[]           $supplements
     */
    public function updatePriceSetting(Tournament $tournament, bool $isUnpaid, array $paymentsBox, array $supplements): Tournament
    {
        // vytvoříme clone všech platebních možností
        $tournamentPayments = clone $tournament->getPayments();
        foreach ($paymentsBox as $paymentBox) {
            $tournamentPayment = $tournament->findPayment($paymentBox->getPayment());

            // pokud existuje platební možnost, tak provedeme update a odebereme jí ze seznamu (načetli jsme si nazačátku). Pokud neexistuje, tak jí založíme
            if ($tournamentPayment instanceof TournamentPayment) {
                $this->facadeTournamentPayment->update($tournamentPayment, $paymentBox->getPrice());
                $tournamentPayments->removeElement($tournamentPayment);
            } else {
                $this->facadeTournamentPayment->create($tournament, $paymentBox->getPayment(), $paymentBox->getPrice());
            }
        }

        // zůstali nám platební možnosti, které již nejsou použity a proto je odebereme
        foreach ($tournamentPayments as $tournamentPayment) {
            $this->facadeTournamentPayment->remove($tournamentPayment);
        }

        // nastavíme příplatky
        $tournament->updatePriceSetting($isUnpaid, $supplements);

        $this->em->persist($tournament);
        $this->em->flush();

        return $tournament;
    }

    /**
     * @param Customer|RegistrationTeam|null $winner
     * @param Customer|RegistrationTeam|null $secondPlace
     * @param Customer|RegistrationTeam|null $thirdPlace
     */
    public function updateWinner(Tournament $tournament, $winner, $secondPlace, $thirdPlace): Tournament
    {
        $tournament->updateWinner($winner, $secondPlace, $thirdPlace);

        $this->em->persist($tournament);
        $this->em->flush();

        return $tournament;
    }

    /**
     * @param Game[]|null $games
     */
    public function getModelByGameIds(?array $games = null): QueryBuilder
    {
        $repository = $this->em->getRepository($this->table);
        assert($repository instanceof EntityRepository);

        $criteria = Criteria::create();

        if ($games !== null && count($games) > 0) {
            $criteria->where(Criteria::expr()->in('game', $games));
        }

        return $repository->createQueryBuilder('a')
            ->addCriteria($criteria);
    }

    public function get(?int $id = null): Tournament
    {
        if ($id === null) {
            return new Tournament();
        }

        $tournament = parent::get($id);

        if ($tournament === null) {
            return new Tournament();
        }

        return $tournament;
    }

    public function getCountInFuture(bool $onlyActive = false): int
    {
        $qb = $this->findInFutureQb($onlyActive);
        $qb->select('count(a.id)');

        return intval($qb->getQuery()
            ->getSingleScalarResult());
    }

    /**
     * @return Tournament[]
     */
    public function findInFuture(bool $onlyActive, ?int $limit = null, ?int $offset = null): array
    {
        return $this->findInFutureQb($onlyActive, $limit, $offset)
            ->getQuery()
            ->getResult();
    }

    /**
     * @param array<Game>|null $games
     *
     * @return Tournament[]
     */
    public function findInFutureByGameIds(bool $onlyActive, ?array $games = null): array
    {
        $qb = $this->findInFutureQb($onlyActive);

        $criteria = Criteria::create();

        if ($games !== null && count($games) > 0) {
            $criteria->where(Criteria::expr()->in('game', $games));
        }

        $qb->addCriteria($criteria);

        return $qb->getQuery()
            ->getResult();
    }

    private function findInFutureQb(bool $onlyActive, ?int $limit = null, ?int $offset = null): QueryBuilder
    {
        $qb = $this->em
            ->getRepository($this->table)
            ->createQueryBuilder('a');
        assert($qb instanceof QueryBuilder);

        $qb->setMaxResults($limit)
            ->setFirstResult($offset)
            ->orderBy('a.termFrom', 'ASC');

        $criteria = Criteria::create();
        $criteria->where(Criteria::expr()->gte('a.termTo', new DateTime()));

        if ($onlyActive) {
            $criteria->andWhere(Criteria::expr()->eq('a.isActive', true));
        }

        $qb->addCriteria($criteria);

        return $qb;
    }

    /**
     * @return Tournament[]
     */
    public function getBetweenDates(DateTimeInterface $dateFrom, DateTimeInterface $dateTo, bool $onlyActive = false): array
    {
        $qb = $this->em
            ->getRepository($this->table)
            ->createQueryBuilder('a');
        assert($qb instanceof QueryBuilder);

        $qb->select('a')
            ->orderBy('a.termFrom', 'ASC');

        $criteria = Criteria::create();
        $criteria->where(Criteria::expr()->gte('a.termFrom', $dateFrom));
        $criteria->andWhere(Criteria::expr()->lte('a.termFrom', $dateTo));

        if ($onlyActive) {
            $criteria->andWhere(Criteria::expr()->eq('a.isActive', true));
        }

        $qb->addCriteria($criteria);

        return $qb->getQuery()
            ->getResult();
    }

    public function findByWebalize(string $webalize): ?Tournament
    {
        $criteria = ['webalize' => $webalize];

        return $this->em
            ->getRepository($this->table)
            ->findOneBy($criteria);
    }

    public function findByCode(string $code): ?Tournament
    {
        $criteria = ['code' => $code];

        return $this->em
            ->getRepository($this->table)
            ->findOneBy($criteria);
    }

    public function cancaleRegistrationByUser(Tournament $tournament, Customer $user): RegistrationUser
    {
        $registration = $tournament->cancaleRegistrationByUser($user);

        $this->em->persist($tournament);
        $this->em->flush();

        return $registration;
    }

    public function cancelRegistrationForTeam(Tournament $tournament, Customer $user, int $registrationId): ?RegistrationTeam
    {
        $registration = $this->getRegistrationForTeam($tournament, $user, $registrationId);

        if ($registration === null) {
            return null;
        }

        $registration->setStatus(RegistrationTeam::STATUS_CANCALED);

        $this->em->persist($tournament);
        $this->em->flush();

        return $registration;
    }

    public function confirmRegistrationByUser(Tournament $tournament, Customer $user): RegistrationUser
    {
        $registration = $tournament->confirmRegistrationByUser($user);
        $this->em->persist($tournament);
        $this->em->flush();

        return $registration;
    }

    public function confirmRegistrationForTeam(Tournament $tournament, Customer $user, int $registrationId): ?RegistrationTeam
    {
        $registration = $this->getRegistrationForTeam($tournament, $user, $registrationId);

        if ($registration === null) {
            return null;
        }

        $registration->setStatus(RegistrationTeam::STATUS_CONFIRMED);

        $this->em->persist($tournament);
        $this->em->flush();

        return $registration;
    }

    private function getRegistrationForTeam(Tournament $tournament, Customer $user, int $registrationId): ?RegistrationTeam
    {
        $registrationsForTeam = $tournament->getRegistrationsForTeam($user);

        $registration = null;
        foreach ($registrationsForTeam as $registrationTeam) {
            if ($registrationTeam->getId() !== $registrationId) {
                continue;
            }

            $registration = $registrationTeam;
        }

        return $registration;
    }

    public function restoreRegistrationByUser(Tournament $tournament, Customer $user): ?RegistrationUser
    {
        $registration = $tournament->restoreRegistrationByUserId($user);
        $this->em->persist($tournament);
        $this->em->flush();

        return $registration;
    }
}
