<?php

declare(strict_types=1);

namespace Skadmin\TournamentGame\Doctrine\TournamentBracket;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping as ORM;
use Skadmin\TournamentGame\Doctrine\Tournament\Tournament;
use Skadmin\TournamentGame\Doctrine\TournamentBracketStage\ATournamentBracketStage;
use SkadminUtils\DoctrineTraits\Entity;

use function dump;

#[ORM\Entity]
#[ORM\HasLifecycleCallbacks]
class TournamentBracket
{
    use Entity\BaseEntity;
    use Entity\Name;

    #[ORM\Column]
    private ?int $stage = 0;

    #[ORM\OneToOne(targetEntity: Tournament::class, inversedBy: 'bracket')]
    #[ORM\JoinColumn(onDelete: 'cascade')]
    private Tournament $tournament;

    /** @var ArrayCollection|Collection|ATournamentBracketStage[] */
    #[ORM\OneToMany(targetEntity: ATournamentBracketStage::class, mappedBy: 'bracket')]
    #[ORM\OrderBy(['sequence' => 'ASC'])]
    private ArrayCollection|Collection|array $stages;

    public function __construct()
    {
        $this->stages = new ArrayCollection();
    }

    public function create(Tournament $tournament, string $name): void
    {
        $this->tournament = $tournament;
        $this->name       = $name;
    }

    /**
     * @return ArrayCollection|Collection|ATournamentBracketStage[]
     */
    public function getStages(): ArrayCollection|Collection|array
    {
        return $this->stages;
    }

    public function getCurrentStage(): ?ATournamentBracketStage
    {
        $criteria = Criteria::create();
        $criteria->where(Criteria::expr()->eq('sequence', $this->stage));

        $currentStage = $this->stages->matching($criteria)->first();

        return $currentStage instanceof ATournamentBracketStage ? $currentStage : null;
    }

    /**
     * @return ArrayCollection|Collection|array<ATournamentBracketStage>
     */
    public function getFinishedStages(): array
    {
        $criteria = Criteria::create();
        $criteria->where(Criteria::expr()->lt('sequence', $this->stage));

        dump($this->stages->matching($criteria));
        exit;

        return $this->stages->matching($criteria);
    }

    public function getTournament(): Tournament
    {
        return $this->tournament;
    }
}
