<?php

declare(strict_types=1);

namespace Skadmin\TournamentGame\Doctrine\TournamentBracket;

use Nettrine\ORM\EntityManagerDecorator;
use Skadmin\TournamentGame\Doctrine\Tournament\Tournament;
use SkadminUtils\DoctrineTraits\Facade;

use function sprintf;

final class TournamentBracketFacade extends Facade
{
    public function __construct(EntityManagerDecorator $em)
    {
        parent::__construct($em);
        $this->table = TournamentBracket::class;
    }

    public function create(Tournament $tournament): TournamentBracket
    {
        $tournamentBracket = $this->findByTournament($tournament);

        if ($tournamentBracket instanceof TournamentBracket) {
            return $tournamentBracket;
        }

        $tournamentBracket = $this->get();
        $tournamentBracket->create($tournament, sprintf('Bracket of %s', $tournament->getName()));

        $this->em->persist($tournamentBracket);
        $this->em->flush();

        return $tournamentBracket;
    }

    public function findByTournament(Tournament $tournament): ?TournamentBracket
    {
        return $this->em
            ->getRepository($this->table)
            ->findOneBy(['tournament' => $tournament]);
    }

    public function get(?int $id = null): TournamentBracket
    {
        if ($id === null) {
            return new TournamentBracket();
        }

        $tournamentBracket = parent::get($id);

        if ($tournamentBracket === null) {
            return new TournamentBracket();
        }

        return $tournamentBracket;
    }

    public function delete(TournamentBracket $tournamentBracket): void
    {
        $this->em->remove($tournamentBracket);
        $this->em->flush();
    }
}
