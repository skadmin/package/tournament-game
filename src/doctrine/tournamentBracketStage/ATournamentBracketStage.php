<?php

declare(strict_types=1);

namespace Skadmin\TournamentGame\Doctrine\TournamentBracketStage;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping as ORM;
use ReflectionClass;
use Skadmin\TournamentGame\Doctrine\TournamentBracket\TournamentBracket;
use Skadmin\TournamentGame\Doctrine\TournamentBracketStage\Type\TournamentBracketStageDoubleElimination;
use Skadmin\TournamentGame\Doctrine\TournamentBracketStage\Type\TournamentBracketStageRoundRobin;
use Skadmin\TournamentGame\Doctrine\TournamentBracketStage\Type\TournamentBracketStageRoundRobinDuel;
use Skadmin\TournamentGame\Doctrine\TournamentBracketStage\Type\TournamentBracketStageSingleElimination;
use Skadmin\TournamentGame\Doctrine\TournamentBracketStageMatch\TournamentBracketStageMatch;
use Skadmin\TournamentGame\Doctrine\TournamentBracketStageParticipant\TournamentBracketStageParticipant;
use SkadminUtils\DoctrineTraits\Entity;

use function assert;
use function count;
use function explode;
use function intval;
use function is_string;
use function preg_replace;
use function str_replace;

#[ORM\Entity]
#[ORM\Table(name: 'tournament_bracket_stage')]
#[ORM\Index(name: 'tournament_bracket_stage_type', columns: ['type'])]
#[ORM\InheritanceType(value: 'JOINED')]
#[ORM\DiscriminatorColumn(name: 'type', type: 'string')]
#[ORM\DiscriminatorMap([
    TournamentBracketStageSingleElimination::TYPE => TournamentBracketStageSingleElimination::class,
    TournamentBracketStageDoubleElimination::TYPE => TournamentBracketStageDoubleElimination::class,
    TournamentBracketStageRoundRobin::TYPE => TournamentBracketStageRoundRobin::class,
    TournamentBracketStageRoundRobinDuel::TYPE => TournamentBracketStageRoundRobinDuel::class,
])]
#[ORM\HasLifecycleCallbacks]
abstract class ATournamentBracketStage
{
    use Entity\BaseEntity;
    use Entity\Name;
    use Entity\Sequence;
    use Entity\IsPublic;

    #[ORM\Column(options: ['default' => true])]
    private bool $willPlayInTheFinals = true;

    #[ORM\Column(options: ['default' => false])]
    private bool $sequentialDisplayOfDuels = false;

    #[ORM\Column(nullable: true)]
    private ?int $numberOfRounds = null;

    #[ORM\Column]
    private int $bestOfGame = 1;

    #[ORM\ManyToOne(targetEntity: TournamentBracket::class, inversedBy: 'stages')]
    #[ORM\JoinColumn(onDelete: 'cascade')]
    private TournamentBracket $bracket;

    /** @var ArrayCollection|Collection|array<TournamentBracketStageParticipant> */
    #[ORM\OneToMany(targetEntity: TournamentBracketStageParticipant::class, mappedBy: 'stage')]
    private ArrayCollection|Collection|array $participants;

    /** @var ArrayCollection|Collection|array<TournamentBracketStageMatch> */
    #[ORM\OneToMany(targetEntity: TournamentBracketStageMatch::class, mappedBy: 'stage')]
    private ArrayCollection|Collection|array $matches;

    public function __construct()
    {
        $this->participants = new ArrayCollection();
        $this->matches = new ArrayCollection();
    }

    public function create(TournamentBracket $bracket, string $name, bool $willPlayInTheFinals, bool $sequentialDisplayOfDuels, ?int $numberOfRounds, int $bestOfGame): void
    {
        $this->bracket = $bracket;
        $this->update($name, $willPlayInTheFinals, $sequentialDisplayOfDuels, $numberOfRounds, $bestOfGame);
    }

    public function update(string $name, bool $willPlayInTheFinals, bool $sequentialDisplayOfDuels, ?int $numberOfRounds, int $bestOfGame): void
    {
        $this->name = $name;
        $this->willPlayInTheFinals = $willPlayInTheFinals;
        $this->sequentialDisplayOfDuels = $sequentialDisplayOfDuels;
        $this->numberOfRounds = $numberOfRounds;
        $this->bestOfGame = $bestOfGame;
    }

    public function isWillPlayInTheFinals(): bool
    {
        return $this->willPlayInTheFinals;
    }

    public function isSequentialDisplayOfDuels(): bool
    {
        return $this->sequentialDisplayOfDuels;
    }

    public function getNumberOfRounds(): ?int
    {
        return $this->numberOfRounds;
    }

    public function getBestOfGame(): int
    {
        return $this->bestOfGame;
    }

    public function getBracket(): TournamentBracket
    {
        return $this->bracket;
    }

    /**
     * @return ArrayCollection|Collection|array<TournamentBracketStageParticipant>
     */
    public function getParticipants(): ArrayCollection|Collection|array
    {
        return $this->participants;
    }

    /**
     * @return ArrayCollection|Collection|TournamentBracketStageMatch[]
     */
    public function getMatches(?int $roundId = null, ?int $status = null, ?int $groupId = null, ?array $orderBy = null): ArrayCollection|Collection|array
    {
        $criteria = Criteria::create();

        if ($roundId !== null) {
            $criteria->where(Criteria::expr()->eq('roundId', $roundId));
        }

        if ($status !== null) {
            $criteria->where(Criteria::expr()->eq('status', $status));
        }

        if ($groupId !== null) {
            $criteria->where(Criteria::expr()->eq('groupId', $groupId));
        }

        if ($orderBy !== null) {
            $criteria->orderBy($orderBy);
        }

        return $this->matches->matching($criteria);
    }

    public function findMatch(?TournamentBracketStageParticipant $participant, int $groupId, int $roundId): ?TournamentBracketStageMatch
    {
        if ($participant === null) {
            return null;
        }

        $criteria = Criteria::create();
        $expr = Criteria::expr();

        $criteria
            ->andWhere($expr->orX(
                $expr->eq('challenger', $participant),
                $expr->eq('opponent', $participant)
            ))
            ->andWhere($expr->eq('groupId', $groupId))
            ->andWhere($expr->eq('roundId', $roundId));

        $match = $this->matches->matching($criteria)->first();

        if ($match instanceof TournamentBracketStageMatch) {
            return $match;
        }

        return null;
    }

    public function findMatchById(int $matchId): ?TournamentBracketStageMatch
    {
        $criteria = Criteria::create();

        $criteria->where(Criteria::expr()->eq('id', $matchId));

        $match = $this->matches->matching($criteria)->first();

        if ($match instanceof TournamentBracketStageMatch) {
            return $match;
        }

        return null;
    }

    abstract public function getType(): string;

    abstract public function getBracketViewerData(bool $asJson = false): string|array;

    /**
     * @return array{id: string, tournament_id: int, name: string, logo: string}
     */
    protected function prepareParticipants(): array
    {
        $participants = [];
        foreach ($this->getParticipants() as $participant) {
            $participants[] = [
                'id' => intval($participant->getCode()),
                'tournament_id' => $this->getBracket()->getTournament()->getId(),
                'name' => $participant->getName(),
                'logo' => $participant->getLogo(),
            ];
        }

        return $participants;
    }

    /**
     * @return array<mixed> {id: int, number: int, stage_id: int, group_id: int, round_id: int, child_count: 0, status: int, opponent1: {id: int, position: int, score: int, result: win|loss|null}, opponent2: {id: int, position: int, score: int, result: win|loss|null}}
     */
    protected function prepareMatches(): array
    {
        $position = 1;
        $matches = [];

        foreach ($this->getMatches() as $match) {
            $opponent1 = ['id' => null];
            $opponent2 = ['id' => null];
            $isDraw = null;
            $challengerWin = null;

            if ($match->getChallenger() instanceof TournamentBracketStageParticipant && $match->getOpponent() instanceof TournamentBracketStageParticipant) {
                if ($match->getChallengerResult() !== null && $match->getOpponentResult() !== null) {
                    $isDraw = $match->getChallengerResult() === $match->getOpponentResult();

                    if (!$isDraw) {
                        $challengerWin = $match->getChallengerResult() > $match->getOpponentResult();
                    }
                }
            }

            if ($match->getChallenger() instanceof TournamentBracketStageParticipant) {
                $opponent1 = [
                    'id' => intval($match->getChallenger()->getCode()),
                    'position' => $position++,
                    'score' => $match->getChallengerResult() ?? 0,
                ];

                if ($isDraw !== null) {
                    if ($challengerWin !== null) {
                        $opponent1['result'] = $challengerWin ? 'win' : 'loss';
                    } else {
                        $opponent1['result'] = 'draw';
                    }
                }
            }

            if ($match->getOpponent() instanceof TournamentBracketStageParticipant) {
                $opponent2 = [
                    'id' => intval($match->getOpponent()->getCode()),
                    'position' => $position++,
                    'score' => $match->getOpponentResult() ?? '0',
                ];

                if ($isDraw !== null) {
                    if ($challengerWin !== null) {
                        $opponent2['result'] = $challengerWin ? 'loss' : 'win';
                    } else {
                        $opponent2['result'] = 'draw';
                    }
                }
            }

            $matches[] = [
                'id' => $match->getId(),
                'number' => $match->getNumber(),
                'stage_id' => $this->getId(),
                'group_id' => $match->getGroupId(),
                'round_id' => $match->getRoundId(),
                'child_count' => 0,
                'status' => $match->getStatus(),
                'opponent1' => $opponent1,
                'opponent2' => $opponent2,
            ];
        }

        return $matches;
    }

    /**
     * @return array<string, string>
     */
    public static function generateAllowedTypes(): array
    {
        foreach ((new ReflectionClass(self::class))->getAttributes() as $attribute) {
            if ($attribute->getName() === 'Doctrine\ORM\Mapping\DiscriminatorMap') {
                return $attribute->getArguments()[0];
            }
        }
    }
}
