<?php

declare(strict_types=1);

namespace Skadmin\TournamentGame\Doctrine\TournamentBracketStage;

use Doctrine\Common\Collections\Criteria;
use Nettrine\ORM\EntityManagerDecorator;
use Skadmin\TournamentGame\Doctrine\TournamentBracket\TournamentBracket;
use Skadmin\TournamentGame\Doctrine\TournamentBracketStage\Type\TournamentBracketStageDoubleElimination;
use Skadmin\TournamentGame\Doctrine\TournamentBracketStage\Type\TournamentBracketStageRoundRobin;
use Skadmin\TournamentGame\Doctrine\TournamentBracketStage\Type\TournamentBracketStageRoundRobinDuel;
use Skadmin\TournamentGame\Doctrine\TournamentBracketStage\Type\TournamentBracketStageSingleElimination;
use Skadmin\TournamentGame\Doctrine\TournamentBracketStageParticipant\TournamentBracketStageParticipantFacade;
use SkadminUtils\DoctrineTraits\Facade;

use function intval;

final class TournamentBracketStageFacade extends Facade
{
    private TournamentBracketStageService $serviceTournamentBracketStage;

    private TournamentBracketStageParticipantFacade $facadeTournamentBracketStageParticipant;

    public function __construct(EntityManagerDecorator $em, TournamentBracketStageService $serviceTournamentBracketStage, TournamentBracketStageParticipantFacade $facadeTournamentBracketStageParticipant)
    {
        parent::__construct($em);
        $this->table = ATournamentBracketStage::class;

        $this->serviceTournamentBracketStage           = $serviceTournamentBracketStage;
        $this->facadeTournamentBracketStageParticipant = $facadeTournamentBracketStageParticipant;
    }

    public function create(TournamentBracket $bracket, string $type, string $name, bool $willPlayInTheFinals, bool $sequentialDisplayOfDuels, int $numberOfRounds, int $bestOfGame): ATournamentBracketStage
    {
        switch ($type) {
            case TournamentBracketStageSingleElimination::class:
                $bracketStage = new TournamentBracketStageSingleElimination();
                break;
            case TournamentBracketStageDoubleElimination::class:
                $bracketStage = new TournamentBracketStageDoubleElimination();
                break;
            case TournamentBracketStageRoundRobin::class:
                $bracketStage = new TournamentBracketStageRoundRobin();
                break;
            case TournamentBracketStageRoundRobinDuel::class:
                $bracketStage = new TournamentBracketStageRoundRobinDuel();
                break;
        }

        $bracketStage->create($bracket, $name, $willPlayInTheFinals, $sequentialDisplayOfDuels, $numberOfRounds, $bestOfGame);
        $bracketStage->updateSequence($this->getValidSequence($bracket));

        $this->em->persist($bracketStage);
        $this->em->flush();

        return $bracketStage;
    }

    public function update(int $bracketStageId, string $name, bool $willPlayInTheFinals, bool $sequentialDisplayOfDuels, int $numberOfRounds, int $bestOfGame): ATournamentBracketStage
    {
        $bracketStage = $this->get($bracketStageId);
        $bracketStage->update($name, $willPlayInTheFinals, $sequentialDisplayOfDuels, $numberOfRounds, $bestOfGame);

        $this->em->flush();

        return $bracketStage;
    }

    public function createParticipants(ATournamentBracketStage $bracketStage, array $participants): ATournamentBracketStage
    {
        $this->clearMatchs($bracketStage->getId());
        $this->clearParticipants($bracketStage->getId());

        $participantIndex = 1;
        foreach ($participants as $participantId) {
            $registration = $bracketStage->getBracket()->getTournament()->findRegistration($participantId);
            $this->facadeTournamentBracketStageParticipant->create($bracketStage, $participantIndex++, $registration);
        }

        return $bracketStage;
    }

    public function updateDoubleElimination(TournamentBracketStageDoubleElimination $bracketStage, int $bestOfGameLoser): ATournamentBracketStage
    {
        $bracketStage->setBestOfGameLoser($bestOfGameLoser);

        $this->em->flush();

        return $bracketStage;
    }

    public function updateRoundRobin(TournamentBracketStageRoundRobin $bracketStage): ATournamentBracketStage
    {
        //$this->em->flush();
        return $bracketStage;
    }

    public function updateRoundRobinDuel(TournamentBracketStageRoundRobinDuel $bracketStage, int $numberOfDuels, int $numberOfRoundDuels): ATournamentBracketStage
    {
        $bracketStage->setNumberOfDuels($numberOfDuels);
        $bracketStage->setNumberOfRoundDuels($numberOfRoundDuels);

        $this->em->flush();

        return $bracketStage;
    }

    public function get(?int $id = null, bool $clearCache = false): ?ATournamentBracketStage
    {
        if ($id === null) {
            return null;
        }

        if ($clearCache) {
            $this->em->clear(ATournamentBracketStage::class);
        }

        $bracketStage = parent::get($id);

        if ($bracketStage === null) {
            return null;
        }

        return $bracketStage;
    }

    public function delete(ATournamentBracketStage $bracketStage): void
    {
        $this->em->remove($bracketStage);
        $this->em->flush();
    }

    public function prepareStage(int $stageId): ATournamentBracketStage
    {
        $stage = $this->get($stageId, true);

        if ($stage->getParticipants()->count() !== 0 && $stage->getMatches()->count() !== 0) {
            return $stage;
        }

        $participants = $this->serviceTournamentBracketStage->prepareParticipants($stage);
        $this->serviceTournamentBracketStage->prepareMatches($stage, $participants);

        return $this->get($stage->getId(), true);
    }

    public function togglePublic(int $stageId): ATournamentBracketStage
    {
        $stage = $this->get($stageId);
        $stage->setIsPublic(! $stage->isPublic());

        $this->em->flush();

        if ($stage->isPublic()) {
            $this->serviceTournamentBracketStage->checkFirstRound($stage);
        }

        return $stage;
    }

    public function getValidSequence(TournamentBracket $bracket): int
    {
        $criteria = Criteria::create();
        $criteria->where(Criteria::expr()->eq('bracket', $bracket));

        $sequence = $this->getModel()
            ->select('count(a.id)')
            ->addCriteria($criteria)
            ->getQuery()
            ->getSingleScalarResult();

        return intval($sequence);
    }

    public function clearMatchs(int $stageId): void
    {
        $stage = $this->get($stageId);

        if (! $stage instanceof ATournamentBracketStage || $stage->isPublic()) {
            return;
        }

        foreach ($stage->getMatches() as $match) {
            $this->em->remove($match);
        }

        $this->em->flush();
    }

    public function clearParticipants(int $stageId): void
    {
        $stage = $this->get($stageId);

        if (! $stage instanceof ATournamentBracketStage || $stage->isPublic()) {
            return;
        }

        foreach ($stage->getParticipants() as $participant) {
            $this->em->remove($participant);
        }

        $this->em->flush();
    }

    /**
     * @param array<int, int> $newOrder
     */
    public function updateSequence(array $newOrder): void
    {
        foreach ($newOrder as $stageId => $sequence) {
            $gallery = $this->get($stageId);

            if (! $gallery->isLoaded()) {
                continue;
            }

            $gallery->updateSequence($sequence);
            $this->em->persist($gallery);
        }

        $this->em->flush();
    }
}
