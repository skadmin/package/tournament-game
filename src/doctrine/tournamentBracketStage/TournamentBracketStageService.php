<?php

declare(strict_types=1);

namespace Skadmin\TournamentGame\Doctrine\TournamentBracketStage;

use Exception;
use Nette\Application\UI\Presenter;
use Skadmin\TournamentGame\Doctrine\TournamentBracketStage\Traits\TTournamentBracketStageDoubleElimination;
use Skadmin\TournamentGame\Doctrine\TournamentBracketStage\Traits\TTournamentBracketStageRoundRobin;
use Skadmin\TournamentGame\Doctrine\TournamentBracketStage\Traits\TTournamentBracketStageRoundRobinDuel;
use Skadmin\TournamentGame\Doctrine\TournamentBracketStage\Traits\TTournamentBracketStageSingleElimination;
use Skadmin\TournamentGame\Doctrine\TournamentBracketStage\Type\TournamentBracketStageDoubleElimination;
use Skadmin\TournamentGame\Doctrine\TournamentBracketStage\Type\TournamentBracketStageRoundRobin;
use Skadmin\TournamentGame\Doctrine\TournamentBracketStage\Type\TournamentBracketStageRoundRobinDuel;
use Skadmin\TournamentGame\Doctrine\TournamentBracketStage\Type\TournamentBracketStageSingleElimination;
use Skadmin\TournamentGame\Doctrine\TournamentBracketStageMatch\TournamentBracketStageMatch;
use Skadmin\TournamentGame\Doctrine\TournamentBracketStageMatch\TournamentBracketStageMatchFacade;
use Skadmin\TournamentGame\Doctrine\TournamentBracketStageMatch\TournamentBracketStageMatchService;
use Skadmin\TournamentGame\Doctrine\TournamentBracketStageParticipant\TournamentBracketStageParticipant;
use Skadmin\TournamentGame\Doctrine\TournamentBracketStageParticipant\TournamentBracketStageParticipantFacade;
use Skadmin\TournamentGame\Doctrine\TournamentResult\TournamentResult;
use Skadmin\TournamentGame\Doctrine\TournamentResult\TournamentResultFacade;

use function ceil;
use function count;
use function dump;
use function intval;
use function shuffle;
use function sprintf;
use function time;

final class TournamentBracketStageService
{
    use TTournamentBracketStageSingleElimination;
    use TTournamentBracketStageDoubleElimination;
    use TTournamentBracketStageRoundRobin;
    use TTournamentBracketStageRoundRobinDuel;

    private TournamentBracketStageParticipantFacade $facadeTournamentBracketStageParticipant;
    private TournamentBracketStageMatchFacade       $facadeTournamentBracketStageMatch;
    private TournamentResultFacade                  $facadeTournamentResult;
    private TournamentBracketStageMatchService      $serviceTournamentBracketStageMatch;

    /** @var array<TournamentBracketStageParticipant> */
    private array $participants = [];

    public function __construct(TournamentBracketStageParticipantFacade $facadeTournamentBracketStageParticipant, TournamentBracketStageMatchFacade $facadeTournamentBracketStageMatch, TournamentResultFacade $facadeTournamentResult, TournamentBracketStageMatchService $serviceTournamentBracketStageMatch)
    {
        $this->facadeTournamentBracketStageParticipant = $facadeTournamentBracketStageParticipant;
        $this->facadeTournamentBracketStageMatch       = $facadeTournamentBracketStageMatch;
        $this->facadeTournamentResult                  = $facadeTournamentResult;

        $this->serviceTournamentBracketStageMatch = $serviceTournamentBracketStageMatch;
    }

    public function checkFirstRound(ATournamentBracketStage $stage): void
    {
        foreach ($stage->getMatches(0) as $match) {
            if (! ($match->getOpponent() instanceof TournamentBracketStageParticipant) || $match->getChallenger() !== null) {
                continue;
            }

            $this->facadeTournamentBracketStageMatch->updateMatch(
                $match->getId(),
                $match->getStatus(),
                $match->getOpponent(), // toggle challenger
                null, // set opponent
                null,
                null
            );
        }

        foreach ($stage->getMatches(0) as $match) {
            if (! ($match->getChallenger() instanceof TournamentBracketStageParticipant) || $match->getOpponent() !== null) {
                continue;
            }

            $this->facadeTournamentBracketStageMatch->updateMatchStatus($match->getId(), TournamentBracketStageMatch::STATUS_COMPLETED);

            if ($match->getNumber() % 2 === 1) {
                $challenger = $match->getChallenger();
                $opponent   = $match->getMatchWinner()->getOpponent();
            } else {
                $challenger = $match->getMatchWinner()->getChallenger();
                $opponent   = $match->getChallenger();
            }

            $this->facadeTournamentBracketStageMatch->updateMatch(
                $match->getMatchWinner()->getId(),
                TournamentBracketStageMatch::STATUS_AWAITING_PARTICIPANTS,
                $challenger,
                $opponent,
                null,
                null
            );

            $this->facadeTournamentBracketStageMatch->updateResults(
                $match->getId(),
                intval(ceil($stage->getBestOfGame() / 2)),
                null
            );
        }
    }

    /**
     * @return array<TournamentBracketStageParticipant>
     */
    public function prepareParticipants(ATournamentBracketStage $stage): array
    {
        $participants = [];
        if ($stage->getParticipants()->count() === 0) {
            $participantIndex = 1;
            // switcher odkud se mají brát učastníci
            if ($stage->getBracket()->getTournament()->isOnline()) {
                $registrations = $stage->getBracket()->getTournament()->getConfirmedRegistrations();
            } else {
                $registrations = $stage->getBracket()->getTournament()->getPaidRegistrations();
            }

            foreach ($registrations as $registration) {
                $participants[] = $this->facadeTournamentBracketStageParticipant->create($stage, $participantIndex++, $registration);
            }
        } else {
            $participants = $stage->getParticipants()->toArray();
        }

        return $participants;
    }

    public function prepareMatches(ATournamentBracketStage $stage, array $participants): void
    {
        if ($stage->getMatches()->count() !== 0) {
            return;
        }

        if ($stage->getType() !== TournamentBracketStageRoundRobin::TYPE) {
            shuffle($participants);
        }

        $this->participants = $participants;
        $countParticipants  = count($participants);

        switch ($stage->getType()) {
            case TournamentBracketStageSingleElimination::TYPE:
                $this->prepareMatchesSingleElimination($stage, $countParticipants);
                break;
            case TournamentBracketStageDoubleElimination::TYPE:
                $this->prepareMatchesDoubleElimination($stage, $countParticipants);
                break;
            case TournamentBracketStageRoundRobin::TYPE:
                $this->prepareMatchesRoundRobin($stage, $countParticipants);
                break;
            case TournamentBracketStageRoundRobinDuel::TYPE:
                $this->prepareMatchesRoundRobinDuel($stage, $countParticipants);
                break;
            default:
                throw new Exception(sprintf('Not support stage type [%s]', $stage->getType()));
        }
    }

    private function loadParticipant(int $index): ?TournamentBracketStageParticipant
    {
        return $this->participants[$index] ?? null;
    }

    /**
     * @param int $limitWaintingResultLong seconds
     * @param int $limitResolveMatch seconds
     */
    public function checkTheStatusOfMatches(ATournamentBracketStage $stage, int $limitWaintingResultLong, int $limitResolveMatch, ?Presenter $presenter = null): void
    {
        foreach ($stage->getMatches(null, TournamentBracketStageMatch::STATUS_AWAITING_RESULTS) as $match) {
            foreach ($match->getResults(TournamentResult::STATUS_NEW) as $result) {
                $diff = time() - $result->getCreatedAt()->getTimestamp();
                if ($diff < $limitWaintingResultLong) {
                    continue;
                }

                $this->facadeTournamentBracketStageMatch->updateMatchStatus($match->getId(), TournamentBracketStageMatch::STATUS_AWAITING_RESULTS_LONG);
            }
        }

        foreach ($stage->getMatches(null, TournamentBracketStageMatch::STATUS_AWAITING_RESULTS_LONG) as $match) {
            $resolve = false;
            foreach ($match->getResults(TournamentResult::STATUS_NEW) as $result) {
                $diff = time() - $result->getCreatedAt()->getTimestamp();
                if ($diff < $limitResolveMatch) {
                    continue;
                }

                $this->facadeTournamentResult->updateStatus($result->getId(), TournamentResult::STATUS_CONFIRMED);

                $resolve = true;
            }

            if (! $resolve) {
                continue;
            }

            $this->serviceTournamentBracketStageMatch->updateMatch($match, $presenter);
        }
    }
}
