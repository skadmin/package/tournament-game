<?php

declare(strict_types=1);

namespace Skadmin\TournamentGame\Doctrine\TournamentBracketStage\Traits;

use Skadmin\TournamentGame\Doctrine\TournamentBracketStage\Type\TournamentBracketStageDoubleElimination;
use Skadmin\TournamentGame\Doctrine\TournamentBracketStageMatch\TournamentBracketStageMatch;

use function ceil;
use function intval;

trait TTournamentBracketStageDoubleElimination
{
    private function prepareMatchesDoubleElimination(TournamentBracketStageDoubleElimination $stage, int $countParticipants): void
    {
        $round = 0;

        // winners
        $powNumberOfMatches = 2;
        while ($powNumberOfMatches < $countParticipants) {
            $powNumberOfMatches *= 2;
        }

        $numberOfMatches = $powNumberOfMatches;

        $roundMatches = ['winner' => [], 'losser' => []];
        while ($numberOfMatches > 1) {
            $numberOfMatches                = intval(ceil($numberOfMatches / 2));
            $roundMatches['winner'][$round] = [];

            $number = 1;
            for ($i = 0; $i < $numberOfMatches; $i++) {
                if ($round === 0) {
                    $challenger = $this->loadParticipant($i);
                    $opponent   = $this->loadParticipant($powNumberOfMatches / 2 + $i);

                    $currentMatch = $this->facadeTournamentBracketStageMatch->create($stage, $number++, 0, $round, null, null, $challenger, $opponent);

                    if ($currentMatch === null) {
                        continue;
                    }

                    $this->facadeTournamentBracketStageMatch->updateMatchStatus($currentMatch->getId(), TournamentBracketStageMatch::STATUS_READY);
                    $roundMatches['winner'][$round][] = $currentMatch;
                } else {
                    $currentMatch = $this->facadeTournamentBracketStageMatch->create($stage, $number++, 0, $round);

                    if ($currentMatch === null) {
                        continue;
                    }

                    $roundMatches['winner'][$round][] = $currentMatch;
                }

                if ($round <= 0) {
                    continue;
                }

                $this->facadeTournamentBracketStageMatch->setMatchWinner($roundMatches['winner'][$round - 1][$i * 2], $currentMatch);
                $this->facadeTournamentBracketStageMatch->setMatchWinner($roundMatches['winner'][$round - 1][$i * 2 + 1], $currentMatch);
            }

            $round++;
        }

        // lossers
        $powNumberOfMatches = 2;
        while ($powNumberOfMatches < intval(ceil($countParticipants / 2))) {
            $powNumberOfMatches *= 2;
        }

        $numberOfMatches = $powNumberOfMatches;

        $redundantRounds = $round;
        $lastWinnerRound = $round - 1;
        $winnerRound     = 0;
        while ($numberOfMatches > 1) {
            $numberOfMatches = ceil($numberOfMatches / 2);

            // losser WB vs losser WB
            $roundMatches['losser'][$round] = [];

            $number = 1;
            for ($i = 0; $i < $numberOfMatches; $i++) {
                $currentMatch = $this->facadeTournamentBracketStageMatch->create($stage, $number++, 1, $round, null, null, null, null, $redundantRounds);

                if ($currentMatch === null) {
                    continue;
                }

                $roundMatches['losser'][$round][] = $currentMatch;

                if ($winnerRound === 0) {
                    $this->facadeTournamentBracketStageMatch->setMatchDefeated($roundMatches['winner'][$winnerRound][$i * 2], $currentMatch);
                    $this->facadeTournamentBracketStageMatch->setMatchDefeated($roundMatches['winner'][$winnerRound][$i * 2 + 1], $currentMatch);
                } else {
                    $this->facadeTournamentBracketStageMatch->setMatchWinner($roundMatches['losser'][$round - 1][$i * 2], $currentMatch);
                    $this->facadeTournamentBracketStageMatch->setMatchDefeated($roundMatches['losser'][$round - 1][$i * 2 + 1], $currentMatch);
                }
            }

            $round++;
            if ($winnerRound === 0) {
                $winnerRound++;
            }

            // winner LS vs losser WB
            if ($numberOfMatches === 1) {
                continue;
            }

            $roundMatches['losser'][$round] = [];

            $number = 1;
            for ($i = 0; $i < $numberOfMatches; $i++) {
                $currentMatch = $this->facadeTournamentBracketStageMatch->create($stage, $number++, 1, $round, null, null, null, null, $redundantRounds);

                if ($currentMatch === null) {
                    continue;
                }

                $roundMatches['losser'][$round][] = $currentMatch;

                $this->facadeTournamentBracketStageMatch->setMatchWinner($roundMatches['losser'][$round - 1][$i], $currentMatch);
                $this->facadeTournamentBracketStageMatch->setMatchDefeated($roundMatches['winner'][$winnerRound][$i], $currentMatch);
            }

            $round++;
            $winnerRound++;
        }

        // Grand final

        if ($currentMatch === null) {
            return;
        }

        $currentMatch = $this->facadeTournamentBracketStageMatch->create($stage, 1, 2, $round);
        $this->facadeTournamentBracketStageMatch->setMatchWinner($roundMatches['winner'][$lastWinnerRound][0], $currentMatch);
        $this->facadeTournamentBracketStageMatch->setMatchWinner($roundMatches['losser'][$round - 1][0], $currentMatch);
    }
}
