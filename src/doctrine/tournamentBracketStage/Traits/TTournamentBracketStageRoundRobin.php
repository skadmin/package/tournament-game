<?php

declare(strict_types=1);

namespace Skadmin\TournamentGame\Doctrine\TournamentBracketStage\Traits;

use Nette\Utils\Arrays;
use Skadmin\TournamentGame\Doctrine\TournamentBracketStage\Type\TournamentBracketStageRoundRobin;
use Skadmin\TournamentGame\Doctrine\TournamentBracketStageParticipant\TournamentBracketStageParticipant;

use function array_reverse;
use function ceil;
use function intval;

trait TTournamentBracketStageRoundRobin
{
    /**
     * https://cs.wikipedia.org/wiki/Ka%C5%BEd%C3%BD_s_ka%C5%BEd%C3%BDm
     */
    private function prepareMatchesRoundRobin(TournamentBracketStageRoundRobin $stage, int $countParticipants): void
    {
        if ($countParticipants % 2 === 1) {
            $numberOfRounds = $countParticipants;
        } else {
            $numberOfRounds = $countParticipants - 1;
        }

        $numberOfMatchesInRound = intval(ceil($countParticipants / 2));

        $challengerRoundIndexes = [];
        $opponentRoundIndexes   = [];

        $participantRoundMatches = [];

        // vytvoříme indexy pro vyzivatele
        $matchCounter = 0;
        for ($round = 0; $round < $numberOfRounds; $round++) {
            $matchIndex = 0;
            while ($matchIndex++ < $numberOfMatchesInRound) {
                $challengerRoundIndexes[$round][] = $matchCounter++ % $numberOfRounds;
            }
        }

        // vytvoříme indexy pro oponenty
        $matchCounter = 0;
        for ($round = $numberOfRounds - 1; $round >= 0; $round--) {
            $matchIndex = 0;
            while ($matchIndex++ < $numberOfMatchesInRound) {
                $opponentRoundIndexes[$round][] = $matchCounter++ % $numberOfRounds;
            }

            $opponentRoundIndexes[$round] = array_reverse($opponentRoundIndexes[$round]);
            $matchCounter--;
        }

        $opponentRoundIndexes = array_reverse($opponentRoundIndexes, true);

        // vytvoříme výsledné duely
        foreach ($challengerRoundIndexes as $round => $challengerIndexes) {
            foreach ($challengerIndexes as $index => $challengerIndex) {
                $opponentIndex = $opponentRoundIndexes[$round][$index];

                // pokud je lichý počet účastníků, tak má každý free win
                if ($countParticipants % 2 === 1) {
                    if ($challengerIndex !== $opponentIndex) {
                        $participantRoundMatches[$round][] = [$challengerIndex, $opponentIndex];
                    }
                } else { // pokud je sudý počet účastníků, tak posledního hráče doplníme místo duplicit
                    if ($challengerIndex === $opponentIndex) {
                        if ($round % 2 === 0) {
                            $opponentIndex = $countParticipants - 1;
                        } else {
                            $challengerIndex = $countParticipants - 1;
                        }
                    }

                    $participantRoundMatches[$round][] = [$challengerIndex, $opponentIndex];
                }
            }
        }

        // vytvoříme samotné kola a zápasy
        foreach ($participantRoundMatches as $round => $participantRoundMatch) {
            foreach ($participantRoundMatch as $number => $challengerAndOpponent) {
                [$challenger, $opponent] = Arrays::map($challengerAndOpponent, fn (?int $index): ?TournamentBracketStageParticipant => $this->loadParticipant($index ?? -1));
                $this->facadeTournamentBracketStageMatch->create($stage, $number, 0, $round, null, null, $challenger, $opponent);
            }
        }
    }
}
