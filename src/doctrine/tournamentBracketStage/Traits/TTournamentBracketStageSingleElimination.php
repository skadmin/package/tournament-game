<?php

declare(strict_types=1);

namespace Skadmin\TournamentGame\Doctrine\TournamentBracketStage\Traits;

use Skadmin\TournamentGame\Doctrine\TournamentBracketStage\Type\TournamentBracketStageSingleElimination;
use Skadmin\TournamentGame\Doctrine\TournamentBracketStageMatch\TournamentBracketStageMatch;

use function ceil;
use function intval;

trait TTournamentBracketStageSingleElimination
{
    private function prepareMatchesSingleElimination(TournamentBracketStageSingleElimination $stage, int $countParticipants): void
    {
        $round = 0;

        // winners
        $powNumberOfMatches = 2;
        while ($powNumberOfMatches < $countParticipants) {
            $powNumberOfMatches *= 2;
        }

        $numberOfMatches = $powNumberOfMatches;

        $roundMatches = ['winner' => []];
        while ($numberOfMatches > 1) {
            $numberOfMatches                = intval(ceil($numberOfMatches / 2));
            $roundMatches['winner'][$round] = [];

            $number = 1;
            for ($i = 0; $i < $numberOfMatches; $i++) {
                if ($round === 0) {
                    $challenger = $this->loadParticipant($i);
                    $opponent   = $this->loadParticipant($powNumberOfMatches / 2 + $i);

                    $currentMatch = $this->facadeTournamentBracketStageMatch->create($stage, $number++, 0, $round, null, null, $challenger, $opponent);

                    if ($currentMatch === null) {
                        continue;
                    }

                    $this->facadeTournamentBracketStageMatch->updateMatchStatus($currentMatch->getId(), TournamentBracketStageMatch::STATUS_READY);
                    $roundMatches['winner'][$round][] = $currentMatch;
                } else {
                    $currentMatch = $this->facadeTournamentBracketStageMatch->create($stage, $number++, 0, $round);

                    if ($currentMatch === null) {
                        continue;
                    }

                    $roundMatches['winner'][$round][] = $currentMatch;
                }

                if ($round <= 0) {
                    continue;
                }

                $this->facadeTournamentBracketStageMatch->setMatchWinner($roundMatches['winner'][$round - 1][$i * 2], $currentMatch);
                $this->facadeTournamentBracketStageMatch->setMatchWinner($roundMatches['winner'][$round - 1][$i * 2 + 1], $currentMatch);
            }

            $round++;
        }

        // Grand final
        $currentMatch = $this->facadeTournamentBracketStageMatch->create($stage, 1, 1, $round);

        if ($currentMatch === null) {
            return;
        }

        $this->facadeTournamentBracketStageMatch->setMatchDefeated($roundMatches['winner'][$round - 2][0], $currentMatch);
        $this->facadeTournamentBracketStageMatch->setMatchDefeated($roundMatches['winner'][$round - 2][1], $currentMatch);
    }
}
