<?php

declare(strict_types=1);

namespace Skadmin\TournamentGame\Doctrine\TournamentBracketStage\Type;

use Doctrine\ORM\Mapping as ORM;
use Skadmin\TournamentGame\Doctrine\TournamentBracketStage\ATournamentBracketStage;

use function ceil;
use function count;
use function floor;
use function json_encode;

#[ORM\Entity]
#[ORM\HasLifecycleCallbacks]
class TournamentBracketStageDoubleElimination extends ATournamentBracketStage
{
    public const TYPE = 'doubleElimination';

    #[ORM\Column]
    protected int $bestOfGameLoser = 1;

    public function getType(): string
    {
        return self::TYPE;
    }

    public function getBestOfGameLoser(): int
    {
        return $this->bestOfGameLoser;
    }

    public function setBestOfGameLoser(int $bestOfGameLoser): void
    {
        $this->bestOfGameLoser = $bestOfGameLoser;
    }

    public function getBracketViewerData(bool $asJson = false): string|array
    {
        $roundId = 0;
        $rounds  = [];

        $participants = $this->prepareParticipants();

        $numberOfMatches = count($participants);
        $number          = 1;
        while ($numberOfMatches > 1) {
            $numberOfMatches = ceil($numberOfMatches / 2);

            $rounds[] = [
                'id'       => $roundId++,
                'number'   => $number++,
                'stage_id' => $this->getId(),
                'group_id' => 0,
            ];
        }

        $numberOfMatches = ceil(count($participants) / 2);
        $number          = 1;
        while ($numberOfMatches > 1) {
            $numberOfMatches = ceil($numberOfMatches / 2);

            $rounds[] = [
                'id'       => $roundId++,
                'number'   => $number++,
                'stage_id' => $this->getId(),
                'group_id' => 1,
            ];

            $rounds[] = [
                'id'       => $roundId++,
                'number'   => $number++,
                'stage_id' => $this->getId(),
                'group_id' => 1,
            ];
        }

        $rounds[] = [
            'id'       => $roundId++,
            'number'   => 1,
            'stage_id' => $this->getId(),
            'group_id' => 2,
        ];

        if (($this->getParticipants()->count() / 2) % 2 === 1) {
            $stageSize = (floor($this->getParticipants()->count() / 2) + 1) * 2;
        } else {
            $stageSize = $this->getParticipants()->count();
        }

        $data = [
            'participant' => $participants,
            'stage'       => [
                [
                    'id'            => $this->getId(),
                    'tournament_id' => $this->getBracket()->getTournament()->getId(),
                    'name'          => $this->getName(),
                    'type'          => 'double_elimination',
                    'number'        => 1,
                    'settings'      => [
                        'size'              => $stageSize,
                        'seedOrdering'      => [
                            'natural',
                            'natural',
                            'reverse_half_shift',
                            'reverse',
                        ],
                        'grandFinal'        => 'double',
                        'matchesChildCount' => 0,
                    ],
                ],
            ],
            'group'       => [
                [
                    'id'       => 0,
                    'stage_id' => $this->getId(),
                    'number'   => 1,
                ],
                [
                    'id'       => 1,
                    'stage_id' => $this->getId(),
                    'number'   => 2,
                ],
                [
                    'id'       => 2,
                    'stage_id' => $this->getId(),
                    'number'   => 3,
                ],
            ],
            'round'       => $rounds,
            'match'       => $this->prepareMatches(),
            'match_game'  => [],
        ];

        if ($asJson) {
            return json_encode($data);
        }

        return $data;
    }
}
