<?php

declare(strict_types=1);

namespace Skadmin\TournamentGame\Doctrine\TournamentBracketStage\Type;

use Doctrine\ORM\Mapping as ORM;
use Skadmin\TournamentGame\Doctrine\TournamentBracketStage\ATournamentBracketStage;
use Tracy\Debugger;

use function ceil;
use function count;
use function floor;
use function json_encode;

#[ORM\Entity]
#[ORM\HasLifecycleCallbacks]
class TournamentBracketStageRoundRobinDuel extends ATournamentBracketStage
{
    public const TYPE = 'roundRobinDuel';

    #[ORM\Column]
    protected int $numberOfDuels = 2;

    #[ORM\Column]
    protected int $numberOfRoundDuels = 1;

    public function getType(): string
    {
        return self::TYPE;
    }

    public function getNumberOfDuels(): int
    {
        return $this->numberOfDuels;
    }

    public function setNumberOfDuels(int $numberOfDuels): void
    {
        $this->numberOfDuels = $numberOfDuels;
    }

    public function getNumberOfRoundDuels(): int
    {
        return $this->numberOfRoundDuels;
    }

    public function setNumberOfRoundDuels(int $numberOfRoundDuels): void
    {
        $this->numberOfRoundDuels = $numberOfRoundDuels;
    }

    public function getBracketViewerData(bool $asJson = false): string|array
    {
        $roundId = 0;
        $rounds  = [];

        $participants = $this->prepareParticipants();

        $numberOfMatches = count($participants);
        $number          = 1;
        while ($numberOfMatches > 1) {
            $numberOfMatches = ceil($numberOfMatches / 2);

            $rounds[] = [
                'id'       => $roundId++,
                'number'   => $number++,
                'stage_id' => $this->getId(),
                'group_id' => 0,
            ];
        }

        $rounds[] = [
            'id'       => $roundId++,
            'number'   => 1,
            'stage_id' => $this->getId(),
            'group_id' => 1,
        ];

        if (($this->getParticipants()->count() / 2) % 2 === 1) {
            $stageSize = (floor($this->getParticipants()->count() / 2) + 1) * 2;
        } else {
            $stageSize = $this->getParticipants()->count();
        }

        $data = [
            'participant' => $participants,
            'stage'       => [
                [
                    'id'            => $this->getId(),
                    'tournament_id' => $this->getBracket()->getTournament()->getId(),
                    'name'          => $this->getName(),
                    'type'          => 'round_robin',
                    'number'        => 1,
                    'settings'      => [
                        'size'              => $stageSize,
                        'seedOrdering'      => [
                            'natural',
                            'natural',
                            'reverse_half_shift',
                            'reverse',
                        ],
                        'grandFinal'        => 'double',
                        'matchesChildCount' => 0,
                    ],
                ],
            ],
            'group'       => [
                [
                    'id'       => 0,
                    'stage_id' => $this->getId(),
                    'number'   => 1,
                ],
                [
                    'id'       => 1,
                    'stage_id' => $this->getId(),
                    'number'   => 2,
                ],
            ],
            'round'       => $rounds,
            'match'       => $this->prepareMatches(),
            'match_game'  => [],
        ];

        Debugger::barDump($data);

        if ($asJson) {
            return json_encode($data);
        }

        return $data;
    }
}
