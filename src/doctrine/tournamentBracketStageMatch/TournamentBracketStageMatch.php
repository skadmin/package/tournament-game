<?php

declare(strict_types=1);

namespace Skadmin\TournamentGame\Doctrine\TournamentBracketStageMatch;

use App\Model\Doctrine\Customer\Customer;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Nette\Utils\DateTime;
use Skadmin\TournamentGame\Doctrine\TeamPlayer\TeamPlayer;
use Skadmin\TournamentGame\Doctrine\Tournament\Tournament;
use Skadmin\TournamentGame\Doctrine\TournamentBracketStage\ATournamentBracketStage;
use Skadmin\TournamentGame\Doctrine\TournamentBracketStageMatchDuel\TournamentBracketStageMatchDuel;
use Skadmin\TournamentGame\Doctrine\TournamentBracketStageParticipant\TournamentBracketStageParticipant;
use Skadmin\TournamentGame\Doctrine\TournamentResult\TournamentResult;
use SkadminUtils\DoctrineTraits\Entity;
use DateTimeInterface;

#[ORM\Entity]
#[ORM\HasLifecycleCallbacks]
class TournamentBracketStageMatch
{
    use Entity\BaseEntity;

    public const STATUS_AWAITING_PARTICIPANTS = 0; // awaiting participants; čeká na hráče
    public const STATUS_READY = 1; // ready; připraveno
    public const STATUS_RUNNING = 2; // running; probíhá (vyvolají hráči)
    public const STATUS_AWAITING_RESULTS = 3; // awaiting results; čeká se na výsledky (nastavením stavu se spouští odpočet automatického potvrzení)
    public const STATUS_COMPLETED = 4; // completed; dohráno
    public const STATUS_AWAITING_ADMIN = 5; // awaiting admin; červený tlačítko, je potřeba zásah admina
    public const STATUS_MAINTENANCE = 6; // maintenance; admin řeší - info pro ostatní že není třeba dalšího
    public const STATUS_AWAITING_RESULTS_LONG = 7; // awaiting results; čeká se na výsledky (nastavením stavu se spouští odpočet automatického potvrzení)


    public const STATUTES = [
        self::STATUS_AWAITING_PARTICIPANTS => 'tournament-bracket-stage-match.awaiting-participants',
        self::STATUS_READY => 'tournament-bracket-stage-match.ready',
        self::STATUS_RUNNING => 'tournament-bracket-stage-match.running',
        self::STATUS_AWAITING_RESULTS => 'tournament-bracket-stage-match.awaiting-results',
        self::STATUS_AWAITING_RESULTS_LONG => 'tournament-bracket-stage-match.awaiting-results-long',
        self::STATUS_COMPLETED => 'tournament-bracket-stage-match.completed',
        self::STATUS_AWAITING_ADMIN => 'tournament-bracket-stage-match.awaiting-admin',
        self::STATUS_MAINTENANCE => 'tournament-bracket-stage-match.maintenance',
    ];

    public const STATUS_FINAL = [
        self::STATUS_COMPLETED,
        self::STATUS_AWAITING_ADMIN,
        self::STATUS_MAINTENANCE,
    ];

    public const STATUS_DUELS_AWAITING_PARTICIPANTS = 0; // awaiting participants; čeká na hráče
    public const STATUS_DUELS_READY = 1; // ready; připraveno

    #[ORM\Column]
    private int $number;

    #[ORM\Column]
    private int $groupId;

    #[ORM\Column]
    private int $roundId;

    #[ORM\Column]
    private int $status = self::STATUS_AWAITING_PARTICIPANTS;

    #[ORM\Column]
    private int $statusOfChallengerDuels = self::STATUS_DUELS_AWAITING_PARTICIPANTS;

    #[ORM\Column]
    private int $statusOfOpponentDuels = self::STATUS_DUELS_AWAITING_PARTICIPANTS;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    private ?DateTimeInterface $duelsChallengerReadyAt = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    private ?DateTimeInterface $duelsOpponentReadyAt = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    private ?DateTimeInterface $allowedFrom = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    private ?DateTimeInterface $allowedFromDraft = null;

    #[ORM\ManyToOne(targetEntity: TournamentBracketStageParticipant::class)]
    #[ORM\JoinColumn(onDelete: 'cascade')]
    private ?TournamentBracketStageParticipant $allowedFromDraftBy = null;

    #[ORM\ManyToOne(targetEntity: ATournamentBracketStage::class, inversedBy: 'matches')]
    #[ORM\JoinColumn(onDelete: 'cascade')]
    private ATournamentBracketStage $stage;

    #[ORM\Column(nullable: true)]
    private ?int $challengerResult = null;

    #[ORM\Column(nullable: true)]
    private ?int $opponentResult = null;

    #[ORM\ManyToOne(targetEntity: TournamentBracketStageParticipant::class)]
    #[ORM\JoinColumn(onDelete: 'cascade')]
    private ?TournamentBracketStageParticipant $challenger = null;

    #[ORM\ManyToOne(targetEntity: TournamentBracketStageParticipant::class)]
    #[ORM\JoinColumn(onDelete: 'cascade')]
    private ?TournamentBracketStageParticipant $opponent = null;

    #[ORM\ManyToOne(targetEntity: self::class)]
    #[ORM\JoinColumn(onDelete: 'cascade')]
    private ?TournamentBracketStageMatch $matchWinner = null;

    #[ORM\ManyToOne(targetEntity: self::class)]
    #[ORM\JoinColumn(onDelete: 'cascade')]
    private ?TournamentBracketStageMatch $matchDefeated = null;

    /** @var ArrayCollection|Collection|TournamentResult[] */
    #[ORM\ManyToMany(targetEntity: TournamentResult::class)]
    #[ORM\JoinTable(name: 'tournament_bracket_stage_match_rel_result')]
    private ArrayCollection|Collection|array $results;

    /** @var ArrayCollection|Collection|TournamentBracketStageMatchDuel[] */
    #[ORM\OneToMany(targetEntity: TournamentBracketStageMatchDuel::class, mappedBy: 'match', cascade: ['persist', 'remove'], orphanRemoval: true)]
    private ArrayCollection|Collection|array $duels;

    public function __construct()
    {
        $this->results = new ArrayCollection();
        $this->duels = new ArrayCollection();
    }

    public function create(ATournamentBracketStage $stage, int $number, int $groupId, int $roundId, ?TournamentBracketStageMatch $matchWinner, ?TournamentBracketStageMatch $matchDefeated, ?TournamentBracketStageParticipant $challenger = null, ?TournamentBracketStageParticipant $opponent = null): void
    {
        $this->number = $number;
        $this->groupId = $groupId;
        $this->roundId = $roundId;

        $this->stage = $stage;

        $this->matchWinner = $matchWinner;
        $this->matchDefeated = $matchDefeated;

        $this->challenger = $challenger;
        $this->opponent = $opponent;
    }

    public function updateStatus(int $status): void
    {
        $this->status = $status;
    }

    public function getStatusOfChallengerDuels(): int
    {
        return $this->statusOfChallengerDuels;
    }

    public function getStatusOfOpponentDuels(): int
    {
        return $this->statusOfOpponentDuels;
    }

    public function getDuelsChallengerReadyAt(): ?DateTimeInterface
    {
        return $this->duelsChallengerReadyAt;
    }

    public function getDuelsOpponentReadyAt(): ?DateTimeInterface
    {
        return $this->duelsOpponentReadyAt;
    }

    public function getDuelsIsReady(): bool
    {
        return $this->statusOfChallengerDuels === self::STATUS_DUELS_READY && $this->statusOfOpponentDuels === self::STATUS_DUELS_READY;
    }

    public function challengerDuelsIsReady(): void
    {
        $this->statusOfChallengerDuels = self::STATUS_DUELS_READY;
        $this->duelsChallengerReadyAt = new DateTime();
    }

    public function opponentDuelsIsReady(): void
    {
        $this->statusOfOpponentDuels = self::STATUS_DUELS_READY;
        $this->duelsOpponentReadyAt = new DateTime();
    }

    public function challengerDuelsReset(): void
    {
        $this->statusOfChallengerDuels = self::STATUS_DUELS_AWAITING_PARTICIPANTS;
        $this->duelsChallengerReadyAt = null;
    }

    public function opponentDuelsReset(): void
    {
        $this->statusOfOpponentDuels = self::STATUS_DUELS_AWAITING_PARTICIPANTS;
        $this->duelsOpponentReadyAt = null;
    }

    public function updateAllowedFrom(?DateTimeInterface $allowedFrom): void
    {
        $this->allowedFrom = $allowedFrom;
    }

    public function updateAllowedFromDraft(TournamentBracketStageParticipant $participant, ?DateTimeInterface $allowedFromDraft): void
    {
        $this->allowedFromDraftBy = $participant;
        $this->allowedFromDraft = $allowedFromDraft;
    }

    public function resolveAllowedFromDraft(bool $enable): void
    {
        if ($enable) {
            $this->allowedFrom = $this->allowedFromDraft;
        }

        $this->allowedFromDraftBy = null;
        $this->allowedFromDraft = null;
        $this->updateResults(0, 0);

        $this->statusOfChallengerDuels = self::STATUS_DUELS_AWAITING_PARTICIPANTS;
        $this->statusOfOpponentDuels = self::STATUS_DUELS_AWAITING_PARTICIPANTS;

        $this->duelsChallengerReadyAt = null;
        $this->duelsOpponentReadyAt = null;
    }

    public function setChallenger(?TournamentBracketStageParticipant $challenger): void
    {
        $this->challenger = $challenger;
    }

    public function setOpponent(?TournamentBracketStageParticipant $opponent): void
    {
        $this->opponent = $opponent;
    }

    public function setMatchWinner(?TournamentBracketStageMatch $matchWinner): void
    {
        $this->matchWinner = $matchWinner;
    }

    public function setMatchDefeated(?TournamentBracketStageMatch $matchDefeated): void
    {
        $this->matchDefeated = $matchDefeated;
    }

    public function updateResults(?int $challengerResult, ?int $opponentResult): void
    {
        $this->challengerResult = $challengerResult;
        $this->opponentResult = $opponentResult;
    }

    public function getNumber(): int
    {
        return $this->number;
    }

    public function getGroupId(): int
    {
        return $this->groupId;
    }

    public function getRoundId(): int
    {
        return $this->roundId;
    }

    public function getStatus(): int
    {
        return $this->status;
    }

    public function getAllowedFrom(): ?DateTimeInterface
    {
        return $this->allowedFrom;
    }

    public function isAllowed(): bool
    {
        return $this->allowedFrom === null || $this->allowedFrom <= new DateTime();
    }

    public function getAllowedFromDraft(): ?DateTimeInterface
    {
        return $this->allowedFromDraft;
    }

    public function getAllowedFromDraftBy(): ?TournamentBracketStageParticipant
    {
        return $this->allowedFromDraftBy;
    }

    public function getStage(): ATournamentBracketStage
    {
        return $this->stage;
    }

    public function getChallengerResult(): ?int
    {
        return $this->challengerResult;
    }

    public function getOpponentResult(): ?int
    {
        return $this->opponentResult;
    }

    public function getChallenger(): ?TournamentBracketStageParticipant
    {
        return $this->challenger;
    }

    public function getOpponent(): ?TournamentBracketStageParticipant
    {
        return $this->opponent;
    }

    public function getMatchWinner(): ?TournamentBracketStageMatch
    {
        return $this->matchWinner;
    }

    public function getMatchDefeated(): ?TournamentBracketStageMatch
    {
        return $this->matchDefeated;
    }

    /**
     * @return ArrayCollection|Collection|TournamentResult[]
     */
    public function getResults(?int $status = null): ArrayCollection|Collection|array
    {
        if ($status === null) {
            return $this->results;
        }

        $criteria = Criteria::create();
        $criteria->where(Criteria::expr()->eq('status', $status));

        return $this->results->matching($criteria);
    }

    public function addResult(TournamentResult $result): void
    {
        $this->results->add($result);
    }

    public function getResult(int $resultId): ?TournamentResult
    {
        $criteria = Criteria::create();
        $criteria->where(Criteria::expr()->eq('id', $resultId));

        $result = $this->results->matching($criteria)->first();

        return $result instanceof TournamentResult ? $result : null;
    }

    /**
     * @return ArrayCollection|Collection|TournamentBracketStageMatchDuel[]
     */
    public function getDuels(): ArrayCollection|Collection|array
    {
        return $this->duels;
    }

    public function getDuel(int $numberOfDuel): ?TournamentBracketStageMatchDuel
    {
        $criteria = Criteria::create();
        $criteria->where(Criteria::expr()->eq('numberOfDuel', $numberOfDuel));

        $duel = $this->duels->matching($criteria)->first();

        return $duel instanceof TournamentBracketStageMatchDuel ? $duel : null;
    }

    public function addDuel(TournamentBracketStageMatchDuel $duel): void
    {
        if (!$this->duels->contains($duel)) {
            $this->duels->add($duel);
        }
    }

    public function isParticipant(?Customer $customer, ?bool $asChallenger = null): bool
    {
        if (!$customer instanceof Customer) {
            return false;
        }

        if ($this->getTournament()->isOneVsOne()) {
            $challenger = $this->getChallenger() instanceof TournamentBracketStageParticipant && $this->getChallenger()->getRegistration()->getUser() === $customer;
            $opponent = $this->getOpponent() instanceof TournamentBracketStageParticipant && $this->getOpponent()->getRegistration()->getUser() === $customer;
        } else {
            $challenger = $this->getChallenger() instanceof TournamentBracketStageParticipant && $this->getChallenger()->getRegistration()->isPlayerIn($customer);
            $opponent = $this->getOpponent() instanceof TournamentBracketStageParticipant && $this->getOpponent()->getRegistration()->isPlayerIn($customer);
        }

        if ($asChallenger === null) {
            return $challenger || $opponent;
        }

        return $asChallenger ? $challenger : $opponent;
    }

    public function isAnyCaptionOrManager(Customer $customer): bool
    {
        if ($this->getTournament()->isOneVsOne()) {
            $challenger = $this->getChallenger() instanceof TournamentBracketStageParticipant && $this->getChallenger()->getRegistration()->getUser() === $customer;
            $opponent = $this->getOpponent() instanceof TournamentBracketStageParticipant && $this->getOpponent()->getRegistration()->getUser() === $customer;
        } else {
            $roles = [TeamPlayer::ROLE_CAPTAIN, TeamPlayer::ROLE_DEPUTY_CAPTAIN, TeamPlayer::ROLE_MANAGER];
            $challenger = $this->getChallenger() instanceof TournamentBracketStageParticipant && $this->getChallenger()->getRegistration()->isInRole($customer, $roles);
            $opponent = $this->getOpponent() instanceof TournamentBracketStageParticipant && $this->getOpponent()->getRegistration()->isInRole($customer, $roles);
        }

        return $challenger || $opponent;
    }

    public function getTournament(): Tournament
    {
        return $this->getStage()->getBracket()->getTournament();
    }

    public function isDuelsReady(bool $forChallenger = true, bool $excludeAce = true): bool
    {
        $limit = $excludeAce ? 1 : 0;

        return $this->getDuels()
                ->matching(Criteria::create()
                    ->where(Criteria::expr()
                        ->eq($forChallenger ? 'challenger' : 'opponent', null)
                    )
                )->count() <= $limit; // jelikož ACE se vyplňuje až po výsledku, tak se počítá i s ním
    }

    public function getCurrentNumberOfDuel(): int
    {
        $resultByDuels = [];
        foreach ($this->getResults() as $result) {
            if ($result->getStatus() !== TournamentResult::STATUS_CONFIRMED) {
                continue;
            }

            $resultByDuels[$result->getNumberOfDuel()][$result->getNumberOfMatch()] = 1;
        }

        if ($this->getStage() instanceof TournamentBracketStageMatchDuel) {
            $resultByDuels = array_filter($resultByDuels, fn($duel): bool => count($duel) === $this->getStage()->getNumberOfRoundDuels());
        }
        
        return count($resultByDuels) + 1;
    }

    public function isNeedAce(): bool
    {
        return ($this->isDuelsReady(true, false) && $this->isDuelsReady(false, false))
            || ($this->getCurrentNumberOfDuel() > $this->getStage()->getNumberOfDuels() && $this->getChallengerResult() === $this->getOpponentResult());
    }

}
