<?php

declare(strict_types=1);

namespace Skadmin\TournamentGame\Doctrine\TournamentBracketStageMatch;

use Doctrine\Common\Collections\Criteria;
use Nette\Utils\DateTime;
use Nettrine\ORM\EntityManagerDecorator;
use Skadmin\FileStorage\FileStorage;
use Skadmin\TournamentGame\Doctrine\TournamentBracketStage\ATournamentBracketStage;
use Skadmin\TournamentGame\Doctrine\TournamentBracketStage\Type\TournamentBracketStageDoubleElimination;
use Skadmin\TournamentGame\Doctrine\TournamentBracketStage\Type\TournamentBracketStageRoundRobinDuel;
use Skadmin\TournamentGame\Doctrine\TournamentBracketStageMatchDuel\TournamentBracketStageMatchDuelService;
use Skadmin\TournamentGame\Doctrine\TournamentBracketStageParticipant\TournamentBracketStageParticipant;
use SkadminUtils\DoctrineTraits\Facade;
use DateTimeInterface;

final class TournamentBracketStageMatchFacade extends Facade
{

    private FileStorage $fileStorage;

    public function __construct(
        EntityManagerDecorator $em,
        FileStorage $fileStorage
    ) {
        parent::__construct($em);
        $this->fileStorage = $fileStorage;
        $this->table = TournamentBracketStageMatch::class;
    }

    public function create(
        ATournamentBracketStage $stage,
        int $number,
        int $group,
        int $round,
        ?TournamentBracketStageMatch $matchWinner = null,
        ?TournamentBracketStageMatch $matchDefeated = null,
        ?TournamentBracketStageParticipant $challenger = null,
        ?TournamentBracketStageParticipant $opponent = null,
        int $redundantRounds = 0
    ): ?TournamentBracketStageMatch {
        if (!$stage->isWillPlayInTheFinals() && ($round - $redundantRounds) >= $stage->getNumberOfRounds()) {
            return null;
        }

        $bracketStageMatch = new TournamentBracketStageMatch();
        $bracketStageMatch->create(
            $stage,
            $number,
            $group,
            $round,
            $matchWinner,
            $matchDefeated,
            $challenger,
            $opponent
        );

        if ($stage instanceof TournamentBracketStageRoundRobinDuel) {
            $allowedFrom = DateTime::from($stage->getBracket()->getTournament()->getTermFrom()->getTimestamp());
            $allowedFrom->modify(sprintf('+%d days', $round * 7));
            $bracketStageMatch->updateAllowedFrom($allowedFrom);
        }

        $this->em->persist($bracketStageMatch);
        $this->em->flush();

        return $bracketStageMatch;
    }

    public function update(
        int $bracketStageId,
        string $name,
        bool $willPlayInTheFinals,
        int $numberOfRounds,
        int $bestOfGame
    ): TournamentBracketStageMatch {
        $bracketStage = $this->get($bracketStageId);
        $bracketStage->update($name, $willPlayInTheFinals, $numberOfRounds, $bestOfGame);

        $this->em->flush();

        return $bracketStage;
    }

    public function setMatchWinner(TournamentBracketStageMatch $match, TournamentBracketStageMatch $matchWinner): void
    {
        $match->setMatchWinner($matchWinner);
        $this->em->flush();
    }

    public function setMatchDefeated(
        TournamentBracketStageMatch $match,
        TournamentBracketStageMatch $matchDefeated
    ): void {
        $match->setMatchDefeated($matchDefeated);
        $this->em->flush();
    }

    public function updateMatchStatus(int $matchId, int $status): TournamentBracketStageMatch
    {
        $match = $this->get($matchId);
        $match->updateStatus($status);

        $this->em->flush();

        return $match;
    }

    public function updateMatch(
        int $matchId,
        int $status,
        ?TournamentBracketStageParticipant $challenger,
        ?TournamentBracketStageParticipant $opponent,
        ?int $challengerResult,
        ?int $opponentResult,
        bool $evaluateTheResult = false
    ): TournamentBracketStageMatch {
        $match = $this->get($matchId);
        $stage = $match->getStage();

        // najdeme původní zápasy
        $challengerMatch = $stage->findMatch($challenger, $match->getGroupId(), $match->getRoundId());
        $opponentMatch = $stage->findMatch($opponent, $match->getGroupId(), $match->getRoundId());

        // vyměníme případné účastníky ve starých zápasech
        if ($challengerMatch !== null && $challengerMatch->getId() !== $match->getId()) {
            if ($challengerMatch->getChallenger() === $challenger) {
                $challengerMatch->setChallenger($match->getChallenger());
            } elseif ($challengerMatch->getOpponent() === $challenger) {
                $challengerMatch->setOpponent($match->getChallenger());
            }
        }

        if ($opponentMatch !== null && $opponentMatch->getId() !== $match->getId()) {
            if ($opponentMatch->getChallenger() === $opponent) {
                $opponentMatch->setChallenger($match->getOpponent());
            } elseif ($opponentMatch->getOpponent() === $opponent) {
                $opponentMatch->setOpponent($match->getOpponent());
            }
        }

        // upravíme účastníky v aktulním zápasu
        $match->setChallenger($challenger);
        $match->setOpponent($opponent);
        $match->updateStatus($status);

        $this->updateResultAndEvaluate($matchId, $challengerResult, $opponentResult, $evaluateTheResult);

        $this->em->flush();

        return $match;
    }

    public function updateResults(
        int $matchId,
        ?int $challengerResult,
        ?int $opponentResult
    ): TournamentBracketStageMatch {
        $match = $this->get($matchId);

        $match->updateResults($challengerResult, $opponentResult);

        $this->em->flush();

        return $match;
    }

    public function updateResultAndEvaluate(
        int $matchId,
        ?int $challengerResult,
        ?int $opponentResult,
        bool $evaluateTheResult = false,
        bool $flush = false
    ): TournamentBracketStageMatch {
        $match = $this->get($matchId);
        $match->updateResults($challengerResult, $opponentResult);

        if ($evaluateTheResult && $challengerResult !== null && $opponentResult !== null) {
            $match->updateStatus(TournamentBracketStageMatch::STATUS_COMPLETED);

            $matchWinner = $match->getMatchWinner();
            $matchDefeated = $match->getMatchDefeated();
            if ($challengerResult > $opponentResult) {
                $matchWinnerChallenger = $match->getChallenger();
                $matchDefeatedChallenger = $match->getOpponent();
            } else {
                $matchWinnerChallenger = $match->getOpponent();
                $matchDefeatedChallenger = $match->getChallenger();
            }

            if ($match->getStage() instanceof TournamentBracketStageDoubleElimination) {
                switch ($match->getGroupId()) {
                    case 0:
                        if ($match->getRoundId() === 0) {
                            if ($match->getNumber() % 2 === 1) {
                                if ($matchWinner !== null) {
                                    $matchWinner->setChallenger($matchWinnerChallenger);
                                }

                                if ($matchDefeated !== null) {
                                    $matchDefeated->setChallenger($matchDefeatedChallenger);
                                }
                            } else {
                                if ($matchWinner !== null) {
                                    $matchWinner->setOpponent($matchWinnerChallenger);
                                }

                                if ($matchDefeated !== null) {
                                    $matchDefeated->setOpponent($matchDefeatedChallenger);
                                }
                            }
                        } else {
                            if ($match->getNumber() % 2 === 1) {
                                if ($matchWinner !== null) {
                                    $matchWinner->setChallenger($matchWinnerChallenger);
                                }
                            } else {
                                if ($matchWinner !== null) {
                                    $matchWinner->setOpponent($matchWinnerChallenger);
                                }
                            }

                            if ($matchDefeated !== null) {
                                $matchDefeated->setChallenger($matchDefeatedChallenger);
                            }
                        }

                        break;
                    case 1:
                        $firstMatch = $match->getStage()->getMatches(null, null, 1, ['roundId' => Criteria::ASC]
                        )->first();
                        $firstRoundDE = $firstMatch instanceof TournamentBracketStageMatch ? $firstMatch->getRoundId(
                        ) : 0;

                        $lastMatch = $match->getStage()->getMatches(null, null, 1, ['roundId' => Criteria::DESC]
                        )->first();
                        $lastRoundDE = $lastMatch instanceof TournamentBracketStageMatch ? $lastMatch->getRoundId() : 0;

                        if ($match->getRoundId() === $lastRoundDE) {
                            $matchWinner->setOpponent($matchWinnerChallenger);
                        } elseif (($match->getRoundId() - $firstRoundDE) % 2 === 0) {
                            if ($matchWinner !== null) {
                                $matchWinner->setOpponent($matchWinnerChallenger);
                            }
                        } elseif ($match->getNumber() % 2 === 1) {
                            if ($matchWinner !== null) {
                                $matchWinner->setChallenger($matchWinnerChallenger);
                            }
                        } else {
                            if ($matchDefeated !== null) {
                                $matchDefeated->setOpponent($matchWinnerChallenger);
                            }
                        }

                        break;
                }
            } else {
                if ($match->getNumber() % 2 === 1) {
                    if ($matchWinner !== null) {
                        $matchWinner->setChallenger($matchWinnerChallenger);
                    }

                    if ($matchDefeated !== null) {
                        $matchDefeated->setChallenger($matchDefeatedChallenger);
                    }
                } else {
                    if ($matchWinner !== null) {
                        $matchWinner->setOpponent($matchWinnerChallenger);
                    }

                    if ($matchDefeated !== null) {
                        $matchDefeated->setOpponent($matchDefeatedChallenger);
                    }
                }
            }

            if ($matchWinner !== null && $matchWinner->getChallenger() !== null && $matchWinner->getOpponent(
                ) !== null) {
                $matchWinner->updateStatus(TournamentBracketStageMatch::STATUS_READY);
            }

            if ($matchDefeated !== null && $matchDefeated->getChallenger() !== null && $matchDefeated->getOpponent(
                ) !== null) {
                $matchDefeated->updateStatus(TournamentBracketStageMatch::STATUS_READY);
            }
        }

        if ($flush) {
            $this->em->flush();
        }

        return $match;
    }

    public function updateAllowedFrom(int $matchId, ?DateTimeInterface $allowedFrom): TournamentBracketStageMatch
    {
        $match = $this->get($matchId);
        $match->updateAllowedFrom($allowedFrom);
        $this->em->flush();

        return $match;
    }

    public function updateAllowedFromDraft(
        int $matchId,
        TournamentBracketStageParticipant $participant,
        ?DateTimeInterface $allowedFromDraft
    ): TournamentBracketStageMatch {
        $match = $this->get($matchId);
        $match->updateAllowedFromDraft($participant, $allowedFromDraft);
        $this->em->flush();

        return $match;
    }

    public function confirmAllowedFromDraft(int $matchId): TournamentBracketStageMatch
    {
        $match = $this->get($matchId);
        $match->resolveAllowedFromDraft(true);

        foreach ($match->getDuels() as $duel) {
            $this->em->remove($duel);
        }

        foreach ($match->getResults() as $result) {
            $this->em->remove($result);
        }

        $this->em->flush();

        return $match;
    }

    public function rejectAllowedFromDraft(int $matchId): TournamentBracketStageMatch
    {
        $match = $this->get($matchId);
        $match->resolveAllowedFromDraft(false);
        $this->em->flush();

        return $match;
    }

    public function get(?int $id = null): ?TournamentBracketStageMatch
    {
        if ($id === null) {
            return null;
        }

        $bracketStage = parent::get($id);

        if ($bracketStage === null) {
            return null;
        }

        return $bracketStage;
    }

    public function duelsIsReady(int $matchId, bool $isChallenger): ?TournamentBracketStageMatch
    {
        $match = $this->get($matchId);

        if (!$match->isLoaded()) {
            return null;
        }

        foreach ($match->getDuels() as $duel) {
            if ($duel->getNumberOfDuel() > $match->getStage()->getNumberOfDuels()) {
                continue;
            }

            if ($isChallenger) {
                if ($duel->getChallenger() === null) {
                    return null;
                }
            } else {
                if ($duel->getOpponent() === null) {
                    return null;
                }
            }
        }

        if ($isChallenger) {
            $match->challengerDuelsIsReady();
        } else {
            $match->opponentDuelsIsReady();
        }

        $this->em->flush();

        return $match;
    }


    public function duelsReset(int $matchId, bool $isChallenger): ?TournamentBracketStageMatch
    {
        $match = $this->get($matchId);

        if (!$match->isLoaded()) {
            return null;
        }

        if ($isChallenger) {
            $match->challengerDuelsReset();
            foreach ($match->getDuels() as $duel) {
                if ($duel->getChallenger() !== null) {
                    $duel->setChallenger(null);
                    $duel->setChallengerType();
                }
            }
        } else {
            $match->opponentDuelsReset();
            foreach ($match->getDuels() as $duel) {
                if ($duel->getOpponent() !== null) {
                    $duel->setOpponent(null);
                    $duel->setOpponentType();
                }
            }
        }

        foreach ($match->getResults() as $result) {
            $this->fileStorage->remove($result->getIdentifier());
            $this->em->remove($result);
        }

        $this->em->flush();

        return $match;
    }

}
