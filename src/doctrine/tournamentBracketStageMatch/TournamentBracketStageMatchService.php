<?php

declare(strict_types=1);

namespace Skadmin\TournamentGame\Doctrine\TournamentBracketStageMatch;

use App\Model\Doctrine\Customer\CustomerFacade;
use Nette\Application\UI\Presenter;
use Nette\Security\User as LoggedUser;
use Skadmin\TournamentGame\Doctrine\TournamentBracketStage\Type\TournamentBracketStageRoundRobinDuel;
use Skadmin\TournamentGame\Doctrine\TournamentBracketStageMatchDuel\TournamentBracketStageMatchDuelFacade;
use Skadmin\TournamentGame\Doctrine\TournamentResult\TournamentResult;

use function assert;
use function ceil;
use function count;
use function in_array;
use function intval;

final class TournamentBracketStageMatchService
{
    private LoggedUser $loggedUser;
    private CustomerFacade $facadeCustomer;
    private TournamentBracketStageMatchFacade $facadeTournamentBracketStageMatch;
    private TournamentBracketStageMatchDuelFacade $facadeTournamentBracketStageMatchDuel;

    public function __construct(LoggedUser $loggedUser, CustomerFacade $facadeCustomer, TournamentBracketStageMatchFacade $facadeTournamentBracketStageMatch, TournamentBracketStageMatchDuelFacade $facadeTournamentBracketStageMatchDuel)
    {
        $this->loggedUser = $loggedUser;
        $this->facadeCustomer = $facadeCustomer;
        $this->facadeTournamentBracketStageMatch = $facadeTournamentBracketStageMatch;
        $this->facadeTournamentBracketStageMatchDuel = $facadeTournamentBracketStageMatchDuel;
    }

    public function updateMatch(TournamentBracketStageMatch $match, ?Presenter $presenter): void
    {
        if ($match->getStatus() === TournamentBracketStageMatch::STATUS_COMPLETED) {
            return;
        }

        if (!$match->getStage() instanceof TournamentBracketStageRoundRobinDuel && $match->getResults(TournamentResult::STATUS_NEW)->count() > 0) {
            if (!in_array($match->getStatus(), TournamentBracketStageMatch::STATUS_FINAL)) {
                $this->facadeTournamentBracketStageMatch->updateMatchStatus($match->getId(), TournamentBracketStageMatch::STATUS_AWAITING_RESULTS);
            }

            return;
        }

        $tournament = $match->getTournament();

        $customer = $this->facadeCustomer->get($this->loggedUser->getId());
        $fncGetChallengerAndOpponentResult = static function (TournamentResult $result) use ($match, $tournament): array {
            if ($tournament->isOneVsOne()) {
                if ($match->getChallenger()->getRegistration()->getUser() === $result->getSubmitter()) {
                    return ['chr' => $result->getSubmitterResult(), 'or' => $result->getOpponentResult()];
                }

                return ['chr' => $result->getOpponentResult(), 'or' => $result->getSubmitterResult()];
            }

            if ($match->getChallenger()->getRegistration()->getTeam() === $result->getSubmitter()) {
                return ['chr' => $result->getSubmitterResult(), 'or' => $result->getOpponentResult()];
            }

            return ['chr' => $result->getOpponentResult(), 'or' => $result->getSubmitterResult()];
        };

        $resultsByRounds = [];
        foreach ($match->getResults(TournamentResult::STATUS_CONFIRMED) as $result) {
            $resultsByRounds[sprintf('%s_%s', $result->getNumberOfDuel(), $result->getNumberOfMatch())][] = $result;
        }

        /** @var TournamentResult[] $matchResultByRounds */
        $matchResultByRounds = [];
        foreach ($resultsByRounds as $numberOfRound => $resultsByRound) {
            $resultStatus = null;
            $resultIsSame = true;

            foreach ($resultsByRound as $result) {
                assert($result instanceof TournamentResult);
                $_result = $fncGetChallengerAndOpponentResult($result);
                $submitterWin = $_result['chr'] > $_result['or'];

                if ($resultStatus === null) {
                    $resultStatus = $submitterWin;
                }

                if ($resultStatus === $submitterWin) {
                    continue;
                }

                $resultIsSame = false;
            }

            if (!$resultIsSame) {
                continue;
            }

            $matchResultByRounds[$numberOfRound] = $resultsByRound[0];
        }

        if ($match->getStage() instanceof TournamentBracketStageRoundRobinDuel) {
            $numberOfRounds = $match->getStage()->getNumberOfRoundDuels();
            $totalNumberOfResults = $match->getStage()->getNumberOfDuels() * $numberOfRounds;
            $totalNumberOfResultsWithAce = ($match->getStage()->getNumberOfDuels()  + 1) * $numberOfRounds;
            $countResults = count($matchResultByRounds);

            $challengerResult = 0;
            $opponentResult = 0;

            $resultsDuels = [];

            /** @var TournamentResult $matchResultByRound */
            foreach ($matchResultByRounds as $key => $matchResultByRound) {
                [$duelRound, $matchRound] = explode('_', $key);

                $resultsDuels[$duelRound][] = $matchResultByRound;
            }

            $totalChallengerResult = 0;
            $totalOpponentResult = 0;

            // projdeme všechny duely a zjistíme, jestli máme všechny výsledky
            foreach ($resultsDuels as $duelRound => $resultsByDuel) {
                $duel = $match->getDuel($duelRound);

                $challengerResult = 0;
                $opponentResult = 0;

                foreach ($resultsByDuel as $resultByDuel) {
                    $results = $fncGetChallengerAndOpponentResult($resultByDuel);
                    $challengerResult += $results['chr'];
                    $opponentResult += $results['or'];
                }

                $totalChallengerResult += $challengerResult;
                $totalOpponentResult += $opponentResult;

                $this->facadeTournamentBracketStageMatchDuel->updateResults($duel, $challengerResult, $opponentResult);
            }

            // pokud máme všechny základní výsledky a není stejné skore nebo se odehrál i ACE, tak je vyhodnotíme
            if (($totalNumberOfResults === $countResults && $totalChallengerResult !== $totalOpponentResult) || $totalNumberOfResultsWithAce === $countResults) {
                $this->facadeTournamentBracketStageMatch->updateResultAndEvaluate($match->getId(), $totalChallengerResult, $totalOpponentResult, true, true);
            } else {
                $this->facadeTournamentBracketStageMatch->updateResultAndEvaluate($match->getId(), $totalChallengerResult, $totalOpponentResult, false, true);
            }
        } elseif ($match->getStage()->getBestOfGame() === 1 && count($matchResultByRounds) === 1) {
            // jedná se o Bo1, takže nás vyhodnotíme výsledky rovnou (skore = výsledkům)

            $results = $fncGetChallengerAndOpponentResult(array_values($matchResultByRounds)[0]);
            $this->facadeTournamentBracketStageMatch->updateResultAndEvaluate($match->getId(), $results['chr'], $results['or'], true, true);
        } else {
            // jedná se o BoX, projdeme všechny výsledky a vyhodnotíme skóre, pokud počet výsledků roven BoX, tak posuneme zápas dále

            $challengerResult = 0;
            $opponentResult = 0;
            $bestOfMax = intval(ceil($match->getStage()->getBestOfGame() / 2));

            foreach ($matchResultByRounds as $matchResultByRound) {
                $results = $fncGetChallengerAndOpponentResult($matchResultByRound);
                if ($results['chr'] > $results['or']) {
                    $challengerResult++;
                } else {
                    $opponentResult++;
                }
            }

            $evaluateTheResult = $challengerResult === $bestOfMax || $opponentResult === $bestOfMax;
            $this->facadeTournamentBracketStageMatch->updateResultAndEvaluate($match->getId(), $challengerResult, $opponentResult, $evaluateTheResult, true);

            if ($evaluateTheResult) {
                if ($presenter instanceof Presenter) {
                    $presenter->redrawControl('snipModal');
                }
            } elseif (!in_array($match->getStatus(), TournamentBracketStageMatch::STATUS_FINAL)) {
                $this->facadeTournamentBracketStageMatch->updateMatchStatus($match->getId(), TournamentBracketStageMatch::STATUS_RUNNING);
            }
        }
    }
}
