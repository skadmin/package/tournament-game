<?php

declare(strict_types=1);

namespace Skadmin\TournamentGame\Doctrine\TournamentBracketStageMatchDuel;

use App\Model\Doctrine\Customer\Customer;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Nette\Utils\DateTime;
use Skadmin\TournamentGame\Doctrine\Tournament\Tournament;
use Skadmin\TournamentGame\Doctrine\TournamentBracketStage\ATournamentBracketStage;
use Skadmin\TournamentGame\Doctrine\TournamentBracketStageMatch\TournamentBracketStageMatch;
use Skadmin\TournamentGame\Doctrine\TournamentBracketStageParticipant\TournamentBracketStageParticipant;
use Skadmin\TournamentGame\Doctrine\TournamentResult\TournamentResult;
use SkadminUtils\DoctrineTraits\Entity;
use DateTimeInterface;

#[ORM\Entity]
#[ORM\HasLifecycleCallbacks]
class TournamentBracketStageMatchDuel
{
    use Entity\BaseEntity;

    #[ORM\Column]
    private int $numberOfDuel;

    #[ORM\Column]
    private int $challengerResult = 0;

    #[ORM\Column]
    private int $opponentResult = 0;

    #[ORM\Column]
    private string $challengerType = '';

    #[ORM\Column]
    private string $opponentType = '';

    #[ORM\ManyToOne(targetEntity: TournamentBracketStageMatch::class, inversedBy: 'duels')]
    #[ORM\JoinColumn(onDelete: 'cascade')]
    private TournamentBracketStageMatch $match;

    #[ORM\ManyToOne(targetEntity: Customer::class)]
    #[ORM\JoinColumn(onDelete: 'cascade')]
    private ?Customer $challenger = null;

    #[ORM\ManyToOne(targetEntity: Customer::class)]
    #[ORM\JoinColumn(onDelete: 'cascade')]
    private ?Customer $opponent = null;

    public function create(TournamentBracketStageMatch $match, int $numberOfDuel): void
    {
        $this->match        = $match;
        $this->numberOfDuel = $numberOfDuel;
    }

    public function getChallengerResult(): int
    {
        return $this->challengerResult;
    }

    public function setChallengerResult(int $challengerResult): void
    {
        $this->challengerResult = $challengerResult;
    }

    public function getOpponentResult(): int
    {
        return $this->opponentResult;
    }

    public function setOpponentResult(int $opponentResult): void
    {
        $this->opponentResult = $opponentResult;
    }

    public function getChallengerType(string $default = ''): string
    {
        return trim($this->challengerType) ? trim($this->challengerType) : $default;
    }

    public function setChallengerType(string $challengerType = 'default'): void
    {
        $this->challengerType = $challengerType;
    }

    public function getOpponentType(string $default = ''): string
    {
        return trim($this->opponentType) ? trim($this->opponentType) : $default;
    }

    public function setOpponentType(string $opponentType = 'default'): void
    {
        $this->opponentType = $opponentType;
    }

    public function getChallenger(): ?Customer
    {
        return $this->challenger;
    }

    public function setChallenger(?Customer $challenger): void
    {
        $this->challenger = $challenger;
    }

    public function getOpponent(): ?Customer
    {
        return $this->opponent;
    }

    public function setOpponent(?Customer $opponent): void
    {
        $this->opponent = $opponent;
    }

    public function getClassScoreForChallenger(string $classWinning = 'text-success', string $classLossing = 'text-danger'): string
    {
        if ($this->getChallengerResult() > $this->getOpponentResult()) {
            return $classWinning;
        }

        if ($this->getChallengerResult() < $this->getOpponentResult()) {
            return $classLossing;
        }

        return '';
    }

    public function getClassScoreForOpponent(string $classWinning = 'text-success', string $classLossing = 'text-danger'): string
    {
        if ($this->getChallengerResult() < $this->getOpponentResult()) {
            return $classWinning;
        }

        if ($this->getChallengerResult() > $this->getOpponentResult()) {
            return $classLossing;
        }

        return '';
    }

    public function getNumberOfDuel(): int
    {
        return $this->numberOfDuel;
    }

    public function getMatch(): TournamentBracketStageMatch
    {
        return $this->match;
    }
}
