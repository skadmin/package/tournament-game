<?php

declare(strict_types=1);

namespace Skadmin\TournamentGame\Doctrine\TournamentBracketStageMatchDuel;

use App\Model\Doctrine\Customer\Customer;
use App\Model\Doctrine\Customer\CustomerFacade;
use Doctrine\Common\Collections\Criteria;
use Nette\Utils\DateTime;
use Nettrine\ORM\EntityManagerDecorator;
use Skadmin\TournamentGame\Doctrine\TournamentBracketStage\ATournamentBracketStage;
use Skadmin\TournamentGame\Doctrine\TournamentBracketStage\Type\TournamentBracketStageDoubleElimination;
use Skadmin\TournamentGame\Doctrine\TournamentBracketStage\Type\TournamentBracketStageRoundRobinDuel;
use Skadmin\TournamentGame\Doctrine\TournamentBracketStageMatch\TournamentBracketStageMatch;
use Skadmin\TournamentGame\Doctrine\TournamentBracketStageParticipant\TournamentBracketStageParticipant;
use SkadminUtils\DoctrineTraits\Facade;

final class TournamentBracketStageMatchDuelFacade extends Facade
{

    private CustomerFacade $facadeCustomer;

    public function __construct(EntityManagerDecorator $em, CustomerFacade $facadeCustomer)
    {
        parent::__construct($em);
        $this->table = TournamentBracketStageMatchDuel::class;

        $this->facadeCustomer = $facadeCustomer;
    }

    public function create(TournamentBracketStageMatch $match, int $numberOfDuel): ?TournamentBracketStageMatchDuel
    {
        $duel = $this->get();
        $duel->create($match, $numberOfDuel);

        $this->em->persist($duel);
        $this->em->flush();

        $match->addDuel($duel);

        return $duel;
    }

    public function updateResults(TournamentBracketStageMatchDuel $duel, int $challengerResult, int $opponentResult): void
    {
        $duel->setChallengerResult($challengerResult);
        $duel->setOpponentResult($opponentResult);

        $this->em->persist($duel);
        $this->em->flush();
    }

    public function updateChallenger(TournamentBracketStageMatchDuel $duel, int $customerId, string $type): void
    {
        $duel->setChallenger($this->facadeCustomer->get($customerId));
        $duel->setChallengerType($type);

        $this->em->persist($duel);
        $this->em->flush();
    }

    public function updateOpponent(TournamentBracketStageMatchDuel $duel, int $customerId, string $type): void
    {
        $duel->setOpponent($this->facadeCustomer->get($customerId));
        $duel->setOpponentType($type);

        $this->em->persist($duel);
        $this->em->flush();
    }

    public function resetChallenger(TournamentBracketStageMatchDuel $duel): void
    {
        $duel->setChallenger(null);
        $duel->setChallengerType();

        $this->em->persist($duel);
        $this->em->flush();
    }

    public function resetOpponent(TournamentBracketStageMatchDuel $duel): void
    {
        $duel->setOpponent(null);
        $duel->setOpponentType();

        $this->em->persist($duel);
        $this->em->flush();
    }

    public function get(?int $id = null): ?TournamentBracketStageMatchDuel
    {
        if ($id === null) {
            return new TournamentBracketStageMatchDuel();
        }

        $duel = parent::get($id);

        if ($duel === null) {
            return new TournamentBracketStageMatchDuel();
        }

        return $duel;
    }

}
