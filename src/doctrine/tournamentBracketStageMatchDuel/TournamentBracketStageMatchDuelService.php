<?php

declare(strict_types=1);

namespace Skadmin\TournamentGame\Doctrine\TournamentBracketStageMatchDuel;

use Nettrine\ORM\EntityManagerDecorator;
use Skadmin\TournamentGame\Doctrine\TournamentBracketStageMatch\TournamentBracketStageMatch;
use SkadminUtils\DoctrineTraits\Facade;

final class TournamentBracketStageMatchDuelService
{
    private TournamentBracketStageMatchDuelFacade $facadeTournamentBracketStageMatchDuel;

    public function __construct(TournamentBracketStageMatchDuelFacade $facadeTournamentBracketStageMatchDuel)
    {
        $this->facadeTournamentBracketStageMatchDuel = $facadeTournamentBracketStageMatchDuel;
    }

    public function createDuels(TournamentBracketStageMatch $match): void
    {
        if (!method_exists($match->getStage(), 'getNumberOfDuels')) {
            return;
        }

        if ($match->getDuels()->count() > 0) {
            return;
        }

        for ($i = 1; $i <= $match->getStage()->getNumberOfDuels(); $i++) {
            $this->facadeTournamentBracketStageMatchDuel->create($match, $i);
        }

        // ACE
        $this->facadeTournamentBracketStageMatchDuel->create($match, $i);
    }

    public function update(): TournamentBracketStageMatchDuel
    {
        $bracketStage = $this->get($bracketStageId);
        $bracketStage->update($name, $willPlayInTheFinals, $numberOfRounds, $bestOfGame);

        $this->em->flush();

        return $bracketStage;
    }

    public function resetDuel(int $duelId, bool $isChallenger = true): void
    {
        $duel = $this->facadeTournamentBracketStageMatchDuel->get($duelId);

        if (!$duel->isLoaded()) {
            return;
        }

        if ($isChallenger) {
            $this->facadeTournamentBracketStageMatchDuel->resetChallenger($duel);
        } else {
            $this->facadeTournamentBracketStageMatchDuel->resetOpponent($duel);
        }
    }

}
