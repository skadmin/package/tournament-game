<?php

declare(strict_types=1);

namespace Skadmin\TournamentGame\Doctrine\TournamentBracketStageParticipant;

use Doctrine\ORM\Mapping as ORM;
use Skadmin\TournamentGame\Doctrine\Registration\RegistrationTeam;
use Skadmin\TournamentGame\Doctrine\Registration\RegistrationUser;
use Skadmin\TournamentGame\Doctrine\TournamentBracketStage\ATournamentBracketStage;
use SkadminUtils\DoctrineTraits\Entity;

use function trim;

#[ORM\Entity]
#[ORM\HasLifecycleCallbacks]
class TournamentBracketStageParticipant
{
    use Entity\BaseEntity;
    use Entity\Code;

    #[ORM\ManyToOne(targetEntity: ATournamentBracketStage::class, inversedBy: 'participants')]
    #[ORM\JoinColumn(onDelete: 'cascade')]
    private ATournamentBracketStage $stage;

    #[ORM\ManyToOne(targetEntity: RegistrationUser::class)]
    #[ORM\JoinColumn(onDelete: 'cascade')]
    private ?RegistrationUser $registrationUser = null;

    #[ORM\ManyToOne(targetEntity: RegistrationTeam::class)]
    #[ORM\JoinColumn(onDelete: 'cascade')]
    private ?RegistrationTeam $registrationTeam = null;

    public function create(ATournamentBracketStage $stage, int $code, RegistrationUser|RegistrationTeam $registration): void
    {
        $this->stage = $stage;

        if ($registration instanceof RegistrationUser) {
            $this->registrationUser = $registration;
        } else {
            $this->registrationTeam = $registration;
        }

        $this->setCode((string) $code);
    }

    public function getStage(): ATournamentBracketStage
    {
        return $this->stage;
    }

    public function getRegistration(): RegistrationUser|RegistrationTeam
    {
        return $this->registrationUser instanceof RegistrationUser ? $this->registrationUser : $this->registrationTeam;
    }

    public function getName(): string
    {
        if ($this->getRegistration() instanceof RegistrationTeam) {
            $teamName = $this->getRegistration()->getTeamNickname();

            return trim($teamName) === '' ? $this->getRegistration()->getTeam()->getName() : $teamName;
        }

        return $this->getRegistration()->getNickname();
    }

    public function getLogo(): ?string
    {
        if ($this->getRegistration() instanceof RegistrationTeam) {
            return $this->getRegistration()->getTeam()->getImagePreview();
        }

        return $this->getRegistration()->getUser()->getImagePreview();
    }
}
