<?php

declare(strict_types=1);

namespace Skadmin\TournamentGame\Doctrine\TournamentBracketStageParticipant;

use Nettrine\ORM\EntityManagerDecorator;
use Skadmin\TournamentGame\Doctrine\Registration\RegistrationTeam;
use Skadmin\TournamentGame\Doctrine\Registration\RegistrationUser;
use Skadmin\TournamentGame\Doctrine\TournamentBracketStage\ATournamentBracketStage;
use SkadminUtils\DoctrineTraits\Facade;

final class TournamentBracketStageParticipantFacade extends Facade
{
    public function __construct(EntityManagerDecorator $em)
    {
        parent::__construct($em);
        $this->table = TournamentBracketStageParticipant::class;
    }

    public function get(?int $id = null): ?TournamentBracketStageParticipant
    {
        if ($id === null) {
            return null;
        }

        $bracketStage = parent::get($id);

        if ($bracketStage === null) {
            return null;
        }

        return $bracketStage;
    }

    public function create(ATournamentBracketStage $stage, int $code, RegistrationUser|RegistrationTeam $registration): TournamentBracketStageParticipant
    {
        $bracketStageParticipant = new TournamentBracketStageParticipant();
        $bracketStageParticipant->create($stage, $code, $registration);

        $this->em->persist($bracketStageParticipant);
        $this->em->flush();

        return $bracketStageParticipant;
    }
}
