<?php

declare(strict_types=1);

namespace Skadmin\TournamentGame\Doctrine\TournamentPayment;

use Doctrine\ORM\Mapping as ORM;
use Skadmin\Payment\Doctrine\Payment\Payment;
use Skadmin\TournamentGame\Doctrine\Tournament\Tournament;
use SkadminUtils\DoctrineTraits\Entity;

#[ORM\Entity]
#[ORM\Table(name: 'tournament_game_payment')]
#[ORM\HasLifecycleCallbacks]
class TournamentPayment
{
    use Entity\Id;

    #[ORM\Column]
    private int $price = 0;

    #[ORM\ManyToOne(targetEntity: Payment::class)]
    #[ORM\JoinColumn(onDelete: 'cascade')]
    private Payment $payment;

    #[ORM\ManyToOne(targetEntity: Tournament::class, inversedBy: 'payments')]
    #[ORM\JoinColumn(onDelete: 'cascade')]
    private Tournament $tournament;

    public function create(Tournament $tournament, Payment $payment, int $price): void
    {
        $this->tournament = $tournament;
        $this->payment    = $payment;
        $this->price      = $price;
    }

    public function update(int $price): void
    {
        $this->price = $price;
    }

    public function getTournament(): Tournament
    {
        return $this->tournament;
    }

    public function getPrice(): int
    {
        return $this->price;
    }

    public function getPayment(): Payment
    {
        return $this->payment;
    }
}
