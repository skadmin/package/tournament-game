<?php

declare(strict_types=1);

namespace Skadmin\TournamentGame\Doctrine\TournamentPayment;

use Nettrine\ORM\EntityManagerDecorator;
use Skadmin\Payment\Doctrine\Payment\Payment;
use Skadmin\TournamentGame\Doctrine\Tournament\Tournament;
use SkadminUtils\DoctrineTraits\Facade;

final class TournamentPaymentFacade extends Facade
{
    use Facade\Webalize;

    public function __construct(EntityManagerDecorator $em)
    {
        parent::__construct($em);
        $this->table = TournamentPayment::class;
    }

    public function create(Tournament $tournament, Payment $payment, int $price): TournamentPayment
    {
        $tournamentPayment = $this->get();
        $tournamentPayment->create($tournament, $payment, $price);

        $this->em->persist($tournamentPayment);
        $this->em->flush();

        return $tournamentPayment;
    }

    public function update(TournamentPayment $tournamentPayment, int $price): TournamentPayment
    {
        $tournamentPayment->update($price);

        $this->em->persist($tournamentPayment);
        $this->em->flush();

        return $tournamentPayment;
    }

    public function remove(TournamentPayment $tournamentPayment): void
    {
        $this->em->remove($tournamentPayment);
        $this->em->flush();
    }

    public function get(?int $id = null): TournamentPayment
    {
        if ($id === null) {
            return new TournamentPayment();
        }

        $tournamentPayment = parent::get($id);

        if ($tournamentPayment === null) {
            return new TournamentPayment();
        }

        return $tournamentPayment;
    }
}
