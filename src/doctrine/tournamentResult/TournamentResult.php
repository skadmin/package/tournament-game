<?php

declare(strict_types=1);

namespace Skadmin\TournamentGame\Doctrine\TournamentResult;

use App\Model\Doctrine\Customer\Customer;
use Doctrine\ORM\Mapping as ORM;
use Nette\Utils\Strings;
use Skadmin\TournamentGame\Doctrine\Registration\RegistrationTeam;
use Skadmin\TournamentGame\Doctrine\Team\Team;
use Skadmin\TournamentGame\Doctrine\Tournament\Tournament;
use SkadminUtils\DoctrineTraits\Entity;

use function end;
use function explode;
use function sprintf;

#[ORM\Entity]
#[ORM\Table(name: 'tournament_game_result')]
#[ORM\HasLifecycleCallbacks]
class TournamentResult
{
    use Entity\Id;
    use Entity\Content;
    use Entity\File;
    use Entity\IsValid;
    use Entity\Status;

    public const STATUS_NEW           = 1;
    public const STATUS_CONFIRMED     = 2;
    public const STATUS_NOT_CONFIRMED = 3;

    public const STATUSES = [
        self::STATUS_NEW           => 'tournament-result.status.new',
        self::STATUS_CONFIRMED     => 'tournament-result.status.confirmed',
        self::STATUS_NOT_CONFIRMED => 'tournament-result.status.not-confirmed',
    ];

    #[ORM\ManyToOne(targetEntity: Tournament::class, inversedBy: 'results')]
    #[ORM\JoinColumn(onDelete: 'cascade')]
    private Tournament $tournament;

    #[ORM\ManyToOne(targetEntity: Customer::class)]
    #[ORM\JoinColumn(onDelete: 'SET NULL')]
    private ?Customer $submitterUser;

    #[ORM\ManyToOne(targetEntity: Customer::class)]
    #[ORM\JoinColumn(onDelete: 'SET NULL')]
    private ?Customer $opponentUser;

    #[ORM\ManyToOne(targetEntity: Team::class)]
    #[ORM\JoinColumn(onDelete: 'SET NULL')]
    private ?Team $submitterTeam;

    #[ORM\ManyToOne(targetEntity: Team::class)]
    #[ORM\JoinColumn(onDelete: 'SET NULL')]
    private ?Team $opponentTeam;

    #[ORM\Column(nullable: true)]
    private ?string $submitterAdditionalInfo = null;

    #[ORM\Column(nullable: true)]
    private ?string $opponentAdditionalInfo = null;

    #[ORM\Column(nullable: true)]
    protected ?int $numberOfMatch = null;

    #[ORM\Column(nullable: true)]
    protected ?int $numberOfDuel = null;

    #[ORM\Column(nullable: true)]
    protected ?int $submitterResult = null;

    #[ORM\Column(nullable: true)]
    protected ?int $opponentResult = null;

    public function __construct()
    {
        $this->status = self::STATUS_NEW;
    }

    public function create(Tournament $tournament, Team|Customer $submitter, null|Team|Customer $opponent, string $name, string $identifier, int $size, string $mimeType, string $content, ?RegistrationTeam $submitterRegistration, ?RegistrationTeam $opponentRegistration, ?int $numberOfMatch, ?int $numberOfDuel, ?int $submitterResult, ?int $opponentResult): void
    {
        $this->tournament = $tournament;
        if ($this->tournament->isOneVsOne()) {
            $this->submitterUser = $submitter;
            $this->opponentUser  = $opponent;

            $this->submitterTeam = null;
            $this->opponentTeam  = null;
        } else {
            $this->submitterTeam = $submitter;
            $this->opponentTeam  = $opponent;

            $this->submitterUser = null;
            $this->opponentUser  = null;
        }

        // file
        $this->name       = $name;
        $this->identifier = $identifier;
        $this->size       = $size;
        $this->mimeType   = $mimeType;

        // other
        $this->content = $content;

        // result
        $this->numberOfMatch   = $numberOfMatch;
        $this->numberOfDuel    = $numberOfDuel;
        $this->submitterResult = $submitterResult;
        $this->opponentResult  = $opponentResult;

        // additional info
        if ($submitterRegistration !== null) {
            $this->submitterAdditionalInfo = $submitterRegistration->getTeamNickname();
        }

        if ($opponentRegistration === null) {
            return;
        }

        $this->opponentAdditionalInfo = $opponentRegistration->getTeamNickname();
    }

    public function updateContent(string $content): void
    {
        $this->content = $content;
    }

    public function updateStatus(int $status): void
    {
        $this->status = $status;
    }

    public function getTournament(): Tournament
    {
        return $this->tournament;
    }

    public function getSubmitter(): Customer|Team|null
    {
        return $this->submitterUser ?? $this->submitterTeam;
    }

    public function getOpponent(): Customer|Team|null
    {
        return $this->opponentUser ?? $this->opponentTeam;
    }

    public function getSubmitterAdditionalInfo(): ?string
    {
        return $this->submitterAdditionalInfo;
    }

    public function getOpponentAdditionalInfo(): ?string
    {
        return $this->opponentAdditionalInfo;
    }

    public function getNumberOfMatch(): ?int
    {
        return $this->numberOfMatch;
    }

    public function getNumberOfDuel(): ?int
    {
        return $this->numberOfDuel;
    }

    public function getSubmitterResult(): ?int
    {
        return $this->submitterResult;
    }

    public function getOpponentResult(): ?int
    {
        return $this->opponentResult;
    }

    public function getStatusText(): string
    {
        return self::STATUSES[$this->getStatus()] ?? self::STATUSES[self::STATUS_NEW];
    }

    public function getFileName(?int $counter = null, bool $withExt = true): string
    {
        $isUser   = $this->submitterTeam === null;
        $fileName = 'default-result';

        if ($isUser && $this->submitterUser !== null) {
            $fileName = $this->submitterUser->getFullName();
        } elseif ($this->submitterTeam !== null) {
            $fileName = sprintf('%s[%s]', $this->submitterTeam->getName(), $this->submitterTeam->getNickname());
        }

        if ($this->opponentUser !== null || $this->opponentTeam !== null) {
            if ($isUser && $this->opponentUser !== null) {
                $opponent = $this->opponentUser->getFullName();
            } elseif ($this->opponentTeam !== null) {
                $opponent = sprintf('%s[%s]', $this->opponentTeam->getName(), $this->opponentTeam->getNickname());
            }

            $fileName .= sprintf(' vs %s', $opponent ?? 'oponent');
        }

        $fileName = Strings::webalize($fileName, '[]');

        $ext = explode('.', $this->getName());

        if (! $withExt) {
            return $fileName;
        }

        if ($counter === null) {
            return Strings::lower(sprintf('%s.%s', $fileName, end($ext)));
        }

        return Strings::lower(sprintf('%s-%s.%s', $fileName, $counter, end($ext)));
    }
}
