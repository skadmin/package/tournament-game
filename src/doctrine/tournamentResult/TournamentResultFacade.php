<?php

declare(strict_types=1);

namespace Skadmin\TournamentGame\Doctrine\TournamentResult;

use App\Model\Doctrine\Customer\Customer;
use Nettrine\ORM\EntityManagerDecorator;
use Skadmin\TournamentGame\Doctrine\Registration\RegistrationTeam;
use Skadmin\TournamentGame\Doctrine\Team\Team;
use Skadmin\TournamentGame\Doctrine\Tournament\Tournament;
use Skadmin\TournamentGame\Doctrine\TournamentBracketStageMatch\TournamentBracketStageMatch;
use SkadminUtils\DoctrineTraits\Facade;

final class TournamentResultFacade extends Facade
{
    use Facade\Hash;

    public function __construct(EntityManagerDecorator $em)
    {
        parent::__construct($em);
        $this->table = TournamentResult::class;
    }

    public function create(Tournament $tournament, Team|Customer $submitter, null|Team|Customer $opponent, string $name, string $identifier, int $size, string $mimeType, string $content, ?RegistrationTeam $submitterRegistration, ?RegistrationTeam $opponentRegistration, ?int $numberOfMatch = null, ?int $submitterResult = null, ?int $opponentResult = null, ?TournamentBracketStageMatch $match = null, ?int $numberOfDuel = null): TournamentResult
    {
        $tournamentResult = $this->get();

        $tournamentResult->create(
            $tournament,
            $submitter,
            $opponent,
            $name,
            $identifier,
            $size,
            $mimeType,
            $content,
            $submitterRegistration,
            $opponentRegistration,
            $numberOfMatch,
            $numberOfDuel,
            $submitterResult,
            $opponentResult
        );

        if (! $tournamentResult->isLoaded()) {
            $tournamentResult->setHash($this->getValidHash());
        }

        $this->em->persist($tournamentResult);

        if ($match instanceof TournamentBracketStageMatch) {
            $match->addResult($tournamentResult);
        }

        $this->em->flush();

        return $tournamentResult;
    }

    public function get(?int $id = null): TournamentResult
    {
        if ($id === null) {
            return new TournamentResult();
        }

        $tournament = parent::get($id);

        if ($tournament === null) {
            return new TournamentResult();
        }

        return $tournament;
    }

    public function findByHash(string $hash): ?TournamentResult
    {
        $criteria = ['hash' => $hash];

        return $this->em
            ->getRepository($this->table)
            ->findOneBy($criteria);
    }

    public function remove(TournamentResult $tournamentResult): void
    {
        $this->em->remove($tournamentResult);
        $this->em->flush();
    }

    public function updateContent(int $id, string $content): TournamentResult
    {
        $tournamentResult = $this->get($id);

        $tournamentResult->updateContent($content);

        $this->em->persist($tournamentResult);
        $this->em->flush();

        return $tournamentResult;
    }

    public function updateStatus(int $id, int $status): TournamentResult
    {
        $tournamentResult = $this->get($id);

        $tournamentResult->updateStatus($status);

        $this->em->persist($tournamentResult);
        $this->em->flush();

        return $tournamentResult;
    }
}
