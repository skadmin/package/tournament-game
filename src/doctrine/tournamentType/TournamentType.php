<?php

declare(strict_types=1);

namespace Skadmin\TournamentGame\Doctrine\TournamentType;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Skadmin\TournamentGame\Doctrine\Tournament\Tournament;
use SkadminUtils\DoctrineTraits\Entity;

#[ORM\Entity]
#[ORM\Table(name: 'tournament_game_type')]
#[ORM\HasLifecycleCallbacks]
class TournamentType
{
    use Entity\Id;
    use Entity\WebalizeName;
    use Entity\Content;

    #[ORM\Column]
    private int $numberOfParticipantsFrom = 0;

    #[ORM\Column]
    private int $numberOfParticipantsTo = 0;

    /** @var ArrayCollection|Collection|Tournament[] */
    #[ORM\OneToMany(targetEntity: Tournament::class, mappedBy: 'tournamentType')]
    private ArrayCollection|Collection|array $tournaments;

    public function __construct()
    {
        $this->tournaments = new ArrayCollection();
    }

    public function getNumberOfParticipantsFrom(): int
    {
        return $this->numberOfParticipantsFrom;
    }

    public function getNumberOfParticipantsTo(): int
    {
        return $this->numberOfParticipantsFrom < $this->numberOfParticipantsTo ? $this->numberOfParticipantsTo : $this->numberOfParticipantsFrom;
    }

    /**
     * @return ArrayCollection|Collection|Tournament[]
     */
    public function getTournaments(): ArrayCollection|Collection|array
    {
        return $this->tournaments;
    }

    public function update(string $name, string $content, int $numberOfParticipantsFrom, int $numberOfParticipantsTo): void
    {
        $this->name                     = $name;
        $this->content                  = $content;
        $this->numberOfParticipantsFrom = $numberOfParticipantsFrom;
        $this->numberOfParticipantsTo   = $numberOfParticipantsFrom < $numberOfParticipantsTo ? $numberOfParticipantsTo : $numberOfParticipantsFrom;
    }
}
