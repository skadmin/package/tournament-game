<?php

declare(strict_types=1);

namespace Skadmin\TournamentGame\Doctrine\TournamentType;

use Nettrine\ORM\EntityManagerDecorator;
use SkadminUtils\DoctrineTraits\Facade;

final class TournamentTypeFacade extends Facade
{
    use Facade\Webalize;

    public function __construct(EntityManagerDecorator $em)
    {
        parent::__construct($em);
        $this->table = TournamentType::class;
    }

    public function create(string $name, string $content, int $numberOfParticipantsFrom, int $numberOfParticipantsTo): TournamentType
    {
        return $this->update(null, $name, $content, $numberOfParticipantsFrom, $numberOfParticipantsTo);
    }

    public function update(?int $id, string $name, string $content, int $numberOfParticipantsFrom, int $numberOfParticipantsTo): TournamentType
    {
        $tournamentType = $this->get($id);
        $tournamentType->update($name, $content, $numberOfParticipantsFrom, $numberOfParticipantsTo);

        if (! $tournamentType->isLoaded()) {
            $tournamentType->setWebalize($this->getValidWebalize($name));
        }

        $this->em->persist($tournamentType);
        $this->em->flush();

        return $tournamentType;
    }

    public function get(?int $id = null): TournamentType
    {
        if ($id === null) {
            return new TournamentType();
        }

        $tournamentType = parent::get($id);

        if ($tournamentType === null) {
            return new TournamentType();
        }

        return $tournamentType;
    }
}
