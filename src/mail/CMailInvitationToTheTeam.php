<?php

declare(strict_types=1);

namespace Skadmin\TournamentGame\Mail;

use Haltuf\Genderer\Genderer;
use Skadmin\Mailing\Model\CMail;
use Skadmin\Mailing\Model\MailParameterValue;
use Skadmin\Mailing\Model\MailTemplateParameter;
use Skadmin\TournamentGame\Doctrine\TeamInvitation\TeamInvitation;

use function sprintf;

class CMailInvitationToTheTeam extends CMail
{
    public const TYPE = 'tournament-game-invitation-to-the-team';

    private string $name;
    private string $vocativName;
    private string $email;
    private string $teamName;
    private string $teamCode;
    private string $hash;
    private string $href;
    private string $link;

    public function __construct(TeamInvitation $teamInvitation, string $href, string $link)
    {
        $user = $teamInvitation->getUser();
        $team = $teamInvitation->getTeam();

        $genderer = new Genderer();

        $this->name        = $user->getFullName();
        $this->vocativName = $genderer->getVocative($user->getFullName());
        $this->email       = $user->getEmail();

        $this->teamName = sprintf('%s [%s]', $team->getName(), $team->getNickname());
        $this->teamCode = $team->getCode();

        $this->hash = $teamInvitation->getHash();

        $this->href = $href;
        $this->link = $link;
    }

    /**
     * @return mixed[]
     */
    public static function getModelForSerialize(): array
    {
        $params = [
            'name',
            'vocativ-name',
            'email',
            'team-name',
            'team-code',
            'hash',
            'href',
            'link',
        ];
        $model  = [];

        foreach ($params as $param) {
            $description = sprintf('mail.tournament-game-invitation-to-the-team.parameter.%s.description', $param);
            $example     = sprintf('mail.tournament-game-invitation-to-the-team.parameter.%s.example', $param);

            $model[] = (new MailTemplateParameter($param, $description, $example))->getDataForSerialize();
        }

        return $model;
    }

    /**
     * @return MailParameterValue[]
     */
    public function getParameterValues(): array
    {
        return [
            new MailParameterValue('name', $this->getName()),
            new MailParameterValue('vocativ-name', $this->getVocativName()),
            new MailParameterValue('email', $this->getEmail()),
            new MailParameterValue('team-name', $this->getTeamName()),
            new MailParameterValue('team-code', $this->getTeamCode()),
            new MailParameterValue('hash', $this->getHash()),
            new MailParameterValue('href', $this->getHref()),
            new MailParameterValue('link', $this->getLink()),
        ];
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): CMailInvitationToTheTeam
    {
        $this->name = $name;

        return $this;
    }

    public function getVocativName(): string
    {
        return $this->vocativName;
    }

    public function setVocativName(string $vocativName): CMailInvitationToTheTeam
    {
        $this->vocativName = $vocativName;

        return $this;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function setEmail(string $email): CMailInvitationToTheTeam
    {
        $this->email = $email;

        return $this;
    }

    public function getTeamName(): string
    {
        return $this->teamName;
    }

    public function setTeamName(string $teamName): CMailInvitationToTheTeam
    {
        $this->teamName = $teamName;

        return $this;
    }

    public function getTeamCode(): string
    {
        return $this->teamCode;
    }

    public function setTeamCode(string $teamCode): CMailInvitationToTheTeam
    {
        $this->teamCode = $teamCode;

        return $this;
    }

    public function getHash(): string
    {
        return $this->hash;
    }

    public function setHash(string $hash): CMailInvitationToTheTeam
    {
        $this->hash = $hash;

        return $this;
    }

    public function getHref(): string
    {
        return $this->href;
    }

    public function setHref(string $href): CMailInvitationToTheTeam
    {
        $this->href = $href;

        return $this;
    }

    public function getLink(): string
    {
        return $this->link;
    }

    public function setLink(string $link): CMailInvitationToTheTeam
    {
        $this->link = $link;

        return $this;
    }
}
