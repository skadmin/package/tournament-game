<?php

declare(strict_types=1);

namespace Skadmin\TournamentGame\Mail;

use DateTimeInterface;
use Haltuf\Genderer\Genderer;
use Nette\Utils\Html;
use Skadmin\Mailing\Model\CMail;
use Skadmin\Mailing\Model\MailParameterValue;
use Skadmin\Mailing\Model\MailTemplateParameter;
use Skadmin\TournamentGame\Doctrine\Registration\RegistrationUser;
use Skadmin\Translator\Translator;

use function number_format;
use function sprintf;

class CMailRegistrationUserCreate extends CMail
{
    public const TYPE = 'tournament-game-registration-user-create';

    private string             $name;
    private string             $vocativName;
    private string             $email;
    private string             $phone;
    private string             $facebook;
    private string             $status;
    private string             $variableSymbol;
    private string             $payment;
    private string             $paymentContent;
    private ?DateTimeInterface $paidAt = null;
    private string             $price;
    private string             $pricePayment;
    private string             $game;
    private string             $gameType;
    private string             $team;
    private string             $tournament;
    private string             $tournamentCode;
    private string             $tournamentTerm;
    private string             $tournamentAddress;
    private Html               $tournamentAddressLink;
    private string             $tournamentConfirmation;
    private DateTimeInterface  $createdAt;
    private Html               $priceOverview;

    public function __construct(RegistrationUser $registration, Translator $translator)
    {
        $genderer = new Genderer();

        $this->name           = $registration->getFullName();
        $this->vocativName    = $genderer->getVocative($registration->getFullName());
        $this->email          = $registration->getEmail();
        $this->phone          = $registration->getPhone();
        $this->facebook       = $registration->getFacebook();
        $this->status         = $translator->translate($registration->getStatusText());
        $this->variableSymbol = $registration->getVariableSymbol();
        $this->paidAt         = $registration->getPaidAt();
        $this->price          = number_format($registration->getPrice(), 2, ',', ' ');
        $this->pricePayment   = number_format($registration->getPricePayment(), 2, ',', ' ');
        $this->createdAt      = $registration->getCreatedAt();
        $this->team           = $registration->getTeam() === null ? '--' : $registration->getTeam()->getName();

        $this->payment        = $translator->translate($registration->getPaymentText());
        $this->paymentContent = $registration->getPayment()->getContent();

        $tournament = $registration->getTournament();

        $this->tournament             = $tournament->getName();
        $this->tournamentCode         = $tournament->getCode();
        $this->tournamentTerm         = $tournament->getTermWithTimeClever();
        $this->tournamentAddress      = $tournament->getPlaceAddress();
        $this->tournamentAddressLink  = Html::el('a', [
            'href'   => $tournament->getPlaceLinkToMap(),
            'target' => '_blank',
        ])->setText($tournament->getPlaceAddress());
        $this->tournamentConfirmation = sprintf('%s - %s', $tournament->getConfirmationFrom(), $tournament->getConfirmationTo());

        $this->game     = $tournament->getGame()->getName();
        $this->gameType = $tournament->getTournamentType() !== null ? $tournament->getTournamentType()->getName() : '';

        // PRICE OVERVIEW
        $priceOverview = Html::el('table', ['style' => 'width: 100%; border-collapse: collapse;']);
        $priceOverview->addHtml($this->returnTableTr($registration->getPayment()->getName(), $registration->getPricePayment()));
        foreach ($registration->getSupplements() as $supplement) {
            $priceOverview->addHtml($this->returnTableTr($supplement->getSupplement()->getName(), $supplement->getPrice()));
        }

        $this->priceOverview = $priceOverview;
    }

    private function returnTableTr(string $name, int $price): Html
    {
        $tableTr = Html::el('tr');

        $tableTrTd = Html::el('td', ['style' => 'border-bottom: 1px solid black;'])
            ->setText($name);
        $tableTr->addHtml($tableTrTd);

        $tableTrTd = Html::el('td', ['style' => 'border-bottom: 1px solid black; text-align: right;'])
            ->setText(sprintf('%s', number_format($price, 2, ',', ' ')));
        $tableTr->addHtml($tableTrTd);

        return $tableTr;
    }

    /**
     * @return mixed[]
     */
    public static function getModelForSerialize(): array
    {
        $params = [
            'name',
            'vocativ-name',
            'email',
            'phone',
            'facebook',
            'status',
            'variable-symbol',
            'payment',
            'payment-content',
            'paid-at',
            'price',
            'price-payment',
            'game',
            'game-type',
            'team',
            'tournament',
            'tournament-code',
            'tournament-term',
            'tournament-address',
            'tournament-address-link',
            'tournament-confirmation',
            'created-at',
            'price-overview',
        ];
        $model  = [];

        foreach ($params as $param) {
            $description = sprintf('mail.tournament-game-registration-user-create.parameter.%s.description', $param);
            $example     = sprintf('mail.tournament-game-registration-user-create.parameter.%s.example', $param);

            $model[] = (new MailTemplateParameter($param, $description, $example))->getDataForSerialize();
        }

        return $model;
    }

    /**
     * @return MailParameterValue[]
     */
    public function getParameterValues(): array
    {
        return [
            new MailParameterValue('name', $this->getName()),
            new MailParameterValue('vocativ-name', $this->getVocativName()),
            new MailParameterValue('email', $this->getEmail()),
            new MailParameterValue('phone', $this->getPhone()),
            new MailParameterValue('facebook', $this->getFacebook()),
            new MailParameterValue('status', $this->getStatus()),
            new MailParameterValue('variable-symbol', $this->getVariableSymbol()),
            new MailParameterValue('payment', $this->getPayment()),
            new MailParameterValue('payment-content', $this->getPaymentContent()),
            new MailParameterValue('paid-at', $this->getPaidAt()),
            new MailParameterValue('price', $this->getPrice()),
            new MailParameterValue('price-payment', $this->getPricePayment()),
            new MailParameterValue('game', $this->getGame()),
            new MailParameterValue('game-type', $this->getGameType()),
            new MailParameterValue('team', $this->getTeam()),
            new MailParameterValue('tournament', $this->getTournament()),
            new MailParameterValue('tournament-code', $this->getTournamentCode()),
            new MailParameterValue('tournament-term', $this->getTournamentTerm()),
            new MailParameterValue('tournament-address', $this->getTournamentAddress()),
            new MailParameterValue('tournament-address-link', $this->getTournamentAddressLink()),
            new MailParameterValue('tournament-confirmation', $this->getTournamentConfirmation()),
            new MailParameterValue('created-at', $this->getCreatedAt()),
            new MailParameterValue('price-overview', $this->getPriceOverview()),
        ];
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): CMailRegistrationUserCreate
    {
        $this->name = $name;

        return $this;
    }

    public function getVocativName(): string
    {
        return $this->vocativName;
    }

    public function setVocativName(string $vocativName): CMailRegistrationUserCreate
    {
        $this->vocativName = $vocativName;

        return $this;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function setEmail(string $email): CMailRegistrationUserCreate
    {
        $this->email = $email;

        return $this;
    }

    public function getPhone(): string
    {
        return $this->phone;
    }

    public function setPhone(string $phone): CMailRegistrationUserCreate
    {
        $this->phone = $phone;

        return $this;
    }

    public function getFacebook(): string
    {
        return $this->facebook;
    }

    public function setFacebook(string $facebook): CMailRegistrationUserCreate
    {
        $this->facebook = $facebook;

        return $this;
    }

    public function getStatus(): string
    {
        return $this->status;
    }

    public function setStatus(string $status): CMailRegistrationUserCreate
    {
        $this->status = $status;

        return $this;
    }

    public function getVariableSymbol(): string
    {
        return $this->variableSymbol;
    }

    public function setVariableSymbol(string $variableSymbol): CMailRegistrationUserCreate
    {
        $this->variableSymbol = $variableSymbol;

        return $this;
    }

    public function getPayment(): string
    {
        return $this->payment;
    }

    public function setPayment(string $payment): CMailRegistrationUserCreate
    {
        $this->payment = $payment;

        return $this;
    }

    public function getPaymentContent(): string
    {
        return $this->paymentContent;
    }

    public function setPaymentContent(string $paymentContent): CMailRegistrationUserCreate
    {
        $this->paymentContent = $paymentContent;

        return $this;
    }

    public function getPaidAt(): ?DateTimeInterface
    {
        return $this->paidAt;
    }

    public function setPaidAt(?DateTimeInterface $paidAt): CMailRegistrationUserCreate
    {
        $this->paidAt = $paidAt;

        return $this;
    }

    public function getPrice(): string
    {
        return $this->price;
    }

    public function setPrice(string $price): CMailRegistrationUserCreate
    {
        $this->price = $price;

        return $this;
    }

    public function getPricePayment(): string
    {
        return $this->pricePayment;
    }

    public function setPricePayment(string $pricePayment): CMailRegistrationUserCreate
    {
        $this->pricePayment = $pricePayment;

        return $this;
    }

    public function getGame(): string
    {
        return $this->game;
    }

    public function setGame(string $game): CMailRegistrationUserCreate
    {
        $this->game = $game;

        return $this;
    }

    public function getGameType(): string
    {
        return $this->gameType;
    }

    public function setGameType(string $gameType): CMailRegistrationUserCreate
    {
        $this->gameType = $gameType;

        return $this;
    }

    public function getTeam(): string
    {
        return $this->team;
    }

    public function setTeam(string $team): CMailRegistrationUserCreate
    {
        $this->team = $team;

        return $this;
    }

    public function getTournament(): string
    {
        return $this->tournament;
    }

    public function setTournament(string $tournament): CMailRegistrationUserCreate
    {
        $this->tournament = $tournament;

        return $this;
    }

    public function getTournamentCode(): string
    {
        return $this->tournamentCode;
    }

    public function setTournamentCode(string $tournamentCode): CMailRegistrationUserCreate
    {
        $this->tournamentCode = $tournamentCode;

        return $this;
    }

    public function getTournamentTerm(): string
    {
        return $this->tournamentTerm;
    }

    public function setTournamentTerm(string $tournamentTerm): CMailRegistrationUserCreate
    {
        $this->tournamentTerm = $tournamentTerm;

        return $this;
    }

    public function getTournamentAddress(): string
    {
        return $this->tournamentAddress;
    }

    public function setTournamentAddress(string $tournamentAddress): CMailRegistrationUserCreate
    {
        $this->tournamentAddress = $tournamentAddress;

        return $this;
    }

    public function getTournamentAddressLink(): Html
    {
        return $this->tournamentAddressLink;
    }

    public function setTournamentAddressLink(Html $tournamentAddressLink): CMailRegistrationUserCreate
    {
        $this->tournamentAddressLink = $tournamentAddressLink;

        return $this;
    }

    public function getTournamentConfirmation(): string
    {
        return $this->tournamentConfirmation;
    }

    public function setTournamentConfirmation(string $tournamentConfirmation): CMailRegistrationUserCreate
    {
        $this->tournamentConfirmation = $tournamentConfirmation;

        return $this;
    }

    public function getCreatedAt(): DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(DateTimeInterface $createdAt): CMailRegistrationUserCreate
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getPriceOverview(): Html
    {
        return $this->priceOverview;
    }

    public function setPriceOverview(Html $priceOverview): CMailRegistrationUserCreate
    {
        $this->priceOverview = $priceOverview;

        return $this;
    }
}
