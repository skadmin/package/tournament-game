<?php

declare(strict_types=1);

namespace Skadmin\TournamentGame\Mail;

use App\Model\System\Strings;
use Haltuf\Genderer\Genderer;
use ReflectionProperty;
use Skadmin\Mailing\Model\CMail;
use Skadmin\Mailing\Model\MailParameterValue;
use Skadmin\Mailing\Model\MailTemplateParameter;
use Skadmin\TournamentGame\Doctrine\TeamInvitation\TeamInvitation;

use function assert;
use function call_user_func;
use function method_exists;
use function sprintf;

class CMailRequestToJoinTheTeam extends CMail
{
    public const TYPE = 'tournament-game-request-to-join-the-team';

    private string $playerName;
    private string $playerCode;
    private string $vocativName;
    private string $email;
    private string $teamName;
    private string $teamCode;
    private string $hash;
    private string $href;
    private string $link;

    public function __construct(TeamInvitation $teamInvitation, string $href, string $link)
    {
        $user = $teamInvitation->getUser();
        $team = $teamInvitation->getTeam();

        $genderer = new Genderer();

        $this->playerName  = $user->getFullName();
        $this->playerCode  = $user->getCode();
        $this->vocativName = $genderer->getVocative($user->getFullName());
        $this->email       = $user->getEmail();

        $this->teamName = sprintf('%s [%s]', $team->getName(), $team->getNickname());
        $this->teamCode = $team->getCode();

        $this->hash = $teamInvitation->getHash();

        $this->href = $href;
        $this->link = $link;
    }

    /**
     * @return mixed[]
     */
    public static function getModelForSerialize(): array
    {
        $model = [];

        foreach (self::getProperties(self::class) as $property) {
            assert($property instanceof ReflectionProperty);
            $description = sprintf('mail.%s.parameter.%s.description', self::TYPE, $property);
            $example     = sprintf('mail.%s.parameter.%s.example', self::TYPE, $property);

            $model[] = (new MailTemplateParameter($property, $description, $example))->getDataForSerialize();
        }

        return $model;
    }

    /**
     * @return MailParameterValue[]
     */
    public function getParameterValues(): array
    {
        $mailParameterValue = [];

        foreach (self::getProperties(self::class) as $property) {
            assert($property instanceof ReflectionProperty);
            $method = sprintf('get%s', Strings::camelize($property));

            if (! method_exists($this, $method)) {
                continue;
            }

            $mailParameterValue[] = new MailParameterValue($property, call_user_func([$this, $method]));
        }

        return $mailParameterValue;
    }

    public function getPlayerName(): string
    {
        return $this->playerName;
    }

    public function setPlayerName(string $playerName): CMailRequestToJoinTheTeam
    {
        $this->playerName = $playerName;

        return $this;
    }

    public function getPlayerCode(): string
    {
        return $this->playerCode;
    }

    public function setPlayerCode(string $playerCode): CMailRequestToJoinTheTeam
    {
        $this->playerCode = $playerCode;

        return $this;
    }

    public function getVocativName(): string
    {
        return $this->vocativName;
    }

    public function setVocativName(string $vocativName): CMailRequestToJoinTheTeam
    {
        $this->vocativName = $vocativName;

        return $this;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function setEmail(string $email): CMailRequestToJoinTheTeam
    {
        $this->email = $email;

        return $this;
    }

    public function getTeamName(): string
    {
        return $this->teamName;
    }

    public function setTeamName(string $teamName): CMailRequestToJoinTheTeam
    {
        $this->teamName = $teamName;

        return $this;
    }

    public function getTeamCode(): string
    {
        return $this->teamCode;
    }

    public function setTeamCode(string $teamCode): CMailRequestToJoinTheTeam
    {
        $this->teamCode = $teamCode;

        return $this;
    }

    public function getHash(): string
    {
        return $this->hash;
    }

    public function setHash(string $hash): CMailRequestToJoinTheTeam
    {
        $this->hash = $hash;

        return $this;
    }

    public function getHref(): string
    {
        return $this->href;
    }

    public function setHref(string $href): CMailRequestToJoinTheTeam
    {
        $this->href = $href;

        return $this;
    }

    public function getLink(): string
    {
        return $this->link;
    }

    public function setLink(string $link): CMailRequestToJoinTheTeam
    {
        $this->link = $link;

        return $this;
    }
}
