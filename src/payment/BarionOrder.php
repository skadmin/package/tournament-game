<?php

declare(strict_types=1);

namespace Skadmin\TournamentGame\Payment;

use Nette\Utils\Strings;
use Skadmin\TournamentGame\Doctrine\Registration\RegistrationTeam;
use Skadmin\TournamentGame\Doctrine\Registration\RegistrationUser;
use SkadminUtils\Gateway\Transaction\AGatewayTransaction;
use SkadminUtils\GatewayBarion\Transaction\BarionGatewayTransaction;
use SkadminUtils\GatewayBarion\Transaction\BarionGatewayTransactionItem;

use function class_exists;
use function sprintf;

class BarionOrder
{
    public static function self(AGatewayTransaction $gatewayTransaction): AGatewayTransaction
    {
        return $gatewayTransaction;
    }

    public static function createFromRegistration(RegistrationTeam|RegistrationUser $registration): ?AGatewayTransaction
    {
        if (class_exists('\SkadminUtils\GatewayBarion\Transaction\BarionGatewayTransaction')) {
            $tournament = $registration->getTournament();

            $items = [
                new BarionGatewayTransactionItem(
                    sprintf('%s - %s', $tournament->getCode(), $tournament->getName()),
                    $tournament->getTournamentType() !== null ? $tournament->getTournamentType()->getName() : 'tournament',
                    1,
                    'x',
                    $registration->getPricePayment(),
                    $tournament->getCode()
                ),
            ];

            foreach ($registration->getSupplements() as $_supplement) {
                $supplement = $_supplement->getSupplement();
                $items[]    = new BarionGatewayTransactionItem(
                    $supplement->getName(),
                    $supplement->getName(),
                    1,
                    'x',
                    $_supplement->getPrice(),
                    Strings::webalize($supplement->getName())
                );
            }

            return new BarionGatewayTransaction($registration->getEmail(), $registration->getPhone(), $registration->getVariableSymbol(), $items);
        }

        return null;
    }
}
