<?php

declare(strict_types=1);

namespace Skadmin\TournamentGame\Payment;

use Skadmin\TournamentGame\Doctrine\Registration\RegistrationTeam;
use Skadmin\TournamentGame\Doctrine\Registration\RegistrationUser;

class RegistrationSwitcher
{
    public static function switchRegistration(RegistrationUser|RegistrationTeam $registration): RegistrationUser|RegistrationTeam|RegistrationTeamOffline|RegistrationUserOffline
    {
        if ($registration->getTournament()->isOnline()) {
            return $registration;
        }

        if ($registration instanceof RegistrationUser) {
            return new RegistrationUserOffline($registration);
        }

        return new RegistrationTeamOffline($registration);
    }
}
