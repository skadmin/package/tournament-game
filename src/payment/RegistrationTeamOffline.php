<?php

declare(strict_types=1);

namespace Skadmin\TournamentGame\Payment;

use SkadminUtils\Gateway\AGatewayTransactionBox;

class RegistrationTeamOffline extends AGatewayTransactionBox
{
}
