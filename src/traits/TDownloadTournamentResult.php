<?php

declare(strict_types=1);

namespace Skadmin\TournamentGame\Traits;

use Exception;
use Skadmin\FileStorage\FileStorage;
use Skadmin\TournamentGame\Doctrine\TournamentResult\TournamentResult;
use Skadmin\TournamentGame\Doctrine\TournamentResult\TournamentResultFacade;

use function header;
use function readfile;
use function sprintf;

trait TDownloadTournamentResult
{
    private TournamentResultFacade $facadeTournamentResult;
    private FileStorage            $fileStorage;

    public function handleDownloadResult(string $hash): void
    {
        $result = $this->facadeTournamentResult->findByHash($hash);

        if (! $result instanceof TournamentResult || $result->getIdentifier() === null) {
            throw new Exception('Tournament result not found!');
        }

        $this->fileStorage->download($result->getIdentifier(), $result->getFileName(), $result->getMimeType(), $result->getSize());
    }

    public function handleLoadImage(string $hash): void
    {
        $result = $this->facadeTournamentResult->findByHash($hash);

        if (! $result instanceof TournamentResult || $result->getIdentifier() === null) {
            throw new Exception('Tournament result not found!');
        }

        $filePreview = $this->fileStorage->getFilePreview($result->getIdentifier(), $result->getName(), (string) $result->getSize());

        switch ($result->getMimeType()) {
            case 'gif':
                $ctype = 'image/gif';
                break;
            case 'png':
                $ctype = 'image/png';
                break;
            case 'svg':
                $ctype = 'image/svg+xml';
                break;
            default:
                $ctype = 'image/jpeg';
                break;
        }

        header(sprintf('Content-Type: %s', $ctype));
        readfile($filePreview->getPath());
        exit;
    }
}
