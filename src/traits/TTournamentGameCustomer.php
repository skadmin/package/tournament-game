<?php

declare(strict_types=1);

namespace Skadmin\TournamentGame\Traits;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Nette\Utils\Html;
use Skadmin\TournamentGame\Doctrine\Registration\RegistrationUser;
use Skadmin\TournamentGame\Doctrine\Team\Team;
use Skadmin\TournamentGame\Doctrine\TeamPlayer\TeamPlayer;
use Skadmin\TournamentGame\Doctrine\Tournament\Tournament;

use function assert;
use function count;
use function is_array;
use function is_string;
use function krsort;
use function sprintf;
use function trim;

trait TTournamentGameCustomer
{
    #[ORM\Column(options: ['default' => true])]
    private bool $showTeamsHistory = true;

    /** @var string[]|bool */
    #[ORM\Column(type: Types::ARRAY)]
    private array|bool $gameId = [];

    /** @var ArrayCollection|Collection|TeamPlayer[] */
    #[ORM\OneToMany(targetEntity: TeamPlayer::class, mappedBy: 'user')]
    private $teams;

    /** @var ArrayCollection|Collection|RegistrationUser[] */
    #[ORM\OneToMany(targetEntity: RegistrationUser::class, mappedBy: 'user')]
    private $registrationsUser;

    /** @var ArrayCollection|Collection|Tournament[] */
    #[ORM\OneToMany(targetEntity: Tournament::class, mappedBy: 'winner')]
    #[ORM\OrderBy(['termFrom' => 'DESC'])]
    private $wonTournaments;

    /** @var ArrayCollection|Collection|Tournament[] */
    #[ORM\OneToMany(targetEntity: Tournament::class, mappedBy: 'secondPlace')]
    #[ORM\OrderBy(['termFrom' => 'DESC'])]
    private $secondPlaces;

    /** @var ArrayCollection|Collection|Tournament[] */
    #[ORM\OneToMany(targetEntity: Tournament::class, mappedBy: 'thirdPlace')]
    #[ORM\OrderBy(['termFrom' => 'DESC'])]
    private $thirdPlaces;

    /** @var ArrayCollection|Collection|Team[] */
    #[ORM\ManyToMany(targetEntity: Team::class)]
    #[ORM\JoinTable(name: 'customer_remove_from_team')]
    private $teamsHistory;

    public function constructorTournament(): void
    {
        $this->teams             = new ArrayCollection();
        $this->registrationsUser = new ArrayCollection();
        $this->wonTournaments    = new ArrayCollection();
        $this->secondPlaces      = new ArrayCollection();
        $this->thirdPlaces       = new ArrayCollection();
        $this->teamsHistory      = new ArrayCollection();
    }

    /**
     * @param string[]|int[] $gameId
     */
    public function updateTournametData(bool $showTeamsHistory = true, array $gameId = [], bool $forceGameId = false): void
    {
        $this->showTeamsHistory = $showTeamsHistory;

        $newGameId = [];
        foreach ($gameId as $_gameId => $_gameIdValue) {
            $oldGameId = $this->getGameId($_gameId);
            if ($oldGameId !== '' && ! $forceGameId) {
                $newGameId[$_gameId] = $oldGameId;
            } else {
                $newGameId[$_gameId] = $_gameIdValue;
            }
        }

        $this->gameId = $newGameId; //@phpstan-ignore-line
    }

    /**
     * @return ArrayCollection|Collection|TeamPlayer[]
     */
    public function getTeams(bool $asCaptainOrManager = false, ?int $minPlayers = null)
    {
        if (! $asCaptainOrManager && $minPlayers === null) {
            return $this->teams;
        }

        $criteria = Criteria::create();

        if ($asCaptainOrManager) {
            $criteria->andWhere(Criteria::expr()->in('role', [TeamPlayer::ROLE_CAPTAIN, TeamPlayer::ROLE_DEPUTY_CAPTAIN, TeamPlayer::ROLE_MANAGER]));
        }

        $teamPlayers = $this->teams->matching($criteria);

        if ($minPlayers === null) {
            return $teamPlayers;
        }

        return $teamPlayers->filter(static fn (TeamPlayer $teamPlayer): bool => $teamPlayer->getTeam()->getPlayers()->count() >= $minPlayers);
    }

    /**
     * @return ArrayCollection|Collection|Team[]
     */
    public function getTeamsHistory()
    {
        return $this->teamsHistory;
    }

    /**
     * @return ArrayCollection|Collection|RegistrationUser[]
     */
    public function getRegistrationsUser()
    {
        return $this->registrationsUser;
    }

    /**
     * @return array<Tournament>
     */
    public function getParticipatingTournamentsUser(): array
    {
        /** @var array<Tournament> $_tournaments */
        $_tournaments = $this->registrationsUser->map(static fn (RegistrationUser $registration): Tournament => $registration->getTournament())->toArray();

        $tournaments = [];
        foreach ($_tournaments as $tournament) {
            $tournaments[sprintf('%d%d-u', $tournament->getTermFrom()->format('YmdHis'), $tournament->getId())] = $tournament;
        }

        return $tournaments;
    }

    /**
     * @return array<Tournament>
     */
    public function getParticipatingTournamentsTeam(): array
    {
        $tournaments = [];
        foreach ($this->getTeams()->map(static fn (TeamPlayer $tm): Team => $tm->getTeam()) as $team) {
            assert($team instanceof Team);
            foreach ($team->getRegistrationsTeam() as $registration) {
                $tournament = $registration->getTournament();

                if (! $registration->isPlayerIn($this)) {
                    continue;
                }

                $tournaments[sprintf('%d%d-t', $tournament->getTermFrom()->format('YmdHis'), $tournament->getId())] = $tournament;
            }
        }

        foreach ($this->getTeamsHistory() as $team) {
            foreach ($team->getRegistrationsTeam() as $registration) {
                if (! $registration->isPlayerIn($this)) {
                    continue;
                }

                $tournament = $registration->getTournament();

                $tournaments[sprintf('%d%d-th', $tournament->getTermFrom()->format('YmdHis'), $tournament->getId())] = $tournament;
            }
        }

        return $tournaments;
    }

    public function showTeamsHistory(): bool
    {
        return $this->showTeamsHistory;
    }

    /**
     * @return string[]|string
     */
    public function getGameId(?int $id = null): array|string
    {
        if ($id === null) {
            if (is_array($this->gameId) && count($this->gameId) !== 0) {
                return $this->gameId;
            }

            return [];
        }

        return trim($this->gameId[$id] ?? '');
    }

    /**
     * @return ArrayCollection|Collection|Tournament[]
     */
    public function getWonTournaments()
    {
        return $this->wonTournaments;
    }

    /**
     * @return ArrayCollection|Collection|Tournament[]
     */
    public function getSecondPlaces()
    {
        return $this->secondPlaces;
    }

    /**
     * @return ArrayCollection|Collection|Tournament[]
     */
    public function getThirdPlaces()
    {
        return $this->thirdPlaces;
    }

    /**
     * @return Tournament[]
     */
    public function getAllTournamentsWithPlacement(): array
    {
        $tournaments = [];
        foreach ($this->getWonTournaments() as $tournament) {
            $key               = sprintf('%d1', $tournament->getTermFrom()->format('Ymd'));
            $tournaments[$key] = $tournament;
        }

        foreach ($this->getSecondPlaces() as $tournament) {
            $key               = sprintf('%d2', $tournament->getTermFrom()->format('Ymd'));
            $tournaments[$key] = $tournament;
        }

        foreach ($this->getThirdPlaces() as $tournament) {
            $key               = sprintf('%d3', $tournament->getTermFrom()->format('Ymd'));
            $tournaments[$key] = $tournament;
        }

        krsort($tournaments);

        return $tournaments;
    }

    public function getAdditionalInfoForTournament(Tournament $tournament): Html
    {
        $html = new Html();

        $gameId     = $this->getGameId($tournament->getGame()->getId());
        $htmlGameId = Html::el('div')
            ->setHtml(sprintf('Game Id<br/><strong class="pl-2 text-primary font-weight-bold">%s</strong>', is_string($gameId) && $gameId !== '' ? $gameId : '--'));
        $html->addHtml($htmlGameId);

        return $html;
    }

    public function canJoinToTeam(): bool
    {
        return $this->teams->count() < self::MAX_TEAMS;
    }
}
