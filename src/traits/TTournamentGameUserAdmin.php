<?php

declare(strict_types=1);

namespace Skadmin\TournamentGame\Traits;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Skadmin\Game\Doctrine\Game\Game;

trait TTournamentGameUserAdmin
{
    /** @var ArrayCollection|Collection|Game[] */
    #[ORM\ManyToMany(targetEntity: Game::class)]
    #[ORM\JoinTable(name: 'user_allowed_game_rel')]
    #[ORM\OrderBy(['name' => 'ASC'])]
    private $allowedGames = [];

    public function constructorTournament(): void
    {
        $this->allowedGames = new ArrayCollection();
    }

    /**
     * @param Game[] $allowedGames
     */
    public function updateTournametData(array $allowedGames = []): void
    {
        $this->allowedGames->clear();
        foreach ($allowedGames as $allowedGame) {
            if (! ($allowedGame instanceof Game) || $this->allowedGames->contains($allowedGame)) {
                continue;
            }

            $this->allowedGames->add($allowedGame);
        }
    }

    /**
     * @return ArrayCollection|Collection|Game[]
     */
    public function getAllowedGames()
    {
        return $this->allowedGames;
    }
}
